from django.conf.urls import url
from finance import views
from academics.views import AdmissionAutocomplete
from finance.views import AccountGroupAutocomplete,AccountHeadAutocomplete, AssetCategoryAutocomplete, AssetSubCategoryAutocomplete, LiabilityCategoryAutocomplete, LiabilityAutocomplete, LiabilitySubCategoryAutocomplete
from staffs.views import StaffAutocomplete

urlpatterns = [

    url(r'^liability-subcategory-autocomplete/$',LiabilitySubCategoryAutocomplete.as_view(),name='liability_subcategory_autocomplete'),
    url(r'^liability-autocomplete/$',LiabilityAutocomplete.as_view(),name='liability_autocomplete'),
    url(r'^liability-category-autocomplete/$',LiabilityCategoryAutocomplete.as_view(),name='liability_category_autocomplete'),
    url(r'^asset-subcategory-autocomplete/$',AssetSubCategoryAutocomplete.as_view(),name='asset_subcategory_autocomplete'),
    url(r'^asset-category-autocomplete/$',AssetCategoryAutocomplete.as_view(),name='asset_category_autocomplete'),
    url(r'^account-head-autocomplete/$',AccountHeadAutocomplete.as_view(),name='account_head_autocomplete'), 
    url(r'^account-group-autocomplete/$',AccountGroupAutocomplete.as_view(),name='account_group_autocomplete'), 
    url(r'^staff-autocomplete/$',StaffAutocomplete.as_view(),name='staffs_autocomplete'), 
    url(r'^admission-autocomplete/$',AdmissionAutocomplete.as_view(),name='admission_autocomplete'),
    url(r'^$', views.dashboard,name='dashboard'),     

    url(r'^create-fee-payment/(?P<pk>.*)/$',views.create_fee_payment,name='create_fee_payment'),
    url(r'^create-monthly-fee-payment/(?P<pk>.*)/$',views.create_monthly_fee_payment,name='create_monthly_fee_payment'),
    url(r'^fee-payments/$',views.fee_payments,name='fee_payments'),  
    url(r'^fee-payment/edit/(?P<pk>.*)/$',views.edit_fee_payment,name='edit_fee_payment'),
    url(r'^print-fee-slip/(?P<pk>.*)/$',views.print_fee_slip,name='print_fee_slip'),
    
    url(r'^create-transport-fee-payment/(?P<pk>.*)/$',views.create_transport_fee_payment,name='create_transport_fee_payment'),
    url(r'^create-special-fee-payment/(?P<pk>.*)/$',views.create_special_fee_payment,name='create_special_fee_payment'),
    url(r'^create-other-fee-payment/(?P<pk>.*)/$',views.create_other_fee_payment,name='create_other_fee_payment'),
    url(r'^other-fee-payment/edit/(?P<pk>.*)/$',views.edit_other_fee_payment,name='edit_other_fee_payment'),

    url(r'^account-group/create/$',views.create_account_group,name='create_account_group'),
    url(r'^account-group/edit/(?P<pk>.*)/$',views.edit_account_group,name='edit_account_group'),
    url(r'^account-groups/',views.account_groups,name='account_groups'),
    url(r'^account-group/view/(?P<pk>.*)/$',views.account_group,name='account_group'),
    url(r'^account-group/delete/(?P<pk>.*)/$',views.delete_account_group,name='delete_account_group'),
    url(r'^account-group/delete-selected/$', views.delete_selected_account_groups, name='delete_selected_account_groups'),

    url(r'^account-head/create/$',views.create_account_head,name='create_account_head'),
    url(r'^account-head/edit/(?P<pk>.*)/$',views.edit_account_head,name='edit_account_head'),
    url(r'^account-heads/',views.account_heads,name='account_heads'),
    url(r'^account-head/view/(?P<pk>.*)/$',views.account_head,name='account_head'),
    url(r'^account-head/delete/(?P<pk>.*)/$',views.delete_account_head,name='delete_account_head'),

    url(r'^income/create/$',views.create_income,name='create_income'),
    url(r'^income/edit/(?P<pk>.*)/$',views.edit_income,name='edit_income'),
    url(r'^incomes/$',views.incomes,name='incomes'),
    url(r'^expenses/$',views.expenses,name='expenses'),

    url(r'^transactions/$',views.transactions,name='transactions'),
    url(r'^transaction/view/(?P<pk>.*)/$',views.transaction,name='transaction'),
    url(r'^transaction/print/(?P<pk>.*)/$',views.print_transaction,name='print_transaction'),
    url(r'^transaction/delete/(?P<pk>.*)/$',views.delete_transaction,name='delete_transaction'),
    url(r'^transaction/delete-selected/$', views.delete_selected_transactions, name='delete_selected_transactions'),

    url(r'^expense/create/$',views.create_expense,name='create_expense'),
    url(r'^expense/edit(?P<pk>.*)/$',views.edit_expense,name='edit_expense'),

    url(r'^get-account-heads/$',views.get_accountheads,name='get_accountheads'),
    url(r'^get-asset-subcategories/$',views.get_asset_subcategories,name='get_asset_subcategories'),
    url(r'^get-liability-subcategories/$',views.get_liability_subcategories,name='get_liability_subcategories'),

    url(r'^print-transactions/$',views.print_transactions,name='print_transactions'),
    url(r'^print-assets/$',views.print_assets,name='print_assets'),
    url(r'^print-liabilities/$',views.print_liabilities,name='print_liabilities'),

    url(r'^asset-category/create/$',views.create_asset_category,name='create_asset_category'),
    url(r'^asset-category/edit/(?P<pk>.*)/$',views.edit_asset_category,name='edit_asset_category'),
    url(r'^asset-categories/',views.asset_categories,name='asset_categories'),
    url(r'^asset-category/view/(?P<pk>.*)/$',views.asset_category,name='asset_category'),
    url(r'^asset-category/delete/(?P<pk>.*)/$',views.delete_asset_category,name='delete_asset_category'),

    url(r'^asset-subcategory/create/$',views.create_asset_subcategory,name='create_asset_subcategory'),
    url(r'^asset-subcategory/edit/(?P<pk>.*)/$',views.edit_asset_subcategory,name='edit_asset_subcategory'),
    url(r'^asset-subcategories/',views.asset_subcategories,name='asset_subcategories'),
    url(r'^asset-subcategory/view/(?P<pk>.*)/$',views.asset_subcategory,name='asset_subcategory'),
    url(r'^asset-subcategory/delete/(?P<pk>.*)/$',views.delete_asset_subcategory,name='delete_asset_subcategory'),

    url(r'^asset/create/$',views.create_asset,name='create_asset'),
    url(r'^asset/edit/(?P<pk>.*)/$',views.edit_asset,name='edit_asset'),
    url(r'^assets/',views.assets,name='assets'),
    url(r'^asset/view/(?P<pk>.*)/$',views.asset,name='asset'),
    url(r'^asset/delete/(?P<pk>.*)/$',views.delete_asset,name='delete_asset'),

    url(r'^liability-category/create/$',views.create_liability_category,name='create_liability_category'),
    url(r'^liability-category/edit/(?P<pk>.*)/$',views.edit_liability_category,name='edit_liability_category'),
    url(r'^liability-categories/',views.liability_categories,name='liability_categories'),
    url(r'^liability-category/view/(?P<pk>.*)/$',views.liability_category,name='liability_category'),
    url(r'^liability-category/delete/(?P<pk>.*)/$',views.delete_liability_category,name='delete_liability_category'),

    url(r'^liability-subcategory/create/$',views.create_liability_subcategory,name='create_liability_subcategory'),
    url(r'^liability-subcategory/edit/(?P<pk>.*)/$',views.edit_liability_subcategory,name='edit_liability_subcategory'),
    url(r'^liability-subcategories/',views.liability_subcategories,name='liability_subcategories'),
    url(r'^liability-subcategory/view/(?P<pk>.*)/$',views.liability_subcategory,name='liability_subcategory'),
    url(r'^liability-subcategory/delete/(?P<pk>.*)/$',views.delete_liability_subcategory,name='delete_liability_subcategory'),

    url(r'^liability/create/$',views.create_liability,name='create_liability'),
    url(r'^liability/edit/(?P<pk>.*)/$',views.edit_liability,name='edit_liability'),
    url(r'^liabilities/',views.liabilities,name='liabilities'),
    url(r'^liability/view/(?P<pk>.*)/$',views.liability,name='liability'),
    url(r'^liability/delete/(?P<pk>.*)/$',views.delete_liability,name='delete_liability'),

    url(r'^get-amount/',views.get_amount,name='get_amount'),
    url(r'^print-expense-transaction/(?P<pk>.*)/$',views.print_expense_transaction,name='print_expense_transaction'),
    
    url(r'^balance-sheet/create/$',views.create_balance_sheet,name='create_balance_sheet'),
    url(r'^balance-sheet/$',views.balance_sheet,name='balance_sheet'),

    url(r'^cash-book/create/$',views.create_cash_book,name='create_cash_book'),
    url(r'^cash-book/$',views.cash_book,name='cash_book'),

    url(r'^bank-book/create/$',views.create_bank_book,name='create_bank_book'),
    url(r'^bank-book/$',views.bank_book,name='bank_book'),
   
    url(r'^monthly-transaction/create/$',views.create_monthly_transaction,name='create_monthly_transaction'),
    url(r'^monthly-transaction/$',views.monthly_transaction,name='monthly_transaction'),

    url(r'^compare-transaction/create/$',views.create_compare_transaction,name='create_compare_transaction'),
    url(r'^compare-transaction/$',views.compare_transaction,name='compare_transaction'),

    url(r'^ledger-transaction/create/$',views.create_ledger_transaction,name='create_ledger_transaction'),
    url(r'^ledger-transaction/$',views.ledger_transaction,name='ledger_transaction'),

    url(r'^daily-fee-report/create/$',views.create_daily_fee_report,name='create_daily_fee_report'),
    url(r'^daily-fee-report/$',views.daily_fee_report,name='daily_fee_report'),

    url(r'^trail-balance/create/$',views.create_trail_balance,name='create_trail_balance'),
    url(r'^trail-balance/$',views.trail_balance,name='trail_balance'),

    url(r'^get-chart/$',views.get_chart,name='get_chart'),
    url(r'^get-compare-chart/$',views.get_compare_chart,name='get_compare_chart'),

    url(r'^income-expenditure/create/$',views.create_income_expenditure_account,name='create_income_expenditure_account'),
    url(r'^income_expenditure-account/$',views.income_expenditure_account,name='income_expenditure_account'),

    url(r'^fee/create-fee/$',views.create_fee,name='create_fee'),  
    url(r'^fee/create-fee-from-admission/(?P<pk>.*)/$',views.create_fee_from_admission,name='create_fee_from_admission'),  
    url(r'^fee/view/$',views.fees,name='fees'), 
    url(r'^fee/fee/view/(?P<pk>.*)/$', views.fee, name='fee'),
    url(r'^fee/fee/edit-fee/(?P<pk>.*)/$', views.edit_fee, name='edit_fee'),
    url(r'^fee/fee/delete/(?P<pk>.*)/$', views.delete_fee, name='delete_fee'),
    url(r'^fee/delete-selected-fees/$', views.delete_selected_fees, name='delete_selected_fees'),
    url(r'^fee/print/(?P<pk>.*)/$',views.print_fee,name='print_fee'),

    # url(r'^create-fee-payment/$',views.create_fee_payment,name='create_fee_payment'),    
    # url(r'^fee-payment/edit/(?P<pk>.*)/$',views.edit_fee_payment,name='edit_fee_payment'),
    # url(r'^fee-payments/$',views.fee_payments,name='fee_payments'),
    
    url(r'^create-sale-payment/$',views.create_sale_payment,name='create_sale_payment'),
    url(r'^sale-payment/edit/(?P<pk>.*)/$',views.edit_sale_payment,name='edit_sale_payment'),
    url(r'^sale-payments/$',views.sale_payments,name='sale_payments'),
    
    url(r'^day-report/$',views.day_report,name='day_report'),
    url(r'^monthly-report/$',views.monthly_report,name='monthly_report'),
    url(r'^yearly-report/$',views.yearly_report,name='yearly_report'),

    url(r'^print-fee-slip/(?P<pk>.*)/$',views.print_fee_slip,name='print_fee_slip'),
    url(r'^print-sale-receipt/(?P<pk>.*)/$',views.print_sale_receipt,name='print_sale_receipt'),

    url(r'^bank-account/create/$',views.create_bank_account,name='create_bank_account'),
    url(r'^bank-account/edit/(?P<pk>.*)/$',views.edit_bank_account,name='edit_bank_account'),
    url(r'^bank-accounts/$',views.bank_accounts,name='bank_accounts'),
    url(r'^bank-account/view/(?P<pk>.*)/$',views.bank_account,name='bank_account'),
    url(r'^bank-account/delete/(?P<pk>.*)/$',views.delete_bank_account,name='delete_bank_account'),
    url(r'^bank-account/delete-selected/$', views.delete_selected_bank_accounts, name='delete_selected_bank_accounts'),

    url(r'^cash-account/create/$',views.create_cash_account,name='create_cash_account'),
    url(r'^cash-account/edit/(?P<pk>.*)$',views.edit_cash_account,name='edit_cash_account'),
    url(r'^cash-accounts/$',views.cash_accounts,name='cash_accounts'),
    url(r'^cash-account/view/(?P<pk>.*)/$',views.cash_account,name='cash_account'),
    url(r'^cash-account/delete/(?P<pk>.*)$',views.delete_cash_account,name='delete_cash_account'),
    url(r'^cash-account/delete-selected/$', views.delete_selected_cash_accounts, name='delete_selected_cash_accounts'),

    url(r'^create-salary-payment/(?P<pk>.*)/$',views.create_salary_payment,name='create_salary_payment'),
    # url(r'^salary-payments/$',views.salary_payments,name='salary_payments'),  
    # url(r'^salary-payment/edit/(?P<pk>.*)/$',views.edit_salary_payment,name='edit_salary_payment'),
    # url(r'^print-salary-slip/(?P<pk>.*)/$',views.print_salary_slip,name='print_salary_slip'),
]