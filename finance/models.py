from django.db import models
from django.utils.translation import ugettext_lazy as _
from main.models import BaseModel
from django.core.validators import MinValueValidator
from decimal import Decimal


ACC_TYPE = (
    ('savings','Savings'),
    ('current','Current'),
)

TRANSACTION_CATEGORIES = (
    ('income','Income'),
    ('expense','Expense'),
    ('asset','Asset'),
    ('liability','Liability'),
)

TRANSACTION_MODE = (
    ('cash','Cash'),
    ('bank','Bank'),
)

MONTH = (
    ('january','January'),
    ('february','February'),
    ('march','March'),
    ('april','April'),
    ('may','May'),
    ('june','June'),
    ('july','July'),
    ('august','August'),
    ('september','September'),
    ('october','October'),
    ('november','November'),
    ('december','December'),
)

PAYMENT_MODE = (
    ('cheque_payment','Cheque Payment'),
    ('internet_banking','Internet Banking'),
    ('card_payment','Card Payment'),
)

PAYMENT_TO = (
    ('bank_account','Bank Account'),
    ('cash_account','Cash Account'),
    ('both','Both'),
)

BALANCE_TYPE = (
    ('credit','Credit'),
    ('debit',"Debit")
)

LIABILITY_TYPE = (
    ('borrow','Borrow'),
    ('liquidate','Liquidate'),
    ('lend','Lend'),
    ('satisfy','Satisfy'),
)


class TransactionCategory(BaseModel):
    name = models.CharField(max_length=128)
    category_type = models.CharField(max_length=7,choices=TRANSACTION_CATEGORIES)
    is_system_generated = models.BooleanField(default=False)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'transaction_category'
        verbose_name = 'transaction category'
        verbose_name_plural = 'transaction categories'
        ordering = ('category_type',)

    def __str__(self):
        return self.name


class AssetCategory(BaseModel):
    name = models.CharField(max_length=128,blank=True, null=True)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'asset_category'
        verbose_name = _('asset_category')
        verbose_name_plural = _('asset_categories')
        ordering = ('name',)

    def __str__(self):
        return self.name


class AssetSubCategory(BaseModel):
    name = models.CharField(max_length=128,blank=True, null=True)
    asset_category = models.ForeignKey('finance.AssetCategory',limit_choices_to={'is_deleted': False},blank=True,null=True,on_delete=models.CASCADE)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'asset_subcategory'
        verbose_name = _('asset_subcategory')
        verbose_name_plural = _('asset_subcategories')
        ordering = ('name',)

    def __str__(self):
        return self.name


class Asset(BaseModel):
    name = models.CharField(max_length=128,blank=True, null=True)
    account_group = models.ForeignKey("finance.AccountGroup",blank=True,null=True,on_delete=models.CASCADE)
    asset_category = models.ForeignKey('finance.AssetCategory',limit_choices_to={'is_deleted': False},blank=True,null=True,on_delete=models.CASCADE)
    asset_subcategory = models.ForeignKey('finance.AssetSubCategory',limit_choices_to={'is_deleted': False},blank=True,null=True,on_delete=models.CASCADE)
    count = models.DecimalField(max_digits=20,decimal_places=2,default=0,validators=[MinValueValidator(Decimal('0.00'))])
    amount = models.DecimalField(max_digits=20,decimal_places=2,default=0,validators=[MinValueValidator(Decimal('0.00'))])

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'asset'
        verbose_name = _('asset')
        verbose_name_plural = _('assets')
        ordering = ('name',)

    def __str__(self):
        return self.name


class LiabilityCategory(BaseModel):
    name = models.CharField(max_length=128,blank=True, null=True)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'liability_category'
        verbose_name = _('liability category')
        verbose_name_plural = _('liability categories')
        ordering = ('name',)

    def __str__(self):
        return self.name


class LiabilitySubCategory(BaseModel):
    name = models.CharField(max_length=128,blank=True, null=True)
    liability_category = models.ForeignKey('finance.LiabilityCategory',limit_choices_to={'is_deleted': False},blank=True,null=True,on_delete=models.CASCADE)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'liability_subcategory'
        verbose_name = _('liability subcategory')
        verbose_name_plural = _('liability subcategories')
        ordering = ('name',)

    def __str__(self):
        return self.name


class Liability(BaseModel):
    name = models.CharField(max_length=128,blank=True, null=True)
    liability_type = models.CharField(max_length=10,choices=LIABILITY_TYPE,default="borrow")
    liability_category = models.ForeignKey('finance.LiabilityCategory',limit_choices_to={'is_deleted': False},blank=True,null=True,on_delete=models.CASCADE)
    liability_subcategory = models.ForeignKey('finance.LiabilitySubCategory',limit_choices_to={'is_deleted': False},blank=True,null=True,on_delete=models.CASCADE)
    account_group = models.ForeignKey("finance.AccountGroup",blank=True,null=True,on_delete=models.CASCADE)
    
    amount = models.DecimalField(max_digits=20,decimal_places=2,default=0,validators=[MinValueValidator(Decimal('0.00'))])
    paid = models.DecimalField(max_digits=20,decimal_places=2,default=0,validators=[MinValueValidator(Decimal('0.00'))])
    balance = models.DecimalField(max_digits=20,decimal_places=2,default=0,validators=[MinValueValidator(Decimal('0.00'))])

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'liability'
        verbose_name = _('liability')
        verbose_name_plural = _('liabilities')
        ordering = ('name',)

    def __str__(self):
        return self.name


class Transaction(BaseModel):
    admission = models.ForeignKey("academics.Admission",null=True,blank=True,on_delete=models.CASCADE)
    sale = models.ForeignKey("product.Sale",null=True,blank=True,on_delete=models.CASCADE,related_name="sale%(class)s_objects")
    staff = models.ForeignKey("staffs.Staff",null=True,blank=True,on_delete=models.CASCADE)
    transaction_category = models.ForeignKey("finance.TransactionCategory",blank=True,null=True,on_delete=models.CASCADE)
    fee_category = models.ForeignKey("academics.FeeCategory",blank=True,null=True,on_delete=models.CASCADE)
    transaction_type = models.CharField(max_length=10,choices=TRANSACTION_CATEGORIES,default="income",blank=True)
    account_group = models.ForeignKey("finance.AccountGroup",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    account_head = models.ForeignKey("finance.AccountHead",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    amount = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.0)
    title = models.CharField(max_length=128,blank=True,null=True)
    description = models.CharField(max_length=128,blank=True,null=True)
    balance = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.0)
    paid = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.0)

    date = models.DateTimeField(blank=True,null=True)
    month = models.CharField(max_length=128,choices=MONTH,blank=True,null=True)

    payment_to = models.CharField(max_length=128,choices=PAYMENT_TO,default="bank_account",null=True,blank=True) 
    payment_mode = models.CharField(max_length=128,choices=PAYMENT_MODE,blank=True,null=True)
    transaction_mode = models.CharField(max_length=15,choices=TRANSACTION_MODE,default="cash")
    cheque_details = models.CharField(max_length=128,null=True,blank=True)
    is_cheque_withdrawed = models.BooleanField(default=False)
    card_details = models.CharField(max_length=128,null=True,blank=True)     
    bank_account = models.ForeignKey("finance.BankAccount",null=True,blank=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    cash_account = models.ForeignKey("finance.CashAccount",null=True,blank=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)        
   
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'finance_transaction'
        verbose_name = 'transaction'
        verbose_name_plural = 'transactions'
        ordering = ('-auto_id',)

    def __str__(self):
        return str(self.id)


class AccountGroup(BaseModel):
    name = models.CharField(max_length=128)
    category_type = models.CharField(max_length=20,choices=TRANSACTION_CATEGORIES)
    is_system_generated = models.BooleanField(default=False)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'finance_account_group'
        verbose_name = 'transaction category'
        verbose_name_plural = 'transaction categories'
        ordering = ('category_type',)

    def __str__(self):
        value =  self.name.replace("_", " ")
        return value.capitalize()


class AccountHead(BaseModel):
    account_group = models.ForeignKey("finance.AccountGroup",null=True,blank=True,on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    is_system_generated = models.BooleanField(default=False)
    opening_balance = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.0)
    balance_type = models.CharField(max_length=128,choices=BALANCE_TYPE,default="debit")

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'finance_account_head'
        verbose_name = 'account head'
        verbose_name_plural = 'account heads'
        ordering = ('name',)

    def __str__(self):
        value =  self.name.replace("_", " ")
        return value.capitalize()


class Journel(models.Model):
    date = models.DateField()

    cash_debit = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    cash_credit = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])

    bank_debit = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    bank_credit = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])

    transaction = models.ForeignKey("finance.Transaction",null=True,blank=True,on_delete=models.CASCADE)
    liability = models.ForeignKey("finance.Liability",null=True,blank=True,on_delete=models.CASCADE)
    income = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    expense = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'finance_journel'
        verbose_name = 'journel'
        verbose_name_plural = 'journels'

    class Admin:
        list_display = ('date')

    def __str__(self):
        return str(self.date)


class SalePayment(BaseModel):
    college_sale = models.ForeignKey("product.Sale",null=True,blank=True,on_delete=models.CASCADE)
    transaction = models.ForeignKey("finance.Transaction",null=True,blank=True,on_delete=models.CASCADE)
    amount = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])    
    
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'sale_payment'
        verbose_name = 'sale payment'
        verbose_name_plural = 'sale payments'
        ordering = ('a_id',)
        
    class Admin:
        list_display = ('transaction', 'sale', 'amount')
        
    def __str__(self):
        return self.amount 


class Fee(BaseModel):
    student = models.ForeignKey("academics.Admission",on_delete=models.CASCADE)
    batch = models.ForeignKey("main.Batch",null=True,blank=True,on_delete=models.CASCADE)
    date = models.DateField()
    from_month = models.CharField(max_length=15,choices=MONTH,null=True,blank=True)
    to_month = models.CharField(max_length=15,choices=MONTH,null=True,blank=True)
    application_fee = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.00)
    registration_fee = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.00)
    admission_fee = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.00)
    special_fee = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.00)
    caution_deposit = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.00)
    tution_fee = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.00)
    bus_fare = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.00)
    magazine_fee = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.00)    
    computer_fee = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.00)
    examination_fee = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.00)
    other_income = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.00)
    development_fund = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))],default=0.00)
    subtotal = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    
    is_deleted = models.BooleanField(default=False)

    is_tution_fee = models.BooleanField(default=False)
    is_bus_fee = models.BooleanField(default=False)
    is_admission_fee = models.BooleanField(default=False)
    is_exam_fee = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'fee'
        verbose_name = 'fee'
        verbose_name_plural = 'fees'
        ordering = ('student',)
        
    def __str__(self):
        return self.student      


class CashAccount(BaseModel):
    user = models.ForeignKey("auth.User",on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    is_system_generated = models.BooleanField(default=False)
    first_time_balance = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    balance = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'cash_account'
        verbose_name = _('cash account')
        verbose_name_plural = _('cash accounts')
        ordering = ('name',)
        
    def __str__(self):
        return self.name
    

class BankAccount(BaseModel):
    name = models.CharField(max_length=128)
    ifsc = models.CharField(max_length=128)
    branch = models.CharField(max_length=128)
    account_type = models.CharField(max_length=128,choices=ACC_TYPE)
    account_no = models.CharField(max_length=128)
    first_time_balance = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    balance = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'bank_account'
        verbose_name = _('bank account')
        verbose_name_plural = _('bank accounts')
        ordering = ('name',)
        
    def __str__(self):
        return self.name