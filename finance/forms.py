from finance.models import Transaction, AccountGroup, AccountHead
from django.utils.translation import ugettext_lazy as _
from django import forms
from finance.models import AssetCategory,Fee, AssetSubCategory,TransactionCategory,BankAccount,CashAccount, Asset, Liability, LiabilityCategory, LiabilitySubCategory
from django.forms.widgets import Select, TextInput, Textarea, CheckboxInput
from django.forms.fields import Field
from dal import autocomplete
setattr(Field, 'is_checkbox', lambda self: isinstance(
    self.widget, forms.CheckboxInput))


TRANSACTION_CATEGORY_TYPES = (
    ('', '---------------'),
    ('income', 'Income'),
    ('expense', 'Expense'),
)

class TransactionCategoryForm(forms.ModelForm):
    
    class Meta:
        model = TransactionCategory
        exclude = ['updator','creator','is_system_generated','is_deleted','auto_id']
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}),
            'category_type' : Select(attrs={'class': 'required form-control selectordie'}),
        }
        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
            'department' : {
                'required' : _("Department field is required."),
            },
            'category_type' : {
                'required' : _("Category Type field is required."),
            }
        }


class CreditDebitForm(forms.ModelForm):

    class Meta:
        model = Transaction
        exclude = ['creator', 'updater', 'teacher', 'transaction_type',
                   'auto_id', 'paid', 'balance', 'month' ,'college','a_id']
        widgets = {
            "transaction_category": Select(attrs={"class": "single required form-control select2"}),
            "amount": TextInput(attrs={"class": "required form-control number", "placeholder": "Enter amount"}),
            "date": TextInput(attrs={"class": "date-picker required form-control", "placeholder": "Enter date"}),
            "description": TextInput(attrs={"class": "form-control", "placeholder": "description"}),
        }
        error_messages = {
            'transaction_category': {
                'required': _("Transaction Category field is required."),
            },
            'amount': {
                'required': _("Amount field is required."),
            },
            'date': {
                'required': _("Date field is required."),
            }
        }


class AssetCategoryForm(forms.ModelForm):

    class Meta:
        model = AssetCategory
        exclude = ['creator', 'updater', 'auto_id', 'is_deleted','college','a_id']
        widgets = {
            "name": TextInput(attrs={"class": "required form-control", "placeholder": "Enter Name of Asset category"}),
        }
        error_messages = {
            'name': {
                'required': _("Name field is required."),
            }
        }


class AssetSubCategoryForm(forms.ModelForm):

    class Meta:
        model = AssetSubCategory
        exclude = ['creator', 'updater', 'auto_id', 'is_deleted','college','a_id']
        widgets = {
            "name": TextInput(attrs={"class": "required form-control", "placeholder": "Enter Name of Asset Sub Category"}),
            "asset_category": autocomplete.ModelSelect2(url='finance:asset_category_autocomplete',attrs={'data-placeholder': 'Asset Category','data-minimum-input-length': 1},),
        }
        error_messages = {
            'name': {
                'required': _("Name field is required."),
            },
            'asset_category': {
                'required': _("Asset Category field is required."),
            }
        }


class AssetForm(forms.ModelForm):

    class Meta:
        model = Asset
        exclude = ['creator', 'updater', 'auto_id', 'is_deleted','college','a_id']
        widgets = {
            "name" : TextInput(attrs={"class":"required form-control","placeholder":"Enter Name of Asset "}),
            "asset_category" : autocomplete.ModelSelect2(url='finance:asset_category_autocomplete',attrs={'data-placeholder': 'Asset Category','data-minimum-input-length': 1},),
            "account_group": autocomplete.ModelSelect2(url='finance:account_group_autocomplete',attrs={'data-placeholder': 'Account Group','data-minimum-input-length': 1},),
            "asset_subcategory" : autocomplete.ModelSelect2(url='finance:asset_subcategory_autocomplete',attrs={'data-placeholder': 'Asset Subcategory','data-minimum-input-length': 1},),
            "count" : TextInput(attrs={"class":"required form-control number","placeholder":"Enter Count"}),
            "amount" : TextInput(attrs={"class":"required form-control number","placeholder":"Enter Amount"}),            

        }
        error_messages = {
            'name': {
                'required': _("Name field is required."),
            },
            'asset_category': {
                'required': _("Asset Category field is required."),
            },
            'asset_subcategory': {
                'required': _("Asset Sub Category field is required."),
            },
            'count': {
                'required': _("Count field is required."),
            }
            
        }


class LiabilityCategoryForm(forms.ModelForm):

    class Meta:
        model = LiabilityCategory
        exclude = ['creator', 'updater', 'auto_id', 'is_deleted','college','a_id']
        widgets = {
            "name": TextInput(attrs={"class": "required form-control", "placeholder": "Enter Name of Liability category"}),
        }
        error_messages = {
            'name': {
                'required': _("Name field is required."),
            }
        }


class LiabilitySubCategoryForm(forms.ModelForm):

    class Meta:
        model = LiabilitySubCategory
        exclude = ['creator', 'updater', 'auto_id', 'is_deleted','college','a_id']
        widgets = {
            "name": TextInput(attrs={"class": "required form-control", "placeholder": "Enter Name of Liability Sub Category"}),
            "liability_category": autocomplete.ModelSelect2(url='finance:liability_category_autocomplete',attrs={'data-placeholder': 'Liability Category','data-minimum-input-length': 1},),

        }
        error_messages = {
            'name': {
                'required': _("Name field is required."),
            },
            'liability_category': {
                'required': _("Liability Category field is required."),
            }
        }


class LiabilityForm(forms.ModelForm):

    class Meta:
        model = Liability
        exclude = ['creator', 'updater', 'auto_id', 'is_deleted','paid','college','a_id','liability']
        widgets = {
            "name": TextInput(attrs={"class": "required form-control", "placeholder": "Enter Name of Liability"}),
            "liability_type": Select(attrs={"class": "form-control select2"}),
            "liability_category": autocomplete.ModelSelect2(url='finance:liability_category_autocomplete',attrs={'data-placeholder': 'Liability Category','data-minimum-input-length': 1},),
            "account_group": autocomplete.ModelSelect2(url='finance:account_group_autocomplete',attrs={'data-placeholder': 'Account Group','data-minimum-input-length': 1},),
            "liability_subcategory": autocomplete.ModelSelect2(url='finance:liability_subcategory_autocomplete',attrs={'data-placeholder': 'Liability Subcategory','data-minimum-input-length': 1},),
            "amount": TextInput(attrs={"class": "required form-control number", "placeholder": "Enter Amount"}),
            "balance": TextInput(attrs={"class": "required form-control number", "placeholder": "Enter Amount"}),
            

        }
        error_messages = {
            'name': {
                'required': _("Name field is required."),
            },
            'liability_category': {
                'required': _("Liability Category field is required."),
            },
            'liablity_subcategory': {
                'required': _("Liability Sub Category field is required."),
            },
            'amount': {
                'required': _("Amount field is required."),
            },
            'institute': {
                'required': _("Institute field is required."),
            },
            'member': {
                'required': _("Member field is required."),
            },
            'family': {
                'required': _("Family field is required."),
            }
        }


class TransactionForm(forms.ModelForm):

    class Meta:
        model = Transaction
        fields = ['amount', 'date']
        widgets = {
            'amount': TextInput(attrs={'class': 'required form-control', 'placeholder': 'Enter Amount'}),
            'date': TextInput(attrs={'class': 'required form-control date-picker', 'placeholder': 'Enter Amount'}),
        }

        error_messages = {
            'date': {
                'required': _("date field is required."),
            },
            'amount': {
                'required': _("amount field is required."),
            },

        }


class TransactionBasicForm(forms.ModelForm):

    class Meta:
        model = Transaction
        exclude = ['creator', 'updater', 'transaction_type','transaction_category',
                   'auto_id', 'paid', 'balance','college','a_id']
        widgets = {
            "account_group": Select(attrs={"class": "required form-control select2"}),
            "account_head": autocomplete.ModelSelect2(url='finance:account_head_autocomplete',attrs={'data-placeholder': 'Account Head','data-minimum-input-length': 1},),
            "transaction_mode": Select(attrs={"class": "required form-control select2"}),
            "payment_mode": Select(attrs={"class": "bank_item required form-control select2"}),
            "amount": TextInput(attrs={"class": "required form-control number", "placeholder": "Enter amount"}),
            "cheque_details": TextInput(attrs={"class": "bank_item cheque_item required form-control", "rows": "5", "placeholder": "Enter cheque details"}),
            "card_details": Textarea(attrs={"class": "bank_item card_item required form-control", "rows": "5", "placeholder": "Enter card details"}),
            "is_cheque_withdrawed": CheckboxInput(attrs={"class": "bank_item cheque_item form-control"}),
            "title": TextInput(attrs={"class": "form-control", "placeholder": "Title"}),
            "description": TextInput(attrs={"class": "form-control", "placeholder": "Description"}),
            "date": TextInput(attrs={"class": "date-picker form-control", "placeholder": "Enter date"}),

        }
        error_messages = {
            'account_group': {
                'required': _("Transaction Category field is required."),
            },
            'account_head': {
                'required': _("Account Head field is required."),
            },
            'transaction_mode': {
                'required': _("Transaction Mode field is required."),
            },
            'payment_mode': {
                'required': _("Payment Mode field is required."),
            },
            'payment_to': {
                'required': _("Payment To field is required."),
            },
            'amount': {
                'required': _("Amount field is required."),
            },
            'cheque_details': {
                'required': _("Cheque Details field is required."),
            },
            'card_details': {
                'required': _("Card Details field is required."),
            },
            'date': {
                'required': _("Date field is required."),
            }
        }


class CollectionTransactionForm(forms.ModelForm):

    class Meta:
        model = Transaction
        exclude = ['creator', 'updater', 'transaction_type','transaction_category',
                   'auto_id', 'paid', 'balance','college','a_id']
        widgets = {
            "account_group": Select(attrs={"class": "required form-control select2"}),
            "account_head": Select(attrs={"class": "required form-control select2"}),
            "transaction_mode": Select(attrs={"class": "required form-control select2"}),
            "payment_mode": Select(attrs={"class": "bank_item required form-control select2"}),
            "amount": TextInput(attrs={"class": "required form-control number", "placeholder": "Enter amount"}),
            "cheque_details": TextInput(attrs={"class": "bank_item cheque_item required form-control", "rows": "5", "placeholder": "Enter cheque details"}),
            "card_details": Textarea(attrs={"class": "bank_item card_item required form-control", "rows": "5", "placeholder": "Enter card details"}),
            "is_cheque_withdrawed": CheckboxInput(attrs={"class": "bank_item cheque_item form-control"}),
            "title": TextInput(attrs={"class": "form-control", "placeholder": "Title"}),
            "description": TextInput(attrs={"class": "form-control", "placeholder": "Description"}),
            "date": TextInput(attrs={"class": "date-picker required form-control", "placeholder": "Enter date"}),

        }
        error_messages = {
            'transaction_mode': {
                'required': _("Transaction Mode field is required."),
            },
            'payment_mode': {
                'required': _("Payment Mode field is required."),
            },
            'payment_to': {
                'required': _("Payment To field is required."),
            },
            'amount': {
                'required': _("Amount field is required."),
            },
            'cheque_details': {
                'required': _("Cheque Details field is required."),
            },
            'card_details': {
                'required': _("Card Details field is required."),
            },
            'date': {
                'required': _("Date field is required."),
            }
        }


    def __init__(self, *args, **kwargs):
        super(CollectionTransactionForm, self).__init__(*args, **kwargs)
        self.fields['account_group'].empty_label = None

        if self.fields['payment_mode'].choices[0][0] == '':
            payment_mode_choices = self.fields['payment_mode'].choices
            del payment_mode_choices[0]
            self.fields['payment_mode'].choices = payment_mode_choices

    def clean(self):
        account_group = self.cleaned_data.get('account_group')


class AccountGroupForm(forms.ModelForm):

    class Meta:
        model = AccountGroup
        exclude = ['updater', 'creator','is_system_generated', 'is_deleted', 'auto_id','college','a_id']
        widgets = {
            'department': Select(attrs={'class': 'required form-control select2'}),
            'name': TextInput(attrs={'class': 'required form-control', 'placeholder': 'Name'}),
            'category_type': Select(attrs={'class': 'required form-control select2'}),
        }
        error_messages = {
            'name': {
                'required': _("Name field is required."),
            },
            'department': {
                'required': _("Department field is required."),
            },
            'category_type': {
                'required': _("Category Type field is required."),
            }
        }


class AccountHeadForm(forms.ModelForm):

    class Meta:
        model = AccountHead
        exclude = ['creator', 'updater', 'auto_id',
                   'is_deleted', 'is_system_generated','college','a_id']
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control', 'placeholder': 'Enter account head'}),
            'account_group': autocomplete.ModelSelect2(url='finance:account_group_autocomplete',attrs={'data-placeholder': 'Account Group','data-minimum-input-length': 1},),
            "balance_type": Select(attrs={"class": "required form-control select2"}),
            "opening_balance": TextInput(attrs={'class': 'required form-control', 'placeholder': 'Opening Balance'}),
        }

        error_messages = {
            'name': {
                'required': _("Name field is required."),
            },
            'account_group': {
                'required': _("Account group field is required."),
            },
            'opening_balance': {
                'required': _("Opening Balance field is required."),
            },
        }


class SalaryPaymentForm(forms.ModelForm):

    class Meta:
        model = Transaction
        exclude = ['creator', 'updator', 'auto_id', 'transaction_type', 'title','transaction_category',
                   'is_deleted', 'staff_salary', 'account_head', 'balance', 'paid', 'advance', 'college','a_id']
        widgets = {
            "transaction_mode": Select(attrs={"class": "required form-control select2"}),
            "cash_account": Select(attrs={"class": "required form-control select2"}),
            "bank_account": Select(attrs={"class": "bank_item required form-control select2"}),
            "payment_mode": Select(attrs={"class": "bank_item required form-control select2"}),
            "payment_to": Select(attrs={"class": "single bank_item required form-control selectpicker"}),
            "amount": TextInput(attrs={"class": "required number form-control", "placeholder": "Enter amount"}),
            "cheque_details": Textarea(attrs={"class": "bank_item cheque_item text required form-control", "rows": "5", "placeholder": "Enter cheque details"}),
            "card_details": Textarea(attrs={"class": "bank_item card_item required text form-control", "rows": "5", "placeholder": "Enter card details"}),
            "is_cheque_withdrawed": CheckboxInput(attrs={"class": "bank_item cheque_item form-control"}),
            "date": TextInput(attrs={"class": "date-picker required form-control", "placeholder": "Enter Date"}),
        }

        labels = {
            "date": "Date"
        }

        error_messages = {
            'transaction_mode': {
                'required': _("Transaction Mode field is required."),
            },
            'payment_mode': {
                'required': _("Payment Mode field is required."),
            },
            'payment_to': {
                'required': _("Payment To field is required."),
            },
            'bank_account': {
                'required': _("Bank Account field is required."),
            },
            'cash_account': {
                'required': _("Cash Account field is required."),
            },
            'amount': {
                'required': _("Amount field is required."),
            },
            'cheque_details': {
                'required': _("Cheque Details field is required."),
            },
            'card_details': {
                'required': _("Card Details field is required."),
            },
            'date': {
                'required': _("Date field is required."),
            },
        }

    def __init__(self, *args, **kwargs):
        super(SalaryPaymentForm, self).__init__(*args, **kwargs)

        if self.fields['payment_mode'].choices[0][0] == '':
            payment_mode_choices = self.fields['payment_mode'].choices
            del payment_mode_choices[0]
            self.fields['payment_mode'].choices = payment_mode_choices


class LedgerForm(forms.Form):
    from_date = forms.DateField(widget=forms.TextInput(attrs={"class": 'form-control date-picker'}))
    to_date = forms.DateField(widget=forms.TextInput(attrs={"class": 'form-control date-picker'}))


class DateForm(forms.Form):
    date = forms.DateField(widget=forms.TextInput(attrs={"class": 'form-control date-picker'}))


class CompareForm(forms.Form):
    from_date = forms.DateField(widget=forms.TextInput(attrs={"class": 'form-control date-picker'}))
    to_date = forms.DateField(widget=forms.TextInput(attrs={"class": 'form-control date-picker'}))
    start_date = forms.DateField(widget=forms.TextInput(attrs={"class": 'form-control date-picker'}))
    end_date = forms.DateField(widget=forms.TextInput(attrs={"class": 'form-control date-picker'}))


class LedgerTransactionForm(forms.ModelForm):
    from_date = forms.DateField(widget=forms.TextInput(attrs={"class": 'form-control date-picker'}))
    to_date = forms.DateField(widget=forms.TextInput(attrs={"class": 'form-control date-picker'}))
    class Meta:
        model = Transaction
        fields =['account_head']
        widgets = {
            "account_head": autocomplete.ModelSelect2(url='finance:account_head_autocomplete',attrs={'data-placeholder': 'Account Head','data-minimum-input-length': 1},),
        }
        error_messages = {
            'account_head' : {
                'required' : _("Transaction Category field is required."),
            },
        }
        

class FeeForm(forms.ModelForm):
    
    class Meta:
        model = Fee
        exclude = ['updater','creator','is_deleted','auto_id','subtotal','college','a_id']
        widgets = {
            'student': autocomplete.ModelSelect2(url='academics:admission_autocomplete', attrs={'placeholder': 'Student', 'data-minimum-input-length': 1},),
            'date': TextInput(attrs={'class': 'required form-control date-picker','placeholder' : 'Date'}),
            "from_month" : Select(attrs={"class":"required form-control selectpicker"}),
            "to_month" : Select(attrs={"class":" required form-control selectpicker"}),
            "application_fee" : TextInput(attrs={"class":"form-control number","placeholder":"Application Fee"}),
            "registration_fee" : TextInput(attrs={"class":"form-control number","placeholder":"Registration Fee"}),
            "admission_fee" : TextInput(attrs={"class":"form-control number","placeholder":"Admission Fee"}),
            "special_fee" : TextInput(attrs={"class":"form-control number","placeholder":"Special Fee"}),
            "caution_deposit" : TextInput(attrs={"class":"form-control number","placeholder":"Caution Deposit"}),
            "tution_fee" : TextInput(attrs={"class":"form-control number","placeholder":"Tution Fee"}),
            "bus_fare" : TextInput(attrs={"class":"form-control number","placeholder":"Bus Fare"}),
            "magazine_fee" : TextInput(attrs={"class":"form-control number","placeholder":"Magazine Fee"}),
            "computer_fee" : TextInput(attrs={"class":"form-control number","placeholder":"Computer Fee"}),
            "examination_fee" : TextInput(attrs={"class":"form-control number","placeholder":"Examination Fee"}),
            "other_income" : TextInput(attrs={"class":"form-control number","placeholder":"Other Income"}),
            "development_fund" : TextInput(attrs={"class":"form-control number","placeholder":"Development Fund"}),
        }
        error_messages = {
            'student' : {
                'required' : _("Student field is required."),
            },
            'month' : {
                'required' : _("Month field is required."),
            },
        }


class FeeTransactionForm(forms.ModelForm):
    
    class Meta:
        model = Transaction
        exclude = ['creator','updator','auto_id','transaction_category','college','a_id','admission','description','student_fee','date','transaction_type','is_deleted','paid','teacher','balance','amount','month']
        widgets = {
            "transaction_mode" : Select(attrs={"class":"single required form-control selectpicker"}),
            "payment_mode" : Select(attrs={"class":"single bank_item required form-control selectpicker"}),
            "payment_to" : Select(attrs={"class":"single bank_item required form-control selectpicker"}),
            "bank_account" : Select(attrs={"class":"single bank_item required form-control selectpicker"}),
            "cash_account" : Select(attrs={"class":"single required form-control selectpicker"}),
            "cheque_details" : Textarea(attrs={"class":"bank_item cheque_item text required form-control","rows":"5","placeholder":"Enter cheque details"}),
            "card_details" : Textarea(attrs={"class":"bank_item card_item required text form-control","rows":"5","placeholder":"Enter card details"}),
            "is_cheque_withdrawed" : CheckboxInput(attrs={"class":"bank_item cheque_item form-control"}),
        }
        error_messages = {
           
            'amount' : {
                'required' : _("Amount field is required."),
            },
        }

        
class FeePaymentForm(forms.ModelForm):
    
    class Meta:
        model = Transaction
        exclude = ['creator','updater','transaction_category','auto_id','transaction_type','sale','is_deleted','title','college','student_fee','a_id','staff','staff_salary']
        widgets = {
            'admission': autocomplete.ModelSelect2(url='academics:admission_autocomplete', attrs={'data-placeholder': 'Student', 'data-minimum-input-length': 1},),
            'fee_category': autocomplete.ModelSelect2(url='academics:fee_category_autocomplete', attrs={'data-placeholder': 'Fee Category', 'data-minimum-input-length': 1},),
            'account_group': autocomplete.ModelSelect2(url='finance:account_group_autocomplete', attrs={'data-placeholder': 'Account Group', 'data-minimum-input-length': 1},),
            'account_head': autocomplete.ModelSelect2(url='finance:account_head_autocomplete', attrs={'data-placeholder': 'Account Head', 'data-minimum-input-length': 1},),
            "transaction_mode" : Select(attrs={"class":"single required form-control selectpicker"}),
            "payment_mode" : Select(attrs={"class":"single bank_item required form-control selectpicker"}),
            "payment_to" : Select(attrs={"class":"single bank_item required form-control selectpicker"}),
            "bank_account" : Select(attrs={"class":"single bank_item required form-control selectpicker"}),
            "cash_account" : Select(attrs={"class":"single required form-control selectpicker"}),
            "month" : Select(attrs={"class":"single required form-control selectpicker"}),
            "amount" : TextInput(attrs={"class":"required number form-control","placeholder":"Enter amount"}),
            "description" : TextInput(attrs={"class":"required number form-control","placeholder":"Enter description"}),
            "balance" : TextInput(attrs={"class":"required number form-control","placeholder":"Enter balance amount"}),
            "paid" : TextInput(attrs={"class":"required number form-control","placeholder":"Enter paid amount"}),
            "cheque_details" : Textarea(attrs={"class":"bank_item cheque_item text required form-control","rows":"5","placeholder":"Enter cheque details"}),
            "card_details" : Textarea(attrs={"class":"bank_item card_item required text form-control","rows":"5","placeholder":"Enter card details"}),
            "is_cheque_withdrawed" : CheckboxInput(attrs={"class":"bank_item cheque_item form-control"}),
            "date" : TextInput(attrs={"class":"date-picker required form-control","placeholder":"Enter date"}),

        }
        error_messages = {
            
            'transaction_mode' : {
                'required' : _("Transaction Mode field is required."),
            },
            'payment_mode' : {
                'required' : _("Payment Mode field is required."),
            },
            'payment_to' : {
                'required' : _("Payment To field is required."),
            },
            'bank_account' : {
                'required' : _("Bank Account field is required."),
            },
            'cash_account' : {
                'required' : _("Cash Account field is required."),
            },
            'amount' : {
                'required' : _("Amount field is required."),
            },
            'cheque_details' : {
                'required' : _("Cheque Details field is required."),
            },
            'card_details' : {
                'required' : _("Card Details field is required."),
            },
            'date' : {
                'required' : _("Date field is required."),
            },
        }
    
    def __init__(self, *args, **kwargs):
        super(FeePaymentForm, self).__init__(*args, **kwargs)
        self.fields['bank_account'].empty_label = None
        self.fields['cash_account'].empty_label = None
        
        if self.fields['payment_to'].choices[0][0] == '':
            payment_to_choices = self.fields['payment_to'].choices
            del payment_to_choices[0]
            self.fields['payment_to'].choices = payment_to_choices
            
        if self.fields['payment_mode'].choices[0][0] == '':
            payment_mode_choices = self.fields['payment_mode'].choices
            del payment_mode_choices[0]
            self.fields['payment_mode'].choices = payment_mode_choices

    def clean(self):
        cleaned_data=super(FeePaymentForm, self).clean()
        
        student = self.cleaned_data.get('student')
        
        if not student:
            raise forms.ValidationError(
                "Select a student"
            )
            
        return cleaned_data


class SalePaymentForm(forms.ModelForm):
    
    class Meta:
        model = Transaction
        exclude = ['creator','updater','transaction_category','auto_id','transaction_type','title','is_deleted','college','a_id','student_fee','staff','staff_salary','sale']
        widgets = {
            'student': autocomplete.ModelSelect2(url='academics:admission_autocomplete', attrs={'data-placeholder': 'Student', 'data-minimum-input-length': 1},),
            "transaction_mode" : Select(attrs={"class":"single required form-control selectpicker"}),
            "payment_mode" : Select(attrs={"class":"single bank_item required form-control selectpicker"}),
            "payment_to" : Select(attrs={"class":"single bank_item required form-control selectpicker"}),
            "bank_account" : Select(attrs={"class":"single bank_item required form-control selectpicker"}),
            "cash_account" : Select(attrs={"class":"single required form-control selectpicker"}),
            "amount" : TextInput(attrs={"class":"required number form-control","placeholder":"Enter amount"}),
            "cheque_details" : Textarea(attrs={"class":"bank_item cheque_item text required form-control","rows":"5","placeholder":"Enter cheque details"}),
            "card_details" : Textarea(attrs={"class":"bank_item card_item required text form-control","rows":"5","placeholder":"Enter card details"}),
            "paid" : TextInput(attrs={"class":"required number form-control","placeholder":"Enter paid"}),
            "balance" : TextInput(attrs={"class":"required number form-control","placeholder":"Enter balance"}),
            # "is_cheque_withdrawed" : CheckboxInput(attrs={"class":"bank_item cheque_item form-control"}),
            "date" : TextInput(attrs={"class":"date-picker required form-control","placeholder":"Enter date"}),

        }
        error_messages = {
            
            'transaction_mode' : {
                'required' : _("Transaction Mode field is required."),
            },
            'payment_mode' : {
                'required' : _("Payment Mode field is required."),
            },
            'payment_to' : {
                'required' : _("Payment To field is required."),
            },
            'bank_account' : {
                'required' : _("Bank Account field is required."),
            },
            'cash_account' : {
                'required' : _("Cash Account field is required."),
            },
            'amount' : {
                'required' : _("Amount field is required."),
            },
            'cheque_details' : {
                'required' : _("Cheque Details field is required."),
            },
            'card_details' : {
                'required' : _("Card Details field is required."),
            },
            'date' : {
                'required' : _("Date field is required."),
            },
        }
    
    def __init__(self, *args, **kwargs):
        super(SalePaymentForm, self).__init__(*args, **kwargs)
        self.fields['bank_account'].empty_label = None
        self.fields['cash_account'].empty_label = None
        
        if self.fields['payment_to'].choices[0][0] == '':
            payment_to_choices = self.fields['payment_to'].choices
            del payment_to_choices[0]
            self.fields['payment_to'].choices = payment_to_choices
            
        if self.fields['payment_mode'].choices[0][0] == '':
            payment_mode_choices = self.fields['payment_mode'].choices
            del payment_mode_choices[0]
            self.fields['payment_mode'].choices = payment_mode_choices


class BankAccountForm(forms.ModelForm):
    
    class Meta:
        model = BankAccount
        exclude = ['updator','creator','is_deleted','auto_id','a_id','balance','college']
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}),
            'ifsc': TextInput(attrs={'class': 'required form-control','placeholder' : 'IFSC'}),
            'branch': TextInput(attrs={'class': 'required form-control','placeholder' : 'Branch'}),
            'account_type': Select(attrs={'class': 'required form-control selectpicker'}),
            'account_no': TextInput(attrs={'class': 'required form-control','placeholder' : 'Account number'}),
            "first_time_balance" : TextInput(attrs={"class":"required form-control number","placeholder":"Enter amount"}),
        }
        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
            'ifsc' : {
                'required' : _("Ifsc field is required."),
            },
            'branch' : {
                'required' : _("Branch field is required."),
            },
            'account_type' : {
                'required' : _("Account Type field is required."),
            },
            'account_no' : {
                'required' : _("Account No field is required."),
            }
        }
        
        
class CashAccountForm(forms.ModelForm):
    
    class Meta:
        model = CashAccount
        exclude = ['updator','creator','is_system_generated','is_deleted','auto_id','college','a_id','balance','user']
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}),
            "first_time_balance" : TextInput(attrs={"class":"required form-control number","placeholder":"Enter amount"}),
        }
        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
            'first_time_balance' : {
                'required' : _("First Time Balance field is required."),
            }
        }


class OtherFeePaymentForm(forms.ModelForm):
    
    class Meta:
        model = Transaction
        exclude = ['creator','updator','date','auto_id','transaction_category','admission','description','student_fee','date','transaction_type','is_deleted','paid','teacher','month','a_id','college']
        widgets = {
            "balance" : TextInput(attrs={"class":"required number form-control","placeholder":"Enter Balance Amount"}),  
            "amount" : TextInput(attrs={"class":"required number form-control","placeholder":"Enter Balance Amount"}),  
            'fee_category': Select(attrs={'class': 'required form-control selectordie'}), 
        }
        error_messages = {
           
            'balance' : {
                'required' : _("Amount field is required."),
            },
        }


class PaymentForm(forms.Form):
    no_of_month = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control number','placeholder':"Number of Month"}))
    amount_paying = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control number','placeholder':"Amount Paying"}))
    total_amount = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control number','placeholder':"Amount",'disabled':"disabled"}))
