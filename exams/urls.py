from django.conf.urls import url
from exams import views
from exams.views import SubjectAutocomplete
from academics.views import ClassAutocomplete
from dal import autocomplete

urlpatterns = [

    
    url(r'^class-autocomplete/$', ClassAutocomplete.as_view(),name='class_autocomplete'),
	url(r'^subject-autocomplete/$',SubjectAutocomplete.as_view(),name='subject_autocomplete'), 

    url(r'^create/exam/$', views.create_exam, name='create_exam'),
    url(r'^view/exam/(?P<pk>.*)/$', views.view_exam, name='view_exam'),
    url(r'^edit/exam/(?P<pk>.*)/$', views.edit_exam, name='edit_exam'),
    url(r'^delete/exam/(?P<pk>.*)/$', views.delete_exam, name='delete_exam'),
    url(r'^delete/selected-exams/$', views.delete_selected_exams, name='delete_selected_exams'),
    url(r'^exams/$', views.exams, name='exams'),
    url(r'^exam/(?P<pk>.*)/$', views.exam, name='exam'),

    url(r'^list-class-and-division-for-exams/$', views.list_class_and_division_for_exams, name='list_class_and_division_for_exams'),

    url(r'^create/evaluation/(?P<pk>.*)/$', views.create_evaluation, name='create_evaluation'),
    url(r'^evaluations/(?P<pk>.*)/$', views.evaluations, name='evaluations'),
    url(r'^view-evaluation/(?P<pk>.*)/$', views.view_evaluation, name='view_evaluation'),
    url(r'^edit/evaluation/(?P<pk>.*)/$', views.edit_evaluation, name='edit_evaluation'),
    url(r'^delete/evaluation/(?P<pk>.*)/$', views.delete_evaluation, name='delete_evaluation'),
    url(r'^list-divisions-for-evaluation-results/(?P<pk>.*)/$', views.list_divisions_for_evaluation_results, name='list_divisions_for_evaluation_results'),
    url(r'^create-exam-result/(?P<pk>[0-9a-f-]+)/division/(?P<division_pk>[0-9a-f-]+)/$', views.create_exam_result, name='create_exam_result'),
    url(r'^edit/exam-result/(?P<pk>.*)/$', views.edit_exam_result, name='edit_exam_result'),
    url(r'^delete/exam-result/(?P<pk>.*)/$', views.delete_exam_result, name='delete_exam_result'),
    url(r'^list-divisions-for-view-results/(?P<pk>.*)/$', views.list_divisions_for_view_results, name='list_divisions_for_view_results'), 
    url(r'^view/exam-result/(?P<pk>.*)/$', views.view_exam_result, name='view_exam_result'),
    url(r'^exam-results/(?P<pk>[0-9a-f-]+)/division/(?P<division_pk>[0-9a-f-]+)/$', views.exam_results, name='exam_results'),
]