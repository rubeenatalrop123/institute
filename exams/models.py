# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from main.models import BaseModel
from django.core.validators import MinValueValidator
from decimal import Decimal
from django.utils.translation import ugettext_lazy as _


class Exam(BaseModel):
    title = models.CharField(max_length=128)
    student_batch = models.ForeignKey("main.Batch",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    from_date = models.DateField()
    to_date = models.DateField()
    department = models.ForeignKey("academics.Department",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'exams_exam'
        verbose_name = _('exam')
        verbose_name_plural = _('exams')

    def __str__(self):
        return self.title


class Evaluation(BaseModel):
    subject = models.ForeignKey("academics.Subject",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    exam = models.ForeignKey("exams.Exam",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE) 
    exam_date = models.DateField()
    from_time = models.TimeField()
    to_time = models.TimeField()
    department = models.ForeignKey("academics.Department",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    sub_division = models.ForeignKey("academics.SubDivision",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE,blank=True,null=True)
    max_score = models.PositiveIntegerField(default=80)
    ce_max_score = models.PositiveIntegerField(default=20)
    student_batch = models.ForeignKey("main.Batch",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'exams_evaluation'
        verbose_name = _('evaluation')
        verbose_name_plural = _('evaluations')

    def __str__(self):
        return self.subject.name


class ExamResult(BaseModel):
    evaluation = models.ForeignKey("exams.Evaluation",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    admission = models.ForeignKey("academics.Admission",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    score = models.PositiveIntegerField(default=0)
    ce_score = models.PositiveIntegerField(default=0)
    grade = models.CharField(max_length=128)
    principal_remark = models.TextField(blank=True,null=True)
    guardian_remark = models.TextField(blank=True,null=True)
    class_teacher_remark = models.TextField(blank=True,null=True)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'exams_exam_result'
        verbose_name = _('exam result')
        verbose_name_plural = _('exam results')

    def __unicode__(self):
        return self.score