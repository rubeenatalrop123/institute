from django.conf.urls import url
from academics import views
from academics.views import DivisionAutocomplete, AdmissionAutocomplete,CourseAutocomplete,ClassAutocomplete,SubjectAutocomplete, FeeCategoryAutocomplete
from staffs.views import StaffAutocomplete, DesignationAutocomplete

urlpatterns = [
    url(r'^subject-autocomplete/$', SubjectAutocomplete.as_view(),name='subject_autocomplete'),
    url(r'^medium-autocomplete/$', CourseAutocomplete.as_view(),name='medium_autocomplete'), 
    url(r'^class-autocomplete/$', ClassAutocomplete.as_view(),name='class_autocomplete'),
	url(r'^division-autocomplete/$',DivisionAutocomplete.as_view(),name='division_autocomplete'), 
    url(r'^admission-autocomplete/$',AdmissionAutocomplete.as_view(),name='admission_autocomplete'),
    url(r'^fee-category-autocomplete/$',FeeCategoryAutocomplete.as_view(),name='fee_category_autocomplete'),
    url(r'^$', views.dashboard, name='dashboard'),   

    url(r'^student-classes/$', views.departments, name='departments'),
    url(r'^create/student-class/$', views.create_department, name='create_department'),
    url(r'^student-class/(?P<pk>.*)/$', views.view_department, name='view_department'),
    url(r'^edit/student-class/(?P<pk>.*)/$', views.edit_department, name='edit_department'),
    url(r'^delete/student-class/(?P<pk>.*)/$', views.delete_department, name='delete_department'),

    url(r'^student-mediums/$', views.courses, name='courses'),
    url(r'^create/student-medium/$', views.create_course, name='create_course'),
    url(r'^edit/student-medium/(?P<pk>.*)/$', views.edit_course, name='edit_course'),
    url(r'^view/student-medium/(?P<pk>.*)/$', views.view_course, name='view_course'),
    url(r'^delete/student-medium/(?P<pk>.*)$', views.delete_course, name='delete_course'),

    url(r'^student-divisions/$', views.sub_divisions, name='sub_divisions'),
    url(r'^create/student-division/$', views.create_sub_division, name='create_sub_division'),
    url(r'^view/student-division/(?P<pk>.*)/$', views.view_sub_division, name='view_sub_division'),
    url(r'^edit/student-division/(?P<pk>.*)/$', views.edit_sub_division, name='edit_sub_division'),
    url(r'^delete/student-division/(?P<pk>.*)/$', views.delete_sub_division, name='delete_sub_division'),

    url(r'^create/admission/(?P<pk>[0-9a-f-]+)/division/(?P<division_pk>[0-9a-f-]+)/$', views.create_admission, name='create_admission'),
    url(r'^admission/view/$',views.admissions,name='admissions'),
    url(r'^admission/view/(?P<pk>.*)/$',views.admission,name='admission'),
    url(r'^admission/full-view/(?P<pk>.*)/$',views.admission_full_view,name='admission_full_view'),
    url(r'^admission/edit/(?P<pk>.*)/$',views.edit_admission,name='edit_admission'),
    url(r'^admission/delete/(?P<pk>.*)/$',views.delete_admission,name='delete_admission'),

    url(r'^admission/print/(?P<pk>.*)/$',views.print_admission,name='print_admission'),
    url(r'^class/transfer/(?P<pk>.*)/$',views.class_transfer,name='class_transfer'),
    
    url(r'^create-subject/$', views.create_subject, name='create_subject'),
    url(r'^subjects/$', views.subjects, name='subjects'),
	url(r'^subject/(?P<pk>.*)/$', views.subject, name='subject'),
	url(r'^delete-subject/(?P<pk>.*)/$', views.delete_subject, name='delete_subject'),
    url(r'^edit-subject/(?P<pk>.*)/$', views.edit_subject, name='edit_subject'),

    url(r'^add-club-member/(?P<pk>.*)/$', views.add_club_member, name='add_club_member'),
    url(r'^delete-club-member/(?P<pk>.*)/$', views.delete_club_member, name='delete_club_member'),

    url(r'^create-club/$', views.create_club, name='create_club'),
    url(r'^clubs/$', views.clubs, name='clubs'),
    url(r'^club/(?P<pk>.*)/$', views.club, name='club'),
    url(r'^delete-club/(?P<pk>.*)/$', views.delete_club, name='delete_club'),
    url(r'^edit-club/(?P<pk>.*)/$', views.edit_club, name='edit_club'),

    url(r'^create-disability/$', views.create_disability, name='create_disability'),
    url(r'^disabilities/$', views.disabilities, name='disabilities'),
    url(r'^disability/(?P<pk>.*)/$', views.disability, name='disability'),
    url(r'^delete-disability/(?P<pk>.*)/$', views.delete_disability, name='delete_disability'),
    url(r'^edit-disability/(?P<pk>.*)/$', views.edit_disability, name='edit_disability'),

    url(r'^create-facility/$', views.create_facility, name='create_facility'),
    url(r'^facilities/$', views.facilities, name='facilities'),
    url(r'^facility/(?P<pk>.*)/$', views.facility, name='facility'),
    url(r'^delete-facility/(?P<pk>.*)/$', views.delete_facility, name='delete_facility'),
    url(r'^edit-facility/(?P<pk>.*)/$', views.edit_facility, name='edit_facility'),

    url(r'^upload-students/(?P<pk>.*)/$', views.upload_students, name='upload_students'),
    url(r'^upload-students-by-class/(?P<pk>.*)/$', views.upload_students_by_class, name='upload_students_by_class'),

    url(r'^get-divisions/$', views.get_divisions, name='get_divisions'),
    url(r'^list-class-and-divisions-for-admissions/$', views.list_class_and_division_for_admissions, name='list_class_and_division_for_admissions'),
    url(r'^create-class-and-divisions-for-admissions/$', views.create_class_and_division_for_admissions, name='create_class_and_division_for_admissions'),
    url(r'^admissions-by-batch/(?P<pk>[0-9a-f-]+)/division/(?P<division_pk>[0-9a-f-]+)/$', views.admissions_by_batch, name='admissions_by_batch'),
    url(r'^admissions-by-class/(?P<pk>[0-9a-f-]+)/class/(?P<class_pk>[0-9a-f-]+)/$', views.admissions_by_class, name='admissions_by_class'),
    url(r'^admission/(?P<pk>.*)/$', views.admissionn, name='admissionn'),

    url(r'^create/attendance/(?P<pk>.*)/$', views.create_attendance, name='create_attendance'),
    url(r'^edit/attendance/(?P<pk>.*)/$', views.edit_attendance, name='edit_attendance'),
    url(r'^attendances/(?P<pk>.*)/$', views.attendances, name='attendances'),
    url(r'^view/attendance/(?P<pk>.*)/$', views.attendance, name='attendance'),
    url(r'^delete/delete-attendance/(?P<pk>.*)/$', views.delete_attendance, name='delete_attendance'),
    url(r'^show-calendar-attendances/(?P<pk>.*)/$', views.show_calendar_attendances, name='show_calendar_attendances'),
    url(r'^get-periods-by-date/$', views.get_periods_by_date, name='get_periods_by_date'),
    
    url(r'^get-amounts/$', views.get_amounts, name='get_amounts'),

    url(r'^list-weekday-to-time-table/$', views.list_weekday_to_time_table, name='list_weekday_to_time_table'),
    url(r'^list-section-to-time-table/(?P<weekday>\w+?)/$', views.list_section_to_time_table, name='list_section_to_time_table'),
    
    url(r'^create-time-table/(?P<weekday>\w+?)/(?P<section>\w+?)/$', views.create_time_table, name='create_time_table'),
    url(r'^time_tables/$', views.time_tables, name='time_tables'),
    url(r'^time-table/(?P<pk>.*)/$', views.time_table, name='time_table'),
    url(r'^delete-time-table/(?P<pk>.*)/$', views.delete_time_table, name='delete_time_table'),
    url(r'^edit-time-table/(?P<pk>.*)/$', views.edit_time_table, name='edit_time_table'),

    url(r'^create-period/$', views.create_period, name='create_period'),
    url(r'^periods/$', views.periods, name='periods'),
    url(r'^period/(?P<pk>.*)/$', views.period, name='period'),
    url(r'^delete-period/(?P<pk>.*)/$', views.delete_period, name='delete_period'),
    url(r'^edit-period/(?P<pk>.*)/$', views.edit_period, name='edit_period'),

    url(r'^create-class-subject/$', views.create_class_subject, name='create_class_subject'),
    url(r'^class-subjects/$', views.class_subjects, name='class_subjects'),
    url(r'^class-subject/(?P<pk>.*)/$', views.class_subject, name='class_subject'),
    url(r'^delete-class-subject/(?P<pk>.*)/$', views.delete_class_subject, name='delete_class_subject'),
    url(r'^edit-class-subject/(?P<pk>.*)/$', views.edit_class_subject, name='edit_class_subject'),

    url(r'^show-class-attendence/$', views.show_class_attendence, name='show_class_attendence'),
    url(r'^show-division-attendence/(?P<pk>.*)/$', views.show_division_attendence, name='show_division_attendence'),
    
]
