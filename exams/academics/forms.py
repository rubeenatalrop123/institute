from django import forms
from django.forms.widgets import TextInput, Select, Textarea, HiddenInput
from academics.models import Department, Course, SubDivision, Admission,PeriodSubject,TimeTable,Period, Subject, Disability, Attendance, Facility, StudentClub, StudentClubMember
from django.utils.translation import ugettext_lazy as _
from dal import autocomplete
from decimal import Decimal


class DepartmentForm(forms.ModelForm):
    
    class Meta:
        model = Department
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "name": TextInput(attrs = { "class": "required form-control","placeholder" : "Student Class Name",}),
            "fee": TextInput(attrs = {"class": "required form-control","placeholder" : "Fee",}), 
            "diary_fee": TextInput(attrs = {"class": "required form-control", "placeholder" : "Diary Fee", }),
            "admission_fee": TextInput(attrs = {"class": "required form-control", "placeholder" : "Admission Fee",}),
            "computer_fee": TextInput(attrs = {"class": "required form-control","placeholder" : "Computer Fee",}),
            "special_fee": TextInput(attrs = {"class": "required form-control","placeholder" : "Special Fee",}),
            "library_fee": TextInput(attrs = {"class": "required form-control", "placeholder" : "Library Fee", }),                       
        }
        error_messages = {
            "name" : {
                "required" : _("Name field is required.")
            }
        }
        
    def clean(self):
        cleaned_data = super(DepartmentForm, self).clean()
        return cleaned_data


class CourseForm(forms.ModelForm):
    
    class Meta:
        model = Course
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "name": TextInput( attrs = {"class": "required form-control","placeholder" : "Student Medium Name", })

        }
        error_messages = {
            "name" : {
                "required" : _("Name field is required.")
            }
        }        
        
    def clean(self):
        cleaned_data = super(CourseForm, self).clean()
        return cleaned_data


class SubDivisionForm(forms.ModelForm):
    
    class Meta:
        model = SubDivision
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "name": TextInput(attrs = {"class": "required form-control","placeholder" : "Name",}),
            "department" :  autocomplete.ModelSelect2(url='academics:class_autocomplete',attrs={'data-placeholder': 'Classes','data-minimum-input-length': 1},),
            "batch_type": Select(attrs = {"class": "form-control selectordie","placeholder" : "Batch Type",}),
            "course": autocomplete.ModelSelect2(url='academics:medium_autocomplete',attrs={'data-placeholder': 'Medium','data-minimum-input-length': 1},),
        }
        error_messages = {
            "name" : {
                "required" : _("Name field is required.")
            },
            "course" : {
                "required" : _("Student medium field is required.")
            }
        }
             
    def clean(self):
        cleaned_data = super(SubDivisionForm, self).clean()
        return cleaned_data


class AdmissionBatchForm(forms.Form):
    admission_pk = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))
    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))
    division_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))
    class_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))
    sub_division = forms.ModelChoiceField(
        required=False,
        queryset=SubDivision.objects.all(),
        widget=forms.Select( attrs = {"class": "form-control selectordie","placeholder" : "New Division",})
    )        
    new_sub_division = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))
    
    def clean(self):
        cleaned_data = super(AdmissionBatchForm, self).clean()
        return cleaned_data


class AdmissionForm(forms.ModelForm):    
    class Meta:
        model = Admission        
        exclude = ['creator','updater','auto_id','age','actual_fee','department','course','sub_division','batch','fee','paid','balance','is_roll_out','is_deleted','college','disabilities','a_id','fee_amount','admission_fee','diary_fee','facilities']

        widgets = { 
            'former_student' : autocomplete.ModelSelect2(url='academics:admission_autocomplete', attrs={'placeholder': 'Former Student', 'data-minimum-input-length': 1},),
            "name": TextInput(attrs = {"class": "form-control","placeholder" : "Name",}),
            "smart_no": TextInput(attrs = {"class": "form-control","placeholder" : "Smart No",}),
            "register_no": TextInput(attrs = {"class": "form-control","placeholder" : "Register No",}),    
            "admn_no": TextInput(attrs = {"class": "form-control","placeholder" : "Admission No",}),             
            "batch": Select(attrs = {"class": " form-control selectordie","placeholder" : "Batch name",}),
            "course": Select(attrs = {"class": "required form-control selectordie","placeholder" : "Student Medium name",}),
            "department": Select(attrs = {"class": "required form-control selectordie","placeholder" : "Student Class Name",}),
            "sub_division": Select(attrs = {"class": "form-control selectordie","placeholder" : "Student Class Division",}),
            "address": Textarea(attrs = {"class": "form-control","placeholder" : "Enter your address",}),
            "std_phone": TextInput(attrs = {"class": "form-control","placeholder" : "Phone Number",}),
            "aadhar_no": TextInput(attrs = {"class": "form-control","placeholder" : "Aadhar Number",}),
            "blood_group": TextInput(attrs = {"class": "form-control","placeholder" : "Blood Group",}),
            "whatsapp_number": TextInput(attrs = {"class": " form-control","placeholder" : "whatsapp number",}),
            "discount": TextInput(attrs = {"class": "number required form-control","placeholder" : "Discount",}),
            "dob": TextInput(attrs = {"class": "date-picker form-control","placeholder" : "Date Of Birth",}),
            "date_joined": TextInput(attrs={"class": "date-picker form-control", "placeholder": "Enter date"}),
            "gender": Select(attrs = {"class": "form-control selectordie"}), 
            "guardian_name": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Name",}),
            "guardian_email": TextInput(attrs = {"class": " form-control","placeholder" : "Email",}),
            "guardian_phone": TextInput(attrs = {"class": "form-control","placeholder" : "Number",}),
            "guardian_occupation": TextInput(attrs = {"class": "form-control","placeholder" : "Occupation",}),        
            "father_name": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Name",}),
            "father_email": TextInput(attrs = {"class": " form-control","placeholder" : "Fathers Email",}),
            "phone": TextInput(attrs = {"class": "form-control","placeholder" : "Fathers Number",}),
            "father_occupation": TextInput(attrs = {"class": "form-control","placeholder" : "Fathers Occupation",}),
            "father_qualification": TextInput(attrs = {"class": "form-control","placeholder" : "Fathers Qualification",}),
            "father_annual_income": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Annual Income",}),
            "father_aadhar_no": TextInput(attrs = {"class": "form-control","placeholder" : "Aadhar Number",}),            
            "mother_name": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Name",}),
            "mother_email": TextInput(attrs = {"class": "form-control","placeholder" : "Mothers Email",}),
            "mother_phone": TextInput(attrs = {"class": "form-control","placeholder" : "Mothers number",}),
            "mother_occupation": TextInput(attrs = {"class": "form-control","placeholder" : "Mother Occupation",}),
            "mother_qualification": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Qualification",}),
            "mother_annual_income": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Annual Income",}),
            "mother_aadhar_no": TextInput(attrs = {"class": "form-control","placeholder" : "Aadhar Number",}),
            "co_curricular": TextInput(attrs = {"class": "form-control","placeholder" : "Co Curricular",}),           
            "caste": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Caste",}),
            "religion": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Religion",}),
            "category": Select(attrs = {"class": "form-control selectordie","placeholder" : "Category",}),  
            "mode_of_transport": Select(attrs = {"class": "form-control selectordie","placeholder" : "Mode of transport",}), 
            "are_you_hosteler": Select(attrs = {"class": "form-control selectordie","placeholder" : "Select",}), 
            'hostel_name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Enter Hostel Name'}),
            'place': TextInput(attrs={'class': 'required form-control','placeholder' : 'Enter Place'}),
        }

        error_messages = {
            "name": {
                "required" : _("Name field is required.")
            },
            "gender" : {
                    "required" : _("Gender field is required.")
                },
        }
        help_texts = {
        }

        labels = {
            "institute" : "College",
        }
        
    def clean(self):
        cleaned_data = super(AdmissionForm, self).clean()
        return cleaned_data
        

class SubjectForm(forms.ModelForm):
    
    class Meta:
        model = Subject
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "name": TextInput(attrs = { "class": "required form-control","placeholder" : "Subject Name",}),

        }
        error_messages = {
            "name" : {
                "required" : _("Name field is required.")
            }
        }
        help_texts = {
        }
        
    def clean(self):
        cleaned_data = super(SubjectForm, self).clean()
        return cleaned_data



class DisabilityForm(forms.ModelForm):

    class Meta:
        model = Disability
        fields = ['name',]
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Enter disability'}),
        }

        error_messages = {
            'name' : {
                'required' : _("Name field is required."),}  
        }

class FacilityForm(forms.ModelForm):

    class Meta:
        model = Facility
        fields = ['name',]
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Enter facility'}),
        }

        error_messages = {
            'name' : {
                'required' : _("Name field is required."),}  
        }


class FileForm(forms.Form):
    file = forms.FileField()


class AttendanceMaleForm(forms.ModelForm):
    
    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = Attendance
        exclude = ['creator','updater','auto_id','department','sub_division','is_deleted','date','weekday','absent_days_count','college','a_id']
        widgets = {
            'admission' : HiddenInput(), 
            "reason": TextInput(
                attrs = {
                    "class": "form-control",
                }
            ),           
        }
        error_messages = {
            "admission" : {
                    "required" : _("Admission field is required.")
                },
        }


class AttendanceFemaleForm(forms.ModelForm):
    
    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = Attendance
        exclude = ['creator','updater','auto_id','department','sub_division','is_deleted','date','weekday','absent_days_count','college','a_id']
        widgets = {
            'admission' : HiddenInput(),
            "reason": TextInput(
                attrs = {
                    "class": "form-control",
                }
            ),              
        }
        error_messages = {
            "admission" : {
                    "required" : _("Admission field is required.")
                },
        }


class AttendanceDateForm(forms.ModelForm):

    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = Attendance
        exclude = ['creator','updater','auto_id','department','sub_division','is_deleted','admission','is_present','absent_days_count','college','a_id']

        widgets = {      
                "date": TextInput(
                    attrs = {
                        "class": "required form-control date-picker",
                        "placeholder" : " Date ",
                    }
                ),
                "weekday": Select(
                    attrs = {
                        "class": "required form-control selectpicker",
                    }
                )
            }
        error_messages = {
                "date" : {
                    "required" : _("Date field is required.")
                },
                "weekday" : {
                    "required" : _("Period field is required.")
                },
            }  

class AttendanceForm(forms.ModelForm):
    
    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = Attendance
        exclude = ['creator','updater','auto_id','department','sub_division','is_deleted','date','weekday','absent_days_count','college','a_id']
        widgets = {
            'admission' : HiddenInput(),
            "reason": TextInput(attrs = {"class": "form-control"}),
            "behaviour": TextInput(attrs = {"class": "form-control"}),              
        }
        error_messages = {
            "admission" : {
                    "required" : _("Admission field is required.")
                },
        }
        

class AttendanceEditForm(forms.ModelForm):
    
    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = Attendance
        exclude = ['creator','admission','updater','auto_id','department','sub_division','is_deleted','date','weekday','absent_days_count','college','a_id']
        widgets = {
            "reason": TextInput(
                attrs = {
                    "class": "form-control",
                }
            ), 
            "behaviour": TextInput(
                attrs = {
                    "class": "form-control",
                }
            ),              
        }
        error_messages = {
            
        }


class PeriodSubjectForm(forms.ModelForm):
    
    class Meta:
        model = PeriodSubject
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "subject": autocomplete.ModelSelect2(url='academics:subject_autocomplete',attrs={'data-placeholder': 'Subject','data-minimum-input-length': 1},),
            "department": autocomplete.ModelSelect2(url='academics:class_autocomplete',attrs={'data-placeholder': 'Classes','data-minimum-input-length': 1},),
            "no_of_period": TextInput(
                attrs = {
                    "class": "form-control number",
                }
            ),

        }
        error_messages = {
            "no_of_period" : {
                "required" : _("No of Period field is required.")
            }
        }
        help_texts = {
        }
        
    def clean(self):
        cleaned_data = super(PeriodSubjectForm, self).clean()
        return cleaned_data


class PeriodForm(forms.ModelForm):
    
    class Meta:
        model = Period
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "from_time": TextInput(
                attrs = {
                    "class": "form-control" ,'placeholder' : 'HH:MM',
                }
            ),
            "to_time": TextInput(
                attrs = {
                    "class": "form-control" ,'placeholder' : 'HH:MM',
                }
            ),
            "weekday": Select(
                attrs = {
                    "class": "form-control required selectpicker",
                }
            ),
        }
        error_messages = {
            "no_of_period" : {
                "required" : _("No of Period field is required.")
            }
        }
        help_texts = {
        }
        
    def clean(self):
        cleaned_data = super(PeriodForm, self).clean()
        return cleaned_data



class TimeTableForm(forms.ModelForm):
    
    class Meta:
        model = TimeTable
        fields = ["class_subject","sub_division"]
        widgets = {            
            "period": Select(
                attrs = {
                    "class": "required form-control selectordie",
                    "placeholder" : "Period",
                }
            ),   
            "class_subject":  Select(
                attrs = {
                    "class": "required form-control selectordie",
                    "placeholder" : "Teacher",
                }),
            "sub_division": Select(
                attrs = {
                    "class": "required form-control selectordie",
                }),
           
            "weekday": Select(
                attrs = {
                    "class": "required form-control selectordie",
                    "placeholder" : "Weekday",
                }
            ),       
        }
        error_messages = {
            "period" : {
                "required" : _("Period field is required.")
            },
            "class_subject" : {
                "required" : _("Teacher field is required.")
            },
            "sub_division" : {
                "required" : _("Student Division field is required.")
            },
        }
        help_texts = {
        }
        
    def clean(self):
        cleaned_data = super(TimeTableForm, self).clean()
        return cleaned_data 


class ClubForm(forms.ModelForm):
    
    class Meta:
        model = StudentClub
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control extra','placeholder' : 'Club Name'}),
            "issue_date": TextInput(attrs={"class": "date-picker form-control", "placeholder": "Enter Issue date"}),

        }

        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },            
        }

        labels = {
            "name" : "Name",
        }
            

class ClubMemberForm(forms.ModelForm):
    
    class Meta:
        model = StudentClubMember
        exclude = ['creator','updater','auto_id','is_deleted','college','name','club','a_id']
        widgets = {
            'student': autocomplete.ModelSelect2(url='academics:admission_autocomplete', attrs={'placeholder': 'Student Name', 'data-minimum-input-length': 1},),

        }

        error_messages = {
            'student' : {
                'required' : _("Name field is required."),
            },            
        }

        labels = {
            "name" : "Name",
        }