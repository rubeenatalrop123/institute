from django import forms
from django.forms.widgets import TextInput, Select, Textarea, HiddenInput
from enquiries.models import Enquiry
from django.utils.translation import ugettext_lazy as _
from dal import autocomplete
from decimal import Decimal


class EnquiryForm(forms.ModelForm):

    class Meta:
        model = Enquiry
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            'student': autocomplete.ModelSelect2(url='enquiries:admission_autocomplete', attrs={'data-placeholder': 'Student', 'data-minimum-input-length': 1},),
            "name": TextInput(attrs = { "class": "form-control","placeholder" : "Name",}),
            "email": TextInput(attrs = { "class": "form-control","placeholder" : "Email",}),
            "phone": TextInput(attrs = { "class": "form-control","placeholder" : "Phone",}),
            "address": TextInput(attrs = { "class": "form-control","placeholder" : "Address",}),
            "reason": Textarea(attrs = { "class": "form-control","placeholder" : "Reason",}),

        }
        error_messages = {
            "name" : {
                "required" : _("Name field is required.")
            },
            "email" : {
                "required" : _("Email field is required.")
            },
            "phone" : {
                "required" : _("Phone field is required.")
            },
            "address" : {
                "required" : _("Address field is required.")
            },
            "reason" : {
                "required" : _("Reason field is required.")
            },
        }
        help_texts = {
        }

    def clean(self):
        cleaned_data = super(EnquiryForm, self).clean()
        return cleaned_data
