# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from main.models import BaseModel
from django.core.validators import MinValueValidator
from decimal import Decimal
from django.utils.translation import ugettext_lazy as _


class Enquiry(BaseModel):
    student = models.ForeignKey("academics.Admission",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE,blank=True,null=True)
    name = models.CharField(max_length=128,blank=True,null=True)
    email = models.EmailField(blank=True,null=True)
    phone = models.CharField(max_length=128,blank=True,null=True)
    address = models.CharField(max_length=128,blank=True,null=True,)
    reason = models.CharField(max_length=128,blank=True,null=True)

    class Meta:
        db_table = 'enquiries_enquiry'
        verbose_name = _('enquiry')
        verbose_name_plural = _('enquiries')

    def __str__(self):
        return self.name
