from django import forms
from django.forms.widgets import TextInput, Select, Textarea, HiddenInput
from academics.models import Department,Semester, Course, SubDivision, Admission,PeriodSubject,TimeTable,Period, Subject, Disability, Attendance, Facility, StudentClub, StudentClubMember
from django.utils.translation import ugettext_lazy as _
from dal import autocomplete
from decimal import Decimal


class DepartmentForm(forms.ModelForm):

    class Meta:
        model = Department
        exclude = ['course','is_deleted']
        widgets = {
            "name": TextInput(attrs = { "class": "required form-control","placeholder" : "Department name",}),
        }
        error_messages = {
            "name" : {
                "required" : _("Name field is required.")
            }
        }


    def clean(self):
        cleaned_data = super(DepartmentForm, self).clean()
        return cleaned_data


class CourseForm(forms.ModelForm):

    class Meta:
        model = Course
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "name": TextInput( attrs = {"class": "required form-control","placeholder" : "Course Name", })

        }
        error_messages = {
            "name" : {
                "required" : _("Name field is required.")
            }
        }
        labels = {
            "name" : "Course Name"
        }

    def clean(self):
        cleaned_data = super(CourseForm, self).clean()
        return cleaned_data


class SemesterForm(forms.ModelForm):

    class Meta:
        model = Semester
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "semester": Select( attrs = {"class": "required selectpicker form-control","placeholder" : "Semester Name", }),
            "from_date": TextInput(attrs={"class": "date-picker form-control", "placeholder": "Enter date"}),
            "to_date": TextInput(attrs={"class": "date-picker form-control", "placeholder": "Enter date"}),
            "division":autocomplete.ModelSelect2(url='academics:division_autocomplete',attrs={'data-placeholder': 'Division','data-minimum-input-length': 0},),
            "academic_year": autocomplete.ModelSelect2(url='academics:year_autocomplete',attrs={'data-placeholder': 'Batch','data-minimum-input-length': 0})
        }
        error_messages = {
            "semester" : {
                "required" : _("Name field is required.")
            }
        }
        labels = {
            "academic_year" : "Batch"
        }

    def clean(self):
        cleaned_data = super(SemesterForm, self).clean()
        return cleaned_data


class SubDivisionForm(forms.ModelForm):

    class Meta:
        model = SubDivision
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id','computer_fee','diary_fee']
        widgets = {
            "name": TextInput(attrs = {"class": "required form-control","placeholder" : "Name",}),
            "course_code": TextInput(attrs = { "class": "required form-control","placeholder" : "Course code",}),
            "department" :  autocomplete.ModelSelect2(url='academics:department_autocomplete',attrs={'data-placeholder': 'Department','data-minimum-input-length': 0},),
            "fee": TextInput(attrs = {"class": "required number form-control"}),
            "admission_fee": TextInput(attrs = {"class": "required number form-control"}),
            "special_fee": TextInput(attrs = {"class": "required number form-control"}),
            "library_fee": TextInput(attrs = {"class": "required number form-control"}),
            "semester_no" : Select(attrs = {"class": "form-control required selectpicker","placeholder" : "semester",}),
        }
        error_messages = {
            "name" : {
                "required" : _("Name field is required.")
            },
            "course" : {
                "required" : _("Student medium field is required.")
            }
        }
        labels = {
            "semester" : "No of Semester"
        }

    def clean(self):
        cleaned_data = super(SubDivisionForm, self).clean()
        return cleaned_data

class AdmissionBatchForm(forms.Form):
    admission_pk = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))
    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))
    division_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))
    class_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))
    sub_division = forms.ModelChoiceField(
        required=False,
        queryset=SubDivision.objects.all(),
        widget=forms.Select( attrs = {"class": "form-control selectordie","placeholder" : "New Division",})
    )
    new_sub_division = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))

    def clean(self):
        cleaned_data = super(AdmissionBatchForm, self).clean()
        return cleaned_data


class AdmissionForm(forms.ModelForm):
    class Meta:
        model = Admission
        exclude = ['creator','updater','auto_id','age','actual_fee','department','course','sub_division','batch','fee','paid','balance','is_roll_out','is_deleted','college','disabilities','a_id','fee_amount','admission_fee','diary_fee','facilities']

        widgets = {
            'former_student' : autocomplete.ModelSelect2(url='academics:admission_autocomplete', attrs={'class':'form-control','placeholder': 'Former Student', 'data-minimum-input-length': 1},),
            "register_no": TextInput(attrs = {"class": "form-control","placeholder" : "Register No", 'tabindex':"1"}),
            "admn_no": TextInput(attrs = {"class": "form-control","placeholder" : "Admission No", 'tabindex':"2"}),
            "name": TextInput(attrs = {"class": "form-control","placeholder" : "Name", 'tabindex':"3"}),
            "address": Textarea(attrs = {"class": "form-control","placeholder" : "Enter your address", 'tabindex':"4"}),
            "std_phone": TextInput(attrs = {"class": "form-control","placeholder" : "Phone Number", 'tabindex':"5"}),
            "whatsapp_number": TextInput(attrs = {"class": " form-control","placeholder" : "whatsapp number", 'tabindex':"6"}),
            "email": TextInput(attrs = {"class": " form-control","placeholder" : "Email", 'tabindex':"7"}),
            "blood_group": TextInput(attrs = {"class": "form-control","placeholder" : "Blood Group", 'tabindex':"8"}),
            "gender": Select(attrs = {"class": "form-control selectpicker", 'tabindex':"9"}),
            "dob": TextInput(attrs = {"class": "date-picker form-control","placeholder" : "Date Of Birth", 'tabindex':"10"}),
            "date_joined": TextInput(attrs={"class": "date-picker form-control", "placeholder": "Enter date", 'tabindex':"11"}),
            "aadhar_no": TextInput(attrs = {"class": "form-control","placeholder" : "Aadhar Number", 'tabindex':"12"}),

            "caste": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Caste", 'tabindex':"13"}),
            "religion": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Religion", 'tabindex':"14"}),
            "category": Select(attrs = {"class": "form-control selectordie","placeholder" : "Category", 'tabindex':"15"}),
            "co_curricular": TextInput(attrs = {"class": "form-control","placeholder" : "Co Curricular", 'tabindex':"16"}),
            "discount": TextInput(attrs = {"class": "number required form-control","placeholder" : "Discount", 'tabindex':"17"}),
            "mode_of_transport": Select(attrs = {"class": "form-control selectordie","placeholder" : "Mode of transport", 'tabindex':"18"}),
            "are_you_hosteler": Select(attrs = {"class": "form-control selectordie","placeholder" : "Select", 'tabindex':"19"}),
            'hostel_name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Enter Hostel Name', 'tabindex':"20"}),
            'place': TextInput(attrs={'class': 'required form-control','placeholder' : 'Enter Place', 'tabindex':"21"}),

            "father_name": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Name", 'tabindex':"22"}),
            "father_email": TextInput(attrs = {"class": " form-control","placeholder" : "Fathers Email", 'tabindex':"23"}),
            "phone": TextInput(attrs = {"class": "form-control","placeholder" : "Fathers Number", 'tabindex':"24"}),
            "father_qualification": TextInput(attrs = {"class": "form-control","placeholder" : "Fathers Qualification", 'tabindex':"25"}),
            "father_occupation": TextInput(attrs = {"class": "form-control","placeholder" : "Fathers Occupation", 'tabindex':"26"}),
            "father_annual_income": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Annual Income", 'tabindex':"27"}),
            "father_aadhar_no": TextInput(attrs = {"class": "form-control","placeholder" : "Aadhar Number", 'tabindex':"28"}),

            "mother_name": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Name", 'tabindex':"29"}),
            "mother_email": TextInput(attrs = {"class": "form-control","placeholder" : "Mothers Email", 'tabindex':"30"}),
            "mother_phone": TextInput(attrs = {"class": "form-control","placeholder" : "Mothers number", 'tabindex':"31"}),
            "mother_qualification": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Qualification", 'tabindex':"32"}),
            "mother_occupation": TextInput(attrs = {"class": "form-control","placeholder" : "Mother Occupation", 'tabindex':"33"}),
            "mother_annual_income": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Annual Income", 'tabindex':"34"}),
            "mother_aadhar_no": TextInput(attrs = {"class": "form-control","placeholder" : "Aadhar Number", 'tabindex':"35"}),

            "guardian_name": TextInput(attrs = {"class": "form-control","placeholder" : "Enter Name", 'tabindex':"36"}),
            "guardian_email": TextInput(attrs = {"class": " form-control","placeholder" : "Email", 'tabindex':"37"}),
            "guardian_phone": TextInput(attrs = {"class": "form-control","placeholder" : "Number", 'tabindex':"38"}),
            "guardian_occupation": TextInput(attrs = {"class": "form-control","placeholder" : "Occupation", 'tabindex':"39"}),

            "smart_no": TextInput(attrs = {"class": "form-control","placeholder" : "Smart No",}),
            "batch": Select(attrs = {"class": " form-control selectordie","placeholder" : "Batch name",}),
            "course": Select(attrs = {"class": "required form-control selectordie","placeholder" : "Student Medium name",}),
            "sub_division": Select(attrs = {"class": "form-control selectordie","placeholder" : "Student Class Division",}),
            "department": Select(attrs = {"class": "required form-control selectordie","placeholder" : "Student Class Name",}),
        }

        error_messages = {
            "name": {
                "required" : _("Name field is required.")
            },
            "gender" : {
                    "required" : _("Gender field is required.")
                },
        }
        help_texts = {
        }

        labels = {
            "institute" : "College",
        }

    def clean(self):
        cleaned_data = super(AdmissionForm, self).clean()
        return cleaned_data


class SubjectForm(forms.ModelForm):

    class Meta:
        model = Subject
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "name": TextInput(attrs = { "class": "required form-control","placeholder" : "Subject Name",}),
            "semester": Select(attrs = {"class": "form-control selectpicker required","placeholder" : "semester",}),
            "subject_type": Select(attrs = {"class": "form-control selectpicker","placeholder" : "Subject Type",}),
            "subject_code" : TextInput(attrs = { "class" :"form-control","placeholder" : "Subject Code" }),
            "sub_division" : autocomplete.ModelSelect2(url="academics:division_autocomplete",attrs={'data-placeholder' : 'Divisions' ,'data-minimum-input-length': 0 },),
        }
        error_messages = {
            "name" : {
                "required" : _("Name field is required.")
            }
        }
        help_texts = {
        }

        labels = {
            "subject_code" : "Subject Code",
            "subject_type" : "Subject Type",
            "sub_division" : "Division",
        }

    def clean(self):
        cleaned_data = super(SubjectForm, self).clean()
        return cleaned_data



class DisabilityForm(forms.ModelForm):

    class Meta:
        model = Disability
        fields = ['name',]
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Enter disability'}),
        }

        error_messages = {
            'name' : {
                'required' : _("Name field is required."),}
        }

class FacilityForm(forms.ModelForm):

    class Meta:
        model = Facility
        fields = ['name',]
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Enter facility'}),
        }

        error_messages = {
            'name' : {
                'required' : _("Name field is required."),}
        }


class FileForm(forms.Form):
    file = forms.FileField()


class AttendanceMaleForm(forms.ModelForm):

    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = Attendance
        exclude = ['creator','updater','auto_id','department','sub_division','is_deleted','date','weekday','absent_days_count','college','a_id']
        widgets = {
            'admission' : HiddenInput(),
            "reason": TextInput(
                attrs = {
                    "class": "form-control",
                }
            ),
        }
        error_messages = {
            "admission" : {
                    "required" : _("Admission field is required.")
                },
        }


class AttendanceFemaleForm(forms.ModelForm):

    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = Attendance
        exclude = ['creator','updater','auto_id','department','sub_division','is_deleted','date','weekday','absent_days_count','college','a_id']
        widgets = {
            'admission' : HiddenInput(),
            "reason": TextInput(
                attrs = {
                    "class": "form-control",
                }
            ),
        }
        error_messages = {
            "admission" : {
                    "required" : _("Admission field is required.")
                },
        }


class AttendanceDateForm(forms.ModelForm):

    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = Attendance
        exclude = ['creator','updater','auto_id','department','sub_division','is_deleted','admission','is_present','absent_days_count','college','a_id']

        widgets = {
                "date": TextInput(
                    attrs = {
                        "class": "required form-control date-picker",
                        "placeholder" : " Date ",
                    }
                ),
                "weekday": Select(
                    attrs = {
                        "class": "required form-control selectpicker",
                    }
                )
            }
        error_messages = {
                "date" : {
                    "required" : _("Date field is required.")
                },
                "weekday" : {
                    "required" : _("Period field is required.")
                },
            }

class AttendanceForm(forms.ModelForm):

    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = Attendance
        exclude = ['creator','updater','auto_id','department','sub_division','is_deleted','date','weekday','absent_days_count','college','a_id']
        widgets = {
            'admission' : HiddenInput(),
            "reason": TextInput(attrs = {"class": "form-control"}),
            "behaviour": TextInput(attrs = {"class": "form-control"}),
        }
        error_messages = {
            "admission" : {
                    "required" : _("Admission field is required.")
                },
        }


class AttendanceEditForm(forms.ModelForm):

    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = Attendance
        exclude = ['creator','admission','updater','auto_id','department','sub_division','is_deleted','date','weekday','absent_days_count','college','a_id']
        widgets = {
            "reason": TextInput(
                attrs = {
                    "class": "form-control",
                }
            ),
            "behaviour": TextInput(
                attrs = {
                    "class": "form-control",
                }
            ),
        }
        error_messages = {

        }


class PeriodSubjectForm(forms.ModelForm):

    class Meta:
        model = PeriodSubject
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "subject": Select(attrs={"class": "form-control selectpicker required ",'data-placeholder': 'Subject'},),
            "division": autocomplete.ModelSelect2(url='academics:division_autocomplete',attrs={'data-placeholder': 'Divisions'},),
            "no_of_period": TextInput(attrs = {"class": "form-control number"}),
            "batch": autocomplete.ModelSelect2(url='academics:year_autocomplete',attrs={'data-placeholder':'Batch'}),
            "semester" : Select(attrs = {"class": " form-control selectpicker ","placeholder" : "Batch name",}),
            # "semester": autocomplete.ModelSelect2(url='academics:semester_autocomplete',attrs={'data-placeholder': 'Semester'}),

        }
        error_messages = {
            "no_of_period" : {
                "required" : _("No of Period field is required.")
            }
        }

        help_texts = {
        }

        labels  = {
            "sub_division" : "Division",
            "no_of_period": "No of Periods"
        }

    def clean(self):
        cleaned_data = super(PeriodSubjectForm, self).clean()
        return cleaned_data


class PeriodForm(forms.ModelForm):

    class Meta:
        model = Period
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "from_time": TextInput(
                attrs = {
                    "class": "form-control" ,'placeholder' : 'HH:MM',
                }
            ),
            "to_time": TextInput(
                attrs = {
                    "class": "form-control" ,'placeholder' : 'HH:MM',
                }
            ),
            "weekday": Select(
                attrs = {
                    "class": "form-control required selectpicker",
                }
            ),
        }
        error_messages = {
            "no_of_period" : {
                "required" : _("No of Period field is required.")
            }
        }
        help_texts = {
        }

    def clean(self):
        cleaned_data = super(PeriodForm, self).clean()
        return cleaned_data



class TimeTableForm(forms.ModelForm):

    class Meta:
        model = TimeTable
        fields = ["class_subject","sub_division"]
        widgets = {
            "period": Select(
                attrs = {
                    "class": "required form-control selectordie",
                    "placeholder" : "Period",
                }
            ),
            "class_subject":  Select(
                attrs = {
                    "class": "required form-control selectordie",
                    "placeholder" : "Teacher",
                }),
            "sub_division": Select(
                attrs = {
                    "class": "required form-control selectordie",
                }),

            "weekday": Select(
                attrs = {
                    "class": "required form-control selectordie",
                    "placeholder" : "Weekday",
                }
            ),
        }
        error_messages = {
            "period" : {
                "required" : _("Period field is required.")
            },
            "class_subject" : {
                "required" : _("Teacher field is required.")
            },
            "sub_division" : {
                "required" : _("Student Division field is required.")
            },
        }
        help_texts = {
        }

    def clean(self):
        cleaned_data = super(TimeTableForm, self).clean()
        return cleaned_data


class ClubForm(forms.ModelForm):

    class Meta:
        model = StudentClub
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control extra','placeholder' : 'Club Name'}),
            'register_no': TextInput(attrs={'class': 'form-control extra','placeholder' : 'Register Number'}),
            "issue_date": TextInput(attrs={"class": "date-picker form-control", "placeholder": "Enter Issue date"}),

        }

        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
        }

        labels = {
            "name" : "Name",
            "register_no": "Register Number"
        }


class ClubMemberForm(forms.ModelForm):

    class Meta:
        model = StudentClubMember
        exclude = ['creator','updater','auto_id','is_deleted','college','name','club','a_id']
        widgets = {
            # 'student': autocomplete.ModelSelect2(url='academics:admission_autocomplete', attrs={'placeholder': 'Student Name', 'data-minimum-input-length': 1},),
            "student" : Select(attrs = {"class": " form-control selectpicker","placeholder" : "Student name",}),
        }

        error_messages = {
            'student' : {
                'required' : _("Name field is required."),
            },
        }

        labels = {
            "name" : "Name",
        }
