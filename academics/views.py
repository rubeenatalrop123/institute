from __future__ import unicode_literals
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from academics.models import Department,Semester, PeriodSubject,PeriodSubject, SubDivision,Course, Subject,PeriodSubject,Period,TimeTable, Admission, Disability, FeeCategory, Attendance,StudentFee, Facility, StudentClub, StudentClubMember
from academics.forms import DepartmentForm, SemesterForm,CourseForm,PeriodSubjectForm,PeriodSubjectForm,PeriodForm,TimeTableForm, SubDivisionForm, SubjectForm, AdmissionForm, DisabilityForm, FileForm, AttendanceMaleForm, AttendanceFemaleForm, AttendanceDateForm, AttendanceEditForm, FacilityForm, ClubForm, ClubMemberForm, AdmissionBatchForm
from staffs.models import Staff
from django.utils.translation import ugettext_lazy as _
from main.functions import get_auto_id, generate_form_errors, get_current_role, generate_unique_id, get_admn_no, get_a_id, UUIDEncoder
from users.functions import get_current_college, college_access,get_current_batch
from academics.functions import update_student_fee
from django.forms.formsets import formset_factory
import json
from django.forms.models import inlineformset_factory
from django.forms.widgets import TextInput, Select
from main.decorators import check_mode, ajax_required, role_required
from users.models import Notification, NotificationSubject
from finance.models import Transaction
import datetime
import xlrd
import random
from main.models import Place, Batch
from dal import autocomplete
from django.core import serializers
from decimal import Decimal
from django.db.models import Q
from django.contrib.auth.models import User, Group


@check_mode
@login_required
@role_required(['superadmin'])
def dashboard(request):
    return HttpResponseRedirect(reverse('academics:departments'))


class SubjectAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = Subject.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))

        return items


def get_semester(request):
    pk = request.GET.get('id')
    if SubDivision.objects.filter(pk=pk).exists():
        sub_div = SubDivision.objects.get(pk=pk,is_deleted=False)

        response_data = {
            "status" : "true",
            "semesters" : str(sub_div.semester_no),
        }
    else:
        response_data = {
            "status" : "false",
            "message" : "semesters not added"
        }
    return HttpResponse(json.dumps(response_data),content_type='application/javascript')


def get_students_data(request):
    pk = request.GET.get('pk')
    if Admission.objects.filter(sub_division=pk).exists():
        students = Admission.objects.filter(sub_division=pk,is_deleted=False)
        student_datas = []
        for student in students:
            std_item ={
                'id': str(student.pk),
                'name': student.name,
                'admission_no': student.admn_no
            }
            student_datas.append(std_item)
        response_data = {
            "status" : "true",
            "students" : student_datas,
        }
    else:
        response_data = {
            "status" : "false",
            "message" : "Students not added"
        }
    return HttpResponse(json.dumps(response_data),content_type='application/javascript')


class CourseAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = Course.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))

        return items


class DepartmentAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = Department.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q))

        return items


class ClassAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = Department.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))

        return items

class YearAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = Batch.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))

        return items

class SemesterAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = Semester.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))

        return items

class AdmissionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = Admission.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(auto_id__istartswith=self.q) |
                                 Q(name__istartswith=self.q)
                                )
        return items


class DivisionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):

        divisions = SubDivision.objects.filter(is_deleted=False)
        current_role = get_current_role(self.request)

        if self.q:
            divisions = divisions.filter(Q(name__istartswith=self.q))
            print(divisions)

        return divisions


class FeeCategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = FeeCategory.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))
            print(items)
        return items


class CourseAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = Course.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) )
            print(items)
        return items


@check_mode
@login_required
@role_required(['superadmin'])
def departments(request):
    instances = Department.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(name__icontains=query))
        title = "Classes - %s" %query

    context = {
        "instances" : instances,
        "title" : "Departments",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/academics.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def view_department(request,pk):
    instance = get_object_or_404(Department.objects.filter(pk=pk,is_deleted=False))
    context = {
        "title" : "Department",
        'instance' : instance,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/academic.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def create_department(request):
    url = reverse('academics:create_department')
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)

    if request.method == "POST":

        form = DepartmentForm(request.POST)

        if form.is_valid():
            data = form.save(commit=False)
            auto_id = get_auto_id(Department)
            data.auto_id = auto_id
            data.creator = request.user
            data.updater = request.user
            data.college = current_college
            data.batch = current_batch
            data.a_id = get_a_id(Department,request)
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully created",
                "message": "Department successfully created.",
                "redirect": "true",
                "redirect_url": reverse('academics:view_department', kwargs={'pk':data.pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = DepartmentForm()
        division = form.sub_division
        if division:
            print("success")
        else:
            print("false")

        context = {
            "title" : "Create Department",
            "form" : form,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'academics/academic_entry.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_department(request,pk):
    instance = get_object_or_404(Department.objects.filter(pk=pk,is_deleted=False))
    url = reverse('academics:edit_department',kwargs={'pk':pk})
    instance_fee = instance.fee
    instance_admission_fee = instance.admission_fee
    instance_diary_fee = instance.diary_fee
    instance_computer_fee = instance.computer_fee
    instance_library_fee = instance.library_fee
    instance_special_fee = instance.special_fee
    today = datetime.date.today()
    student_fees = StudentFee.objects.filter(fee_category__name="monthly_fee",admission__department=instance,is_deleted=False,admission__batch__start_date__lte=today,admission__batch__end_date__gte=today)

    admission_student_fees = StudentFee.objects.filter(fee_category__name="admission_fee",admission__department=instance,is_deleted=False,admission__batch__start_date__lte=today,admission__batch__end_date__gte=today)

    diary_student_fees = StudentFee.objects.filter(fee_category__name="diary_fee",admission__department=instance,is_deleted=False,admission__batch__start_date__lte=today,admission__batch__end_date__gte=today)

    library_student_fees = StudentFee.objects.filter(fee_category__name="library_fee",admission__department=instance,is_deleted=False,admission__batch__start_date__lte=today,admission__batch__end_date__gte=today)

    computer_student_fees = StudentFee.objects.filter(fee_category__name="computer_fee",admission__department=instance,is_deleted=False,admission__batch__start_date__lte=today,admission__batch__end_date__gte=today)

    special_student_fees = StudentFee.objects.filter(fee_category__name="special_fee",admission__department=instance,is_deleted=False,admission__batch__start_date__lte=today,admission__batch__end_date__gte=today)

    if request.method == "POST":

        form = DepartmentForm(request.POST,instance=instance)
        if form.is_valid():
            fee = form.cleaned_data['fee']
            admission_fees = form.cleaned_data['admission_fee']
            diary_fees = form.cleaned_data['diary_fee']
            computer_fees = form.cleaned_data['computer_fee']
            library_fees = form.cleaned_data['library_fee']
            special_fees = form.cleaned_data['special_fee']

            fee_defference = fee - instance_fee
            admission_defference = admission_fees - instance_admission_fee
            library_defference = library_fees - instance_library_fee
            diary_defference = diary_fees - instance_diary_fee
            computer_defference = computer_fees - instance_computer_fee
            special_defference = special_fees - instance_special_fee

            if student_fees:
                for student_fee in student_fees:
                    student_fee.amount = fee
                    student_fee.balance = fee
                    student_fee.admission.balance += fee_defference
                    student_fee.admission.save()
                    student_fee.save()
            if admission_student_fees:
                for admission_fee in admission_student_fees:
                    admission_fee.amount = admission_fees
                    admission_fee.balance = admission_fees
                    admission_fee.admission.balance += admission_defference
                    admission_fee.admission.save()
                    admission_fee.save()
            if diary_student_fees:
                for diary_fee in diary_student_fees:
                    diary_fee.amount = diary_fees
                    diary_fee.balance = diary_fees
                    diary_fee.admission.balance += diary_defference
                    diary_fee.admission.save()
                    diary_fee.save()
            if library_student_fees:
                for library_fee in library_student_fees:
                    library_fee.amount = library_fees
                    library_fee.balance = library_fees
                    library_fee.admission.balance += library_defference
                    library_fee.admission.save()
                    library_fee.save()
            if computer_student_fees:
                for computer_fee in computer_student_fees:
                    computer_fee.amount = computer_fees
                    computer_fee.balance = computer_fees
                    computer_fee.admission.balance += computer_defference
                    computer_fee.admission.save()
                    computer_fee.save()

            if special_student_fees:
                for special_fee in special_student_fees:
                    special_fee.amount = special_fees
                    special_fee.balance = special_fees
                    special_fee.admission.balance += special_defference
                    special_fee.admission.save()
                    special_fee.save()

            #update student class
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Department successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('academics:view_department',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = DepartmentForm(instance=instance)

        context = {
            "title" : "Edit Department",
            "form" : form,
            "instance" : instance,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'academics/academic_entry.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_department(request,pk):
    Department.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Department successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('academics:departments')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


# medium starts from here #
@check_mode
@login_required
@role_required(['superadmin'])
def courses(request):
    instances = Course.objects.filter(is_deleted=False)
    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(name__icontains=query))
        title = "Mediums - %s" %query

    context = {
        "instances" : instances,
        "title" : "Courses",
        "redirect": "true",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/courses.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def view_course(request,pk):
    instance = get_object_or_404(Course.objects.filter(pk=pk,is_deleted=False))
    departments_instances = Department.objects.filter(course=pk,is_deleted=False)
    context = {
        "title" : "Course",
        'instance' : instance,
        'departments_instances': departments_instances,
        "redirect": "true",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/view_course.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def create_course(request):
    url = reverse('academics:create_course')
    DepartmentFormset = formset_factory(DepartmentForm,extra=1)
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    if request.method == "POST":

        form = CourseForm(request.POST)
        department_formset = DepartmentFormset(request.POST,prefix="department_formset")
        if form.is_valid() and department_formset.is_valid():

            data = form.save(commit=False)
            auto_id = get_auto_id(Course)
            data.auto_id = auto_id
            data.creator = request.user
            data.updater = request.user
            data.college = current_college
            data.batch = current_batch
            data.a_id = get_a_id(Course,request)
            data.save()

            for value in department_formset:
                name = value.cleaned_data["name"]

                dept = Department(course=data, name=name)
                dept.save()

            response_data = {
                "status": "true",
                "title": "Successfully created",
                "message": "Course successfully created.",
                "redirect": "true",
                "redirect_url": reverse('academics:courses')
            }

        else:
            message1 = generate_form_errors(form, formset=False)


            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = CourseForm()
        department_formset = DepartmentFormset(prefix="department_formset")
        context = {
            "title" : "Create Course",
            "form" : form,
            "url" : url,
            'department_formset': department_formset,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }

        return render(request, 'academics/entry_course.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_course(request,pk):
    instance = get_object_or_404(Course.objects.filter(pk=pk,is_deleted=False))
    url = reverse('academics:edit_course',kwargs={'pk':pk})

    if Department.objects.filter(course=instance,is_deleted=False).exists():
        extra = 0
    else:
        extra= 1
    DepartmentFormset = inlineformset_factory(
        Course,
        Department,
        extra = extra,
        exclude = ('course','id'),
        widgets = {
            "name": TextInput(attrs = { "class": "required form-control","placeholder" : "Department name",}),
        }
    )

    if request.method == "POST":

        form = CourseForm(request.POST,instance=instance)
        department_formset = DepartmentFormset(request.POST,prefix="department_formset",instance=instance)
        if form.is_valid() and department_formset.is_valid():

            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            department_items = department_formset.save(commit=False)
            for item in department_items:
                item.course = data
                item.save()

            for obj in department_formset.deleted_objects:
                obj.delete()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Course successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('academics:view_course',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)
            message += generate_form_errors(department_formset,formset=True)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = CourseForm(instance=instance)
        department_formset = DepartmentFormset(prefix="department_formset",instance=instance)

        context = {
            "title" : "Edit student medium",
            "form" : form,
            "instance" : instance,
            "url" : url,
            "department_formset": department_formset,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'academics/entry_course.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_course(request,pk):
    Course.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Course successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('academics:courses')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def create_semester(request):
    if request.method=='POST':
        form = SemesterForm(request.POST)
        current_college = get_current_college(request)
        current_batch = get_current_batch(request)

        if form.is_valid():
            sem = form.cleaned_data['semester']
            div = form.cleaned_data['division']
            batch = form.cleaned_data['academic_year']
            if not Semester.objects.filter(semester=sem,division=div,academic_year=batch,is_deleted=False).exists():
                data = form.save(commit=False)
                data.creator = request.user
                data.updater = request.user
                data.auto_id = get_auto_id(Semester)
                data.college = current_college
                data.batch = current_batch
                data.a_id = get_a_id(Semester,request)
                data.save()

                response_data = {
                    'status' : 'true',
                    'title' : 'successfully Created',
                    'message' : "Successfully Created new Semester",
                    'redirect' : 'true',
                    'redirect_url' : reverse('academics:view_semester',kwargs={"pk":data.pk})
                }
            else:
                response_data = {
                    'status' : 'false',
                    'stable' : 'true',
                    'title' : 'Form Validation Error Occured',
                    'message' : 'Semester already added',
                }

        else:
            message = generate_form_errors(form,formset=False)
            response_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : 'Form Validation Error Occured',
                'message' : message,
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = SemesterForm()
        context = {
            "form" : form,
            "title" : "Create Semester",
            "redirect": True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }

        return render(request,'academics/entry_semester.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def semesters(request):
    instances = Semester.objects.filter(is_deleted=False)
    query = request.GET.get('q')
    if query:
        instances = instances.filter(Q(semester__icontains=query))
    context = {
        'title': "semesters :",
        'instances' : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request,'academics/semesters.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def view_semester(request,pk):
    instance = get_object_or_404(Semester.objects.filter(pk=pk))
    context = {
        'title': "semester : " + instance.semester,
        'instance' : instance,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request,'academics/semester.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_semester(request,pk):
    instance = get_object_or_404(Semester.objects.filter(pk=pk))

    if request.method=='POST':
        form = SemesterForm(request.POST,instance=instance)

        if form.is_valid():
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                'status' : 'true',
                'title' : 'successfully Updated',
                'message' : "Successfully Updated Semester",
                'redirect' : 'true',
                'redirect_url' : reverse('academics:view_semester',kwargs={"pk":data.pk})
            }

        else:
            message = generate_form_errors(form,formset=False)
            responsive_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : 'Form Validation Error Occured',
                'message' : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = SemesterForm(instance=instance)
        context = {
            "form" : form,
            "title" : "Update Semester",
            "redirect": True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }

        return render(request,'academics/entry_semester.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_semester(request,pk):
    Semester.objects.filter(pk=pk).update(is_deleted=True)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Semester Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('academics:semesters')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def list_of_divisions_and_sem(request):
    current_college = get_current_college(request)

    sub_divisions = SubDivision.objects.filter(is_deleted=False,college=current_college)

    context = {
        "divisions" : sub_divisions,
        "title" : "Select Division and SEMESTER",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/list_of_divisions_and_sem.html', context)



@check_mode
@login_required
@role_required(['superadmin'])
def create_subject(request):
    url = reverse('academics:create_subject')
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)

    if request.method == "POST":

        form = SubjectForm(request.POST)
        subject_code = request.POST.get('subject_code')

        if not Subject.objects.filter(subject_code=subject_code).exists():

            if form.is_valid():

                data = form.save(commit=False)
                auto_id = get_auto_id(Subject)
                data.auto_id = auto_id
                data.creator = request.user
                data.updater = request.user
                data.college = current_college
                data.batch = current_batch
                data.a_id = get_a_id(Subject,request)
                data.save()

                response_data = {
                    "status": "true",
                    "title": "Successfully created",
                    "message": "Subject successfully created.",
                    "redirect": "true",
                    "redirect_url": reverse('academics:subject', kwargs={'pk':data.pk})
                    }

                return HttpResponse(json.dumps(response_data), content_type='application/javascript')



            else:
                message = generate_form_errors(form, formset=False)

                response_data = {
                    "status": "false",
                    "stable": "true",
                    "title": "Form validation error",
                    "message": str(message)
                }

            return HttpResponse(json.dumps(response_data), content_type='application/javascript')

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "title": "Subject code is already exists",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')


    else:
        form = SubjectForm()

        context = {
            "title" : "Create Subject",
            "form" : form,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'academics/entry_subject.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_subject(request,pk):
    instance = get_object_or_404(Subject.objects.filter(pk=pk,is_deleted=False))
    url = reverse('academics:edit_subject',kwargs={'pk':pk})

    if request.method == "POST":

        form = SubjectForm(request.POST,instance=instance)

        if form.is_valid():

            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Subject successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('academics:subject',kwargs={"pk":pk})
                }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = SubjectForm(instance=instance)

        context = {
            "title" : "Edit Subject",
            "form" : form,
            "instance" : instance,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'academics/entry_subject.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def subjects(request,pk,sem):
    instances = Subject.objects.filter(is_deleted=False,semester=sem,sub_division=pk)

    query = request.GET.get("q")

    if query:
        instances = instances.filter( Q(name__icontains=query) | Q(subject_code__icontains=query))
        title = "Subjects - %s" %query

    context = {
        "instances" : instances,
        "title" : "Subjects",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/subjects.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def subject(request,pk):
    instance = get_object_or_404(Subject.objects.filter(pk=pk))
    context = {
        'title' : instance.name,
        'instance' : instance,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'academics/subject.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_subject(request,pk):
    instance = get_object_or_404(Subject.objects.filter(pk=pk,is_deleted=False))
    Subject.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))

    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Subject successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('academics:subjects',kwargs={"pk":instance.sub_division,'sem':instance.semester})
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


# division starts from here #
@check_mode
@login_required
@role_required(['superadmin'])
def sub_divisions(request):
    instances = SubDivision.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(name__icontains=query) | Q(course__name__icontains=query))
        title = "Divisions - %s" %query

    context = {
        "instances" : instances,
        "title" : "Sub Divisions",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }

    return render(request, 'academics/sub_divisions.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def view_sub_division(request,pk):
    instance = get_object_or_404(SubDivision.objects.filter(pk=pk,is_deleted=False))
    context = {
        "title" : "Sub Division - %s" %(instance.name),
        'instance' : instance,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/view_sub_division.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def create_sub_division(request):
    url = reverse('academics:create_sub_division')
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    if request.method == "POST":

        form = SubDivisionForm(request.POST)

        if form.is_valid():
            div_name = form.cleaned_data['semester_no']
            print(div_name)
            div_code = form.cleaned_data['course_code']

            if not SubDivision.objects.filter(course_code=div_code).exists():
                #create sub division
                data = form.save(commit=False)
                auto_id = get_auto_id(SubDivision)
                data.auto_id = auto_id
                data.creator = request.user
                data.updater = request.user
                data.college = current_college
                data.a_id = get_a_id(SubDivision,request)
                data.save()
            else:
                if SubDivision.objects.filter(name=div_name).exists():
                    message = "Sub Division name already exist"
                else:
                    message = "Course code already exist"

                response_data = {
                    "status": "false",
                    "stable": "true",
                    "title": "Form validation error",
                    "message": message
                }
                return HttpResponse(json.dumps(response_data), content_type='application/javascript')

            response_data = {
                "status": "true",
                "title": "Successfully created",
                "message": "Division successfully created.",
                "redirect": "true",
                "redirect_url": reverse('academics:view_sub_division',kwargs={'pk':data.pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = SubDivisionForm()

        context = {
            "title" : "Create Division",
            "form" : form,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'academics/sub_division_entry.html', context)


# @check_mode
# @login_required
# def update_division_user(request):
#     instances = SubDivision.objects.all()
#     for instance in instances:
#         if not instance.user:
#             username = "%s" %(instance.name)
#             chars = "abcdefghijklmnopqrstuvwxyz0123456789"
#             password = ''
#             username = username.lower().replace(' ','')
#             exists = User.objects.filter(username=username).exists()
#             while exists:
#                 random_number = generate_unique_id(size=3,chars=string.digits)
#                 username = username + random_number
#                 exists = User.objects.filter(username=username).exists()

#             for c in range(8):
#                 password += random.choice(chars)

#             user = User.objects.create_user(username=username,password=password)
#             instance.division_password = password
#             instance.user = user
#             instance.save()
#         else:
#             chars = "abcdefghijklmnopqrstuvwxyz0123456789"
#             password = ''
#             for c in range(8):
#                 password += random.choice(chars)
#             user = instance.user
#             password = password.encode('utf-8')
#             user.set_password(password)
#             user.save()

#             instance.division_password = password
#             instance.save()

#     return HttpResponseRedirect(reverse('academics:sub_divisions'))


@check_mode
@login_required
@role_required(['superadmin'])
def edit_sub_division(request,pk):
    instance = get_object_or_404(SubDivision.objects.filter(pk=pk,is_deleted=False))
    url = reverse('academics:edit_sub_division',kwargs={'pk':pk})
    if request.method == "POST":

        form = SubDivisionForm(request.POST,instance=instance)
        if form.is_valid():

            #update student division
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Division Successfully Updated.",
                "redirect": "true",
                "redirect_url": reverse('academics:view_sub_division',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = SubDivisionForm(instance=instance)

        context = {
            "title" : "Edit Division",
            "form" : form,
            "instance" : instance,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'academics/sub_division_entry.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_sub_division(request,pk):
    SubDivision.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Division Successfully Deleted.",
        "redirect": "true",
        "redirect_url": reverse('academics:sub_divisions')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


# admission statrs from here #
@check_mode
@login_required
@role_required(['superadmin'])
def admissions(request):
    instances = Admission.objects.filter(is_deleted=False,is_roll_out=False)
    today = datetime.date.today()
    # if current_batch:
    #     student_batches = StudentBatch.objects.filter(is_deleted=False).exclude(pk=current_batch.pk)
    #     instances = Admission.objects.filter(is_deleted=False,student_batch=current_batch,is_roll_out=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( (Q(name__icontains=query)) | (Q(place__icontains=query)) | (Q(phone__icontains=query)) | (Q(paid__startswith=query)) | (Q(fee_amount__startswith=query)) | (Q(balance__startswith=query)) | (Q(address__icontains=query)) | (Q(date_joined__icontains=query)))
        title = "Admissions - %s" %query

    # department = request.GET.get("department")
    # if department:
    #     instances = instances.filter( Q(department__pk=department))
    #     title = "Admissions - %s" %department

    # sub_division = request.GET.get("sub_division")
    # if sub_division:
    #     instances = instances.filter( Q(sub_division__pk=sub_division))
    #     title = "Admissions - %s" %sub_division

    # squadarea = request.GET.get('squadarea')
    # if squadarea:
    #     instances = instances.filter(squadarea__pk=squadarea)

    # gender = request.GET.get("gender")
    # if gender == "male":
    #     instances = instances.filter(gender=gender)
    #     title += " (Gender : Male)"
    # elif gender == "female":
    #     instances = instances.filter(gender=gender)
    #     title += " (Gender : Female)"

    # place = request.GET.get("place")
    # if place:
    #     instances = instances.filter( Q(place=place))
    #     title = "Admissions - %s" %place


    # institute = request.GET.get("institute")
    # if institute:
    #     instances = instances.filter( Q(institute=institute))
    #     title = "Admissions - %s" %institute

    context = {
        "instances" : instances,
        "title" : "Admissions ",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,

    }
    return render(request, 'academics/admission_details.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def admissions_by_batch(request,pk,division_pk):
    student_batch = get_object_or_404(Batch.objects.filter(is_deleted=False,pk=pk))
    instances = Admission.objects.filter(is_deleted=False,batch=student_batch,sub_division__pk=division_pk,is_roll_out=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( (Q(name__icontains=query)) | (Q(place__icontains=query)) | (Q(phone__icontains=query)) | (Q(paid__startswith=query)) | (Q(fee_amount__startswith=query)) | (Q(balance__startswith=query)))
        title = "Admissions - %s" %query

    department = request.GET.get("department")
    if department:
        instances = instances.filter( Q(department=department))
        title = "Admissions - %s" %department

    sub_division = request.GET.get("sub_division")
    if sub_division:
        instances = instances.filter( Q(sub_division=sub_division))
        title = "Admissions - %s" %sub_division

    gender = request.GET.get("gender")
    if gender == "male":
        instances = instances.filter(gender=gender)
        title += " (Gender : Male)"
    elif gender == "female":
        instances = instances.filter(gender=gender)
        title += " (Gender : Female)"

    context = {
        "instances" : instances,
        "title" : "Admissions in %s" %(student_batch),
        "student_batch" : student_batch,
        "division_pk" : division_pk,
        "department" : department,
        "sub_division" : sub_division,
        "query" : query,
        "gender" : gender,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/admissions.html', context)


# @check_mode
# @login_required
# @role_required(['administrator','teacher','division'])
# def print_admissions_by_batch(request,pk,division_pk):
#     student_batch = get_object_or_404(StudentBatch.objects.filter(is_deleted=False,pk=pk))
#     division = get_object_or_404(SubDivision.objects.filter(is_deleted=False,pk=division_pk))
#     instances = Admission.objects.filter(is_deleted=False,student_batch=student_batch,sub_division__pk=division_pk,is_roll_out=False)

#     squad_areas = Place.objects.filter(is_deleted=False)
#     institutes = Institute.objects.filter(is_deleted=False)
#     query = request.GET.get("q")
#     if query:
#         instances = instances.filter( (Q(name__icontains=query)) | (Q(place__name__icontains=query)) | (Q(phone__icontains=query)) )
#         title = "Admissions - %s" %query

#     squad_area = request.GET.get("squad_area")
#     if squad_area:
#         instances = instances.filter( Q(place=squad_area))
#         title = "Admissions - %s" %squad_area

#     institute = request.GET.get("institute")
#     if institute:
#         instances = instances.filter( Q(institute=institute))
#         title = "Admissions - %s" %institute
#     context = {
#         "instances" : instances,
#         "title" : "Admissions in %s" %(division.name),
#         "student_batch" : student_batch,
#         "division_pk" : division_pk,
#         "squad_areas" : squad_areas,
#         "squad_area" : squad_area,
#         "institutes" : institutes,
#         "institute" : institute,
#     }
#     return render(request, 'students/print_admissions_by_class.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def admissions_by_class(request,pk,class_pk):
    student_batch = get_object_or_404(Batch.objects.filter(is_deleted=False,is_active=True,pk=pk))
    instances = Admission.objects.filter(is_deleted=False,batch=student_batch,department__pk=class_pk,is_roll_out=False)
    department = get_object_or_404(Department.objects.filter(is_deleted=False,pk=class_pk))
    sub_divisions = SubDivision.objects.filter(is_deleted=False,department=department)


    query = request.GET.get("q")
    if query:
        instances = instances.filter( (Q(name__icontains=query)) | (Q(phone__icontains=query)) )
        title = "Admissions - %s" %query

    sub_division = request.GET.get("sub_division")
    if sub_division:
        instances = instances.filter( Q(sub_division__pk=sub_division))
        title = "Admissions - %s" %sub_division



    gender = request.GET.get("gender")
    if gender == "male":
        instances = instances.filter(gender=gender)
        title += " (Gender : Male)"
    elif gender == "female":
        instances = instances.filter(gender=gender)
        title += " (Gender : Female)"

    context = {
        "instances" : instances,
        "title" : "Admissions in Class - %s" %(department),
        "student_batch" : student_batch,
        "query" : query,
        "gender" : gender,
        "class_pk" : class_pk,
        "department" : department,
        "sub_divisions" : sub_divisions,
        "sub_division" : sub_division,
    }
    return render(request, 'academics/admissions.html', context)


# @check_mode
# @login_required
# @role_required(['administrator','teacher'])
# def print_admissions_by_class(request,pk,class_pk):
#     student_batch = get_object_or_404(StudentBatch.objects.filter(is_deleted=False,pk=pk))
#     department = get_object_or_404(Department.objects.filter(is_deleted=False,pk=class_pk))
#     instances = Admission.objects.filter(is_deleted=False,student_batch=student_batch,department__pk=class_pk,is_roll_out=False)

#     sub_divisions = SubDivision.objects.filter(is_deleted=False)
#     squad_areas = Place.objects.filter(is_deleted=False)
#     institutes = Institute.objects.filter(is_deleted=False)

#     query = request.GET.get("q")
#     if query:
#         instances = instances.filter( (Q(name__icontains=query)) | (Q(place__name__icontains=query)) | (Q(phone__icontains=query)) )
#         title = "Admissions - %s" %query

#     sub_division = request.GET.get("sub_division")
#     if sub_division:
#         instances = instances.filter( Q(sub_division=sub_division))
#         title = "Admissions - %s" %sub_division

#     squad_area = request.GET.get("squad_area")
#     if squad_area:
#         instances = instances.filter( Q(place=squad_area))
#         title = "Admissions - %s" %squad_area

#     institute = request.GET.get("institute")
#     if institute:
#         instances = instances.filter( Q(institute=institute))
#         title = "Admissions - %s" %institute

#     context = {
#         "instances" : instances,
#         "title" : "Admissions in Class %s" %(department),
#         "student_batch" : student_batch,
#         "class_pk" : class_pk,
#         "sub_divisions" : sub_divisions,
#         "squad_areas" : squad_areas,
#         "institutes" : institutes,
#     }
#     return render(request, 'students/print_admissions.html', context)


# @check_mode
# @login_required
# def print_admissions_by_squadarea(request):
#     instances = Admission.objects.filter(is_deleted=False,is_roll_out=False)
#     squadareas = SquadArea.objects.filter(is_deleted=False)
#     title = "Squadarea Report"

#     query = request.GET.get("q")
#     if query:
#         instances = instances.filter( (Q(name__icontains=query)) |
#             (Q(place__name__icontains=query)) |
#             (Q(phone__icontains=query)) |
#             (Q(student_batch__icontains=query)) |
#             (Q(department__icontains=query)) |
#             (Q(sub_division__icontains=query)) |
#             (Q(student_batch__icontains=query)) |
#             (Q(admn_no__icontains=query)))
#         title = "Admissions (Query : %s)" %query


#     squadarea = request.GET.get('squadarea')
#     if squadarea:
#         instances = instances.filter(squadarea__pk=squadarea)

#     gender = request.GET.get("gender")
#     if gender == "male":
#         instances = instances.filter(gender=gender)
#         title += " (Gender : Male)"
#     elif gender == "female":
#         instances = instances.filter(gender=gender)
#         title += " (Gender : Female)"

#     context = {
#         'title' : title,
#         "squadareas" : squadareas,
#         "squadarea" : squadarea,
#         'query' : query,
#         "instances" : instances,
#         'gender' : gender,

#         "is_need_select_picker" : True,
#         "is_need_popup_box" : True,
#         "is_need_custom_scroll_bar" : True,
#         "is_need_wave_effect" : True,
#         "is_need_bootstrap_growl" : True,
#         "is_need_chosen_select" : True,
#         "is_need_grid_system" : True,
#         "is_need_datetime_picker" : True,
#         "is_need_animations": True,
#     }
#     return render(request, "students/print_admissions_by_squadarea.html", context)

# @check_mode
# @login_required
# @role_required(['administrator','parent','teacher'])
# def print_admission(request,pk):
#     instance = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False,is_roll_out=False))
#     parent_instructions = ParentInstruction.objects.filter(student=instance,is_deleted=False)
#     special_notes = SpecialNote.objects.filter(student=instance,is_deleted=False)
#     fee_payments = StudentFee.objects.filter(admission=instance,paid__gt=0,fee_category__name="monthly_fee",is_deleted=False)
#     vacation_results = ExamResult.objects.filter(admission=instance,is_deleted=False,evaluation__exam__term="vacation")
#     first_term_results = ExamResult.objects.filter(admission=instance,is_deleted=False,evaluation__exam__term="first_term")
#     second_term_results = ExamResult.objects.filter(admission=instance,is_deleted=False,evaluation__exam__term="second_term")

#     context = {
#         "title" : "Admission - %s" %(instance.name),
#         'instance' : instance,
#         "parent_instructions" : parent_instructions,
#         "special_notes" : special_notes,
#         "fee_payments" : fee_payments,
#         "vacation_results" : vacation_results,
#         "first_term_results" : first_term_results,
#         "second_term_results" : second_term_results,
#     }
#     return render(request, 'students/print_admission.html', context)

@check_mode
@login_required
@role_required(['administrator','teacher','parent','division'])
def admissionn(request,pk):
    instance = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False,is_roll_out=False))
    parent_instructions = ParentInstruction.objects.filter(student=instance,is_deleted=False)
    special_notes = SpecialNote.objects.filter(student=instance,is_deleted=False)
    fee_payments = StudentFee.objects.filter(admission=instance,paid__gt=0,fee_category__name="monthly_fee",is_deleted=False)
    vacation_results = ExamResult.objects.filter(admission=instance,is_deleted=False,evaluation__exam__term="vacation")
    first_term_results = ExamResult.objects.filter(admission=instance,is_deleted=False,evaluation__exam__term="first_term")
    second_term_results = ExamResult.objects.filter(admission=instance,is_deleted=False,evaluation__exam__term="second_term")
    siblings = StudentSibling.objects.filter(student=instance)
    relatives = Relatives.objects.filter(student=instance)
    other_fee_payments = StudentFee.objects.filter(admission=instance,paid__gt=0,is_deleted=False).exclude(fee_category__name__in=["monthly_fee"])
    nears = NearbyStudents.objects.filter(student=instance)
    context = {
        "other_fee_payments" : other_fee_payments,
        "title" : "Admission - %s" %(instance.name),
        'instance' : instance,
        "parent_instructions" : parent_instructions,
        "siblings" : siblings,
        "special_notes" : special_notes,
        "relatives" : relatives,
        "nears" : nears,
        "fee_payments" : fee_payments,
        "vacation_results" : vacation_results,
        "first_term_results" : first_term_results,
        "second_term_results" : second_term_results,
    }
    return render(request, 'students/admissionn.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def admission(request,pk):
    instance = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False,is_roll_out=False))
    clubs = StudentClubMember.objects.filter(student=instance,is_deleted=False)
    features = Disability.objects.all()
    facilities = Facility.objects.all()

    context = {
        "title" : "Admission - %s" %(instance.name),
        "features" : features,
        "facilities" : facilities,
        'instance' : instance,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,

    }
    return render(request, 'academics/admission1.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def admission_full_view(request,pk):
    instance = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False,is_roll_out=False))
    batch = get_current_batch(request)
    clubs = StudentClubMember.objects.filter(student=instance,is_deleted=False)
    features = Disability.objects.all()
    facilities = Facility.objects.all()
    context = {
        "title" : "Admission - %s" %(instance.name),
        "features" : features,
        "facilities" : facilities,
        'instance' : instance,
        'clubs'    : clubs,
        'admission': True,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,

    }
    return render(request, 'academics/admission.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def admission_fee_view(request,pk):
    instance = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False,is_roll_out=False))
    batch = get_current_batch(request)
    fee_payments = StudentFee.objects.filter(is_deleted=False,admission=instance,admission__batch=batch)
    context = {
        "title" : "Fee Details - %s" %(instance.name),
        'instance' : instance,
        "fee_payments" : fee_payments,
        'fee': True,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,

    }
    return render(request, 'academics/admission.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def admission_details(request,pk):
    instance = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False,is_roll_out=False))
    features = Disability.objects.all()
    context = {
        "title" : "Admission - %s" %(instance.name),
        "features" : features,
        'instance' : instance,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,

    }
    return render(request, 'academics/admission_details.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def create_admission(request,pk,division_pk):
    url = reverse('academics:create_admission',kwargs={'pk':pk,'division_pk':division_pk}),
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    sub_division = get_object_or_404(SubDivision.objects.filter(pk=division_pk))
    department = sub_division.department
    batch = get_object_or_404(Batch.objects.filter(pk=pk))
    if request.method == "POST":

        form = AdmissionForm(request.POST,request.FILES)
        print(form.errors)

        if form.is_valid():
            #create admission
            admsn_no = form.cleaned_data['admn_no']
            regstr_no = form.cleaned_data['register_no']
            if not admsn_no == None and not regstr_no == None :
                discount = form.cleaned_data['discount']
                data = form.save(commit=False)
                auto_id = get_auto_id(Admission)
                data.auto_id = auto_id
                data.creator = request.user
                data.updater = request.user
                data.college = current_college
                data.batch = current_batch
                data.sub_division = sub_division
                data.department = sub_division.department
                data.a_id = get_a_id(Admission,request)
                data.save()

                computer_fee_amount = sub_division.computer_fee
                library_fee_amount = sub_division.library_fee
                admission_fee_amount = sub_division.admission_fee
                special_fee_amount = sub_division.special_fee
                diary_fee_amount = sub_division.diary_fee
                actual_fee = sub_division.fee
                monthly_fee_amount = actual_fee - discount

                total_fee = computer_fee_amount + admission_fee_amount + library_fee_amount + diary_fee_amount + (12 * monthly_fee_amount)
                data.balance = total_fee
                data.save()

                diary_fee_category = FeeCategory.objects.filter(name="diary_fee",is_deleted=False)[0]
                admission_fee_category = FeeCategory.objects.filter(name="admission_fee",is_deleted=False)[0]
                special_fee_category = FeeCategory.objects.filter(name="special_fee",is_deleted=False)[0]
                monthly_fee_category = FeeCategory.objects.filter(name="monthly_fee",is_deleted=False)[0]
                computer_fee_category = FeeCategory.objects.filter(name="computer_fee",is_deleted=False)[0]
                library_fee_category = FeeCategory.objects.filter(name="library_fee",is_deleted=False)[0]

                update_student_fee(request,data,computer_fee_category,computer_fee_amount)
                update_student_fee(request,data,library_fee_category,library_fee_amount)
                update_student_fee(request,data,admission_fee_category,admission_fee_amount)
                update_student_fee(request,data,diary_fee_category,diary_fee_amount)
                update_student_fee(request,data,monthly_fee_category,monthly_fee_amount,month="april")
                update_student_fee(request,data,monthly_fee_category,monthly_fee_amount,month="may")
                update_student_fee(request,data,monthly_fee_category,monthly_fee_amount,month="june")
                update_student_fee(request,data,monthly_fee_category,monthly_fee_amount,month="july")
                update_student_fee(request,data,monthly_fee_category,monthly_fee_amount,month="august")
                update_student_fee(request,data,monthly_fee_category,monthly_fee_amount,month="september")
                update_student_fee(request,data,monthly_fee_category,monthly_fee_amount,month="october")
                update_student_fee(request,data,monthly_fee_category,monthly_fee_amount,month="november")
                update_student_fee(request,data,monthly_fee_category,monthly_fee_amount,month="december")
                update_student_fee(request,data,monthly_fee_category,monthly_fee_amount,month="january")
                update_student_fee(request,data,monthly_fee_category,monthly_fee_amount,month="february")
                update_student_fee(request,data,monthly_fee_category,monthly_fee_amount,month="march")

                update_student_fee(request,data,special_fee_category,special_fee_amount,month="april")
                update_student_fee(request,data,special_fee_category,special_fee_amount,month="may")
                update_student_fee(request,data,special_fee_category,special_fee_amount,month="june")
                update_student_fee(request,data,special_fee_category,special_fee_amount,month="july")
                update_student_fee(request,data,special_fee_category,special_fee_amount,month="august")
                update_student_fee(request,data,special_fee_category,special_fee_amount,month="september")
                update_student_fee(request,data,special_fee_category,special_fee_amount,month="october")
                update_student_fee(request,data,special_fee_category,special_fee_amount,month="november")
                update_student_fee(request,data,special_fee_category,special_fee_amount,month="december")
                update_student_fee(request,data,special_fee_category,special_fee_amount,month="january")
                update_student_fee(request,data,special_fee_category,special_fee_amount,month="february")
                update_student_fee(request,data,special_fee_category,special_fee_amount,month="march")

                features = request.POST.getlist('feature')
                for feature in features:
                    if Disability.objects.filter(is_deleted=False,id=feature).exists():
                        disability = Disability.objects.get(id=feature,is_deleted=False)
                        data.disabilities.add(disability)


                facilities = request.POST.getlist('facility')
                for facility in facilities:
                    if Facility.objects.filter(is_deleted=False,id=facility).exists():
                        facility = Facility.objects.get(id=facility,is_deleted=False)
                        data.facilities.add(facility)

                return HttpResponseRedirect(reverse('academics:admission',kwargs={'pk': data.pk}))

            else:
                title = str(_("Create Admissions"))
                form = AdmissionForm(request.POST)
                features = Disability.objects.filter(is_deleted=False)
                facilities = Facility.objects.filter(is_deleted=False)

                context = {
                    "title" : "Create Admission",
                    "form" : form,
                    "features" : features,
                    "facilities" : facilities,
                    "redirect": "true",

                    "is_need_select_picker" : True,
                    "is_need_popup_box" : True,
                    "is_need_custom_scroll_bar" : True,
                    "is_need_wave_effect" : True,
                    "is_need_bootstrap_growl" : True,
                    "is_need_chosen_select" : True,
                    "is_need_grid_system" : True,
                    "is_need_datetime_picker" : True,
                    "is_need_animations": True,
                }
            return render(request, 'academics/entry_admissions.html', context)
        else:

            message = generate_form_errors(form, formset=False)
            title = str(_("Create Admissions"))
            form = AdmissionForm(request.POST)
            features = Disability.objects.filter(is_deleted=False)
            facilities = Facility.objects.filter(is_deleted=False)


            context = {
            "title" : "Create Admission",
            "form" : form,
            "features" : features,
            "facilities" : facilities,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'academics/entry_admissions.html', context)

    else:
        form = AdmissionForm()
        features = Disability.objects.filter(is_deleted=False)
        facilities = Facility.objects.filter(is_deleted=False)

        context = {
            "title" : "Create Admission",
            "form" : form,
            "features" : features,
            "facilities" : facilities,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'academics/entry_admissions.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_admission(request,pk):
    instance = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False))
    url = reverse('academics:edit_admission',kwargs={'pk':pk})

    if request.method == "POST":
        form = AdmissionForm(request.POST,request.FILES,instance=instance)
        if form.is_valid():
            admsn_no = form.cleaned_data['admn_no']
            regstr_no = form.cleaned_data['register_no']

            if not admsn_no == None and not regstr_no == None :
                #update student division
                data = form.save(commit=False)
                data.updater = request.user
                data.date_updated = datetime.datetime.now()
                data.save()

                data.disabilities.clear()
                features = request.POST.getlist('feature')
                for feature in features:
                    if Disability.objects.filter(is_deleted=False,id=feature).exists():
                        disability = Disability.objects.get(id=feature,is_deleted=False)
                        data.disabilities.add(disability)

                data.facilities.clear()
                facilities = request.POST.getlist('fac')
                for fac in facilities:
                    if Facility.objects.filter(is_deleted=False,id=fac).exists():
                        facility = Facility.objects.get(id=fac,is_deleted=False)
                        data.facilities.add(facility)

                return HttpResponseRedirect(reverse('academics:admission',kwargs={'pk': data.pk}))
            else:
                message = generate_form_errors(form, formset=False)
                title = str(_("Edit Admissions"))
                form = AdmissionForm(request.POST)
                features = Disability.objects.all()
                facilities = Facility.objects.all()

                context = {
                "title" : "Edit Admission",
                "form" : form,
                "features" : features,
                "facilities" : facilities,
                "redirect": "true",

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
                "is_need_animations": True,
            }
            return render(request, 'academics/entry_admission.html', context)
        else:
            form = AdmissionForm(instance=instance)
            features = Disability.objects.all()
            facilities = Facility.objects.all()

            context = {
                "title" : "Edit Admission",
                "form" : form,
                "features" :features,
                "facilities" : facilities,
                "instance" : instance,
                "redirect": "true",
                "url" : url,

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
                "is_need_animations": True,
            }
            return render(request, 'academics/entry_admissions.html', context)

    else:
        form = AdmissionForm(instance=instance)
        features = Disability.objects.all()
        facilities = Facility.objects.all()

        context = {
            "title" : "Edit Admission",
            "form" : form,
            "features" :features,
            "facilities" : facilities,
            "instance" : instance,
            "redirect": "true",
            "url" : url,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'academics/entry_admissions.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_admission(request,pk):
    Admission.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Student admission successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('academics:admissions')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def print_admission(request,pk):
    instance = get_object_or_404(Admission.objects.filter(is_deleted=False,pk=pk))
    today = datetime.date.today()

    context = {
        'title' : instance,
        'instance' : instance,
        "today"  : today,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'academics/print_admission.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def roll_out_admission(request,pk):
    instance = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False,is_roll_out=False))
    if StudentFee.objects.filter(admission=instance,balance__gt=0).exists():
        StudentFee.objects.filter(admission=instance,balance__gt=0).update(is_deleted=True)

    User.objects.filter(pk=instance.user.pk).update(is_active=False,
                                                    username=instance.user.username + "_deleted" + str(instance.user.id) ,
                                                    email=instance.user.email + "_deleted")

    Admission.objects.filter(pk=pk).update(is_roll_out=True,user=None)
    response_data = {
        "status": "true",
        "title": "Successfully Rollout",
        "message": "Student admission rollout successfully.",
        "redirect": "true",
        "redirect_url": reverse('academics:admissions')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')



@check_mode
@login_required
def create_disability(request):
    if request.method == "POST":
        response_data = {}
        form = DisabilityForm(data=request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            data.save()

            response_data = {
                'status' : 'true',
                'title' : str(_("Successfully Created")),
                'redirect' : 'true',
                'redirect_url' : reverse('academics:disability',kwargs={'pk':data.pk}),
                'message' : str(_("Disability Successfully Created"))
            }
        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : _("Form validation error"),
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Create Disability"))
        form = DisabilityForm()
        context = {
            "form" : form,
            "title" : title,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request, 'academics/entry_disability.html', context)


@check_mode
@login_required
def edit_disability(request,pk):
    instance = get_object_or_404(Disability.objects.filter(pk=pk))
    if request.method == "POST":
        response_data = {}
        form = DisabilityForm(data=request.POST,instance=instance)
        if form.is_valid():
            data = form.save(commit=False)
            data.save()

            response_data = {
                'status' : 'true',
                'title' : str(_("Successfully Updated")),
                'redirect' : 'true',
                'redirect_url' : reverse('academics:disability',kwargs={'pk':data.pk}),
                'message' : str(_("Disability Successfully Updated"))
            }
        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : str(_("Form validation error")),
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Edit Disability"))
        form = DisabilityForm(instance=instance)
        context = {
            "form" : form,
            "title" : title,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request, 'academics/entry_disability.html', context)


@check_mode
@login_required
def disabilities(request):
    instances = Disability.objects.filter(is_deleted=False)
    query = request.GET.get("q")
    if query:
        instances = instances.filter(
                        Q(name__icontains=query)
                    )
        title = "Grade (Query : %s)" %query

    context = {
        'title' : str(_("Disabilities")),
        'instances' : instances,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'academics/disabilities.html',context)


@check_mode
@login_required
def disability(request,pk):
    instance = get_object_or_404(Disability.objects.filter(is_deleted=False,pk=pk))

    context = {
        'title' : instance.name,
        'instance' : instance,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'academics/disability.html',context)


@check_mode
@login_required
def delete_disability(request,pk):
    instance = get_object_or_404(Disability.objects.filter(pk=pk))
    instance.is_deleted = True
    instance.save()

    response_data = {
        "status" : "true",
        "title" : str(_("Successfully deleted")),
        "message" : str(_("Disability deleted successfully")),
        "redirect" : "true",
        "redirect_url" : reverse('academics:disabilities')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_facility(request):
    if request.method == "POST":
        response_data = {}
        form = FacilityForm(data=request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            data.save()

            response_data = {
                'status' : 'true',
                'title' : str(_("Successfully Created")),
                'redirect' : 'true',
                'redirect_url' : reverse('academics:facility',kwargs={'pk':data.pk}),
                'message' : str(_("Facility Successfully Created"))
            }
        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : _("Form validation error"),
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Create Facility"))
        form = FacilityForm()
        context = {
            "form" : form,
            "title" : title,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request, 'academics/entry_facility.html', context)


@check_mode
@login_required
def edit_facility(request,pk):
    instance = get_object_or_404(Facility.objects.filter(pk=pk))
    if request.method == "POST":
        response_data = {}
        form = FacilityForm(data=request.POST,instance=instance)
        if form.is_valid():
            data = form.save(commit=False)
            data.save()

            response_data = {
                'status' : 'true',
                'title' : str(_("Successfully Updated")),
                'redirect' : 'true',
                'redirect_url' : reverse('academics:facility',kwargs={'pk':data.pk}),
                'message' : str(_("Facility Successfully Updated"))
            }
        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : str(_("Form validation error")),
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Edit Facility"))
        form = FacilityForm(instance=instance)
        context = {
            "form" : form,
            "title" : title,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request, 'academics/entry_facility.html', context)


@check_mode
@login_required
def facilities(request):
    instances = Facility.objects.filter(is_deleted=False)
    query = request.GET.get("q")
    if query:
        instances = instances.filter(
                        Q(name__icontains=query)
                    )
        title = "Grade (Query : %s)" %query

    context = {
        'title' : str(_("Facilities")),
        'instances' : instances,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'academics/facilities.html',context)


@check_mode
@login_required
def facility(request,pk):
    instance = get_object_or_404(Facility.objects.filter(is_deleted=False,pk=pk))

    context = {
        'title' : instance.name,
        'instance' : instance,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'academics/facility.html',context)


@check_mode
@login_required
def delete_facility(request,pk):
    instance = get_object_or_404(Facility.objects.filter(pk=pk))
    instance.is_deleted = True
    instance.save()

    response_data = {
        "status" : "true",
        "title" : str(_("Successfully deleted")),
        "message" : str(_("Facility deleted successfully")),
        "redirect" : "true",
        "redirect_url" : reverse('academics:facilities')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def upload_students_by_class(request,pk):
    instance = get_object_or_404(Department.objects.filter(pk=pk))
    current_college = get_current_college(request)
    current_role = get_current_role(request)
    if request.method == 'POST':
        form = FileForm(request.POST,request.FILES)

        if form.is_valid():

            input_excel = request.FILES['file']
            book = xlrd.open_workbook(file_contents=input_excel.read())
            sheet = book.sheet_by_index(0)

            dict_list = []
            keys = [str(sheet.cell(0, col_index).value) for col_index in range(sheet.ncols)]
            for row_index in range(1, sheet.nrows):
                d = {keys[col_index]: str(sheet.cell(row_index, col_index).value)
                    for col_index in range(sheet.ncols)}
                dict_list.append(d)

            is_ok = True
            message = ''
            row_count = 2

            sheet_counter = 1

            for item in dict_list:
                name = item['name']
                admn_no = item['admn_no']
                father_name = item['father_name']
                guardian_name = item['guardian_name']
                father_occupation = item['father_occupation']
                phone = item['phone']
                caste = item['caste']
                category = item['category']
                mode_of_transport = item['mode_of_transport']
                religion = item['religion']
                mother_name = item['mother_name']
                admn_no = item['admn_no']
                mother_occupation = item['mother_occupation']
                mother_phone = item['mother_phone']
                address = item['address']
                date_joined = item['date_joined']
                gender = item['gender']

                try:
                    phone = str(phone.split('.')[0])
                except:
                    phone = str(phone)

                try:
                    admn_no = str(admn_no.split('.')[0])
                except:
                    admn_no = str(admn_no)

                try:
                    mother_phone = str(mother_phone.split('.')[0])
                except:
                    mother_phone = str(mother_phone)

                try:
                    date_joined_object = datetime.strptime(date_joined, '%d.%m.%Y')
                except:
                    date_joined_object = None

                chars = "abcdefghijklmnopqrstuvwxyz0123456789"
                password = ''
                for c in range(8):
                    password += random.choice(chars)

                today = datetime.date.today()
                student_batch = get_current_batch(request)
                admission_fee_amount = instance.admission_fee
                diary_fee_amount = instance.diary_fee
                monthly_fee_amount = instance.fee
                computer_fee_amount = instance.computer_fee
                library_fee_amount = instance.library_fee
                actual_fee = instance.fee
                discount = 0
                fee = (actual_fee - discount) * 12 + admission_fee_amount + diary_fee_amount + computer_fee_amount + library_fee_amount
                if not Admission.objects.filter(name=name,father_name=father_name,mother_name=mother_name,department=instance,is_deleted=False,is_roll_out=False).exists():
                    admission = Admission(
                            auto_id = get_auto_id(Admission),
                            a_id = get_a_id(Admission,request),
                            admn_no = admn_no,
                            creator = request.user,
                            updater = request.user,
                            name = name,
                            college = current_college,
                            father_name = father_name,
                            guardian_name = guardian_name,
                            father_occupation = father_occupation,
                            phone = phone,
                            mother_name = mother_name,
                            mother_occupation = mother_occupation,
                            mother_phone = mother_phone,
                            address = address,
                            caste = caste,
                            mode_of_transport = mode_of_transport,
                            category = category,
                            religion = religion,
                            batch = student_batch,
                            sub_division = instance,
                            department = instance.department,
                            fee = fee,
                            gender = gender,
                            actual_fee = actual_fee,
                            balance = fee,
                        )
                    admission.save()

                    if date_joined_object:
                        admission.date_joined = date_joined_object
                        admission.save()


                    admission_fee_category = FeeCategory.objects.filter(name="admission_fee",is_deleted=False)[0]
                    diary_fee_category = FeeCategory.objects.filter(name="diary_fee",is_deleted=False)[0]
                    monthly_fee_category = FeeCategory.objects.filter(name="monthly_fee",is_deleted=False)[0]
                    computer_fee_category = FeeCategory.objects.filter(name="computer_fee",is_deleted=False)[0]
                    library_fee_category = FeeCategory.objects.filter(name="library_fee",is_deleted=False)[0]

                    update_student_fee(request,admission,computer_fee_category,computer_fee_amount)
                    update_student_fee(request,admission,library_fee_category,library_fee_amount)
                    update_student_fee(request,admission,admission_fee_category,admission_fee_amount)
                    update_student_fee(request,admission,diary_fee_category,diary_fee_amount)
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="april")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="may")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="june")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="july")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="august")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="september")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="october")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="november")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="december")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="january")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="february")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="march")

                else:
                    admission = Admission.objects.get(name=name,father_name=father_name,mother_name=mother_name,department=instance,is_deleted=False,is_roll_out=False)
                    username = "%s%s" %(admission.admn_no,admission.name[:3])

                sheet_counter += 1

            redirect_url = reverse('academics:admissions')

            return HttpResponseRedirect(redirect_url)
        else:
            form = FileForm()

            context = {
                "form" : form,
                "title" : "Upload Contacts",

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
            }
            return render(request, 'academics/upload_file.html', context)
    else:
        form = FileForm()

        context = {
            "form" : form,
            "title" : "Upload Students in %s" %(instance.name),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'academics/upload_file.html', context)



def upload_students(request,pk):
    instance = get_object_or_404(SubDivision.objects.filter(pk=pk))
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    current_role = get_current_role(request)
    if request.method == 'POST':
        form = FileForm(request.POST,request.FILES)

        if form.is_valid():

            input_excel = request.FILES['file']
            book = xlrd.open_workbook(file_contents=input_excel.read())
            sheet = book.sheet_by_index(0)

            dict_list = []
            keys = [str(sheet.cell(0, col_index).value) for col_index in range(sheet.ncols)]
            for row_index in range(1, sheet.nrows):
                d = {keys[col_index]: str(sheet.cell(row_index, col_index).value)
                    for col_index in range(sheet.ncols)}
                dict_list.append(d)

            is_ok = True
            message = ''
            row_count = 2

            sheet_counter = 1

            for item in dict_list:
                name = item['name']
                father_name = item['father_name']
                guardian_name = item['guardian_name']
                father_occupation = item['father_occupation']
                phone = item['phone']
                caste = item['caste']
                category = item['category']
                mode_of_transport = item['mode_of_transport']
                religion = item['religion']
                mother_name = item['mother_name']
                mother_occupation = item['mother_occupation']
                mother_phone = item['mother_phone']
                address = item['address']
                date_joined = item['date_joined']
                gender = item['gender']

                try:
                    phone = str(phone.split('.')[0])
                except:
                    phone = str(phone)

                try:
                    mother_phone = str(mother_phone.split('.')[0])
                except:
                    mother_phone = str(mother_phone)

                try:
                    date_joined_object = datetime.strptime(date_joined, '%d.%m.%Y')
                except:
                    date_joined_object = None


                chars = "abcdefghijklmnopqrstuvwxyz0123456789"
                password = ''
                for c in range(8):
                    password += random.choice(chars)

                today = datetime.date.today()
                student_batch = get_current_batch(request)
                admission_fee_amount = instance.department.admission_fee
                diary_fee_amount = instance.department.diary_fee
                monthly_fee_amount = instance.department.fee
                computer_fee_amount = instance.department.computer_fee
                library_fee_amount = instance.department.library_fee
                special_fee_amount = instance.department.special_fee
                actual_fee = instance.department.fee
                discount = 0
                fee = (actual_fee - discount) * 12 + admission_fee_amount + diary_fee_amount + computer_fee_amount + library_fee_amount + (special_fee_amount * 12)
                if not Admission.objects.filter(name=name,father_name=father_name,mother_name=mother_name,sub_division=instance,is_deleted=False,is_roll_out=False).exists():
                    admission = Admission(
                            auto_id = get_auto_id(Admission),
                            a_id = get_a_id(Admission,request),
                            creator = request.user,
                            updater = request.user,
                            name = name,
                            college = current_college,
                            father_name = father_name,
                            guardian_name = guardian_name,
                            father_occupation = father_occupation,
                            phone = phone,
                            mother_name = mother_name,
                            mother_occupation = mother_occupation,
                            mother_phone = mother_phone,
                            address = address,
                            caste = caste,
                            mode_of_transport = mode_of_transport,
                            category = category,
                            religion = religion,
                            batch = student_batch,
                            sub_division = instance,
                            department = instance.department,
                            fee = fee,
                            gender = gender,
                            actual_fee = actual_fee,
                            balance = fee,
                        )
                    admission.save()


                    if date_joined_object:
                        admission.date_joined = date_joined_object
                        admission.save()

                    admission_fee_category = FeeCategory.objects.filter(name="admission_fee",is_deleted=False)[0]
                    diary_fee_category = FeeCategory.objects.filter(name="diary_fee",is_deleted=False)[0]
                    monthly_fee_category = FeeCategory.objects.filter(name="monthly_fee",is_deleted=False)[0]
                    special_fee_category = FeeCategory.objects.filter(name="special_fee",is_deleted=False)[0]
                    computer_fee_category = FeeCategory.objects.filter(name="computer_fee",is_deleted=False)[0]
                    library_fee_category = FeeCategory.objects.filter(name="library_fee",is_deleted=False)[0]

                    update_student_fee(request,admission,computer_fee_category,computer_fee_amount)
                    update_student_fee(request,admission,library_fee_category,library_fee_amount)
                    update_student_fee(request,admission,admission_fee_category,admission_fee_amount)
                    update_student_fee(request,admission,diary_fee_category,diary_fee_amount)
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="june")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="july")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="august")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="september")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="october")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="november")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="december")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="january")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="february")
                    update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="march")


                    update_student_fee(request,admission,special_fee_category,special_fee_amount,month="june")
                    update_student_fee(request,admission,special_fee_category,special_fee_amount,month="july")
                    update_student_fee(request,admission,special_fee_category,special_fee_amount,month="august")
                    update_student_fee(request,admission,special_fee_category,special_fee_amount,month="september")
                    update_student_fee(request,admission,special_fee_category,special_fee_amount,month="october")
                    update_student_fee(request,admission,special_fee_category,special_fee_amount,month="november")
                    update_student_fee(request,admission,special_fee_category,special_fee_amount,month="december")
                    update_student_fee(request,admission,special_fee_category,special_fee_amount,month="january")
                    update_student_fee(request,admission,special_fee_category,special_fee_amount,month="february")
                    update_student_fee(request,admission,special_fee_category,special_fee_amount,month="march")

                else:
                    admission = Admission.objects.get(name=name,father_name=father_name,mother_name=mother_name,sub_division=instance,is_deleted=False,is_roll_out=False)
                    username = "%s%s" %(admission.admn_no,admission.name[:3])

                sheet_counter += 1

            redirect_url = reverse('academics:admissions')

            return HttpResponseRedirect(redirect_url)
        else:
            form = FileForm()

            context = {
                "form" : form,
                "title" : "Upload Contacts",

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
            }
            return render(request, 'academics/upload_file.html', context)
    else:
        form = FileForm()

        context = {
            "form" : form,
            "title" : "Upload Students in %s" %(instance.name),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'academics/upload_file.html', context)



@check_mode
@login_required
def list_class_and_division_for_admissions(request):
    today = datetime.date.today()
    batch = get_current_batch(request)

    batch_pk = request.GET.get('batch')
    if batch_pk:
        if Batch.objects.filter(pk=batch_pk).exists():
            batch = Batch.objects.get(pk=batch_pk)

    sub_divisions = SubDivision.objects.filter(is_deleted=False)
    departments = Department.objects.filter(is_deleted=False)
    if batch:
        student_batches = Batch.objects.filter(is_deleted=False).exclude(pk=batch.pk)
    else:
        student_batches = Batch.objects.filter(is_deleted=False)

    context = {
        "title" : "Class and Divisions in %s" %batch,
        "current_batch" : batch,
        "student_batches" : student_batches,
        "sub_divisions" : sub_divisions,
        "departments" : departments,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
}
    return render(request, 'academics/list_class_and division_for_admissions.html', context)


def get_divisions(request):
    pk = request.GET.get('pk')
    instances = SubDivision.objects.filter(department__pk=pk)

    json_models = serializers.serialize("json", instances)
    return HttpResponse(json_models, content_type="application/javascript")


@check_mode
@login_required
@role_required(['administrator'])
def student_batches(request):
    instances = Batch.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(name__icontains=query))
        title = "Batches - %s" %query

    context = {
        "instances" : instances,
        "title" : "Student batches"
    }
    return render(request, 'academics/student_batches.html', context)


@check_mode
@login_required
def create_class_and_division_for_admissions(request):
    current_college = get_current_college(request)
    today = datetime.date.today()
    batch = get_current_batch(request)

    batch_pk = request.GET.get('batch')
    if batch_pk:
        if Batch.objects.filter(pk=batch_pk,college=current_college).exists():
            batch = Batch.objects.get(pk=batch_pk,college=current_college)

    sub_divisions = SubDivision.objects.filter(is_deleted=False,college=current_college)
    departments = Department.objects.filter(is_deleted=False)
    if batch:
        student_batches = Batch.objects.filter(is_deleted=False,college=current_college).exclude(pk=batch.pk)
    else:
        student_batches = Batch.objects.filter(is_deleted=False,college=current_college)

    context = {
        "title" : "Class and Divisions in %s" %batch,
        "current_batch" : batch,
        "student_batches" : student_batches,
        "sub_divisions" : sub_divisions,
        "departments" : departments,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request, 'academics/create_class_and division_for_admissions.html', context)


@check_mode
@login_required
def create_attendance(request,pk):
    college = get_current_college(request)
    sub_division = get_object_or_404(SubDivision.objects.filter(pk=pk,is_deleted=False))
    today = datetime.date.today()
    last_day = today - datetime.timedelta(days=1)
    second_last_day = today - datetime.timedelta(days=2)

    student_batch = get_current_batch(request)

    admissions = Admission.objects.filter(is_deleted=False,batch=student_batch,sub_division=sub_division,department=sub_division.department)

    AttendanceMaleFormset = formset_factory(AttendanceMaleForm,extra=0)
    AttendanceFemaleFormset = formset_factory(AttendanceFemaleForm,extra=0)

    if request.method == 'POST':

        attendance_male_formset = AttendanceMaleFormset(request.POST,prefix='attendance_male_formset')
        attendance_female_formset = AttendanceFemaleFormset(request.POST,prefix='attendance_female_formset')
        attendance_date_form = AttendanceDateForm(request.POST)

        if attendance_male_formset.is_valid() and attendance_date_form.is_valid() and attendance_female_formset.is_valid():
            weekday = attendance_date_form.cleaned_data['weekday']
            date = attendance_date_form.cleaned_data['date']

            for attendance in attendance_male_formset:
                reason = ""
                is_genuene = False
                is_present = attendance.cleaned_data['is_present']

                behaviour = attendance.cleaned_data['behaviour']
                if not behaviour:
                    behaviour = None

                if not is_present:
                    reason = attendance.cleaned_data['reason']
                    is_genuene = attendance.cleaned_data['is_genuene']

                admission = attendance.cleaned_data['admission']
                auto_id = get_auto_id(Attendance)
                a_id = get_a_id(Attendance,request)

                absent_days_count = 0
                if not is_present:
                    absent_days_count = 1
                    last_day_attendance = Attendance.objects.filter(date=last_day,sub_division=sub_division,is_deleted=False,admission=admission)
                    if last_day_attendance:
                        if last_day_attendance[0].is_present == False:
                            previous_absent_days_count = last_day_attendance[0].absent_days_count
                            absent_days_count = previous_absent_days_count + 1

                if not Attendance.objects.filter(weekday=weekday,date=date,sub_division=sub_division,admission=admission,is_deleted=False).exists():
                    Attendance(
                        weekday = weekday,
                        date = date,
                        admission = admission,
                        is_present = is_present,
                        absent_days_count = absent_days_count,
                        is_genuene = is_genuene,
                        sub_division = sub_division,
                        department = sub_division.department,
                        behaviour = behaviour,
                        reason = reason,
                        auto_id = auto_id,
                        a_id = a_id,
                        creator = request.user,
                        updater = request.user,
                        college = college
                    ).save()

                elif not Attendance.objects.filter(weekday=weekday,date=date,sub_division=sub_division,admission=admission,is_present=is_present).exists():
                    attendance = Attendance.objects.get(weekday=weekday,date=date,admission=admission,sub_division=sub_division)
                    attendance.is_present=is_present
                    attendance.save()

                if is_present == False and is_genuene == False:

                    notification_subject = NotificationSubject.objects.get(code="absent")
                    Notification(
                        subject = notification_subject,
                        user = request.user,
                        admission = admission,
                        description = "Absent Today.",
                        division = sub_division,
                    ).save()

                    if absent_days_count >= 3:
                        Notification(
                            subject = notification_subject,
                            user = request.user,
                            admission = admission,
                            description = "Absent more than three days.",
                            division = sub_division,
                        ).save()

            for attendance in attendance_female_formset:
                is_present = attendance.cleaned_data['is_present']
                admission = attendance.cleaned_data['admission']
                reason = ""

                behaviour = attendance.cleaned_data['behaviour']
                if not behaviour:
                    behaviour = None

                is_genuene = False
                if not is_present:
                    reason = attendance.cleaned_data['reason']
                    is_genuene = attendance.cleaned_data['is_genuene']
                auto_id = get_auto_id(Attendance)
                a_id = get_a_id(Attendance,request)

                absent_days_count = 0
                if not is_present:
                    absent_days_count = 1
                    last_day_attendance = Attendance.objects.filter(date=last_day,sub_division=sub_division,is_deleted=False,admission=admission)
                    if last_day_attendance:
                        if last_day_attendance[0].is_present == False:
                            previous_absent_days_count = last_day_attendance[0].absent_days_count
                            absent_days_count = previous_absent_days_count + 1

                if not Attendance.objects.filter(weekday=weekday,date=date,sub_division=sub_division,admission=admission,is_deleted=False).exists():
                    Attendance(
                        weekday = weekday,
                        date = date,
                        admission = admission,
                        is_present = is_present,
                        is_genuene = is_genuene,
                        sub_division = sub_division,
                        absent_days_count = absent_days_count,
                        department = sub_division.department,
                        reason = reason,
                        behaviour = behaviour,
                        auto_id = auto_id,
                        a_id = a_id,
                        creator = request.user,
                        updater = request.user,
                        college = college
                    ).save()

                elif not Attendance.objects.filter(weekday=weekday,date=date,sub_division=sub_division,admission=admission,is_present=is_present).exists():
                    attendance = Attendance.objects.get(weekday=weekday,date=date,admission=admission,sub_division=sub_division)
                    attendance.is_present=is_present
                    attendance.save()

                if is_present == False and is_genuene == False:
                    Notification(
                        subject = notification_subject,
                        user = request.user,
                        admission = admission,
                        description = "Absent Today.",
                        division = sub_division,
                    ).save()

                    if absent_days_count >= 3:
                        Notification(
                            subject = notification_subject,
                            user = request.user,
                            admission = admission,
                            description = "Absent more than three days.",
                            division = sub_division,
                        ).save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Attendance created successfully.",
                "redirect" : "true",
                "redirect_url": reverse('academics:attendances' , kwargs={'pk':pk})
            }

        else:
            message = generate_form_errors(attendance_female_formset,formset=True)
            message = generate_form_errors(attendance_male_formset,formset=True)
            message = generate_form_errors(attendance_date_form,formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        male_initial = []
        female_initial = []
        for admission in admissions:
            today_attendance = None
            is_present = ""
            is_genuene = ""
            reason = ""
            behaviour = None
            if Attendance.objects.filter(date=today,admission=admission).exists():
                today_attendance = Attendance.objects.filter(date=today,admission=admission)[0]
                is_present = today_attendance.is_present
                is_genuene = today_attendance.is_genuene
                reason = today_attendance.reason
                behaviour = today_attendance.behaviour
            if admission.gender == "male":
                attendance_male_dict = {
                    'admission' : admission,
                    'admission_name' : admission.name,
                    'is_present' : is_present,
                    'is_genuene' : is_genuene,
                    "behaviour" : behaviour,
                    'reason' : reason,
                }
                male_initial.append(attendance_male_dict)
            elif admission.gender == "female":
                attendance_female_dict = {
                    'admission' : admission,
                    'admission_name' : admission.name,
                    'is_present' : is_present,
                    "behaviour" : behaviour,
                    'is_genuene' : is_genuene,
                    'reason' : reason,
                }
                female_initial.append(attendance_female_dict)
        weekday_no = today.weekday()
        if weekday_no == 0:
            weekday = "monday"
        elif weekday_no == 1:
            weekday = "tuesday"
        elif weekday_no == 2:
            weekday = "wednesday"
        elif weekday_no == 3:
            weekday = "thursday"
        elif weekday_no == 4:
            weekday = "friday"
        elif weekday_no == 5:
            weekday = "saturday_forenoon"
        elif weekday_no == 6:
            weekday = "sunday_forenoon"

        attendance_male_formset = AttendanceMaleFormset(prefix='attendance_male_formset',initial=male_initial)
        attendance_female_formset = AttendanceFemaleFormset(prefix='attendance_female_formset',initial=female_initial)
        attendance_date_form = AttendanceDateForm(initial={"date":today,"weekday":weekday})
        context = {
            "title" : "Create Attendance",
            "attendance_male_formset" : attendance_male_formset,
            "attendance_female_formset" : attendance_female_formset,
            "attendance_date_form" : attendance_date_form,
            "url" : reverse('academics:create_attendance',kwargs={'pk':pk}),
            "today" : today,
            "admissions" : admissions,
            "sub_division" : sub_division,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request,'academics/entry_attendance.html',context)


@check_mode
@login_required
def attendances(request,pk):
    batch = get_current_batch(request)
    current_role = get_current_role(request)
    sub_division = get_object_or_404(SubDivision.objects.filter(pk=pk,is_deleted=False))
    instance = get_object_or_404(SubDivision.objects.filter(pk=pk,is_deleted=False))
    department = sub_division.department
    today = datetime.date.today()
    date_string = today
    instances_today = Attendance.objects.filter(is_deleted=False,sub_division=sub_division,date=today)

    title = "Attendances %s - %s " %(sub_division,today)
    month = today.strftime('%m')
    year = today.strftime('%Y')
    instances = Attendance.objects.none()
    morethan_three_absent_students = []
    behaviour_students = []
    morethan_three_absent_students_count = 0
    behaviour_students_count = 0
    date_error = False
    date = request.GET.get("date")
    # tasks = []

    # if Task.objects.filter(is_completed=False,batch=batch,sub_division=sub_division).exists():
    #     task_list = Task.objects.filter(is_completed=False,batch=batch,sub_division=sub_division)
    #     for task in task_list:
    #         tasks.append(task)
    # if Task.objects.filter(is_completed=False,batch=batch,sub_division=None,department=department).exists():
    #     task_list = Task.objects.filter(is_completed=False,batch=batch,sub_division=None,department=department)
    #     for task in task_list:
    #         tasks.append(task)
    # if Task.objects.filter(is_completed=False,batch=batch,sub_division=None,department=None,is_general=True).exists():
    #     task_list = Task.objects.filter(is_completed=False,batch=batch,sub_division=None,department=None,is_general=True)
    #     for task in task_list:
    #         tasks.append(task)
    # print tasks
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
            title = "Attendances %s - %s" %(sub_division,date)
            date_string = date
        except ValueError:
            date_error = True
    if not date_error and date:
        instances_today = []
        instances = Attendance.objects.filter(is_deleted=False,sub_division=sub_division,date=date_string)

    present_students = Attendance.objects.filter(is_deleted=False,sub_division=sub_division,is_present=True,date=date_string).count()
    absent_students = Attendance.objects.filter(is_deleted=False,sub_division=sub_division,is_present=False,date=date_string).count()
    genuene_absent_students = Attendance.objects.filter(is_deleted=False,sub_division=sub_division,is_present=False,date=date_string,is_genuene=True).count()
    unauthorized_absent_students = Attendance.objects.filter(is_deleted=False,sub_division=sub_division,is_present=False,date=date_string,is_genuene=False).count()
    behaviour_students = Attendance.objects.filter(is_deleted=False,sub_division=sub_division,is_present=True,date=date_string).exclude(behaviour=None)
    behaviour_students_count = behaviour_students.count()

    administrator = None
    if current_role == "administrator":
        if Attendance.objects.filter(is_deleted=False,sub_division=sub_division,is_present=False,date=date_string,is_genuene=False,absent_days_count__gte=3).exists():
            morethan_three_absent_students = Attendance.objects.filter(is_deleted=False,sub_division=sub_division,is_present=False,date=date_string,is_genuene=False,absent_days_count__gte=3)
            morethan_three_absent_students_count = morethan_three_absent_students.count()

    present = request.GET.get("present")
    genuene = request.GET.get("genuene")
    if present:
        instances = instances.filter(is_present=present,date=date_string)

    if present and genuene:
        instances = instances.filter(is_present=present,is_genuene=genuene,date=date_string)

    unstudents = request.GET.get("unstudents")
    if unstudents == "yes":
        instances = instances.filter(is_deleted=False,sub_division=sub_division,is_present=False,date=date_string,is_genuene=False,absent_days_count__gte=3)

    attendance_dates = instances
    query = request.GET.get("q")
    if  instances_today:
        if query:
            instances = instances_today.filter( Q(admission__name__icontains=query))
            title = "Attendances- %s" %query
    else:
        if query:
            instances = instances.filter( Q(admission__name__icontains=query))
            title = "Attendances- %s" %query

    period = request.GET.get("period")
    if period:
        instances = instances.filter(period=period)

    behaviours = request.GET.get("behaviours")
    if behaviours == "yes":
        instances = instances.filter(is_deleted=False,sub_division=sub_division,is_present=True,date=date_string).exclude(behaviour=None)


    context = {
        "instances" : instances,
        "instances_today" : instances_today,
        "title" : title,
        "date_string" : date_string,
        "present_students" :  present_students,
        "behaviour_students_count" : behaviour_students_count,
        "morethan_three_absent_students" : morethan_three_absent_students,
        "absent_students" : absent_students,
        "behaviour_students" : behaviour_students,
        "morethan_three_absent_students_count" : morethan_three_absent_students_count,
        "sub_division" : sub_division,
        "genuene_absent_students" : genuene_absent_students,
        "unstudents" : unstudents,
        "behaviours" : behaviours,
        "unauthorized_absent_students" : unauthorized_absent_students,
        "instance" : instance,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'academics/attendances.html',context)


@check_mode
@login_required
def attendance(request,pk):
    instance = get_object_or_404(Attendance.objects.filter(pk=pk,is_deleted=False))

    context = {
        "instance" : instance,
        "title" : "Attendance ",

        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'academics/attendance.html',context)


@check_mode
@login_required
def edit_attendance(request,pk):
    instance = get_object_or_404(Attendance.objects.filter(pk=pk,is_deleted=False))
    url = reverse('academics:edit_attendance',kwargs={'pk':pk})
    if request.method == "POST":
        previous_attendance_status = instance.is_present
        previous_absent_days_status = instance.absent_days_count

        form = AttendanceEditForm(request.POST,instance=instance)

        if form.is_valid():
            is_present = form.cleaned_data['is_present']
            absent_days_count = previous_absent_days_status
            if previous_attendance_status == True and is_present == False:
                absent_days_count = previous_absent_days_status + 1
            elif previous_attendance_status == False and is_present == True:
                if previous_absent_days_status > 0 :
                    absent_days_count = previous_absent_days_status - 1
                else:
                    absent_days_count =previous_absent_days_status

            behaviour = form.cleaned_data['behaviour']
            if not behaviour:
                behaviour = None

            #update attendance
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.absent_days_count = absent_days_count
            data.behaviour = behaviour
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Attendance successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('academics:attendances' , kwargs={'pk':data.sub_division.pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = AttendanceEditForm(instance=instance)
        context = {
            "title" : "Edit Attendance",
            "form" : form,
            "instance" : instance,
            "url" : url,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request, 'academics/edit_attendance.html', context)


@check_mode
@login_required
def delete_attendance(request,pk):
    attandence = get_object_or_404(Attendance.objects.filter(pk=pk,is_deleted=False))
    sub_division = attandence.sub_division
    Attendance.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Attendance successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('academics:attendances' , kwargs={'pk':sub_division.pk})
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')



@check_mode
@login_required
def show_calendar_attendances(request,pk):
    instance = get_object_or_404(SubDivision.objects.filter(pk=pk,is_deleted=False))

    today = datetime.datetime.today()
    weekday_no = today.weekday()
    if weekday_no == 0:
        weekday = "monday"
    elif weekday_no == 1:
        weekday = "tuesday"
    elif weekday_no == 2:
        weekday = "wednesday"
    elif weekday_no == 3:
        weekday = "thursday"
    elif weekday_no == 4:
        weekday = "friday"
    elif weekday_no == 5:
        weekday = "saturday"
    elif weekday_no == 6:
        weekday = "sunday"
    # periods = Period.objects.filter(is_deleted=False,weekday=weekday)
    sub_divisions = SubDivision.objects.filter(is_deleted=False)
    # instances = FacultyAttendance.objects.filter(is_deleted=False,date=today.date())
    # title = "Faculty Attendances - %s " %today.date()

    context = {
        # "title" : title,
        "instance" : instance,
        # "periods" : periods,
        "date" : today.date(),
        "sub_divisions" : sub_divisions,
        "is_need_calender" : True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request, 'academics/attendance_calender.html', context)


@check_mode
@login_required
def show_class_attendence(request):
    today = datetime.date.today()
    batch = get_current_batch(request)

    batch_pk = request.GET.get('batch')
    if batch_pk:
        if Batch.objects.filter(pk=batch_pk).exists():
            batch = Batch.objects.get(pk=batch_pk)

    sub_divisions = SubDivision.objects.filter(is_deleted=False)
    departments = Department.objects.filter(is_deleted=False)
    if batch:
        student_batches = Batch.objects.filter(is_deleted=False).exclude(pk=batch.pk)
    else:
        student_batches = Batch.objects.filter(is_deleted=False)

    context = {
        "title" : "Class and Divisions in %s" %batch,
        "current_batch" : batch,
        "student_batches" : student_batches,
        "sub_divisions" : sub_divisions,
        "departments" : departments,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request, 'academics/show_class_attendence.html', context)


@check_mode
@login_required
def show_division_attendence(request,pk):
    department = get_object_or_404(Department.objects.filter(pk=pk,is_deleted=False))
    instances = SubDivision.objects.filter(department=department,is_deleted=False)
    title = "Divisions inside class %s" %department

    context = {
        "instances" : instances,
        "title" : title,
        "is_need_calender" : True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request, 'academics/show_division_attendence.html', context)


@check_mode
@ajax_required
@login_required
def get_periods_by_date(request):
    date = request.GET.get('date')
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
        except ValueError:
            date_error = True

    weekday_no = date.weekday()
    if weekday_no == 0:
        weekday = "monday"
    elif weekday_no == 1:
        weekday = "tuesday"
    elif weekday_no == 2:
        weekday = "wednesday"
    elif weekday_no == 3:
        weekday = "thursday"
    elif weekday_no == 4:
        weekday = "friday"
    elif weekday_no == 5:
        weekday = "saturday"
    elif weekday_no == 6:
        weekday = "sunday"
    if weekday == "saturday" or weekday == "sunday":
        periods = Period.objects.filter(is_deleted=False,weekday=weekday).exclude(from_time='07:30:00')
    else:
        periods = Period.objects.filter(is_deleted=False,weekday=weekday)

    json_models = serializers.serialize("json", instances)
    return HttpResponse(json_models, content_type="application/javascript")


@check_mode
@ajax_required
@login_required
def get_amounts(request):
    pk = request.GET.get('id')
    admission = request.GET.get('admission')
    instance = StudentFee.objects.get(fee_category__pk=pk,admission__pk=admission,is_deleted=False)
    if instance:
        response_data = {
            "status" : "true",
            'amount' : float(instance.balance),
        }
    else:
        response_data = {
            "status" : "false",
            "message" : "Fee Error"
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def class_transfer(request,pk):
    division = get_object_or_404(SubDivision.objects.filter(pk=pk,is_deleted=False))
    today = datetime.date.today()
    current_batch = get_current_batch(request)
    last_batch_end_date = current_batch.start_date-datetime.timedelta(days=1)

    last_batch = get_object_or_404(Batch.objects.filter(end_date=last_batch_end_date,is_deleted=False))
    admissions = Admission.objects.filter(is_deleted=False,sub_division=division,batch=last_batch.pk)
    AdmissionBatchFormset = formset_factory(AdmissionBatchForm,extra=0)

    if request.method == 'POST':
        admission_batch_formset = AdmissionBatchFormset(request.POST,prefix='admission_batch_formset')

        if admission_batch_formset.is_valid():
            for admission_batch in admission_batch_formset:
                admission_name = admission_batch.cleaned_data['admission_name']
                admission_pk = admission_batch.cleaned_data['admission_pk']
                division_name = admission_batch.cleaned_data['division_name']
                class_name = admission_batch.cleaned_data['class_name']
                sub_division = admission_batch.cleaned_data['new_sub_division']

                if sub_division:
                    print(sub_division)
                    division = get_object_or_404(SubDivision.objects.filter(pk=sub_division,is_deleted=False))
                    department = get_object_or_404(Department.objects.filter(pk=division.department.pk,is_deleted=False))
                else:
                    division = get_object_or_404(SubDivision.objects.filter(pk=pk,is_deleted=False))
                    department = get_object_or_404(Department.objects.filter(pk=division.department.pk,is_deleted=False))


                admission_fee_amount = department.admission_fee
                diary_fee_amount = department.diary_fee
                monthly_fee_amount = department.fee
                computer_fee_amount = department.computer_fee
                library_fee_amount = department.library_fee
                special_fee_amount = department.special_fee

                fee = (monthly_fee_amount * 12) + admission_fee_amount + diary_fee_amount + computer_fee_amount + library_fee_amount + (special_fee_amount * 12)
                admission_fee_category = FeeCategory.objects.filter(name="admission_fee",is_deleted=False)[0]
                diary_fee_category = FeeCategory.objects.filter(name="diary_fee",is_deleted=False)[0]
                monthly_fee_category = FeeCategory.objects.filter(name="monthly_fee",is_deleted=False)[0]
                special_fee_category = FeeCategory.objects.filter(name="special_fee",is_deleted=False)[0]
                computer_fee_category = FeeCategory.objects.filter(name="computer_fee",is_deleted=False)[0]
                library_fee_category = FeeCategory.objects.filter(name="library_fee",is_deleted=False)[0]

                admission = get_object_or_404(Admission.objects.filter(is_deleted=False,pk=admission_pk))

                admission.department = department
                admission.sub_division = division
                admission.save()

                update_student_fee(request,admission,computer_fee_category,computer_fee_amount)
                update_student_fee(request,admission,library_fee_category,library_fee_amount)
                update_student_fee(request,admission,admission_fee_category,admission_fee_amount)
                update_student_fee(request,admission,diary_fee_category,diary_fee_amount)
                update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="june")
                update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="july")
                update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="august")
                update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="september")
                update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="october")
                update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="november")
                update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="december")
                update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="january")
                update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="february")
                update_student_fee(request,admission,monthly_fee_category,monthly_fee_amount,month="march")

                update_student_fee(request,admission,special_fee_category,special_fee_amount,month="june")
                update_student_fee(request,admission,special_fee_category,special_fee_amount,month="july")
                update_student_fee(request,admission,special_fee_category,special_fee_amount,month="august")
                update_student_fee(request,admission,special_fee_category,special_fee_amount,month="september")
                update_student_fee(request,admission,special_fee_category,special_fee_amount,month="october")
                update_student_fee(request,admission,special_fee_category,special_fee_amount,month="november")
                update_student_fee(request,admission,special_fee_category,special_fee_amount,month="december")
                update_student_fee(request,admission,special_fee_category,special_fee_amount,month="january")
                update_student_fee(request,admission,special_fee_category,special_fee_amount,month="february")
                update_student_fee(request,admission,special_fee_category,special_fee_amount,month="march")

            response_data = {
                "status" : "true",
                "title" : "Successfully Transfered",
                "message" : "Class Transfered successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('academics:admissions')
            }

        else:
            message = generate_form_errors(admission_batch_formset,formset=True)
            print(message)
            print(admission_batch_formset.errors)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        initial = []
        for admission in admissions:
            transfer_dict = {
                'admission_pk' : admission.pk,
                'admission_name' : admission.name,
                'division_name' : admission.sub_division.name,
                'class_name' : admission.department.name,
                'former_student' : admission,
            }
            initial.append(transfer_dict)
        old_class = division.department.name
        new_class_name = int(old_class) + 1
        new_class = Department.objects.filter(is_deleted=False,name=str(new_class_name))
        division_class = []
        division_class.append(new_class)
        division_class.append(division.department)
        admission_batch_formset = AdmissionBatchFormset(prefix='admission_batch_formset',initial=initial)
        department = get_object_or_404(Department.objects.filter(pk=division.department.pk,is_deleted=False))
        # for form in admission_batch_formset:
        #     form.fields['sub_division'].queryset = SubDivision.objects.filter(department=department,is_deleted=False)

        context = {
            "title" : "Change Class",
            "admission_batch_formset" : admission_batch_formset,
            "url" : reverse('academics:class_transfer',kwargs={'pk':pk}),

            "is_need_calender" : True,
            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request,'academics/class_transfer.html',context)



@check_mode
@login_required
@role_required(['superadmin'])
def class_subjects(request):
    instances = PeriodSubject.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(subject__name__icontains=query) | Q(department__name__icontains=query) | Q(no_of_period__istartswith=query))
        title = "Subjects - %s" %query

    context = {
        "instances" : instances,
        "title" : "Periods of Subjects",
        "redirect": "true",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/class_subjects.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def class_subject(request,pk):
    instance = get_object_or_404(PeriodSubject.objects.filter(pk=pk,is_deleted=False))
    context = {
        "title" : "Periods of Subject",
        'instance' : instance,
        "redirect": "true",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/class_subject.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def create_class_subject(request):
    url = reverse('academics:create_class_subject')
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    if request.method == "POST":
        form = PeriodSubjectForm(request.POST)

        if form.is_valid():
            subject = form.cleaned_data['subject']
            if not PeriodSubject.objects.filter(subject=subject,is_deleted=False).exists():

                data = form.save(commit=False)
                auto_id = get_auto_id(PeriodSubject)
                data.auto_id = auto_id
                data.creator = request.user
                data.updater = request.user
                data.college = current_college
                data.batch = current_batch
                data.a_id = get_a_id(PeriodSubject,request)
                data.save()

                response_data = {
                    "status": "true",
                    "title": "Successfully created",
                    "message": "Class Subject Successfully Created.",
                    "redirect": "true",
                    "redirect_url": reverse('academics:class_subjects')
                }
            else:
                response_data = {
                    "status": "false",
                    "stable": "true",
                    "title": "Form validation error",
                    "message": "Periods for this Subject is already added. "
                }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = PeriodSubjectForm()

        context = {
            "title" : "Create  Subject Periods",
            "form" : form,
            "url" : url,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }

        return render(request, 'academics/entry_class_subject.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_class_subject(request,pk):
    instance = get_object_or_404(PeriodSubject.objects.filter(pk=pk,is_deleted=False))
    url = reverse('academics:edit_class_subject',kwargs={'pk':pk})
    if request.method == "POST":

        form = PeriodSubjectForm(request.POST,instance=instance)
        if form.is_valid():

            #update student medium
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Class subject successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('academics:class_subject',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = PeriodSubjectForm(instance=instance)

        context = {
            "title" : "Edit Class Subject",
            "form" : form,
            "instance" : instance,
            "url" : url,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'academics/entry_class_subject.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_class_subject(request,pk):
    PeriodSubject.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Class subject successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('academics:class_subjects')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def periods(request):
    instances = Period.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(weekday__icontains=query) | Q(from_time__istartswith=query) | Q(to_time__istartswith=query))
        title = "Periods - %s" %query

    context = {
        "instances" : instances,
        "title" : "Period",
        "redirect": "true",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/periods.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def period(request,pk):
    instance = get_object_or_404(Period.objects.filter(pk=pk,is_deleted=False))
    context = {
        "title" : "Period",
        'instance' : instance,
        "redirect": "true",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/period.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def create_period(request):
    url = reverse('academics:create_period')
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    PeriodFormset = formset_factory(PeriodForm,extra=1)
    if request.method == "POST":

        period_formset = PeriodFormset(request.POST,prefix='period_formset')

        if period_formset.is_valid():

            for period in period_formset:
                weekday = period.cleaned_data['weekday']
                from_time = period.cleaned_data['from_time']
                to_time = period.cleaned_data['to_time']

                Period.objects.create(
                    weekday = weekday,
                    from_time = from_time,
                    to_time = to_time,
                    auto_id = get_auto_id(Period),
                    a_id = get_a_id(Period,request),
                    college = current_college,
                    creator = request.user,
                    updater = request.user
                    )

            response_data = {
                "status": "true",
                "title": "Successfully created",
                "message": "Period Successfully Created.",
                "redirect": "true",
                "redirect_url": reverse('academics:periods')
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        period_formset = PeriodFormset(prefix='period_formset')
        context = {
            "title" : "Create Period",
            "period_formset" : period_formset,
            "url" : url,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
            "is_need_formset" : True,
        }

        return render(request, 'academics/entry_period.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_period(request,pk):
    instance = get_object_or_404(Period.objects.filter(pk=pk,is_deleted=False))
    url = reverse('academics:edit_period',kwargs={'pk':pk})
    if request.method == "POST":

        form = PeriodForm(request.POST,instance=instance)
        if form.is_valid():

            #update student medium
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Period successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('academics:period',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = PeriodForm(instance=instance)

        context = {
            "title" : "Edit Period",
            "form" : form,
            "instance" : instance,
            "url" : url,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'academics/edit_period.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_period(request,pk):
    Period.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Period successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('academics:periods')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def time_tables(request):
    instances = TimeTable.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(period__weekday__icontains=query) | Q(class_subject__subject__name__icontains=query) | Q(department__name__icontains=query) | Q(sub_division__name__icontains=query) | Q(weekday__icontains=query))
        title = "Time Tables - %s" %query

    context = {
        "instances" : instances,
        "title" : "Time Table",
        "redirect": "true",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/time_tables.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def time_table(request,pk):
    instance = get_object_or_404(TimeTable.objects.filter(pk=pk,is_deleted=False))
    context = {
        "title" : "Time Table",
        'instance' : instance,
        "redirect": "true",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/time_table.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def create_time_table(request,weekday,section):
    url = reverse('academics:create_time_table',kwargs={'weekday':weekday,'section':section})
    if section == "lp":
        sections = ['1','2','3','4']
    elif section == "up":
        sections = ['5','6','7']
    elif section == "hs":
        sections = ['8','9','10']
    elif section == "hss":
        sections = ['+1','+2']
    sub_divisions = SubDivision.objects.filter(is_deleted=False,department__name__in=sections)
    faculties = PeriodSubject.objects.filter(is_deleted=False)
    current_college = get_current_college(request)
    # TimeTableFormset = formset_factory(TimeTableForm,extra=1)
    if request.method == "POST":
        counter = 1
        for sub_division in sub_divisions:
            division_name = "division"+str(counter)
            division_pk =request.POST.get(division_name)
            periods = Period.objects.filter(weekday=weekday)
            pcounter = 1
            for period in periods:
                period_name = "period-%s-%s" %(counter,pcounter)
                faculty_name = "faculty-%s-%s" %(counter,pcounter)
                period =request.POST.get(period_name)
                faculty =request.POST.get(faculty_name)
                pcounter += 1
                if period and faculty:
                    period = Period.objects.get(pk=period,is_deleted=False)
                    division = SubDivision.objects.get(pk=division_pk,is_deleted=False)
                    faculty = PeriodSubject.objects.get(pk=faculty,is_deleted=False)
                    total_hour = faculty.no_of_period
                    no_period = 0
                    if TimeTable.objects.filter(sub_division=division,is_deleted=False,class_subject=faculty).exists():
                        time_tables = TimeTable.objects.filter(sub_division=division,is_deleted=False,class_subject=faculty)
                        for time_table in time_tables:
                            no_period += 1
                    print(period)
                    print(division)
                    print(faculty.subject)
                    print('period')
                    print(no_period)
                    print('hour')
                    print(total_hour)
                    if total_hour > no_period:
                        if not TimeTable.objects.filter(period=period,weekday=weekday,sub_division=division,is_deleted=False).exists():
                            TimeTable(
                                weekday = weekday,
                                class_subject = faculty,
                                period = period,
                                creator = request.user,
                                updater = request.user,
                                auto_id = get_auto_id(TimeTable),
                                sub_division = division,
                                department = division.department,
                                college = current_college,
                                a_id = get_a_id(TimeTable,request),
                            ).save()
                        else:
                            time_table = TimeTable.objects.filter(period=period,weekday=weekday,sub_division=division,is_deleted=False)[0]
                            time_table.class_subject = faculty
                            time_table.save()
                    else:
                        response_data = {
                            "status": "False",
                            "message": "Total Number of Period of %s is %s." %(faculty,total_hour),
                            "stable": "true",
                        }

            counter += 1
            if total_hour >= no_period:
                response_data = {
                    "status": "true",
                    "title": "Successfully created",
                    "message": "Time Table successfully created.",
                    "redirect": "true",
                    "redirect_url": reverse('academics:time_tables')
                }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        periods = Period.objects.filter(weekday=weekday)
        context = {
            "title" : "Create Time Table",
            "periods" : periods,
            "faculties" : faculties,
            "sub_divisions" : sub_divisions,
            "url" : url,

            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
        }
        return render(request, 'academics/entry_time_table.html', context)


# @check_mode
# @login_required
# @role_required(['superadmin'])
# def create_time_table(request):
#     url = reverse('academics:create_time_table')
#     current_college = get_current_college(request)
#     current_batch = get_current_batch(request)
#     TimeTableFormset = formset_factory(TimeTableForm,extra=1)
#     if request.method == "POST":

#         time_table_formset = TimeTableFormset(request.POST,prefix="time_table_formset")

#         if time_table_formset.is_valid():
#             for form in time_table_formset:
#                 period = form.cleaned_data['period']
#                 teacher = form.cleaned_data['teacher']
#                 division = form.cleaned_data['sub_division']
#                 weekday = form.cleaned_data['weekday']
#                 TimeTable.objects.create(
#                     auto_id = get_auto_id(TimeTable),
#                     creator = request.user,
#                     updater = request.user,
#                     college = current_college,
#                     a_id = get_a_id(TimeTable,request),
#                     period = period,
#                     department = division.department,
#                     teacher = teacher,
#                     sub_division = division,
#                     weekday = weekday
#                 )


#             response_data = {
#                 "status": "true",
#                 "title": "Successfully created",
#                 "message": "Time Table Successfully Created.",
#                 "redirect": "true",
#                 "redirect_url": reverse('academics:time_tables')
#             }

#         else:
#             message = generate_form_errors(time_table_formset, formset=True)
#             print(time_table_formset.errors)
#             response_data = {
#                 "status": "false",
#                 "stable": "true",
#                 "title": "Form validation error",
#                 "message": str(message)
#             }

#         return HttpResponse(json.dumps(response_data), content_type='application/javascript')

#     else:
#         time_table_formset = TimeTableFormset(prefix="time_table_formset")
#         context = {
#             "title" : "Create Time Table",
#             "time_table_formset" : time_table_formset,
#             "url" : url,
#             "redirect": "true",

#             "is_need_select_picker" : True,
#             "is_need_popup_box" : True,
#             "is_need_custom_scroll_bar" : True,
#             "is_need_wave_effect" : True,
#             "is_need_bootstrap_growl" : True,
#             "is_need_chosen_select" : True,
#             "is_need_grid_system" : True,
#             "is_need_datetime_picker" : True,
#             "is_need_animations": True,
#             "is_need_formset" : True,
#         }

#         return render(request, 'academics/entry_time_table.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_time_table(request,pk):
    instance = get_object_or_404(TimeTable.objects.filter(pk=pk,is_deleted=False))
    url = reverse('academics:edit_time_table',kwargs={'pk':pk})
    if request.method == "POST":

        form = TimeTableForm(request.POST,instance=instance)
        if form.is_valid():

            #update student medium
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Time Table successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('academics:time_table',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = TimeTableForm(instance=instance)

        context = {
            "title" : "Edit Time Table",
            "form" : form,
            "instance" : instance,
            "url" : url,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'academics/edit_time_table.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_time_table(request,pk):
    TimeTable.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Time table successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('academics:time_tables')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def list_weekday_to_time_table(request):
    context = {
        "title" : "List Weekday to Time Table",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/list_weekday.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def list_section_to_time_table(request,weekday):
    context = {
        "title" : "List Section to Time Table",
        "weekday" : weekday,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/list_section.html', context)


def create_club(request):
    current_college = get_current_college(request)
    if request.method == 'POST':
        form = ClubForm(request.POST)

        if form.is_valid():
            data = form.save(commit=False)
            auto_id = get_auto_id(StudentClub)
            a_id = get_a_id(StudentClub,request)
            data.creator = request.user
            data.updater = request.user
            data.college = current_college
            data.auto_id = auto_id
            data.a_id = a_id
            data.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Club created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('academics:club',kwargs={'pk':data.pk})
            }

        else:
            message = generate_form_errors(form,formset=False)
            print (form.errors)

            message = generate_form_errors(form,formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = ClubForm()
        context = {
            "title" : "Create Batch ",
            "form" : form,
            "url" : reverse('academics:create_club'),
            "redirect" : "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'academics/club_entry.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def clubs(request):
    current_college = get_current_college(request)
    instances = StudentClub.objects.filter(is_deleted=False,college=current_college)
    title = "Clubs"

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query))
        title = "Batches - %s" %query

    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'academics/clubs.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def club(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(StudentClub.objects.filter(pk=pk,is_deleted=False,college=current_college))
    club_members = StudentClubMember.objects.filter(is_deleted=False,club=instance)
    divisions = SubDivision.objects.filter(is_deleted=False)

    form = ClubMemberForm()
    batch = get_current_batch(request)

    batch_pk = request.GET.get('batch')
    if batch_pk:
        if Batch.objects.filter(pk=batch_pk).exists():
            batch = Batch.objects.get(pk=batch_pk)

    if batch:
        student_batches = Batch.objects.filter(is_deleted=False).exclude(pk=batch.pk)
    else:
        student_batches = Batch.objects.filter(is_deleted=False)

    context = {
        "instance" : instance,
        "title" : instance.name,
        "single_page" : True,
        "form" : form,
        "redirect" : True,
        "divisions": divisions,
        "student_batches" : student_batches,
        "club_members" : club_members,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'academics/club.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_club(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(StudentClub.objects.filter(pk=pk,is_deleted=False,college=current_college))
    if request.method == 'POST':

        response_data = {}
        form = ClubForm(request.POST,instance=instance)
        if form.is_valid():
            data = form.save(commit=False)
            auto_id = get_auto_id(StudentClub)
            a_id = get_a_id(StudentClub,request)
            data.creator = request.user
            data.updater = request.user
            data.college = current_college
            data.auto_id = auto_id
            data.a_id = a_id
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Club Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('academics:club',kwargs={'pk':data.pk})
            }
        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:

        form = ClubForm(instance=instance,initial={'college':current_college})

        context = {
            "form" : form,
            "title" : "Edit Club : " + instance.name,
            "instance" : instance,
            "is_edit" : True,
            "url" : reverse('academics:edit_club',kwargs={'pk':instance.pk}),
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'academics/club_entry.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_club(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(StudentClub.objects.filter(pk=pk,is_deleted=False,college=current_college))
    clubmembers = StudentClubMember.objects.filter(club=instance).update(is_deleted=True)
    instance.is_deleted = True
    instance.save()

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Club Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('academics:clubs')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@login_required
def club_batch(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(StudentBatch.objects.filter(pk=pk,is_deleted=False))
    total_students = Admission.objects.filter(is_deleted=False,student_batch__from_date__lte=instance.from_date,student_batch__to_date__gte=instance.to_date).count()
    total_teachers = Teacher.objects.filter(is_deleted=False,is_active=True).count()
    income = Transaction.objects.filter(transaction_type="income",admission__student_batch__from_date__lte=instance.from_date,is_deleted=False,admission__student_batch__to_date__gte=instance.to_date)
    income_dict = income.aggregate(Sum('amount'))
    total_income = income_dict['amount__sum']
    total_expense = 0
    other_batches = StudentBatch.objects.filter(is_deleted=False).exclude(pk=pk)
    from_date = instance.from_date
    to_date = instance.to_date
    expense = Transaction.objects.filter(transaction_type="expense",date__gte=from_date,date__lte=to_date,is_deleted=False)
    expense_dict = expense.aggregate(Sum('amount'))
    total_expense = expense_dict['amount__sum']
    context = {
        "title" : "Dashboard",
        "total_teachers" : total_teachers,
        "total_students" : total_students,
        "total_income" : total_income,
        "other_batches" : other_batches,
        "total_expense" : total_expense,
        "instance" : instance
    }
    return render(request, 'main/batch_dashboard.html', context)


def add_club_member(request,pk):
    instance = get_object_or_404(StudentClub.objects.filter(pk=pk, is_deleted=False,))
    current_college = get_current_college(request)
    if request.method == 'POST':
        form = ClubMemberForm(request.POST)

        if form.is_valid():
            student = form.cleaned_data['student']
            if student != "" and student != None:
                if not StudentClubMember.objects.filter(student=student,club=instance.pk,is_deleted=False):
                    data = form.save(commit=False)
                    auto_id = get_auto_id(StudentClubMember)
                    a_id = get_a_id(StudentClubMember,request)
                    data.creator = request.user
                    data.updater = request.user
                    data.college = current_college
                    data.club = instance
                    data.auto_id = auto_id
                    data.a_id = a_id
                    data.save()

                    response_data = {
                        "status" : "true",
                        "title" : "Successfully Created",
                        "message" : "Club member created successfully.",
                        "redirect" : "true",
                        "redirect_url" : reverse('academics:club',kwargs={'pk':instance.pk})
                    }
                else:
                    message = "Student is already member of this club"
                    response_data = {
                        "status" : "false",
                        "stable" : "true",
                        "title" : "Form verification failed",
                        "message" : message
                    }
            else:
                message = "Select a student First"
                response_data = {
                    "status" : "false",
                    "stable" : "true",
                    "title" : "Form verification failed",
                    "message" : message
                }

        else:
            message = generate_form_errors(form,formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def delete_club_member(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(StudentClubMember.objects.filter(pk=pk,is_deleted=False,college=current_college))
    instance.is_deleted = True
    instance.save()

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Student Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('academics:clubs')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def club_members(request):
    instances = StudentClubMember.objects.filter(is_deleted=False)
    context = {
        "instances" : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'academics/club.html', context)

def get_countsemester_details(request):
    id_division = request.GET.get('id_division')
    id_batch = request.GET.get('id_batch')
    print(id_batch)
    print("id for division")
    print(id_division)
    if Semester.objects.filter(division=id_division,academic_year=id_batch,is_deleted=False).exists():
        semester_s = Semester.objects.filter(division=id_division,academic_year=id_batch,is_deleted=False)
        sem_n =[]
        sem_a =[]
        for sem_s in semester_s:
            sem_n.append(str(sem_s.pk))
            sem_a.append(sem_s.semester)

        response_data = {
                "status" : "true",
                "sem_pk": sem_n,
                "sem_num": sem_a,
            }
    else:
        response_data = {
                "status" : "false",
                "message": "No data found..!"
            }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    # if Semester.objects.filter(division=id_division,academic_year=id_batch,is_deleted=False).exists():
    #     print("not okkkk")
    #     # sem = Semester.objects.filter(division=id_division,academic_year=id_batch)
    #     response_data = {
    #         "status" : "true",
    #         "semester_data" : sem,
    #     }
    #     return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    #     # if Subject.objects.filter(semester=semester,sub_division=id_division):

    #     #     subject = Subject.objects.filter(semester=semester,sub_division=id_division)
    #     #     subject_name = subject.name

    #     # else:
    #     #     response_data = {
    #     #     "status" : "false",
    #     #     "message" : "Subject not exist",
    #     # }


    # else:
    #     print("not in list")
    #     response_data = {
    #         "status" : "false",
    #         "message" : "Semester not exist",
    #     }
    # return HttpResponse(json.dumps(response_data), content_type='application/javascript')

def get_subject_details(request):
    id_division = request.GET.get('division')
    id_semester = request.GET.get('semester')
    print(id_division,id_semester)

    if Subject.objects.filter(sub_division=id_division,semester=id_semester).exists():
        subject = Subject.objects.filter(sub_division=id_division,semester=id_semester)
        subject_name = subject.name
        print(subject_name)

        response_data = {
            "status" : "true",
            "subject_name" : subject_name,
        }

    else:
        response_data = {
            "status" : "false",
            "message" : "Subject not exist",
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def get_period_subject_details(request):
    division = request.GET.get('division')
    batch = request.GET.get('batch')
    semester_pk = request.GET.get('semester')

    if Semester.objects.filter(pk=semester_pk,division=division,academic_year=batch).exists():
        sem = Semester.objects.get(pk=semester_pk,division=division,academic_year=batch)
        semester = sem.semester

        if Subject.objects.filter(semester=semester,sub_division=division,is_deleted=False):
            subject_name = Subject.objects.filter(semester=semester,sub_division=division)
            sub_list = []

            for sub in subject_name:
                sub_model = {
                    'id': str(sub.id),
                    'name': sub.name,
                    'code':sub.subject_code,
                }
                sub_list.append(sub_model)

            response_data = {
                "status" : "true",
                "subjects" : sub_list,
            }
        else:
            response_data = {
            "status" : "false",
            "message" : "Subject does not exist",
        }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        response_data = {
            "status" : "false",
            "message" : "Semester not exist",
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def get_semester_countdetails(request):
    id_division = request.GET.get('id_division')
    print("print")
    print(id_division)
    sem = SubDivision.objects.get(pk=id_division,is_deleted=False)
    count = sem.semester_no
    print("...............................sem")
    print(count)

    if SubDivision.objects.filter(pk=id_division,is_deleted=False).exists():

        sem = SubDivision.objects.get(pk=id_division,is_deleted=False)
        count = sem.semester_no
        print("...............................sem")
        print(sem)
        response_data = {
            "status" : "true",
            "count" : count,
        }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        response_data = {
            "status" : "false",
            "message" : "Semester not exist",
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def create_syllabus(request):
    pass