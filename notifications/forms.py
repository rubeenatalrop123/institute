from django import forms
from django.forms.widgets import TextInput, Textarea, Select
from django.utils.translation import ugettext_lazy as _
from notifications.models import Message, MessageRecipient
from dal import autocomplete


class MessageForm(forms.ModelForm):
    
    class Meta:
        model = Message
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            'message': Textarea(attrs={'class': 'form-control','placeholder' : 'Message'}),
            'type': Select(attrs={'class': ' form-control selectpicker',}),
            'gender': Select(attrs={'class': ' form-control selectpicker'}),
            'department': Select(attrs={'class': ' form-control selectpicker'}),
            'sub_division': Select(attrs={'class': ' form-control selectpicker'}),
        }

        error_messages = {
            'message' : {
                'required' : _("Message field is required."),
            },            
        }


class MessageRecipientForm(forms.ModelForm):
    
    class Meta:
        model = MessageRecipient
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id','message']
        widgets = {
            'student': autocomplete.ModelSelect2(url='academics:admission_autocomplete', attrs={'placeholder': 'Student', 'data-minimum-input-length': 1},),
        }