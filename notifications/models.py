# -*- coding: utf-8 -*-
from django.db import models
from main.models import BaseModel
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator
from decimal import Decimal


NotificationType = (
    ('general',"General"),
    ('individual',"Individual"),
    ('division',"Division"),
    ('class',"Class")
)

GENDER = (
    ('',"All"),
    ('male',"Male"),
    ('female',"Female")
)


class Message(BaseModel):
    message = models.TextField()  
    type = models.CharField(max_length=128,choices=NotificationType)
    gender = models.CharField(max_length=128,choices=GENDER,blank=True,null=True)
    department= models.ForeignKey("academics.Department",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE,blank=True,null=True)
    sub_division = models.ForeignKey("academics.SubDivision",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    
    class Meta:
        db_table = 'notification_message'
        verbose_name = _('message')
        verbose_name_plural = _('messages')
        ordering = ('type',)

    def __str__(self):
        return self.type


class MessageRecipient(models.Model):
    message = models.ForeignKey("notifications.Message",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    student = models.ForeignKey("academics.Admission",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=128,blank=True,null=True)
    msg_id = models.CharField(max_length=128,blank=True,null=True)
    status = models.CharField(max_length=128,blank=True,null=True)

    class Meta:
        db_table = 'message_recipient'
        verbose_name = _('message recipient')
        verbose_name_plural = _('message recipient')
        ordering = ('-id',)

    def __str__(self):
        return self.message.type
