from django.conf.urls import url, include
from django.contrib import admin
from notifications import views


urlpatterns = [

    url(r'^message/create/$', views.create_message, name='create_message'),
    url(r'^messages/$', views.messages, name='messages'),
    url(r'^message/edit/(?P<pk>.*)/$', views.edit_message, name='edit_message'),
    url(r'^message/view/(?P<pk>.*)/$', views.message, name='message'),
    url(r'^message/print/(?P<pk>.*)/$', views.print_report, name='print_report'),
    url(r'^message/delete/(?P<pk>.*)/$', views.delete_message, name='delete_message'),
    url(r'^message/delete-selected/$', views.delete_selected_messages, name='delete_selected_messages'),
]
