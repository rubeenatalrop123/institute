from django.shortcuts import render, get_object_or_404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, college_required, ajax_required,check_account_balance,permissions_required,role_required
from notifications.forms import MessageForm, MessageRecipientForm
from main.functions import get_auto_id, generate_form_errors, get_a_id, sendSMS
import json
from users.functions import get_current_college, college_access, get_current_batch
from users.functions import get_current_college, college_access
from notifications.models import Message, MessageRecipient
from django.db.models import Sum, Q
import datetime
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory
from academics.models import Admission


@check_mode
@login_required
def create_message(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    MessageRecipientFormset = formset_factory(MessageRecipientForm)
    if request.method == 'POST':
        form = MessageForm(request.POST)
        message_recipient_formset = MessageRecipientFormset(request.POST,prefix='message_recipient_formset')

        if form.is_valid() and message_recipient_formset.is_valid():

            auto_id = get_auto_id(Message)
            a_id = get_a_id(Message,request)

            data = form.save(commit=False)
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.a_id = a_id
            data.college = current_college
            data.save()

            message_type = form.cleaned_data['type']
            message = form.cleaned_data['message']
            department = form.cleaned_data['department']
            sub_division = form.cleaned_data['sub_division']
            gender = form.cleaned_data['gender']
            students = Admission.objects.filter(is_deleted=False,college=current_college,batch=current_batch)

            if gender == "male":
                students = students.filter(gender="male")
            elif gender == "female":
                students = students.filter(gender="female")
            else:
                students = students.filter(is_deleted=False)

            if department:
                students = students.filter(department=department)
            if sub_division:
                students = students.filter(sub_division=sub_division)

            if message_type == "individual":
                for item in message_recipient_formset:


                    student = item.cleaned_data['student']

                    if student.std_phone:
                        student_phone = student.std_phone
                    else:
                        student_phone = student.phone


                    recipient = MessageRecipient.objects.create(
                        message = data,
                        student = student,
                        phone_number = student_phone
                    )
                    sending = sendSMS(student_phone,message)
                    recipient.status = str(sending).replace("b'",'').replace("'","")
                    recipient.save()


            else:
                for student in students:
                    if student.std_phone:
                        student_phone = student.std_phone
                    else:
                        student_phone = student.phone

                    recipient = MessageRecipient.objects.create(
                        message = data,
                        student = student,
                        phone_number = student_phone
                    )

                    sending = sendSMS(student_phone,message)

                    recipient.status = str(sending).replace("b'",'').replace("'","")

                    recipient.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Message created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('notifications:message',kwargs={'pk':data.pk})
            }

        else:
            message = generate_form_errors(form,formset=False)
            print(form.errors)
            message += generate_form_errors(message_recipient_formset,formset=True)
            print(message_recipient_formset)

            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = MessageForm()
        message_recipient_formset = MessageRecipientFormset(prefix='message_recipient_formset')
        context = {
            "title" : "Create Message",
            "form" : form,
            "url" : reverse('notifications:create_message'),
            "message_recipient_formset" : message_recipient_formset,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,

        }
        return render(request,'notifications/entry_message.html',context)


@check_mode
@login_required
def messages(request):
    instances = Message.objects.filter(is_deleted=False)
    title = "Messages"

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query))
        title = "Messages - %s" %query

    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,

    }
    return render(request,'notifications/messages.html',context)


@check_mode
@login_required
def message(request,pk):
    instance = get_object_or_404(Message.objects.filter(pk=pk,is_deleted=False))
    message_recipients = MessageRecipient.objects.filter(message=instance)

    context = {
        "instance" : instance,
        "message_recipients" : message_recipients,
        "title" : "Message : " + instance.type,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "purchases" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,

    }
    return render(request,'notifications/message.html',context)


@check_mode
@login_required
def print_report(request,pk):
    instance = get_object_or_404(Message.objects.filter(pk=pk,is_deleted=False))
    message_recipients = MessageRecipient.objects.filter(message=instance)
    print(instance.department)

    context = {
        "instance" : instance,
        "message_recipients" : message_recipients,
        "title" : "Message : " + instance.type,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "purchases" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,

    }
    return render(request,'notifications/print_report.html',context)



@check_mode
@login_required
def edit_message(request,pk):
    instance = get_object_or_404(Message.objects.filter(pk=pk,is_deleted=False))

    if MessageRecipient.objects.filter(message=instance).exists():
        extra = 0
    else:
        extra = 1

    MessageRecipientFormset = inlineformset_factory(
        Message,
        MessageRecipient,
        can_delete=True,
        form=MessageRecipientForm,
        extra=extra
    )

    if request.method == 'POST':

        form = MessageForm(request.POST,instance=instance)
        message_recipient_formset = MessageRecipientFormset(request.POST,prefix="message_recipient_formset", instance=instance)

        if form.is_valid() and message_recipient_formset.is_valid():

            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            message_items = message_recipient_formset.save(commit=False)
            for item in message_items:
                item.message = data
                item.save()

            for obj in message_recipient_formset.deleted_objects:
                obj.delete()

            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Message Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('notifications:message',kwargs={'pk':data.pk})
            }
        else:
            message = generate_form_errors(form,formset=False)
            message = generate_form_errors(message_recipient_formset,formset=True)

            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = MessageForm(instance=instance)
        message_recipient_formset = MessageRecipientFormset(prefix='message_recipient_formset',instance=instance)

        context = {
            "form" : form,
            "title" : "Edit Message : " + instance.type,
            "instance" : instance,
            "message_recipient_formset" : message_recipient_formset,
            "url" : reverse('notifications:edit_message',kwargs={'pk':instance.pk}),
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'notifications/entry_message.html', context)


@check_mode
@login_required
def delete_message(request,pk):
    instance = get_object_or_404(Message.objects.filter(pk=pk,is_deleted=False))
    instance.is_deleted = True
    instance.save()

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Message Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('notifications:messages')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def delete_selected_messages(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(Message.objects.filter(pk=pk,is_deleted=False))
            instance.is_deleted = True
            instance.save()

        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Message(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('notifications:messages')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')
