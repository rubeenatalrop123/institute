from django.conf.urls import url, include
from django.contrib import admin
from staffs import views
from staffs.views import StaffAutocomplete, DesignationAutocomplete



urlpatterns = [ 
    url(r'^staff-autocomplete/$',StaffAutocomplete.as_view(),name='staffs_autocomplete'), 
    url(r'^designation-autocomplete/$',DesignationAutocomplete.as_view(create_field='name'),name='designation_autocomplete'),                         

	url(r'^staff/create-enquiry/$',views.create_staff,name='create_staff'),
    url(r'^staff/view/$',views.staffs,name='staffs'),
    url(r'^staff/view/(?P<pk>.*)/$',views.staff,name='staff'),
    url(r'^staff/edit/(?P<pk>.*)/$',views.edit_staff,name='edit_staff'),
    url(r'^staff/delete/(?P<pk>.*)/$',views.delete_staff,name='delete_staff'),
    url(r'^delete-selected-staffs/$', views.delete_selected_staffs, name='delete_selected_staffs'),   
    
    url(r'^staff-salary/create/$',views.create_staff_salary,name='create_staff_salary'),
    url(r'^staff-salaries/view/$',views.staff_salaries,name='staff_salaries'),
    url(r'^staff-salary/view/(?P<pk>.*)/$',views.staff_salary,name='staff_salary'),
    url(r'^staff-salary/edit/(?P<pk>.*)/$',views.edit_staff_salary,name='edit_staff_salary'),
    url(r'^staff-salary/delete/(?P<pk>.*)/$',views.delete_staff_salary,name='delete_staff_salary'),
    url(r'^delete-selected-staff-salaries/$', views.delete_selected_staff_salaries, name='delete_selected_staff_salaries'), 

    url(r'^staff-designation/create/$',views.create_designation,name='create_designation'),
    url(r'^staff-designations/view/$',views.designations,name='designations'),
    url(r'^staff-designation/view/(?P<pk>.*)/$',views.designation,name='designation'),
    url(r'^staff-designation/edit/(?P<pk>.*)/$',views.edit_designation,name='edit_designation'),
    url(r'^staff-designation/delete/(?P<pk>.*)/$',views.delete_designation,name='delete_designation'),
    url(r'^delete-selected-designations/$', views.delete_selected_designations, name='delete_selected_designations'), 

    url(r'^get-salary/$',views.get_salary,name='get_salary'),

    url(r'^staff-qualification/create/$',views.create_qualification,name='create_qualification'),
    url(r'^staff-qualifications/view/$',views.qualifications,name='qualifications'),
    url(r'^staff-qualification/view/(?P<pk>.*)/$',views.qualification,name='qualification'),
    url(r'^staff-qualification/edit/(?P<pk>.*)/$',views.edit_qualification,name='edit_qualification'),
    url(r'^staff-qualification/delete/(?P<pk>.*)/$',views.delete_qualification,name='delete_qualification'),

    url(r'^show-calendar-attendances/$',views.show_calendar_attendances,name='show_calendar_attendances'),
    url(r'^create-staff-attendance/$',views.create_staff_attendance,name='create_staff_attendance'),
    url(r'^edit/staff-attendance/(?P<pk>.*)/$', views.edit_staff_attendance, name='edit_staff_attendance'),
    url(r'^staff-attendances/$', views.staff_attendances, name='staff_attendances'),
    url(r'^view/staff-attendance/(?P<pk>.*)/$', views.staff_attendance, name='staff_attendance'),
    url(r'^delete/delete-staff-attendance/(?P<pk>.*)/$', views.delete_staff_attendance, name='delete_staff_attendance'),
    url(r'^staff/subject/(?P<pk>.*)/$',views.create_staff_subjects,name='create_staff_subjects'),

    url(r'^staff/subject-list/$',views.get_subject_details,name='get_subject_details'),
    url(r'^staff/staff-followups/(?P<pk>.*)/$',views.staff_follow_ups,name='staff_follow_ups'),


]