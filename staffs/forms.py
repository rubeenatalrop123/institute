from django import forms
from django.forms.widgets import TextInput, Textarea, Select, HiddenInput,CheckboxInput
from django.utils.translation import ugettext_lazy as _
from djangoformsetjs.utils import formset_media_js
from staffs.models import Staff, StaffSalary, Designation, Qualification,StaffAttendance,Experience,StaffSubjects,FollowUps,InterviewMail
from dal import autocomplete
from decimal import Decimal


class StaffForm(forms.ModelForm):
    class Meta:
        model = Staff
        exclude = ['batch','creator','updater','auto_id','is_deleted','a_id','college','permissions', 'qualification','card_id']
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name','tabindex' : '1'}),
            'email': TextInput(attrs={'class': 'form-control','placeholder' : 'Email','tabindex' : '4'}),
            'phone': TextInput(attrs={'class': 'form-control','placeholder' : 'Phone/Mobile', 'tabindex': '2'}),
            'gender': Select(attrs={'class': 'form-control selectpicker', 'placeholder': 'Gender', 'tabindex' : '5'}),
            "dob": TextInput(attrs = {"class": "date-picker form-control","placeholder" : "Date Of Birth", "tabindex" : "6"}),
            'address': TextInput(attrs={'class': 'form-control','placeholder' : 'Address','tabindex' : '3'}),
        }
        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
            'phone' : {
                'required' : _("Phone field is required."),
            },
            'address' : {
                'required' : _("Address field is required."),
            },
        }
        labels = {
            "name" : " Name",
            "dob" : "Date of Birth",
            'phone' : "Phone/Mobile",
            'resume' : "Upload Resume"
        }


class QualificationItemForm(forms.ModelForm):
    
    class Meta:
        model = Qualification
        exclude = ['id','is_deleted','staff']
        widgets = {
            'name' : TextInput(attrs={'class' : 'required form-control', 'placeholder' : 'Qualification'}),
            'university' : TextInput(attrs={'class' : 'required form-control', 'placeholder' :'University'}),
            'institution' : TextInput(attrs={'class' : 'required form-control', 'placeholder' : 'institution'}),
            "year_of_passing": TextInput(attrs={'class' : 'required form-control', 'placeholder' : 'Year'}),
            'mark_percentage' : TextInput(attrs={'class':'form-control','placeholder' : 'Mark in Percentage'}),
        }
        error_messages = {
            'staff' : {
                'required' : _("staff field is required."),
            },
            'name' : {
                'required' : _('Qualification is required'),
            },
            'university':{
                'required' : _('University is required'),
            },
            'institution' : {
                'required' : _('Institution is required'),
            },
            'year_of_passing' : {
                'required' : _("Year field is required."),
            },
            'mark_percentage' : {
                'required' : _("mark_percentage field is required."),
            }

        }
        labels ={
            'name' : 'Qualification',
            'year_of_passing' : 'Year of Passing',
            'mark_percentage' : 'Mark Percentage',
        }


class ExperienceForm(forms.ModelForm):

    class Meta:
        model = Experience
        exclude = ['id','is_deleted','staff','duration_year','month_duration']
        widgets = {
            'experience_institution' : TextInput(attrs={"class":"form-control",'placeholder':'Institution'}),
            'designation' : TextInput(attrs={"class":"form-control",'placeholder':'Designation'}),
            "date_of_joining": TextInput(attrs = {"class": "form-control datepicker-inline","placeholder" : "Date Of Birth",}),

            # 'date_of_joining': TextInput(attrs = {"class": "form-control date-picker","placeholder" : "Date of Joining",}),
            'date_of_leaving': TextInput(attrs = {"class": "form-control datepicker-inline","placeholder" : "Date of Joining",}),
        }

        labels ={
            'experience_institution' : 'Institution',
            'date_of_joining' : 'Date of Joining',
            'date_of_leaving' : 'Date of Leaving',
        }

class StaffSubjectForm(forms.ModelForm):

    class Meta:
        model = StaffSubjects
        exclude = ['id','is_deleted','staff']
        widgets = {
            'division' : Select(attrs={"class" :"form-control formset_division selectpicker"}),
            # 'division' : autocomplete.ModelSelect2(url='academics:division_autocomplete',attrs={'class':'formset_division','data-placeholder' : 'division','data-minimum-input-length':0}),
            'semester' : Select(attrs={"class":"form-control formset_semester selectpicker","placeholder":"semester"}),
            "subject": Select(attrs={'class' : 'formset_subject selectpicker form-control','data-placeholder':'subject'}),
        }

class StaffSalaryForm(forms.ModelForm):

    class Meta:
        model = StaffSalary
        exclude = ['creator','updater','auto_id','is_deleted','a_id','college','advance','balance','paid','date','amount']
        widgets = {
            'staff': Select(attrs={'class': 'required form-control selectpicker'}),
            'month': Select(attrs={'class': 'required form-control selectpicker'}),
            'year': TextInput(attrs={'class': 'required form-control','placeholder' : 'yyyy'}),
            'basic_salary': TextInput(attrs={'class': 'form-control','placeholder' : 'Basic Salary'}),
            'allowance': TextInput(attrs={'class': 'form-control','placeholder' : 'Allowance'}),
            'deduction': TextInput(attrs={'class': 'form-control','placeholder' : 'Deduction'}),
            'amount':TextInput(attrs={'class': 'form-control','placeholder' : 'Gross Salary','disabled':'disabled'}),
        }
        error_messages = {
            'staff' : {
                'required' : _("Staff field is required."),
            },
            'date' : {
                'required' : _("Date field is required."),
            },
            'amount' : {
                'required' : _("Amount field is required."),
            }
        }


class DesignationForm(forms.ModelForm):

    class Meta:
        model = Designation
        fields = ['name',]
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Enter Designation'}),
        }

        error_messages = {
            'name' : {
                'required' : _("Name field is required."),}
        }


class AttendanceStaffForm(forms.ModelForm):

    staff_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = StaffAttendance
        exclude = ['creator','updater','auto_id','department','sub_division','is_deleted','date','weekday','absent_days_count','college','a_id']
        widgets = {
            'staff' : HiddenInput(),
            "reason": TextInput( attrs = { "class": "form-control", } ),
            "time": TextInput( attrs = { "class": "form-control", } ),
        }
        error_messages = {
            "staff" : {
                    "required" : _("Admission field is required.")
                },
        }



class AttendanceStaffDateForm(forms.ModelForm):

    staff_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = StaffAttendance
        fields = ['date','weekday']

        widgets = {
                "date": TextInput( attrs = { "class": "required form-control date-picker", "placeholder" : " Date ", } ),
                "weekday": Select( attrs = { "class": "required form-control selectpicker", } )
            }
        error_messages = {
                "date" : {
                    "required" : _("Date field is required.")
                },
                "weekday" : {
                    "required" : _("Period field is required.")
                },
            }


class AttendanceEditForm(forms.ModelForm):

    staff_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = StaffAttendance
        exclude = ['creator','staff','updater','auto_id','is_deleted','date','weekday','absent_days_count','college','a_id']
        widgets = {
            "reason": TextInput( attrs = { "class": "form-control", } ),
            "time": TextInput( attrs = { "class": "form-control", } ),
        }
        error_messages = {

        }

class FollowUpsForm(forms.ModelForm):

    class Meta:
        model = FollowUps
        exclude = ['creator','staff','updater','auto_id','is_deleted','datetime','a_id','college']
        widgets = {
            "followup_type" : Select(attrs={"class" : "form-control selectpicker",}),
            "status" : Select(attrs={"class" : "selectpicker"}),
            "status_attend" : CheckboxInput(attrs={"class" : ""}),
            "status_not_attend" : CheckboxInput(attrs={"class" : ""}),
            "replay" : CheckboxInput(attrs={"class" : ""}),
            "discription" : Textarea(attrs={"class" : "form-control"}),
            "send_mail" : CheckboxInput(attrs={"class" : ""}),
        }  
        error_messages = {
            
        }            
        labels = {
            "followup_type" : "Follow Up Type",
            "status_attend" : "Status Attend",
            # "status_pending" : "Status Pending",
            "status_not_attend" : "Status Not Attend",
            "send_mail" : "Send Mail",
        }

class InterviewMailForm(forms.ModelForm):

    class Meta:
        model = InterviewMail
        exclude = ['staff','creator','updater','auto_id','is_deleted','a_id']
        widgets = {
            "subject" : TextInput(attrs={"class" : "form-control", "placeholder" : "Subject"}),
            "content" : Textarea(attrs={"class" : "form-control", "placeholder" : "Content"}),
            "interview_date": TextInput(attrs = {"class": "date-picker form-control","placeholder" : "Interview Date"}),

        }
        labels ={
            'interview_date' : 'Interview Date',
        }
