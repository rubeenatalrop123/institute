import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator
from decimal import Decimal
import datetime
from main.models import BaseModel


FOLLOWUPS = {
    ('mail','Email'),
    ('call','Call')
}

STATUS = {
    ('pending','PENDING'),
    ('attend','ATTEND'),
    ('not_attend','NOT ATTEND')
}
MONTH = (
    ('1','January'),
    ('2','February'),
    ('3','March'),
    ('4','April'),
    ('5','May'),
    ('6','June'),
    ('7','July'),
    ('8','August'),
    ('9','September'),
    ('10','October'),
    ('11','November'),
    ('12','December'),
)

GENDER_CHOICES = (
    ('M','Male'),
    ('F','Female'),
    ('O','Other')
)

REMARK = (
    ('working','Working'),
    ('deputation','Deputation'),
    ('working_arrangement','Working Arrangement'),
    ('on_long_leave','On Long Leave')
)

CATEGORY = (
     ('teaching','Teaching'),
     ('non_teaching','Non Teaching'),
)
SUB_CATEGORY = (
     ('lab','Lab'),
     ('administrative','Administrative'),
     ('personal_trainer','Personal Triner'),
     ('library','Library'),
     ('office_staff','Office Staff'),
     ('hostel_warden','Hostel Warden'),
)


WEEKDAY_CHOICES = (
    ('monday','MONDAY'),
    ('tuesday','TUESDAY'),
    ('wednesday','WEDNESDAY'),
    ('thursday','THURSDAY'),
    ('friday','FRIDAY'),
    ('saturday','SATURDAY')
)

class Staff(BaseModel):
    user = models.ForeignKey("auth.User",null=True,blank=True,on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    email = models.EmailField()
    phone = models.CharField(max_length=128)
    address = models.TextField()
    gender = models.CharField(max_length=5,choices=GENDER_CHOICES)
    dob = models.DateField(blank=True,null=True)
    photo = models.ImageField(upload_to="staffs/",blank=True,null=True)
    resume = models.ImageField(upload_to="staffs/",blank=True,null=True)

    is_deleted = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = 'staffs_staff'
        verbose_name = _('staff')
        verbose_name_plural = _('staffs')
        ordering = ('name',)

    def __str__(self):
        return self.name


class StaffSalary(BaseModel):
    staff = models.ForeignKey("staffs.Staff",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    date = models.DateField()
    month = models.CharField(max_length=128,choices=MONTH)
    year = models.PositiveIntegerField()
    basic_salary = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    amount = models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    allowance = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    deduction = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    paid = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    balance = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    advance = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'staffs_staffsalary'
        verbose_name = _('staffsalary')
        verbose_name_plural = _('staffsalaries')
        ordering = ('year',)

    def __str__(self):
        return str(self.amount)

class FollowUps(BaseModel):
    staff = models.ForeignKey("staffs.staff",limit_choices_to={'is_deleted':False},on_delete=models.CASCADE)
    followup_type = models.CharField(max_length=128,choices=FOLLOWUPS)
    status = models.CharField(max_length=128,choices=STATUS,blank=True,null=True)
    replay = models.BooleanField(default=False)
    send_mail = models.BooleanField(default=False)
    discription = models.TextField()
    datetime = models.DateTimeField()

    class Meta:
        db_table = 'staffs_followups'
        verbose_name = _('FollowUp')
        verbose_name_plural = _('FollowUps')

    def __str__(self):
        return str(self.staff)   


class Designation(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'staff_designation'
        verbose_name = _('designation')
        verbose_name_plural = _('designations')
        ordering = ('name',)

    def __str__(self):
        return self.name


class Qualification(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    staff = models.ForeignKey("staffs.staff",limit_choices_to={'is_deleted':False},on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    university = models.CharField(max_length=128)
    institution = models.CharField(max_length=128)
    year_of_passing = models.CharField(max_length=128)
    mark_percentage = models.DecimalField(default=0.0,decimal_places=2,max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    certificate_copy = models.ImageField(upload_to="staffs/certificate",blank=True,null=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'staff_qualification'
        verbose_name = _('qualification')
        verbose_name_plural = _('qualifications')
        ordering = ('name',)

    def __str__(self):
        return self.name


class Experience(models.Model):
    id = models.UUIDField(primary_key=True,default=uuid.uuid4,editable=False)
    staff = models.ForeignKey("staffs.staff",limit_choices_to={'is_deleted':False},on_delete=models.CASCADE)
    experience_institution = models.CharField(max_length=128, blank=True, null=True)
    designation = models.CharField(max_length=128, blank=True, null=True)
    date_of_joining= models.DateField(blank=True,null=True)
    date_of_leaving= models.DateField(blank=True,null=True)
    exp_certificate = models.ImageField(upload_to="staffs/experience",blank=True,null=True)
    duration_year = models.PositiveIntegerField(default=0)
    month_duration = models.PositiveIntegerField(default=0)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'staff_experience'
        verbose_name = _('experience')
        verbose_name_plural = _('xperiences')
        ordering = ('id',)

    def __str__(self):
        return self.experience_institution


class StaffAttendance(BaseModel):
    staff = models.ForeignKey("staffs.Staff",on_delete=models.CASCADE)
    weekday = models.CharField(max_length=128,choices=WEEKDAY_CHOICES)
    time = models.TimeField(default=datetime.time(9, 00))
    date = models.DateField()
    is_present = models.BooleanField(default=True)
    is_genuene = models.BooleanField(default=False)
    reason = models.TextField(blank=True,null=True)
    absent_days_count = models.PositiveIntegerField(default=0)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'staff_attendance'
        verbose_name = _('staff attendance')
        verbose_name_plural = _('staff attendances')
        ordering = ('date',)

    def __str__(self):
        return self.date

class StaffSubjects(models.Model):
    id = models.UUIDField(primary_key=True,default=uuid.uuid4,editable=False)
    staff = models.ForeignKey("staffs.Staff",on_delete=models.CASCADE)
    division = models.ForeignKey("academics.SubDivision",on_delete=models.CASCADE)
    semester = models.ForeignKey("academics.Semester", on_delete=models.CASCADE)
    subject = models.ForeignKey("academics.Subject",on_delete=models.CASCADE)

    is_deleted = models.BooleanField(default=False)

    class Meta: 
        db_table = 'staff_subject'
        verbose_name = ('Staff Subject')
        verbose_name_plural = ('Staff Subjects')
        ordering = ('id',)

    def __str__(self):
        return self.subject

class InterviewMail(BaseModel):
    staff = models.ForeignKey("staffs.Staff",on_delete=models.CASCADE)
    subject = models.CharField(max_length=128,blank=True,null=True)
    content = models.TextField(blank=True,null=True)
    interview_date = models.DateField(blank=True,null=True)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'interview_details'
        verbose_name = 'interview_detail'
        verbose_name_plural = 'interview_details'

    def __str__(self):
        return self.staff