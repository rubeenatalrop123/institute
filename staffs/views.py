from __future__ import unicode_literals
from django.shortcuts import render
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from staffs.models import InterviewMail,Staff, StaffSalary, Designation, Qualification, StaffAttendance,Experience,StaffSubjects,FollowUps
from finance.models import Transaction
from staffs.forms import StaffForm, StaffSalaryForm, DesignationForm, QualificationItemForm, AttendanceStaffForm, AttendanceStaffDateForm, AttendanceEditForm,ExperienceForm, StaffSubjectForm,FollowUpsForm,InterviewMailForm
from django.utils.translation import ugettext_lazy as _
from main.functions import get_auto_id, generate_form_errors, get_current_role, generate_unique_id, get_current_batch, get_current_batch, get_admn_no, get_a_id, exp_duration
from users.functions import get_current_college, college_access, get_current_batch
from users.models import StaffNotification, NotificationSubject
from django.forms.formsets import formset_factory
import json
from django.forms.models import inlineformset_factory
from django.views.decorators.http import require_GET
from django.forms.widgets import TextInput, Select
from main.decorators import check_mode, college_required, ajax_required, permissions_required, role_required
import datetime
from django.contrib.auth.models import User, Group
import xlrd
import random
from main.models import Place, Batch, CollegeAccess
from academics.models import Subject,Semester
from dal import autocomplete
from django.core import serializers
from users.models import Permission
from decimal import Decimal
from django.db.models import Q ,Sum
from django.conf import settings
from django.template.loader import render_to_string
from users.functions import send_email

class StaffAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_college = get_current_college(self.request)
        # current_batch = get_current_batch(request)
        staffs = Staff.objects.filter(college=current_college,is_deleted=False)

        if self.q:
            name = staffs.filter(Q(name__istartswith=self.q))

        return name


class DesignationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        current_college = get_current_college(self.request)
        # current_batch = get_current_batch(request)
        designations = Designation.objects.filter(is_deleted=False)

        if self.q:
            designations = designations.filter(Q(name__istartswith=self.q))

        return designations


@check_mode
@login_required
@college_required
@role_required(['superadmin'])
def create_staff(request):
    current_college = get_current_college(request)
    Qualification_formset = formset_factory(QualificationItemForm,extra=1)
    Experience_Item_formset = formset_factory(ExperienceForm,extra=1)    
    current_batch = get_current_batch(request)


    if request.method == 'POST':
        print("its post")
        form = StaffForm(request.POST,request.FILES)
        qualification_formset_data = Qualification_formset(request.POST,request.FILES,prefix="qualificationform")
        experiance_formset_data = Experience_Item_formset(request.POST,request.FILES,prefix="experianceform")

       
        if form.is_valid() and qualification_formset_data.is_valid() and experiance_formset_data.is_valid() :

            email = form.cleaned_data['email']
            phone = form.cleaned_data['phone']
            print(email)
            print(phone)

            if not Staff.objects.filter(email=email,phone=phone).exists():

                print("all are valid.....................")
                data = form.save(commit=False)
                data.auto_id = get_auto_id(Staff)
                data.a_id = get_a_id(Staff,request)
                # if data.a_id < 10:
                #     data.card_id = "ID00"+str(data.a_id)
                # elif data.a_id <100:
                #     data.card_id = "ID0"+str(data.a_id)
                # else:
                #     data.card_id = "ID"+str(data.a_id)
                data.creator = request.user
                data.updater = request.user
                # data.college = current_college
                # data.batch = current_batch
                data.save()


                for qualification in qualification_formset_data:
                    name = qualification.cleaned_data['name']
                    university = qualification.cleaned_data["university"]
                    institution = qualification.cleaned_data["institution"]
                    year = qualification.cleaned_data["year_of_passing"]
                    mark = qualification.cleaned_data["mark_percentage"]

                    qualification_detailes = Qualification(staff=data, name=name, university=university, institution=institution, year_of_passing=year, mark_percentage=mark)
                    qualification_detailes.save()

                for experience in experiance_formset_data:
                    designation = experience.cleaned_data["designation"]
                    institution = experience.cleaned_data["experience_institution"]
                    join = experience.cleaned_data["date_of_joining"]
                    leaving = experience.cleaned_data["date_of_leaving"]
                    duration = exp_duration(join,leaving)
                    print(duration)
                    year = duration["year"]
                    month = duration["month"]
                    image = experience.cleaned_data["exp_certificate"]

                    experiance_details = Experience(staff=data, designation=designation, experience_institution=institution, date_of_joining=join, date_of_leaving=leaving, exp_certificate=image, duration_year= year, month_duration=month)
                    experiance_details.save()

                return HttpResponseRedirect(reverse('staffs:staff',kwargs={"pk":data.pk}))  

            else:
                response_data = {
                    "status" : "false",
                    "message" : "Already  exist staff",
                } 


        else:
            
            message1 = generate_form_errors(qualification_formset_data,formset=True)
            print("formset1 errors")
            print(message1)
            message2 = generate_form_errors(experiance_formset_data,formset=True)
            print("formset2 errors")
            print(message2)
            print(request.POST)

            form = StaffForm(request.POST)
            qualificationform = Qualification_formset(request.POST,prefix='qualificationform')
            experianceitemform = Experience_Item_formset(request.POST,prefix='experianceform')

            context = {
                "title" : "Staff ",
                "form" : form,
                "Qualificationform" : qualificationform,
                "experianceitemform" : experianceitemform,
                "redirect" : True,

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
                "is_need_animations": True,
            }
            return render(request,'staffs/create_staffs.html',context)


    else:
        form = StaffForm()
        qualificationform = Qualification_formset(prefix='qualificationform')
        experianceitemform = Experience_Item_formset(prefix='experianceform')

        context = {
            "title" : "Enquiry",
            "form" : form,
            "Qualificationform" : qualificationform,
            "experianceitemform" : experianceitemform,
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request,'staffs/create_staffs.html',context)


@check_mode
@login_required
def create_staff_subjects(request,pk):
    staff = Staff.objects.get(pk=pk) 
    print(pk)
    staffsubject_formset = formset_factory(StaffSubjectForm,extra=1)

    if request.method == 'POST':
        staffsubject_formset_data = staffsubject_formset(request.POST,prefix="staffsubjectformset")

        if staffsubject_formset_data.is_valid():
           
            for staffsubject in staffsubject_formset_data:
                division = staffsubject.cleaned_data["division"]
                semester = staffsubject.cleaned_data["semester"]
                subject = staffsubject.cleaned_data["subject"]

                subject_detailes = StaffSubjects(staff=staff, division=division, semester=semester, subject=subject)
                subject_detailes.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Staff Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('staffs:staff',kwargs={'pk':staff.pk})
             }
        else:
            message = generate_form_errors(staffsubject_formset_data,formset=True)

            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript') 

    else: 
        staff_subject = staffsubject_formset(prefix='staffsubjectformset')
        context = {
            "staff_subject" : staff_subject,
            "title" : "Staff Subject",
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
    return render(request,'staffs/staff_subject.html',context)


@check_mode
@login_required
@college_required
@role_required(['superadmin'])
def staffs(request):
    instances = Staff.objects.filter(is_deleted=False)
    title = "Staffs"

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(a_id__icontains=query) | Q(name__icontains=query) | Q(email__icontains=query) | Q(phone__icontains=query) | Q(address__icontains=query))
        title = "Staffs - %s" %query

    context = {
        "instances" : instances,
        'title' : title,
        "redirect" : True,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request,'staffs/staffs.html',context)


@check_mode
@login_required
@college_required
@role_required(['superadmin'])
def staff(request,pk):
    followup = FollowUps.objects.filter(staff=pk, is_deleted=False)
    instance = get_object_or_404(Staff.objects.filter(pk=pk,is_deleted=False))
    batch = get_current_batch(request)
    qualification = Qualification.objects.filter(staff=pk,is_deleted=False)
    experiance = Experience.objects.filter(staff=pk,is_deleted=False)
    subjects = StaffSubjects.objects.filter(staff = pk,is_deleted=False)

    staff_salaries = StaffSalary.objects.filter(is_deleted=False,staff=instance,date__lte=batch.end_date,date__gte=batch.start_date)
    features = Qualification.objects.filter(is_deleted=False)
    followupform = FollowUpsForm()
    interviewmail = InterviewMailForm()

    
    print(followup)

    context = {
        "instance" : instance,
        "title" : "Staff : " + instance.name,
        "features" : features,
        "redirect" : True,
        "staff_salaries" : staff_salaries,
        "qualification":qualification,
        "experiance" : experiance,
        "subjects" : subjects,
        "followupform" : followupform,
        "followup" : followup,
        "interviewmail":interviewmail,

        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'staffs/staff_enquiry.html',context)


@check_mode
@login_required
@college_required
@role_required(['superadmin'])
def edit_staff(request,pk):
    instance = get_object_or_404(Staff.objects.filter(pk=pk,is_deleted=False))
    url = reverse('staffs:edit_staff',kwargs={'pk':pk})

    if Qualification.objects.filter(staff=instance,is_deleted=False).exists():
        extra = 0
    else:
        extra= 1
    QualificationItemForm = inlineformset_factory(
        Staff,
        Qualification,
        extra = extra,
        exclude = ('staff','id'),
        widgets = {
            'name' : TextInput(attrs={'class' : 'required form-control', 'placeholder' : 'Qualification'}),
            'university' : TextInput(attrs={'class' : 'required form-control', 'placeholder' :'University'}),
            'institution' : TextInput(attrs={'class' : 'required form-control', 'placeholder' : 'institution'}),
            "year_of_passing": TextInput(attrs={'class' : 'required form-control', 'placeholder' : 'Year'}),
            'mark_percentage' : TextInput(attrs={'class':'form-control','placeholder' : 'Mark in Percentage'}),
        }
    )

    if Experience.objects.filter(staff=instance,is_deleted=False).exists():
        extra = 0
    else:
        extra= 1
    ExperienceForm = inlineformset_factory(
        Staff,
        Experience,
        extra = extra,
        exclude = ('staff','id'),
        widgets = {
            'experience_institution' : TextInput(attrs={"class":"form-control",'placeholder':'Institution'}),
            'designation' : TextInput(attrs={"class":"form-control",'placeholder':'Designation'}),
            "date_of_joining": TextInput(attrs = {"class": "form-control datepicker-inline","placeholder" : "Date Of Birth",}),
            'date_of_leaving': TextInput(attrs = {"class": "form-control datepicker-inline","placeholder" : "Date of Joining",}),
      }
    )

    if request.method == 'POST':

        response_data = {}
        form = StaffForm(request.POST,request.FILES,instance=instance)

        if form.is_valid():

            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            # data.qualification.clear()
            # features = request.POST.getlist('feature')
            # for feature in features:
            #     if Staff.objects.filter(is_deleted=False,id=feature).exists():
            #         staff = Staff.objects.get(id=feature,is_deleted=False)
            #         data.staffs.add(staff)

            return HttpResponseRedirect(reverse('staffs:staff', kwargs={'pk':data.pk}))
        else:
            message = generate_form_errors(form,formset=False)
            features = Qualification.objects.all()
            form = StaffForm(instance=instance)

            context = {
                "form" : form,
                "title" : "Edit Staff  : " + instance.name,
                "instance" : instance,
                "message" : message,
                "features" : features,
                "url" : reverse('staffs:edit_staff',kwargs={'pk':instance.pk}),
                "redirect" : True,

                "is_edit" : True,
                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
            }
            return render(request, 'staffs/create_staffs.html', context)
    else:

        form = StaffForm(instance=instance)
        experianceform = ExperienceForm(prefix='experianceform',instance=instance)
        qualificationform = QualificationItemForm(prefix='qualificationform',instance=instance)
        print(qualificationform)
        features = Qualification.objects.all()

        context = {
            "form" : form,
            "title" : "Edit Staff  : " + instance.name,
            "instance" : instance,
            "features" : features,
            "url" : reverse('staffs:edit_staff',kwargs={'pk':instance.pk}),
            "redirect" : True,
            "Qualificationform" : qualificationform,
            "experianceitemform" : experianceform,


            "is_edit" : True,
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'staffs/create_staffs.html', context)



# def edit_staff(request,pk):
#     current_college = get_current_college(request)
#     instance = get_object_or_404(Staff.objects.filter(pk=pk,is_deleted=False,college=current_college))
#     if request.method == 'POST':

#         response_data = {}
#         form = StaffForm(request.POST,request.FILES,instance=instance)

#         if form.is_valid():
#             data = form.save(commit=False)
#             data.updater = request.user
#             data.date_updated = datetime.datetime.now()
#             data.save()

#             response_data = {
#                 "status" : "true",
#                 "title" : "Successfully Updated",
#                 "message" : "Staff Successfully Updated.",
#                 "redirect" : "true",
#                 "redirect_url" : reverse('staffs:staff',kwargs={'pk':data.pk})
#             }
#         else:
#             message = generate_form_errors(form,formset=False)

#             response_data = {
#                 "status" : "false",
#                 "stable" : "true",
#                 "title" : "Form validation error",
#                 "message" : message
#             }

#         return HttpResponse(json.dumps(response_data), content_type='application/javascript')

#     else:

#         form = StaffForm(instance=instance,initial={'college':current_college})

#         context = {
#             "form" : form,
#             "title" : "Edit Staff : " + instance.name,
#             "instance" : instance,
#             "is_edit" : True,
#             "url" : reverse('staffs:edit_staff',kwargs={'pk':instance.pk}),
#             "redirect" : True,
#             "is_need_select_picker" : True,
#             "is_need_popup_box" : True,
#             "is_need_custom_scroll_bar" : True,
#             "is_need_wave_effect" : True,
#             "is_need_bootstrap_growl" : True,

#             "is_need_grid_system" : True,
#             "is_need_datetime_picker" : True,
#         }
#         return render(request, 'staffs/create_staff.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_staff(request,pk):
    Staff.objects.filter(pk=pk).update(is_deleted=True)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Staff Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('staffs:staffs')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def delete_selected_staffs(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(Staff.objects.filter(pk=pk,is_deleted=False,college=current_college))
            Staff.objects.filter(pk=pk).update(is_deleted=True)

        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Staff(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('staffs:staffs')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select staffs first.",
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@college_required
@role_required(['superadmin'])
def create_staff_salary(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    SalaryFormset = formset_factory(StaffSalaryForm)

    if request.method == 'POST':
        salary_formset = SalaryFormset(request.POST, prefix='salary_formset')

        if salary_formset.is_valid():
            is_ok = True
            message = ""

            for form in salary_formset:
                staff = form.cleaned_data['staff']
                year = form.cleaned_data['year']
                month = form.cleaned_data['month']

                if StaffSalary.objects.filter(staff=staff,month=month,year=year,college=current_college,staff__is_active=True,is_deleted=False).exists():
                    is_ok = False
                    message += "Staff Salary for %s in %s , %s is already exist. <br />" %(staff,month,year)

            if is_ok:
                for form in salary_formset:
                    staff = form.cleaned_data['staff']
                    year = form.cleaned_data['year']
                    month = form.cleaned_data['month']
                    allowance = form.cleaned_data['allowance']
                    deduction = form.cleaned_data['deduction']

                    auto_id = get_auto_id(StaffSalary)
                    a_id = get_a_id(StaffSalary,request)

                    staff_salaries = 0
                    if StaffSalary.objects.filter(staff=staff,college=current_college,staff__is_active=True,is_deleted=False).exists():
                        staff_salaries = StaffSalary.objects.filter(staff=staff,college=current_college,staff__is_active=True,is_deleted=False).aggregate(amount=Sum('amount')).get("amount",0)

                    salary_payments = 0

                    if Transaction.objects.filter(is_deleted=False).exists():
                        salary_payments = Transaction.objects.filter(staff=staff,transaction_category__name="staff_salary",college=current_college,staff__is_active=True,is_deleted=False).aggregate(amount=Sum('amount')).get("amount",0)
                    additional_payments = 0
                    if salary_payments:
                        additional_payments = salary_payments - staff_salaries
                    if Staff.objects.filter(name=staff,college=current_college,is_active=True,is_deleted=False).exists():
                        instance = Staff.objects.get(name=staff,college=current_college,is_active=True,is_deleted=False)

                    salary=staff.salary
                    amount = salary + allowance - deduction
                    paid_amount = 0
                    balance = amount
                    advance=0

                    if additional_payments > 0:
                        if additional_payments > amount:
                            balance  = 0
                            paid_amount = amount
                        else:
                            paid_amount = additional_payments
                            balance = balance - additional_payments

                    StaffSalary(
                            staff=staff,
                            date = datetime.date.today(),
                            amount=amount,
                            year=year,
                            month=month,
                            balance=balance,
                            advance=advance,
                            allowance=allowance,
                            deduction=deduction,
                            basic_salary=salary,
                            paid=paid_amount,
                            creator=request.user,
                            updater=request.user,
                            auto_id=auto_id,
                            a_id=a_id,
                            college=current_college
                        ).save()

                response_data = {
                    "status" : "true",
                    "title" : "Successfully Created",
                    "message" : "Staff Salary created successfully.",
                    "redirect" : "true",
                    "redirect_url" : reverse('staffs:staff_salaries')
                }

            else:
                response_data = {
                    "status" : "false",
                    "stable" : "true",
                    "title" : "Data Already exist.",
                    "message": message
                }

        else:
            message = generate_form_errors(salary_formset,formset=True)

            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        initial = []

        staffs = Staff.objects.filter(is_deleted=False,college=current_college,is_active=True)
        for staff in staffs:
            staff_salaries = 0
            join_date = staff.date_of_joining
            month = join_date.month
            year = join_date.year

            if StaffSalary.objects.filter(staff=staff,college=current_college,staff__is_active=True,is_deleted=False).exists():
                staff_salaries = StaffSalary.objects.filter(staff=staff,college=current_college,staff__is_active=True,is_deleted=False).aggregate(amount=Sum('amount')).get("amount",0)
                latest = StaffSalary.objects.filter(staff=staff,college=current_college,staff__is_active=True,is_deleted=False).latest('date_added')
                latest_month = latest.month
                latest_year = latest.year
                if int(latest_month) == 12:
                    month = 1
                    year = latest_year + 1
                else:
                    month = int(latest_month) + 1
                    year = latest_year
            basic_salary = staff.salary
            if basic_salary > 0:
                staff_dict = {
                    'staff': staff,
                    'month': month,
                    'year' : year,
                    'basic_salary':int(basic_salary),
                }
                initial.append(staff_dict)
        salary_formset = SalaryFormset(prefix='salary_formset',initial=initial)

        context = {
            "title" : "Create Staff Salary",
            "salary_formset" : salary_formset,
            'redirect' : "true",
            "url" : reverse('staffs:create_staff_salary'),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_formset" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_formset" : True
        }
        return render(request,'staffs/create_staff_salary.html',context)


@check_mode
@login_required
@college_required
@role_required(['superadmin'])
def staff_salaries(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    instances = StaffSalary.objects.filter(is_deleted=False,college=current_college)
    title = "Staff Salaries"

    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    month = request.GET.get("month_filter")
    year = request.GET.get("year")
    query = request.GET.get("q")
    date = request.GET.get("date")
    status = request.GET.get("status")

    if status:
        if status == "balance":
            title = "Staff Salaries: Balance"
            instances = instances.filter(Q(balance__gt=0))

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(staff__name__icontains=query) | Q(date__icontains=query) | Q(amount__icontains=query))
        title = "Staff Salaris - %s" %query

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
        except ValueError:
            date_error = "yes"

    filter_period = False
    if from_date and to_date:
        filter_period = True
    if filter_period:
        from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
        to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)
        instances = instances.filter(Q(date__range=[from_date, to_date]))
    elif month and year:
        instances = instances.filter(date__month=month, date__year=year)
    elif month:
        instances = instances.filter(date__month=month, date__year=today.year)
    elif year:
        instances = instances.filter(Q(date__year=year))


    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_chosen_select" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'staffs/staff_salaries.html',context)


@check_mode
@login_required
@college_required
@role_required(['superadmin'])
def staff_salary(request,pk):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    instance = get_object_or_404(StaffSalary.objects.filter(pk=pk,is_deleted=False,college=current_college))
    context = {
        "instance" : instance,
        "title" : "Staff Salary : " + instance.staff.name,
        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_chosen_select" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'staffs/staff_salary.html',context)


# @check_mode
# @login_required
# @college_required
# @role_required(['superadmin'])
# def edit_staff_salary(request,pk):
#     current_college = get_current_college(request)
#     current_batch = get_current_batch(request)
#     instance = get_object_or_404(StaffSalary.objects.filter(pk=pk,is_deleted=False,college=current_college))
#     staff=instance.staff
#     basic=staff.salary
#     balance = instance.balance
#     paid = instance.paid
#     advance = instance.advance
#     salary_amount = instance.amount
#     allowance = instance.allowance
#     deduction = instance.deduction

#     if request.method == 'POST':

#         response_data = {}
#         form = StaffSalaryForm(request.POST,instance=instance)

#         if form.is_valid():
#             is_ok = True
#             message = ""
#             staff= form.cleaned_data['staff']
#             month = form.cleaned_data['month']
#             year = form.cleaned_data['year']
#             allowance = form.cleaned_data['allowance']
#             deduction=form.cleaned_data['deduction']
#             amount = basic + allowance - deduction
#             if StaffSalary.objects.filter(staff=staff,month=month,year=year,deduction=deduction,allowance=allowance,amount=amount,college=current_college,batch=current_batch,is_deleted=False).exists():
#                 is_ok = False
#                 message += "Staff Salary for %s in %s , %s is already exist. <br />" %(staff,month,year)

#             if is_ok:

#                 if salary_amount != amount:
#                     if paid > 0:
#                         if advance >0:
#                             if (paid+advance) == amount:
#                                 balance = 0
#                                 advance=0
#                         else:
#                             balance = amount - paid
#                     else:
#                         balance = form.cleaned_data['amount']

#                 data = form.save(commit=False)
#                 data.updater = request.user
#                 data.date_updated = datetime.datetime.now()
#                 data.balance = balance
#                 data.advance = advance
#                 data.save()

#                 response_data = {
#                     "status" : "true",
#                     "title" : "Successfully Updated",
#                     "message" : "Staff Salary Successfully Updated.",
#                     "redirect" : "true",
#                     "redirect_url" : reverse('staffs:staff_salary',kwargs={'pk':data.pk})
#                 }
#             else:
#                 response_data = {
#                     "status" : "false",
#                     "stable" : "true",
#                     "title" : "Data Already exist.",
#                     "message": message
#                 }
#         else:
#             message = generate_form_errors(form,formset=False)

#             response_data = {
#                 "status" : "false",
#                 "stable" : "true",
#                 "title" : "Form validation error",
#                 "message" : message
#             }

#         return HttpResponse(json.dumps(response_data), content_type='application/javascript')

#     else:

#         form = StaffSalaryForm(instance=instance,initial={'college':current_college})

#         context = {
#             "form" : form,
#             "title" : "Edit Staff Salary : " + instance.staff.name,
#             "instance" : instance,
#             "is_edit" : True,
#             "is_formset" : True,
#             "url" : reverse('staffs:staff_salary',kwargs={'pk':instance.pk}),
#             "redirect" : True,
#             "is_need_select_picker" : True,
#             "is_need_popup_box" : True,
#             "is_need_custom_scroll_bar" : True,
#             "is_need_wave_effect" : True,
#             "is_need_bootstrap_growl" : True,

#             "is_need_grid_system" : True,
#             "is_need_datetime_picker" : True,
#         }
#         return render(request, 'staffs/edit_staff_salary.html', context)


@check_mode
@login_required
@college_required
@role_required(['superadmin'])
def edit_staff_salary(request,pk):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    instance = get_object_or_404(StaffSalary.objects.filter(pk=pk,is_deleted=False,college=current_college))
    staff=instance.staff
    basic=staff.salary
    balance = instance.balance
    paid = instance.paid
    advance = instance.advance
    salary_amount = instance.amount
    allowance = instance.allowance
    deduction = instance.deduction

    if request.method == 'POST':

        response_data = {}
        form = StaffSalaryForm(request.POST,instance=instance)

        if form.is_valid():
            is_ok = True
            message = ""
            staff= form.cleaned_data['staff']
            month = form.cleaned_data['month']
            year = form.cleaned_data['year']
            allowance = form.cleaned_data['allowance']
            deduction=form.cleaned_data['deduction']
            amount = basic + allowance - deduction
            if StaffSalary.objects.filter(staff=staff,month=month,year=year,deduction=deduction,allowance=allowance,amount=amount,is_deleted=False).exists():
                is_ok = False
                message += "Staff Salary for %s in %s , %s is already exist. <br />" %(staff,month,year)

            if is_ok:

                if salary_amount != amount:
                    if paid > 0:
                        if advance >0:
                            if (paid+advance) == amount:
                                balance = 0
                                advance=0
                        else:
                            balance = amount - paid
                    else:
                        balance = amount

                data = form.save(commit=False)
                data.updater = request.user
                data.date_updated = datetime.datetime.now()
                data.balance = balance
                data.advance = advance
                data.save()

                response_data = {
                    "status" : "true",
                    "title" : "Successfully Updated",
                    "message" : "Staff Salary Successfully Updated.",
                    "redirect" : "true",
                    "redirect_url" : reverse('staffs:staff_salary',kwargs={'pk':data.pk})
                }
            else:
                response_data = {
                    "status" : "false",
                    "stable" : "true",
                    "title" : "Data Already exist.",
                    "message": str(message)
                }
        else:
            print(form.errors)
            message = generate_form_errors(form,formset=False)

            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:

        form = StaffSalaryForm(instance=instance,initial={'college':current_college})

        context = {
            "form" : form,
            "title" : "Edit Staff Salary : " + instance.staff.name,
            "instance" : instance,
            "is_edit" : True,
            "is_formset" : True,
            "url" : reverse('staffs:edit_staff_salary',kwargs={'pk':instance.pk}),
            "redirect" : True,
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'staffs/edit_staff_salary.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_staff_salary(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(StaffSalary.objects.filter(pk=pk,is_deleted=False,college=current_college))
    transactions = Transaction.objects.filter(is_deleted=False,college=current_college)

    for transaction in transactions:
        if transaction.transaction_mode == "cash":
            cash_account = transaction.cash_account
            if cash_account:
                balance = cash_account.balance
                balance = balance + transaction.amount
                CashAccount.objects.filter(pk=transaction.cash_account.pk,college=current_college).update(balance=balance)
        elif transaction.transaction_mode == "bank":
            bank_account = transaction.bank_account
            if bank_account:
                balance = bank_account.balance
                balance = balance + transaction.amount
                BankAccount.objects.filter(pk=transaction.bank_account.pk,college=current_college).update(balance=balance)
        advance = instance.advance
        if advance>0:
            latest_staff_salary = StaffSalary.objects.filter(college=current_college,is_deleted=False,staff=instance.staff).latest('date_added')
            latest_paid = latest_staff_salary.paid
            latest_balance = latest_staff_salary.balance
            salary_amount = latest_paid + latest_balance
            latest_amount = latest_staff_salary.amount
            if (latest_balance < latest_amount) & (salary_amount == latest_amount):
                latest_staff_salary.balance = latest_balance + latest_paid
                latest_staff_salary.paid =latest_paid - advance
                latest_staff_salary.save()
        Transaction.objects.filter(staff_salary=transaction.staff_salary,college=current_college).update(is_deleted=True)

    StaffSalary.objects.filter(pk=pk,college=current_college).update(is_deleted=True,staff=instance.staff)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Staff Salary Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('staffs:staff_salaries')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def delete_selected_staff_salaries(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(StaffSalary.objects.filter(pk=pk,is_deleted=False,college=current_college))

            transactions = Transaction.objects.filter(is_deleted=False,college=current_college,batch=current_batch,staff_salary=instance)
            for transaction in transactions:
                if transaction.transaction_mode == "cash":
                    cash_account = transaction.cash_account
                    if cash_account:
                        balance = cash_account.balance
                        balance = balance + transaction.amount
                        CashAccount.objects.filter(pk=transaction.cash_account.pk,college=current_college,batch=current_batch).update(balance=balance)
                elif transaction.transaction_mode == "bank":
                    bank_account = transaction.bank_account
                    if bank_account:
                        balance = bank_account.balance
                        balance = balance + transaction.amount
                        BankAccount.objects.filter(pk=transaction.bank_account.pk,college=current_college,batch=current_batch).update(balance=balance)
                advance = instance.advance
                if advance>0:
                    latest_staff_salary = StaffSalary.objects.filter(college=current_college,batch=current_batch,is_deleted=False,staff=instance.staff).latest('date_added')
                    latest_paid = latest_staff_salary.paid
                    latest_balance = latest_staff_salary.balance
                    salary_amount = latest_paid + latest_balance
                    latest_amount = latest_staff_salary.amount
                    if (latest_balance < latest_amount) & (salary_amount == latest_amount):
                        latest_staff_salary.balance = latest_balance + latest_paid
                        latest_staff_salary.paid =latest_paid - advance
                        latest_staff_salary.save()
                Transaction.objects.filter(staff_salary=transaction.staff_salary,college=current_college,batch=current_batch).update(is_deleted=True)

            StaffSalary.objects.filter(pk=pk).update(is_deleted=True,staff=instance.staff)

        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Staff Salaries Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('staffs:staff_salaries')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def create_designation(request):
    if request.method == "POST":
        response_data = {}
        form = DesignationForm(data=request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            data.save()

            response_data = {
                'status' : 'true',
                'title' : str(_("Successfully Created")),
                'redirect' : 'true',
                'redirect_url' : reverse('staffs:designation',kwargs={'pk':data.pk}),
                'message' : str(_("Designation Successfully Created"))
            }
        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : _("Form validation error"),
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Create Designation"))
        form = DesignationForm()
        context = {
            "form" : form,
            "title" : title,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request, 'staffs/entry_designation.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_designation(request,pk):
    instance = get_object_or_404(Designation.objects.filter(pk=pk))
    if request.method == "POST":
        response_data = {}
        form = DesignationForm(data=request.POST,instance=instance)
        if form.is_valid():
            data = form.save(commit=False)
            data.save()

            response_data = {
                'status' : 'true',
                'title' : str(_("Successfully Updated")),
                'redirect' : 'true',
                'redirect_url' : reverse('staffs:designation',kwargs={'pk':data.pk}),
                'message' : str(_("Designation Successfully Updated"))
            }
        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : str(_("Form validation error")),
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Edit Designation"))
        form = DesignationForm(instance=instance)
        context = {
            "form" : form,
            "title" : title,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request, 'staffs/entry_designation.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def designations(request):
    instances = Designation.objects.filter(is_deleted=False)
    query = request.GET.get("q")
    if query:
        instances = instances.filter(
                        Q(name__icontains=query)
                    )
        title = "Grade (Query : %s)" %query

    context = {
        'title' : str(_("Designations")),
        'instances' : instances,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'staffs/designations.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def designation(request,pk):
    instance = get_object_or_404(Designation.objects.filter(is_deleted=False,pk=pk))

    context = {
        'title' : instance.name,
        'instance' : instance,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'staffs/designation.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_designation(request,pk):
    instance = get_object_or_404(Designation.objects.filter(pk=pk))
    instance.is_deleted = True
    instance.save()

    response_data = {
        "status" : "true",
        "title" : str(_("Successfully deleted")),
        "message" : str(_("Designation deleted successfully")),
        "redirect" : "true",
        "redirect_url" : reverse('staffs:designations')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def delete_selected_designations(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(Designation.objects.filter(pk=pk,is_deleted=False,college=current_college,batch=current_batch))
            Designation.objects.filter(pk=pk).update(is_deleted=True,name=instance.name)

        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Designation(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('staffs:designations')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def get_salary(request):
    pk = request.GET.get('id')
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    instance = Staff.objects.get(pk=pk,college=current_college,is_deleted=False)
    salary_payments = Transaction.objects.filter(staff=instance,college=current_college,batch=current_batch,is_deleted=False,transaction_category__name="staff_salary")

    payments = 0
    if salary_payments:
        payments = salary_payments.aggregate(amount=Sum('amount')).get("amount",0)

    staff_salaries = StaffSalary.objects.filter(staff=instance,college=current_college,batch=current_batch,is_deleted=False)

    salary_amounts = 0
    if staff_salaries:
        salary_amounts = staff_salaries.aggregate(amount=Sum('amount')).get("amount",0)

    balance = salary_amounts - payments
    if instance:
        response_data = {
            "status" : "true",
            'balance' : str(balance),
        }
    else:
        response_data = {
            "status" : "false",
            "message" : "Balance Error"
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def create_qualification(request):
    if request.method == "POST":
        response_data = {}
        form = QualificationItemForm(data=request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            data.save()

            response_data = {
                'status' : 'true',
                'title' : str(_("Successfully Created")),
                'redirect' : 'true',
                'redirect_url' : reverse('staffs:qualification',kwargs={'pk':data.pk}),
                'message' : str(_("Qualification Successfully Created"))
            }
        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : _("Form validation error"),
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Create Qualification"))
        form = QualificationItemForm()
        context = {
            "form" : form,
            "title" : title,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request, 'staffs/entry_qualification.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_qualification(request,pk):
    instance = get_object_or_404(Qualification.objects.filter(pk=pk))
    if request.method == "POST":
        response_data = {}
        form = QualificationForm(data=request.POST,instance=instance)
        if form.is_valid():
            data = form.save(commit=False)
            data.save()

            response_data = {
                'status' : 'true',
                'title' : str(_("Successfully Updated")),
                'redirect' : 'true',
                'redirect_url' : reverse('staffs:qualification',kwargs={'pk':data.pk}),
                'message' : str(_("Qualification Successfully Updated"))
            }
        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : str(_("Form validation error")),
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Edit Qualification"))
        form = QualificationForm(instance=instance)
        context = {
            "form" : form,
            "title" : title,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request, 'staffs/entry_qualification.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def qualifications(request):
    instances = Qualification.objects.filter(is_deleted=False)
    query = request.GET.get("q")
    if query:
        instances = instances.filter(
                        Q(name__icontains=query)
                    )
        title = "Grade (Query : %s)" %query

    context = {
        'title' : str(_("Qualifications")),
        'instances' : instances,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'staffs/qualifications.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def qualification(request,pk):
    instance = get_object_or_404(Qualification.objects.filter(is_deleted=False,pk=pk))

    context = {
        'title' : instance.name,
        'instance' : instance,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'staffs/qualification.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_qualification(request,pk):
    instance = get_object_or_404(Qualification.objects.filter(pk=pk))
    instance.is_deleted = True
    instance.save()

    response_data = {
        "status" : "true",
        "title" : str(_("Successfully deleted")),
        "message" : str(_("Qualification deleted successfully")),
        "redirect" : "true",
        "redirect_url" : reverse('staffs:qualifications')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_staff_attendance(request):
    college = get_current_college(request)
    today = datetime.date.today()
    last_day = today - datetime.timedelta(days=1)
    second_last_day = today - datetime.timedelta(days=2)

    student_batch = get_current_batch(request)

    staffs = Staff.objects.filter(is_deleted=False)

    AttendanceStaffFormset = formset_factory(AttendanceStaffForm,extra=0)

    if request.method == 'POST':

        attendance_staff_formset = AttendanceStaffFormset(request.POST,prefix='attendance_staff_formset')
        attendance_date_form = AttendanceStaffDateForm(request.POST)

        if attendance_staff_formset.is_valid() and attendance_date_form.is_valid():
            weekday = attendance_date_form.cleaned_data['weekday']
            date = attendance_date_form.cleaned_data['date']

            for staff in attendance_staff_formset:
                reason = ""
                is_genuene = False
                staff_name = staff.cleaned_data['staff']
                is_present = staff.cleaned_data['is_present']
                time = staff.cleaned_data['time']

                if not is_present:
                    reason = staff.cleaned_data['reason']
                    is_genuene = staff.cleaned_data['is_genuene']

                auto_id = get_auto_id(StaffAttendance)
                a_id = get_a_id(StaffAttendance,request)

                absent_days_count = 0
                if not is_present:
                    absent_days_count = 1
                    last_day_attendance = StaffAttendance.objects.filter(date=last_day,is_deleted=False,staff=staff_name)
                    if last_day_attendance:
                        if last_day_attendance[0].is_present == False:
                            previous_absent_days_count = last_day_attendance[0].absent_days_count
                            absent_days_count = previous_absent_days_count + 1

                if not StaffAttendance.objects.filter(date=date,staff=staff_name,is_deleted=False).exists():
                    StaffAttendance(
                        weekday = weekday,
                        date = date,
                        time = time,
                        staff = staff_name,
                        is_present = is_present,
                        absent_days_count = absent_days_count,
                        is_genuene = is_genuene,
                        auto_id = auto_id,
                        a_id = a_id,
                        creator = request.user,
                        updater = request.user,
                        college = college
                    ).save()

                elif not StaffAttendance.objects.filter(date=date,staff=staff_name,is_present=is_present).exists():
                    attendance = StaffAttendance.objects.get(date=date,staff=staff_name)
                    attendance.is_present=is_present
                    attendance.save()

                if is_present == False and is_genuene == False:

                    notification_subject = NotificationSubject.objects.get(code="absent")
                    StaffNotification(
                        subject = notification_subject,
                        user = request.user,
                        staff = staff_name,
                        description = "Absent Today.",
                    ).save()

                    if absent_days_count >= 3:
                        StaffNotification(
                            subject = notification_subject,
                            user = request.user,
                            staff = staff_name,
                            description = "Absent more than three days.",
                        ).save()


            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Attendance created successfully.",
                "redirect" : "true",
                "redirect_url": reverse('staffs:staff_attendances')
            }

        else:
            message1 = generate_form_errors(attendance_staff_formset,formset=True)
            message2 = generate_form_errors(attendance_date_form,formset=False)
            message = str(message1) + str(message2)

            response_data = {
                "status" : "False",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        staff_initial = []
        for staff in staffs:
            today_attendance = None
            is_present = ""
            is_genuene = ""
            reason = ""
            if StaffAttendance.objects.filter(date=today,staff=staff).exists():
                today_attendance = StaffAttendance.objects.filter(date=today,staff=staff)[0]
                is_present = today_attendance.is_present
                is_genuene = today_attendance.is_genuene
                reason = today_attendance.reason
            attendance_staff_dict = {
                'staff' : staff,
                'staff_name' : staff.name,
                'is_present' : is_present,
                'is_genuene' : is_genuene,
                'reason' : reason
            }
            staff_initial.append(attendance_staff_dict)
        weekday_no = today.weekday()
        if weekday_no == 0:
            weekday = "monday"
        elif weekday_no == 1:
            weekday = "tuesday"
        elif weekday_no == 2:
            weekday = "wednesday"
        elif weekday_no == 3:
            weekday = "thursday"
        elif weekday_no == 4:
            weekday = "friday"
        elif weekday_no == 5:
            weekday = "saturday_forenoon"
        elif weekday_no == 6:
            weekday = "sunday_forenoon"

        attendance_staff_formset = AttendanceStaffFormset(prefix='attendance_staff_formset',initial=staff_initial)
        attendance_date_form = AttendanceStaffDateForm(initial={"date":today,"weekday":weekday})
        context = {
            "title" : "Create Attendance",
            "attendance_staff_formset" : attendance_staff_formset,
            "attendance_date_form" : attendance_date_form,
            "url" : reverse('staffs:create_staff_attendance'),
            "today" : today,
            "staffs" : staffs,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request,'staffs/entry_attendance.html',context)


@check_mode
@login_required
def show_calendar_attendances(request):

    today = datetime.datetime.today()
    weekday_no = today.weekday()
    if weekday_no == 0:
        weekday = "monday"
    elif weekday_no == 1:
        weekday = "tuesday"
    elif weekday_no == 2:
        weekday = "wednesday"
    elif weekday_no == 3:
        weekday = "thursday"
    elif weekday_no == 4:
        weekday = "friday"
    elif weekday_no == 5:
        weekday = "saturday"
    elif weekday_no == 6:
        weekday = "sunday"

    context = {
        # "title" : title,
        # "periods" : periods,
        "date" : today.date(),
        "is_need_calender" : True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request, 'staffs/staff_attendance_calender.html', context)


@check_mode
@login_required
def staff_attendances(request):
    batch = get_current_batch(request)
    current_role = get_current_role(request)
    today = datetime.date.today()
    date_string = today
    instances_today = StaffAttendance.objects.filter(is_deleted=False,date=today)
    title = "Staff Attendances - %s " %(today)
    month = today.strftime('%m')
    year = today.strftime('%Y')
    instances = StaffAttendance.objects.none()
    morethan_three_absent_staffs = []
    morethan_three_absent_staffs_count = 0
    behaviour_staffs_count = 0
    date_error = False
    date = request.GET.get("date")
    # tasks = []

    # if Task.objects.filter(is_completed=False,batch=batch,sub_division=sub_division).exists():
    #     task_list = Task.objects.filter(is_completed=False,batch=batch,sub_division=sub_division)
    #     for task in task_list:
    #         tasks.append(task)
    # if Task.objects.filter(is_completed=False,batch=batch,sub_division=None,department=department).exists():
    #     task_list = Task.objects.filter(is_completed=False,batch=batch,sub_division=None,department=department)
    #     for task in task_list:
    #         tasks.append(task)
    # if Task.objects.filter(is_completed=False,batch=batch,sub_division=None,department=None,is_general=True).exists():
    #     task_list = Task.objects.filter(is_completed=False,batch=batch,sub_division=None,department=None,is_general=True)
    #     for task in task_list:
    #         tasks.append(task)
    # print tasks
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
            title = "Staff Attendances - %s" %(date)
            date_string = date
        except ValueError:
            date_error = True
    if not date_error and date:
        instances_today = []
        instances = StaffAttendance.objects.filter(is_deleted=False,date=date_string)

    present_staffs = StaffAttendance.objects.filter(is_deleted=False,is_present=True,date=date_string).count()
    absent_staffs = StaffAttendance.objects.filter(is_deleted=False,is_present=False,date=date_string).count()
    genuene_absent_staffs = StaffAttendance.objects.filter(is_deleted=False,is_present=False,date=date_string,is_genuene=True).count()
    unauthorized_absent_staffs = StaffAttendance.objects.filter(is_deleted=False,is_present=False,date=date_string,is_genuene=False).count()

    administrator = None
    if current_role == "administrator":
        if StaffAttendance.objects.filter(is_deleted=False,is_present=False,date=date_string,is_genuene=False,absent_days_count__gte=3).exists():
            morethan_three_absent_staffs = StaffAttendance.objects.filter(is_deleted=False,is_present=False,date=date_string,is_genuene=False,absent_days_count__gte=3)
            morethan_three_absent_staffs_count = morethan_three_absent_staffs.count()

    present = request.GET.get("present")
    genuene = request.GET.get("genuene")
    if present:
        instances = instances.filter(is_present=present,date=date_string)

    if present and genuene:
        instances = instances.filter(is_present=present,is_genuene=genuene,date=date_string)

    unstudents = request.GET.get("unstudents")
    if unstudents == "yes":
        instances = instances.filter(is_deleted=False,is_present=False,date=date_string,is_genuene=False,absent_days_count__gte=3)

    attendance_dates = instances
    query = request.GET.get("q")
    if query:
        instances = instances_today.filter( Q(staff__name__icontains=query))
        title = "Attendances- %s" %query

    period = request.GET.get("period")
    if period:
        instances = instances.filter(period=period)

    behaviours = request.GET.get("behaviours")
    if behaviours == "yes":
        instances = instances.filter(is_deleted=False,is_present=True,date=date_string).exclude(behaviour=None)

    context = {
        "instances" : instances,
        "instances_today" : instances_today,
        "title" : title,
        "date_string" : date_string,
        "present_staffs" :  present_staffs,
        "behaviour_staffs_count" : behaviour_staffs_count,
        "morethan_three_absent_staffs" : morethan_three_absent_staffs,
        "absent_staffs" : absent_staffs,
        "morethan_three_absent_staffs_count" : morethan_three_absent_staffs_count,
        "genuene_absent_staffs" : genuene_absent_staffs,
        "unstudents" : unstudents,
        "behaviours" : behaviours,
        "unauthorized_absent_staffs" : unauthorized_absent_staffs,

        "is_need_calender" : True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'staffs/staff_attendances.html',context)


@check_mode
@login_required
def staff_attendance(request,pk):
    instance = get_object_or_404(StaffAttendance.objects.filter(pk=pk,is_deleted=False))

    context = {
        "instance" : instance,
        "title" : "Attendance ",

        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'staffs/staff_attendance.html',context)


@check_mode
@login_required
def edit_staff_attendance(request,pk):
    instance = get_object_or_404(StaffAttendance.objects.filter(pk=pk,is_deleted=False))
    url = reverse('staffs:edit_staff_attendance',kwargs={'pk':pk})
    if request.method == "POST":
        previous_attendance_status = instance.is_present
        previous_absent_days_status = instance.absent_days_count

        form = AttendanceEditForm(request.POST,instance=instance)

        if form.is_valid():
            is_present = form.cleaned_data['is_present']
            time = form.cleaned_data['time']
            absent_days_count = previous_absent_days_status
            if previous_attendance_status == True and is_present == False:
                absent_days_count = previous_absent_days_status + 1
            elif previous_attendance_status == False and is_present == True:
                if previous_absent_days_status > 0 :
                    absent_days_count = previous_absent_days_status - 1
                else:
                    absent_days_count =previous_absent_days_status

            #update attendance
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.time = time
            data.absent_days_count = absent_days_count
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Attendance successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('staffs:staff_attendances')
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = AttendanceEditForm(instance=instance)
        context = {
            "title" : "Edit Attendance",
            "form" : form,
            "instance" : instance,
            "url" : url,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request, 'staffs/edit_staff_attendance.html', context)


@check_mode
@login_required
def delete_staff_attendance(request,pk):
    attandence = get_object_or_404(StaffAttendance.objects.filter(pk=pk,is_deleted=False))
    StaffAttendance.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Attendance successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('staffs:staff_attendances')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')

@check_mode
@login_required
def get_subject_details(request):
    division = request.GET.get('id_division')
    batch = request.GET.get('id_batch')
    semester= request.GET.get('id_semester')
    print(semester)
    print(batch)
    print(division)

    if Semester.objects.filter(pk=semester,division=division,academic_year=batch).exists():
        sem = Semester.objects.get(pk=semester,division=division,academic_year=batch)
        semester = sem.semester
        print(semester)

        if Subject.objects.filter(semester=semester,sub_division=division,is_deleted=False):
            subject_name = Subject.objects.filter(semester=semester,sub_division=division)
            sub_list = []           

            for sub in subject_name:
                sub_model = {
                    'id': str(sub.id),
                    'name': sub.name,
                    'code':sub.subject_code,
                }
                sub_list.append(sub_model)

            response_data = {
                "status" : "true",
                "subjects" : sub_list,
            }
        else:
            response_data = {
            "status" : "false",
            "message" : "Subject does not exist",
        }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        response_data = {
            "status" : "false",
            "message" : "Semester not exist",
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')

@check_mode
@login_required
def staff_follow_ups(request,pk):
    staff = Staff.objects.get(pk=pk)
    email = staff.email
    print(email)
    current_college = get_current_college(request)
    print("okkkkkkkkkkkkkkkkkkkkkkkk")
    if request.method == "POST":
        form = FollowUpsForm(request.POST)
        interviewmailform = InterviewMailForm(request.POST)
        print("formok")
        if form.is_valid() and interviewmailform.is_valid():
            print("valid")

            mail_subject = interviewmailform.cleaned_data['subject'] 
            mail_content = interviewmailform.cleaned_data['content']

            if mail_subject:

                mail_data = interviewmailform.save(commit=False)
                auto_id = get_auto_id(InterviewMail)
                mail_data.auto_id = auto_id
                mail_data.staff = staff
                mail_data.creator = request.user
                mail_data.updater = request.user
                mail_data.college = current_college
                a_id = get_a_id(InterviewMail,request)
                mail_data.a_id = a_id
                mail_data.save()

                template_name = 'emails/email.html'
                email = settings.ADMIN_EMAIL
                context = {
                    "request" : request,
                    'email' : email,
                    "subject" : mail_subject,
                    "content" : mail_content
                }
                html_content = render_to_string(template_name,context) 
                send_email(email,subject,content,html_content)


            data = form.save(commit=False)
            auto_id = get_auto_id(FollowUps)
            data.auto_id = auto_id
            data.staff = staff
            data.creator = request.user
            data.updater = request.user
            data.college = current_college
            a_id = get_a_id(FollowUps,request)
            data.a_id = a_id
            data.datetime = datetime.datetime.now()
            data.save()



            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Attendance created successfully.",
                "redirect" : "true",
                "redirect_url": reverse('staffs:staffs')
            }
            return HttpResponse(json.dumps(response_data), content_type='application/javascript')


        else:
            message = generate_form_errors(form)
            print(form.errors)

            response_data = {
                "status" : "False",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')


    else:
        followupform = FollowUpsForm()
        interviewmailform = InterviewMailForm()
        context = {
            "title" : "FollowUp",
            "followupform" : followupform,
            "interviewmail" : interviewmailform,
        } 
    return render(request, 'staffs/staff_enquiry.html', context)







