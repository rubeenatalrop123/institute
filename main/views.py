from django.shortcuts import render, get_object_or_404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, college_required, ajax_required, check_account_balance,permissions_required,role_required
from main.forms import CollegeForm, AcademicYearForm
from main.functions import get_auto_id, generate_form_errors, get_a_id
import json
from main.models import College,CollegeAccess
from users.functions import get_current_college, college_access, get_current_batch
from academics.models import FeeCategory
from finance.models import TransactionCategory,AccountGroup,AccountHead
from users.functions import get_current_college, college_access
from users.models import Profile
from main.models import College,CollegeAccess, Batch
from academics.models import Admission
from staffs.models import Staff
from store.product.models import Product
from django.views.decorators.http import require_GET
from django.core import serializers
from django.contrib.auth.models import Group
from django.db.models import Sum, Q
import datetime


@check_mode
@login_required
@college_required
def app(request):
    return HttpResponseRedirect(reverse('dashboard'))


@check_mode
@login_required
@college_required
def dashboard(request):
    today = datetime.date.today()
    month = today.month
    year = today.year
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    recent_admissions = Admission.objects.filter(is_deleted=False,is_roll_out=False)
    recent_products = Product.objects.filter(is_deleted=False)
    instances = Staff.objects.filter(is_deleted=False)
    admissions_count = Admission.objects.filter(is_deleted=False).count()
    staffs_count = Staff.objects.filter(is_deleted=False).count()
    # students = Student.objects.filter(is_deleted=False,college=current_college)

    # students_no =students.count()
    # staffs_no =staffs.count()

    # recent_students = students.order_by('-date_added')[:9]

    # today_students = students.filter(admission__admission_date__year=today.year, admission__admission_date__month=month, admission__admission_date__day=today.day)
    # today_students_no = today_students.count()

    # month_students = students.filter(admission__admission_date__year=today.year, admission__admission_date__month=month)
    # month_students_no = month_students.count()

    # actives=[]
    # active_batches= Batch.objects.filter(is_deleted=False,college=current_college,is_active=True)
    # for active_batch in active_batches:
    #     active_students = students.filter(batch=active_batch)
    #     active_students_no = active_students.count()
    #     active = {
    #         "active_batch":active_batch,
    #         "active_students_no":active_students_no
    #     }
    #     actives.append(active)

    context = {
        "title" : "Dashboard",
        "instances" : instances,
        "recent_admissions" : recent_admissions,
        "staffs_count" : staffs_count,
        "recent_products" : recent_products,
        # "recent_students" : recent_students,
        # "students_no" : students_no,
        # "staffs_no": staffs_no,
        # "today_students_no" : today_students_no,
        # "month_students_no" : month_students_no,
        # "month_students":month_students,
        "admissions_count" : admissions_count,
        # "today_students" :today_students,
        # "actives":actives,

        "month":month,
        "today": today,
        "year":year,

        "is_need_select_picker" : True,
        "is_need_datetime_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
    }
    return render(request,"base.html",context)


@check_mode
@login_required
def search(request):
    query = request.GET.get("q")
    instances = Admission.objects.filter(is_deleted=False)
    if query:
        instances = instances.filter(
            Q(name__icontains=query) |
            Q(std_phone__icontains=query)|
            Q(address__icontains=query)
        )
    context = {
        "title" : "Search Result",
        'instances' : instances,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'staffs/search.html',context)


@check_mode
@login_required
def create_college(request):
    print("College")
    if request.method == 'POST':
        form = CollegeForm(request.POST,request.FILES)

        if form.is_valid():
            auto_id = get_auto_id(College)

            #create college

            data = form.save(commit=False)
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.save()

            if not FeeCategory.objects.filter(name='admission_fee',college=data).exists():
                fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,name="admission_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

            if not FeeCategory.objects.filter(name='diary_fee',college=data).exists():
                fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,name="diary_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

            if not FeeCategory.objects.filter(name='monthly_fee',college=data).exists():
                fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,fee_type="monthly",name="monthly_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

            if not FeeCategory.objects.filter(name='computer_fee',college=data).exists():
                fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,name="computer_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

            if not FeeCategory.objects.filter(name='library_fee',college=data).exists():
                fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,name="library_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

            if not FeeCategory.objects.filter(name='transportation_fee',college=data).exists():
                fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,name="transportation_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

            if not FeeCategory.objects.filter(name='special_fee',college=data).exists():
                fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,name="special_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

            if not TransactionCategory.objects.filter(name='admission_fee',college=data).exists():
                transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="admission_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

            if not TransactionCategory.objects.filter(name='diary_fee',college=data).exists():
                transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="diary_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

            if not TransactionCategory.objects.filter(name='monthly_fee',college=data).exists():
                transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="monthly_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

            if not TransactionCategory.objects.filter(name='computer_fee',college=data).exists():
                transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="computer_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

            if not TransactionCategory.objects.filter(name='library_fee',college=data).exists():
                transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="library_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

            if not TransactionCategory.objects.filter(name='transportation_fee',college=data).exists():
                transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="transportation_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

            if not TransactionCategory.objects.filter(name='special_fee',college=data).exists():
                transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="special_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

            if not TransactionCategory.objects.filter(name='staff_salary',college=data).exists():
                transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="expense",college=data,name="staff_salary",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

            if not AccountGroup.objects.filter(name='bank_account',college=data).exists():
                AccountGroup.objects.create(
                    is_system_generated=True,
                    is_deleted=False,
                    category_type="asset",
                    name="bank_account",
                    auto_id=get_auto_id(AccountGroup),
                    a_id=get_a_id(AccountGroup,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountGroup.objects.filter(name='capital_account',college=data).exists():
                AccountGroup.objects.create(
                    is_system_generated=True,
                    is_deleted=False,
                    category_type="liability",
                    name="capital_account",
                    auto_id=get_auto_id(AccountGroup),
                    a_id=get_a_id(AccountGroup,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountGroup.objects.filter(name='cash_in_hand',college=data).exists():
                AccountGroup.objects.create(
                    is_system_generated=True,
                    is_deleted=False,
                    category_type="asset",
                    name="cash_in_hand",
                    auto_id=get_auto_id(AccountGroup),
                    a_id=get_a_id(AccountGroup,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountGroup.objects.filter(name='current_asset',college=data).exists():
                AccountGroup.objects.create(
                    is_system_generated=True,
                    is_deleted=False,
                    category_type="asset",
                    name="current_asset",
                    auto_id=get_auto_id(AccountGroup),
                    a_id=get_a_id(AccountGroup,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountGroup.objects.filter(name='current_liability',college=data).exists():
                AccountGroup.objects.create(
                    is_system_generated=True,
                    is_deleted=False,
                    category_type="liability",
                    name="current_liability",
                    auto_id=get_auto_id(AccountGroup),
                    a_id=get_a_id(AccountGroup,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountGroup.objects.filter(name='direct_expense',college=data,is_system_generated=True,is_deleted=False).exists():
                AccountGroup.objects.create(
                    is_system_generated=True,
                    is_deleted=False,
                    category_type="expense",
                    name="direct_expense",
                    auto_id=get_auto_id(AccountGroup),
                    a_id=get_a_id(AccountGroup,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountGroup.objects.filter(name='direct_income',college=data).exists():
                AccountGroup.objects.create(
                    is_system_generated=True,
                    is_deleted=False,
                    category_type="income",
                    name="direct_income",
                    auto_id=get_auto_id(AccountGroup),
                    a_id=get_a_id(AccountGroup,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountGroup.objects.filter(name='fees',college=data,is_system_generated=True,is_deleted=False).exists():
                fee_group = AccountGroup.objects.create(
                                is_system_generated=True,
                                is_deleted=False,
                                category_type="income",
                                name="fees",
                                auto_id=get_auto_id(AccountGroup),
                                a_id=get_a_id(AccountGroup,request),
                                college=data,
                                creator=request.user,
                                updater=request.user
                            )
            else:
                fee_group = AccountGroup.objects.filter(name='fees',college=data,is_system_generated=True,is_deleted=False)[0]

            if not AccountGroup.objects.filter(name='indirect_expense',college=data).exists():
                AccountGroup.objects.create(
                    is_system_generated=True,
                    is_deleted=False,
                    category_type="expense",
                    name="indirect_expense",
                    auto_id=get_auto_id(AccountGroup),
                    a_id=get_a_id(AccountGroup,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountGroup.objects.filter(name='indirect_income',college=data).exists():
                AccountGroup.objects.create(
                    is_system_generated=True,
                    is_deleted=False,
                    category_type="income",
                    name="indirect_income",
                    auto_id=get_auto_id(AccountGroup),
                    a_id=get_a_id(AccountGroup,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountGroup.objects.filter(name='loans_and_liability',college=data).exists():
                AccountGroup.objects.create(
                    is_system_generated=True,
                    is_deleted=False,
                    category_type="liability",
                    name="loans_and_liability",
                    auto_id=get_auto_id(AccountGroup),
                    a_id=get_a_id(AccountGroup,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountGroup.objects.filter(name='fixed_asset',college=data).exists():
                AccountGroup.objects.create(
                    is_system_generated=True,
                    is_deleted=False,
                    category_type="asset",
                    name="fixed_asset",
                    auto_id=get_auto_id(AccountGroup),
                    a_id=get_a_id(AccountGroup,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountGroup.objects.filter(name='salary',college=data,is_system_generated=True,is_deleted=False).exists():
                salary_group = AccountGroup.objects.create(
                                is_system_generated=True,
                                is_deleted=False,
                                category_type="expense",
                                name="salary",
                                auto_id=get_auto_id(AccountGroup),
                                a_id=get_a_id(AccountGroup,request),
                                college=data,
                                creator=request.user,
                                updater=request.user
                            )
            else:
                salary_group = AccountGroup.objects.filter(name='salary',college=data,is_system_generated=True,is_deleted=False)[0]

            if not AccountHead.objects.filter(name='admission_fee',college=data).exists():
                AccountHead.objects.create(
                    is_system_generated=True,
                    account_group = fee_group,
                    is_deleted=False,
                    name="admission_fee",
                    auto_id=get_auto_id(AccountHead),
                    a_id=get_a_id(AccountHead,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountHead.objects.filter(name='transportation_fee',college=data).exists():
                AccountHead.objects.create(
                    is_system_generated=True,
                    account_group = fee_group,
                    is_deleted=False,
                    name="transportation_fee",
                    auto_id=get_auto_id(AccountHead),
                    a_id=get_a_id(AccountHead,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountHead.objects.filter(name='computer_fee',college=data).exists():
                AccountHead.objects.create(
                    is_system_generated=True,
                    account_group = fee_group,
                    is_deleted=False,
                    name="computer_fee",
                    auto_id=get_auto_id(AccountHead),
                    a_id=get_a_id(AccountHead,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountHead.objects.filter(name='special_fee',college=data).exists():
                AccountHead.objects.create(
                    is_system_generated=True,
                    account_group = fee_group,
                    is_deleted=False,
                    name="special_fee",
                    auto_id=get_auto_id(AccountHead),
                    a_id=get_a_id(AccountHead,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountHead.objects.filter(name='exam_fee',college=data).exists():
                AccountHead.objects.create(
                    is_system_generated=True,
                    account_group = fee_group,
                    is_deleted=False,
                    name="exam_fee",
                    auto_id=get_auto_id(AccountHead),
                    a_id=get_a_id(AccountHead,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountHead.objects.filter(name='monthly_fee',college=data).exists():
                AccountHead.objects.create(
                    is_system_generated=True,
                    account_group = fee_group,
                    is_deleted=False,
                    name="monthly_fee",
                    auto_id=get_auto_id(AccountHead),
                    a_id=get_a_id(AccountHead,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountHead.objects.filter(name='library_fee',college=data).exists():
                AccountHead.objects.create(
                    is_system_generated=True,
                    account_group = fee_group,
                    is_deleted=False,
                    name="library_fee",
                    auto_id=get_auto_id(AccountHead),
                    a_id=get_a_id(AccountHead,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountHead.objects.filter(name='pta_fee',college=data).exists():
                AccountHead.objects.create(
                    is_system_generated=True,
                    account_group = fee_group,
                    is_deleted=False,
                    name="pta_fee",
                    auto_id=get_auto_id(AccountHead),
                    a_id=get_a_id(AccountHead,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            if not AccountHead.objects.filter(name='salary',college=data).exists():
                AccountHead.objects.create(
                    is_system_generated=True,
                    account_group = salary_group,
                    is_deleted=False,
                    name="salary",
                    auto_id=get_auto_id(AccountHead),
                    a_id=get_a_id(AccountHead,request),
                    college=data,
                    creator=request.user,
                    updater=request.user
                )

            current_college = data

            group = Group.objects.get(name="staff")
            is_default= True
            if CollegeAccess.objects.filter(user=request.user).exists():
                is_default = False

            CollegeAccess(
                user=request.user,
                college=current_college,
                group=group,
                is_accepted=True,
                is_default=is_default).save()

            request.session["current_college"] = str(data.pk)

            request.session["current_college"] = str(data.pk)
            url = reverse('users:settings') + "?active=profile"
            return HttpResponseRedirect(url)

        else:
            context = {
                "title" : "Create College",
                "form" : form,
                "url" : reverse('create_college'),

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_grid_system" : True,
                "is_need_animations" : True,
                "is_need_datetime_picker" : True,
            }
            return render(request,'users/create_college.html',context)

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = CollegeForm()
        context = {
            "title" : "Create College ",
            "form" : form,
            "url" : reverse('create_college'),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_animations" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'users/create_college.html',context)


@login_required
@ajax_required
@require_GET
@role_required(['superadmin'])
def switch_college(request):
    pk = request.GET.get('pk')
    if College.objects.filter(creator=request.user,pk=pk,is_deleted=False).exists():
        request.session["current_college"] = pk
        response_data = {}
        response_data['status'] = 'true'
        response_data['title'] = "Success"
        response_data['message'] = 'College switched successfully.'
    else:
        response_data = {}
        response_data['status'] = 'false'
        response_data['title'] = "No Access"
        response_data['stable'] = "true"
        response_data['message'] = 'You have no access to this college.'

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def delete_college(request,pk):
    current_college = get_current_college(request)

    College.objects.filter(pk=current_college.pk).update(is_deleted=True)
    request.session["current_college"] = ''

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "College Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('app')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def create_batch(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)

    if request.method == 'POST':
        form = AcademicYearForm(request.POST)

        if form.is_valid():

            auto_id = get_auto_id(Batch)
            a_id = get_a_id(Batch,request)

            data = form.save(commit=False)
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.college = current_college
            data.batch = current_batch
            data.a_id = a_id

            if data.is_active == True:
                    Batch.objects.filter(is_deleted=False).update(is_active=False)

            data.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Batch created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('main:batch',kwargs={'pk':data.pk})
            }

        else:
            message = generate_form_errors(form,formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = AcademicYearForm()
        context = {
            "title" : "Create Batch ",
            "form" : form,
            "url" : reverse('main:create_batch'),
            "redirect" : "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'academics/entry_batch.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def batches(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    instances = Batch.objects.filter(is_deleted=False,college=current_college)
    title = "Batches"

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(a_id__icontains=query) | Q(name__icontains=query) | Q(start_date__icontains=query) | Q(end_date__icontains=query))
        title = "Batches - %s" %query

    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'academics/batches.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def batch(request,pk):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    instance = get_object_or_404(Batch.objects.filter(pk=pk,is_deleted=False,college=current_college))
    context = {
        "instance" : instance,
        "title" : "Batch : " + instance.name,
        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'academics/batch.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_batch(request,pk):
    instance = get_object_or_404(Batch.objects.filter(pk=pk,is_deleted=False))
    if request.method == 'POST':

        response_data = {}
        form = AcademicYearForm(request.POST,instance=instance)

        if form.is_valid():
            is_active = form.cleaned_data['is_active']

            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()

            if is_active == True:
                Batch.objects.filter(is_deleted=False).update(is_active=False)

            data.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Batch Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('main:batch',kwargs={'pk':data.pk})
            }
        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:

        form = AcademicYearForm(instance=instance)

        context = {
            "form" : form,
            "title" : "Edit Batch : " + instance.name,
            "instance" : instance,
            "is_edit" : True,
            "url" : reverse('main:edit_batch',kwargs={'pk':instance.pk}),
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'academics/entry_batch.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_batch(request,pk):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    instance = get_object_or_404(Batch.objects.filter(pk=pk,is_deleted=False,college=current_college))

    Batch.objects.filter(pk=pk,college=current_college).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Batch Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('main:batches')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@login_required
@ajax_required
@require_GET
def switch_theme(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    theme = request.GET.get('pk')
    if theme:
        current_college.theme = theme
        current_thame_data = current_college.theme
        current_college.save()
        response_data = {
            "current_thame_data" : current_thame_data
        }
        response_data['status'] = 'true'
        response_data['title'] = "Success"
        response_data['message'] = 'Theme switched successfully.'
    else:
        response_data = {}
        response_data['status'] = 'false'
        response_data['title'] = "Something Wrong!!!"
        response_data['stable'] = "true"
        response_data['message'] = 'Sorry Try again later.'

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@login_required
@ajax_required
@require_GET
@role_required(['superadmin'])
def switch_batch(request):
    pk = request.GET.get('pk')
    if Batch.objects.filter(pk=pk,is_deleted=False).exists():
        request.session["current_batch"] = pk
        response_data = {}
        response_data['status'] = 'true'
        response_data['title'] = "Success"
        response_data['message'] = 'Batch switched successfully.'
    else:
        response_data = {}
        response_data['status'] = 'false'
        response_data['title'] = "No Access"
        response_data['stable'] = "true"
        response_data['message'] = 'You have no access to this batch.'

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')
