from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from finance.forms import CreditDebitForm, LiabilityForm, AssetForm, AssetCategoryForm, \
    AssetSubCategoryForm, LiabilityCategoryForm, LiabilitySubCategoryForm,OtherFeePaymentForm,PaymentForm, AccountGroupForm,FeeTransactionForm,BankAccountForm,CashAccountForm
from django.urls import reverse
from finance.models import Transaction, AccountHead, AccountGroup,BankAccount,CashAccount
from academics.models import Admission,StudentFee,FeeCategory
from staffs.models import Staff,StaffSalary
from django.http.response import HttpResponse, HttpResponseRedirect
import json
from finance.models import  Asset, Journel,TransactionCategory,Fee, LiabilityCategory, Liability, LiabilitySubCategory, AssetCategory, AssetSubCategory
from users.functions import get_current_college, college_access
from main.functions import get_auto_id, generate_form_errors, get_current_role, generate_unique_id, get_current_batch, get_current_batch, get_admn_no, get_a_id
from users.functions import get_current_college, college_access
from finance.forms import AccountHeadForm,SalaryPaymentForm, DateForm, CompareForm,LedgerTransactionForm, TransactionBasicForm,FeeForm, LedgerForm, AccountHeadForm, FeeForm, FeePaymentForm, TransactionForm
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from main.decorators import check_mode, ajax_required, role_required
from django.core import serializers
from django.db.models import Sum
from decimal import Decimal
from calendar import monthrange
from dal import autocomplete
import datetime


@check_mode
@login_required
def dashboard(request):
    return HttpResponseRedirect(reverse('finance:transactions'))


class LiabilitySubCategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = LiabilitySubCategory.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))
        return items


class LiabilityAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = Liability.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))
        return items


class LiabilityCategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = LiabilityCategory.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))
        return items


class AssetSubCategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = AssetSubCategory.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))
        return items


class AssetCategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = AssetCategory.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))
            print(items)
        return items


class AccountHeadAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = AccountHead.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))
            print(items)
        return items


class AccountGroupAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = AccountGroup.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))
            print(items)
        return items




@check_mode
@login_required
@role_required(['superadmin'])
def create_fee_payment(request,pk):  
    instance = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False))
    month_fees = StudentFee.objects.filter(admission=instance,is_deleted=False,fee_category__name="monthly_fee")
    monthly_fee_amount = month_fees[0].amount  
    print("monthly_fee_amount") 
    current_college = get_current_college(request)
    today = datetime.date.today()
    if request.method == "POST":
        transaction_form = FeeTransactionForm(request.POST)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False)

        if transaction_form.is_valid(): 
            
            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']

            monthly_fees =request.POST.getlist('monthly_fee')
            month_names =request.POST.getlist('month_name')
            tution_fees = zip(monthly_fees,month_names)
            amount_paying =request.POST.get('amount_paying')
            discount =request.POST.get('discount')
            amount = Decimal(amount_paying) - Decimal(discount)
            actual_amount = amount
            if monthly_fees:
                if TransactionCategory.objects.filter(name="monthly_fee",is_deleted=False,is_system_generated=True,college=current_college).exists():
                    transaction_category = TransactionCategory.objects.get(name="monthly_fee",is_deleted=False,is_system_generated=True,college=current_college)
                if FeeCategory.objects.filter(name="monthly_fee",is_deleted=False,college=current_college).exists():
                    fee_category = FeeCategory.objects.get(name="monthly_fee",is_deleted=False,college=current_college)
                if AccountHead.objects.filter(name="monthly_fee",is_deleted=False,is_system_generated=True,college=current_college).exists():
                    account_head = AccountHead.objects.get(name="monthly_fee",is_deleted=False,is_system_generated=True,college=current_college)
                #create fee_payment
                if tution_fees:
                    for tution_fee in tution_fees:
                        student_fee = None
                        student_fee = StudentFee.objects.get(pk=tution_fee[0],is_deleted=False,admission=instance)
                        auto_id = get_auto_id(Transaction)
                        if transaction_mode == "cash":
                            cash_account = transaction_form.cleaned_data['cash_account']
                            cash_balance = cash_account.balance
                            new_balance = cash_balance + actual_amount
                            CashAccount.objects.filter(pk=cash_account.pk).update(balance=new_balance)
                            if student_fee.balance > amount:
                                balance = amount
                                student_fee.paid = student_fee.paid + amount
                                student_fee.balance = Decimal(student_fee.amount) - Decimal(student_fee.paid)
                                student_fee.save()
                            else:
                                balance = student_fee.balance
                                student_fee.paid = student_fee.paid + student_fee.balance
                                amount = Decimal(amount) - Decimal(student_fee.balance)
                                student_fee.balance = 0                
                                student_fee.save()

                            transaction = Transaction.objects.create(
                                        auto_id = auto_id,
                                        creator = request.user,
                                        updater = request.user,
                                        transaction_type = "income",
                                        transaction_category = transaction_category,
                                        payment_to = "cash_account",
                                        cash_account = cash_account,
                                        payment_mode = None,
                                        bank_account = None,
                                        card_details = None,
                                        cheque_details = None,
                                        is_cheque_withdrawed = False,
                                        admission = instance,
                                        date = datetime.datetime.now(),
                                        amount = balance,
                                        month = student_fee.month,
                                        fee_category = fee_category,
                                        a_id = get_a_id(BankAccount,request),
                                        college = current_college,
                                        account_head = account_head
                                    )
                            cash_debit = balance
                            bank_debit = 0

                            Journel.objects.create(
                                date = datetime.date.today(),
                                cash_debit = cash_debit,
                                bank_debit = bank_debit,
                                transaction = transaction,
                                income = balance
                            )
                        elif transaction_mode == "bank":
                            bank_account = transaction_form.cleaned_data['bank_account']
                            bank_balance = bank_account.balance
                            new_balance = bank_balance + actual_amount
                            BankAccount.objects.filter(pk=bank_account.pk).update(balance=new_balance)
                            payment_mode = transaction_form.cleaned_data['payment_mode'] 
                            if student_fee.balance > amount:                                
                                balance = amount
                                student_fee.paid = student_fee.paid + amount
                                student_fee.balance = Decimal(student_fee.amount) - Decimal(student_fee.paid)
                                student_fee.save()                                    
                            else:
                                balance = student_fee.balance
                                student_fee.paid = student_fee.paid + student_fee.balance
                                amount = Decimal(amount) - Decimal(student_fee.balance)
                                student_fee.balance = 0                
                                student_fee.save()

                            if payment_mode == "cheque_payment":
                                is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed']
                                cheque_details = transaction_form.cleaned_data['cheque_details']
                                transaction = Transaction.objects.create(
                                        auto_id = auto_id,
                                        creator = request.user,
                                        updater = request.user,
                                        transaction_type = "income",
                                        transaction_category = transaction_category,
                                        transaction_mode = "bank",
                                        payment_to = "bank_account",
                                        bank_account = bank_account,
                                        payment_mode = "cheque_payment",
                                        cash_account = None,
                                        card_details = None,
                                        cheque_details = cheque_details,
                                        is_cheque_withdrawed = is_cheque_withdrawed,
                                        admission = instance,
                                        date = datetime.datetime.now(),
                                        amount = balance,
                                        month = student_fee.month,
                                        fee_category = fee_category,
                                        a_id = get_a_id(BankAccount,request),
                                        college = current_college,
                                        account_head = account_head
                                    )
                                cash_debit = 0
                                bank_debit = balance

                                Journel.objects.create(
                                    date = datetime.date.today(),
                                    cash_debit = cash_debit,
                                    bank_debit = bank_debit,
                                    transaction = transaction,
                                    income = balance
                                )
                            elif payment_mode == "internet_banking":
                                transaction = Transaction.objects.create(
                                        auto_id = auto_id,
                                        creator = request.user,
                                        updater = request.user,
                                        transaction_type = "income",
                                        transaction_category = transaction_category,
                                        payment_to = "bank_account",                                        
                                        transaction_mode = "bank",
                                        bank_account = bank_account,
                                        payment_mode = "internet_banking",
                                        cash_account = None,
                                        card_details = None,
                                        cheque_details = None,
                                        is_cheque_withdrawed = False,
                                        admission = instance,
                                        date = datetime.datetime.now(),
                                        amount = balance,
                                        month = student_fee.month,
                                        fee_category = fee_category,
                                        a_id = get_a_id(BankAccount,request),
                                        college = current_college,
                                        account_head = account_head
                                    ) 
                                cash_debit = 0
                                bank_debit = balance

                                Journel.objects.create(
                                    date = datetime.date.today(),
                                    cash_debit = cash_debit,
                                    bank_debit = bank_debit,
                                    transaction = transaction,
                                    income = balance
                                )  
                            elif payment_mode == "card_payment":
                                card_details = transaction_form.cleaned_data['card_details']
                                transaction = Transaction.objects.create(
                                        auto_id = auto_id,
                                        creator = request.user,
                                        updater = request.user,
                                        transaction_type = "income",
                                        transaction_category = transaction_category,
                                        payment_to = "bank_account",
                                        transaction_mode = "bank",
                                        bank_account = bank_account,
                                        payment_mode = "card_payment",
                                        cash_account = None,
                                        card_details = card_details,
                                        cheque_details = None,
                                        is_cheque_withdrawed = False,
                                        admission = instance,
                                        date = datetime.datetime.now(),
                                        amount = balance,
                                        month = student_fee.month,
                                        fee_category = fee_category,
                                        a_id = get_a_id(BankAccount,request),
                                        college = current_college,
                                        account_head = account_head
                                    )   

                                cash_debit = 0
                                bank_debit = balance

                                Journel.objects.create(
                                    date = datetime.date.today(),
                                    cash_debit = cash_debit,
                                    bank_debit = bank_debit,
                                    transaction = transaction,
                                    income = balance
                                )
                            
                instance.balance = instance.balance - actual_amount
                instance.paid = instance.paid + actual_amount
                instance.save()                  

            response_data = {
                'status' : 'true',     
                'title' : "Fee Payment Created",       
                'redirect' : 'true', 
                'redirect_url' : reverse('academics:admission',kwargs={'pk':pk} ),
                'message' : "Fee Payment Created Successfully"
            }
                    
        else:           
            message = generate_form_errors(transaction_form,formset=False) 
            print(transaction_form.errors)     
            response_data = {
                "status" : 'false',
                "title" : "Form Validation Error" ,
                "stable" : "true",
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        transaction_form = FeeTransactionForm() 
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False)
        context = {
            "transaction_form" : transaction_form,
            "monthly_fees" : month_fees,
            "fee_title" : "Monthly Fee",
            "monthly_fee_amount" : monthly_fee_amount,
            "title" : "Create Monthly Fee Payment",            
            "url" : reverse('finance:create_fee_payment',kwargs={'pk':pk}),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'finance/entry_fee_payment.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def create_monthly_fee_payment(request,pk):  
    instance = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False))

    month_fees = StudentFee.objects.filter(admission=instance,is_deleted=False,fee_category__name="monthly_fee")
    monthly_fee_amount = month_fees[0].amount 
    current_college = get_current_college(request)
    today = datetime.date.today()
    if request.method == "POST":
        transaction_form = FeeTransactionForm(request.POST)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False)
        payment_form = PaymentForm(request.POST)
        if transaction_form.is_valid() and payment_form.is_valid(): 
            
            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']

            no_of_month = payment_form.cleaned_data['no_of_month']
            amount_paying = payment_form.cleaned_data['amount_paying']

            # monthly_fees =request.POST.getlist('monthly_fee')
            # month_names =request.POST.getlist('month_name')
            # tution_fees = zip(monthly_fees,month_names)
            # amount_paying =request.POST.get('amount_paying')
            # discount =request.POST.get('discount')
            # amount = Decimal(amount_paying) - Decimal(discount)
            # actual_amount = amount
            amount = Decimal(amount_paying)
            actual_amount = Decimal(amount_paying)
            monthly_fees = []
            for i in range(int(no_of_month)):
                monthly_fee = StudentFee.objects.filter(admission=instance,balance__gt=0,is_deleted=False,fee_category__name="monthly_fee")[i]
                monthly_fees.append(monthly_fee)
            if monthly_fees:
                if TransactionCategory.objects.filter(name="monthly_fee",is_deleted=False,is_system_generated=True,college=current_college).exists():
                    transaction_category = TransactionCategory.objects.get(name="monthly_fee",is_deleted=False,is_system_generated=True,college=current_college)
                if FeeCategory.objects.filter(name="monthly_fee",is_deleted=False,college=current_college).exists():
                    fee_category = FeeCategory.objects.get(name="monthly_fee",is_deleted=False,college=current_college)
                if AccountHead.objects.filter(name="monthly_fee",is_deleted=False,is_system_generated=True,college=current_college).exists():
                    account_head = AccountHead.objects.get(name="monthly_fee",is_deleted=False,is_system_generated=True,college=current_college)
               
                #create fee_payment
                
                for student_fee in monthly_fees:
                    auto_id = get_auto_id(Transaction)
                    if transaction_mode == "cash":
                        cash_account = transaction_form.cleaned_data['cash_account']
                        cash_balance = cash_account.balance
                        new_balance = cash_balance + Decimal(actual_amount)
                        CashAccount.objects.filter(pk=cash_account.pk).update(balance=new_balance)
                        if student_fee.balance > amount:
                            balance = amount
                            student_fee.paid = student_fee.paid + amount
                            student_fee.balance = Decimal(student_fee.amount) - Decimal(student_fee.paid)
                            student_fee.save()
                        else:
                            balance = student_fee.balance
                            student_fee.paid = student_fee.paid + student_fee.balance
                            amount = Decimal(amount) - Decimal(student_fee.balance)
                            student_fee.balance = 0                
                            student_fee.save()

                        transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "income",
                                    transaction_category = transaction_category,
                                    payment_to = "cash_account",
                                    cash_account = cash_account,
                                    payment_mode = None,
                                    bank_account = None,
                                    card_details = None,
                                    cheque_details = None,
                                    is_cheque_withdrawed = False,
                                    admission = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    fee_category = fee_category,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                )
                        cash_debit = balance
                        bank_debit = 0

                        Journel.objects.create(
                            date = datetime.date.today(),
                            cash_debit = cash_debit,
                            bank_debit = bank_debit,
                            transaction = transaction,
                            income = balance
                        )
                    elif transaction_mode == "bank":
                        bank_account = transaction_form.cleaned_data['bank_account']
                        bank_balance = bank_account.balance
                        new_balance = bank_balance + actual_amount
                        BankAccount.objects.filter(pk=bank_account.pk).update(balance=new_balance)
                        payment_mode = transaction_form.cleaned_data['payment_mode'] 
                        if student_fee.balance > amount:                                
                            balance = amount
                            student_fee.paid = student_fee.paid + amount
                            student_fee.balance = Decimal(student_fee.amount) - Decimal(student_fee.paid)
                            student_fee.save()                                    
                        else:
                            balance = student_fee.balance
                            student_fee.paid = student_fee.paid + student_fee.balance
                            amount = Decimal(amount) - Decimal(student_fee.balance)
                            student_fee.balance = 0                
                            student_fee.save()

                        if payment_mode == "cheque_payment":
                            is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed']
                            cheque_details = transaction_form.cleaned_data['cheque_details']
                            transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "income",
                                    transaction_category = transaction_category,
                                    transaction_mode = "bank",
                                    payment_to = "bank_account",
                                    bank_account = bank_account,
                                    payment_mode = "cheque_payment",
                                    cash_account = None,
                                    card_details = None,
                                    cheque_details = cheque_details,
                                    is_cheque_withdrawed = is_cheque_withdrawed,
                                    admission = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    fee_category = fee_category,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                )
                            cash_debit = 0
                            bank_debit = balance

                            Journel.objects.create(
                                date = datetime.date.today(),
                                cash_debit = cash_debit,
                                bank_debit = bank_debit,
                                transaction = transaction,
                                income = balance
                            )
                        elif payment_mode == "internet_banking":
                            transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "income",
                                    transaction_category = transaction_category,
                                    payment_to = "bank_account",                                        
                                    transaction_mode = "bank",
                                    bank_account = bank_account,
                                    payment_mode = "internet_banking",
                                    cash_account = None,
                                    card_details = None,
                                    cheque_details = None,
                                    is_cheque_withdrawed = False,
                                    admission = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    fee_category = fee_category,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                ) 
                            cash_debit = 0
                            bank_debit = balance

                            Journel.objects.create(
                                date = datetime.date.today(),
                                cash_debit = cash_debit,
                                bank_debit = bank_debit,
                                transaction = transaction,
                                income = balance
                            )  
                        elif payment_mode == "card_payment":
                            card_details = transaction_form.cleaned_data['card_details']
                            transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "income",
                                    transaction_category = transaction_category,
                                    payment_to = "bank_account",
                                    transaction_mode = "bank",
                                    bank_account = bank_account,
                                    payment_mode = "card_payment",
                                    cash_account = None,
                                    card_details = card_details,
                                    cheque_details = None,
                                    is_cheque_withdrawed = False,
                                    admission = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    fee_category = fee_category,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,                                    
                                    account_head = account_head
                                )   

                            cash_debit = 0
                            bank_debit = balance

                            Journel.objects.create(
                                date = datetime.date.today(),
                                cash_debit = cash_debit,
                                bank_debit = bank_debit,
                                transaction = transaction,
                                income = balance
                            )
                            
                instance.balance = instance.balance - actual_amount
                instance.paid = instance.paid + actual_amount
                instance.save()                  

            response_data = {
                'status' : 'true',     
                'title' : "Fee Payment Created",       
                'redirect' : 'true', 
                'redirect_url' : reverse('academics:admission',kwargs={'pk':pk} ),
                'message' : "Fee Payment Created Successfully"
            }
                    
        else:           
            message = generate_form_errors(transaction_form,formset=False) 
            response_data = {
                "status" : 'false',
                "title" : "Form Validation Error" ,
                "stable" : "true",
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        transaction_form = FeeTransactionForm() 
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False)
        payment_form = PaymentForm(initial={'total_amount': monthly_fee_amount})
        context = {
            "transaction_form" : transaction_form,
            "monthly_fees" : month_fees,
            "instance" : instance,
            "fee_title" : "Monthly Fee",
            "monthly_fee_amount" : monthly_fee_amount,
            "title" : "Create Monthly Fee Payment", 
            "payment_form" : payment_form,           
            "url" : reverse('finance:create_monthly_fee_payment',kwargs={'pk':pk}),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'finance/entry_monthly_payment.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def create_transport_fee_payment(request,pk):  
    instance = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False))
    month_fees = StudentFee.objects.filter(admission=instance,is_deleted=False,fee_category__name="transportation_fee")
    if month_fees:
        monthly_fee_amount = month_fees[0].amount    
    current_college = get_current_college(request)
    today = datetime.date.today()
    if request.method == "POST":
        transaction_form = FeeTransactionForm(request.POST)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False)
        payment_form = PaymentForm(request.POST)
        if transaction_form.is_valid() and payment_form.is_valid(): 
            
            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']

            no_of_month = payment_form.cleaned_data['no_of_month']
            amount_paying = payment_form.cleaned_data['amount_paying']

            # monthly_fees =request.POST.getlist('monthly_fee')
            # month_names =request.POST.getlist('month_name')
            # tution_fees = zip(monthly_fees,month_names)
            # amount_paying =request.POST.get('amount_paying')
            # discount =request.POST.get('discount')
            # amount = Decimal(amount_paying) - Decimal(discount)
            # actual_amount = amount
            amount = Decimal(amount_paying)
            actual_amount = Decimal(amount_paying)
            monthly_fees = []
            for i in range(int(no_of_month)):
                monthly_fee = StudentFee.objects.filter(admission=instance,balance__gt=0,is_deleted=False,fee_category__name="transportation_fee")[i]
                monthly_fees.append(monthly_fee)
            if monthly_fees:
                if TransactionCategory.objects.filter(name="transportation_fee",is_deleted=False,is_system_generated=True,college=current_college).exists():
                    transaction_category = TransactionCategory.objects.get(name="transportation_fee",is_deleted=False,college=current_college,is_system_generated=True)
                if FeeCategory.objects.filter(name="transportation_fee",is_deleted=False,college=current_college).exists():
                    fee_category = FeeCategory.objects.get(name="transportation_fee",is_deleted=False,college=current_college)
                if AccountHead.objects.filter(name="transportation_fee",is_deleted=False,is_system_generated=True,college=current_college).exists():
                    account_head = AccountHead.objects.get(name="transportation_fee",is_deleted=False,is_system_generated=True,college=current_college)
                
                #create fee_payment
                
                for student_fee in monthly_fees:
                    auto_id = get_auto_id(Transaction)
                    if transaction_mode == "cash":
                        cash_account = transaction_form.cleaned_data['cash_account']
                        cash_balance = cash_account.balance
                        new_balance = cash_balance + Decimal(actual_amount)
                        CashAccount.objects.filter(pk=cash_account.pk).update(balance=new_balance)
                        if student_fee.balance > amount:
                            balance = amount
                            student_fee.paid = student_fee.paid + amount
                            student_fee.balance = Decimal(student_fee.amount) - Decimal(student_fee.paid)
                            student_fee.save()
                        else:
                            balance = student_fee.balance
                            student_fee.paid = student_fee.paid + student_fee.balance
                            amount = Decimal(amount) - Decimal(student_fee.balance)
                            student_fee.balance = 0                
                            student_fee.save()

                        transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "income",
                                    transaction_category = transaction_category,
                                    payment_to = "cash_account",
                                    cash_account = cash_account,
                                    payment_mode = None,
                                    bank_account = None,
                                    card_details = None,
                                    cheque_details = None,
                                    is_cheque_withdrawed = False,
                                    admission = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    fee_category = fee_category,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                )
                        cash_debit = balance
                        bank_debit = 0

                        Journel.objects.create(
                            date = datetime.date.today(),
                            cash_debit = cash_debit,
                            bank_debit = bank_debit,
                            transaction = transaction,
                            income = balance
                        )
                    elif transaction_mode == "bank":
                        bank_account = transaction_form.cleaned_data['bank_account']
                        bank_balance = bank_account.balance
                        new_balance = bank_balance + actual_amount
                        BankAccount.objects.filter(pk=bank_account.pk).update(balance=new_balance)
                        payment_mode = transaction_form.cleaned_data['payment_mode'] 
                        if student_fee.balance > amount:                                
                            balance = amount
                            student_fee.paid = student_fee.paid + amount
                            student_fee.balance = Decimal(student_fee.amount) - Decimal(student_fee.paid)
                            student_fee.save()                                    
                        else:
                            balance = student_fee.balance
                            student_fee.paid = student_fee.paid + student_fee.balance
                            amount = Decimal(amount) - Decimal(student_fee.balance)
                            student_fee.balance = 0                
                            student_fee.save()

                        if payment_mode == "cheque_payment":
                            is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed']
                            cheque_details = transaction_form.cleaned_data['cheque_details']
                            transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "income",
                                    transaction_category = transaction_category,
                                    transaction_mode = "bank",
                                    payment_to = "bank_account",
                                    bank_account = bank_account,
                                    payment_mode = "cheque_payment",
                                    cash_account = None,
                                    card_details = None,
                                    cheque_details = cheque_details,
                                    is_cheque_withdrawed = is_cheque_withdrawed,
                                    admission = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    fee_category = fee_category,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                )
                            cash_debit = 0
                            bank_debit = balance

                            Journel.objects.create(
                                date = datetime.date.today(),
                                cash_debit = cash_debit,
                                bank_debit = bank_debit,
                                transaction = transaction,
                                income = balance
                            )
                        elif payment_mode == "internet_banking":
                            transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "income",
                                    transaction_category = transaction_category,
                                    payment_to = "bank_account",                                        
                                    transaction_mode = "bank",
                                    bank_account = bank_account,
                                    payment_mode = "internet_banking",
                                    cash_account = None,
                                    card_details = None,
                                    cheque_details = None,
                                    is_cheque_withdrawed = False,
                                    admission = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    fee_category = fee_category,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                ) 
                            cash_debit = 0
                            bank_debit = balance

                            Journel.objects.create(
                                date = datetime.date.today(),
                                cash_debit = cash_debit,
                                bank_debit = bank_debit,
                                transaction = transaction,
                                income = balance
                            )  
                        elif payment_mode == "card_payment":
                            card_details = transaction_form.cleaned_data['card_details']
                            transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "income",
                                    transaction_category = transaction_category,
                                    payment_to = "bank_account",
                                    transaction_mode = "bank",
                                    bank_account = bank_account,
                                    payment_mode = "card_payment",
                                    cash_account = None,
                                    card_details = card_details,
                                    cheque_details = None,
                                    is_cheque_withdrawed = False,
                                    admission = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    fee_category = fee_category,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                )   

                            cash_debit = 0
                            bank_debit = balance

                            Journel.objects.create(
                                date = datetime.date.today(),
                                cash_debit = cash_debit,
                                bank_debit = bank_debit,
                                transaction = transaction,
                                income = balance
                            )
                            
                instance.balance = instance.balance - actual_amount
                instance.paid = instance.paid + actual_amount
                instance.save()                  

            response_data = {
                'status' : 'true',     
                'title' : "Fee Payment Created",       
                'redirect' : 'true', 
                'redirect_url' : reverse('academics:admission',kwargs={'pk':pk} ),
                'message' : "Fee Payment Created Successfully"
            }
                    
        else:           
            message = generate_form_errors(transaction_form,formset=False) 
            print(transaction_form.errors)     
            response_data = {
                "status" : 'false',
                "title" : "Form Validation Error" ,
                "stable" : "true",
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        if month_fees:
            transaction_form = FeeTransactionForm() 
            transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False)
            transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False)
            payment_form = PaymentForm()
            context = {
                "transaction_form" : transaction_form,
                "monthly_fees" : month_fees,
                "fee_title" : "Transportation Fee",
                "title" : "Create Transportation Fee Payment", 
                "payment_form" : payment_form,           
                "url" : reverse('finance:create_transport_fee_payment',kwargs={'pk':pk}),

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
                "is_need_animations": True,
            }
        else:
            context = {
                "monthly_fees" : month_fees,
                "fee_title" : "Transportation Fee",
                "title" : "Create Transportation Fee Payment",        
                "url" : reverse('finance:create_monthly_fee_payment',kwargs={'pk':pk}),

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
                "is_need_animations": True,
            }
        return render(request, 'finance/entry_monthly_payment.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def create_special_fee_payment(request,pk):   
    instance = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False))
    special_fee = instance.department.special_fee
    month_fees = StudentFee.objects.filter(admission=instance,is_deleted=False,fee_category__name="special_fee")
    if month_fees:
        monthly_fee_amount = month_fees[0].amount    
    current_college = get_current_college(request)
    today = datetime.date.today()
    if request.method == "POST":
        transaction_form = FeeTransactionForm(request.POST)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False)
        payment_form = PaymentForm(request.POST)
        if transaction_form.is_valid() and payment_form.is_valid(): 
            
            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']

            no_of_month = payment_form.cleaned_data['no_of_month']
            amount_paying = payment_form.cleaned_data['amount_paying']

            # monthly_fees =request.POST.getlist('monthly_fee')
            # month_names =request.POST.getlist('month_name')
            # tution_fees = zip(monthly_fees,month_names)
            # amount_paying =request.POST.get('amount_paying')
            # discount =request.POST.get('discount')
            # amount = Decimal(amount_paying) - Decimal(discount)
            # actual_amount = amount
            amount = Decimal(amount_paying)
            actual_amount = Decimal(amount_paying)
            monthly_fees = []
            for i in range(int(no_of_month)):
                monthly_fee = StudentFee.objects.filter(admission=instance,balance__gt=0,is_deleted=False,fee_category__name="special_fee")[i]
                monthly_fees.append(monthly_fee)
            if monthly_fees:
                if TransactionCategory.objects.filter(name="special_fee",is_deleted=False,is_system_generated=True,college=current_college).exists():
                    transaction_category = TransactionCategory.objects.get(name="special_fee",is_deleted=False,is_system_generated=True,college=current_college)
                if FeeCategory.objects.filter(name="special_fee",is_deleted=False,college=current_college).exists():
                    fee_category = FeeCategory.objects.get(name="special_fee",is_deleted=False,college=current_college)
                if AccountHead.objects.filter(name="special_fee",is_deleted=False,is_system_generated=True,college=current_college).exists():
                    account_head = AccountHead.objects.get(name="special_fee",is_deleted=False,is_system_generated=True,college=current_college)
               
                #create fee_payment
                
                for student_fee in monthly_fees:
                    auto_id = get_auto_id(Transaction)
                    if transaction_mode == "cash":
                        cash_account = transaction_form.cleaned_data['cash_account']
                        cash_balance = cash_account.balance
                        new_balance = cash_balance + Decimal(actual_amount)
                        CashAccount.objects.filter(pk=cash_account.pk).update(balance=new_balance)
                        if student_fee.balance > amount:
                            balance = amount
                            student_fee.paid = student_fee.paid + amount
                            student_fee.balance = Decimal(student_fee.amount) - Decimal(student_fee.paid)
                            student_fee.save()
                        else:
                            balance = student_fee.balance
                            student_fee.paid = student_fee.paid + student_fee.balance
                            amount = Decimal(amount) - Decimal(student_fee.balance)
                            student_fee.balance = 0                
                            student_fee.save()

                        transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "income",
                                    transaction_category = transaction_category,
                                    payment_to = "cash_account",
                                    cash_account = cash_account,
                                    payment_mode = None,
                                    bank_account = None,
                                    card_details = None,
                                    cheque_details = None,
                                    is_cheque_withdrawed = False,
                                    admission = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    fee_category = fee_category,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                )
                        cash_debit = balance
                        bank_debit = 0

                        Journel.objects.create(
                            date = datetime.date.today(),
                            cash_debit = cash_debit,
                            bank_debit = bank_debit,
                            transaction = transaction,
                            income = balance
                        )
                    elif transaction_mode == "bank":
                        bank_account = transaction_form.cleaned_data['bank_account']
                        bank_balance = bank_account.balance
                        new_balance = bank_balance + actual_amount
                        BankAccount.objects.filter(pk=bank_account.pk).update(balance=new_balance)
                        payment_mode = transaction_form.cleaned_data['payment_mode'] 
                        if student_fee.balance > amount:                                
                            balance = amount
                            student_fee.paid = student_fee.paid + amount
                            student_fee.balance = Decimal(student_fee.amount) - Decimal(student_fee.paid)
                            student_fee.save()                                    
                        else:
                            balance = student_fee.balance
                            student_fee.paid = student_fee.paid + student_fee.balance
                            amount = Decimal(amount) - Decimal(student_fee.balance)
                            student_fee.balance = 0                
                            student_fee.save()

                        if payment_mode == "cheque_payment":
                            is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed']
                            cheque_details = transaction_form.cleaned_data['cheque_details']
                            transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "income",
                                    transaction_category = transaction_category,
                                    transaction_mode = "bank",
                                    payment_to = "bank_account",
                                    bank_account = bank_account,
                                    payment_mode = "cheque_payment",
                                    cash_account = None,
                                    card_details = None,
                                    cheque_details = cheque_details,
                                    is_cheque_withdrawed = is_cheque_withdrawed,
                                    admission = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    fee_category = fee_category,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                )
                            cash_debit = 0
                            bank_debit = balance

                            Journel.objects.create(
                                date = datetime.date.today(),
                                cash_debit = cash_debit,
                                bank_debit = bank_debit,
                                transaction = transaction,
                                income = balance
                            )
                        elif payment_mode == "internet_banking":
                            transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "income",
                                    transaction_category = transaction_category,
                                    payment_to = "bank_account",                                        
                                    transaction_mode = "bank",
                                    bank_account = bank_account,
                                    payment_mode = "internet_banking",
                                    cash_account = None,
                                    card_details = None,
                                    cheque_details = None,
                                    is_cheque_withdrawed = False,
                                    admission = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    fee_category = fee_category,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                ) 
                            cash_debit = 0
                            bank_debit = balance

                            Journel.objects.create(
                                date = datetime.date.today(),
                                cash_debit = cash_debit,
                                bank_debit = bank_debit,
                                transaction = transaction,
                                income = balance
                            )  
                        elif payment_mode == "card_payment":
                            card_details = transaction_form.cleaned_data['card_details']
                            transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "income",
                                    transaction_category = transaction_category,
                                    payment_to = "bank_account",
                                    transaction_mode = "bank",
                                    bank_account = bank_account,
                                    payment_mode = "card_payment",
                                    cash_account = None,
                                    card_details = card_details,
                                    cheque_details = None,
                                    is_cheque_withdrawed = False,
                                    admission = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    fee_category = fee_category,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                )   

                            cash_debit = 0
                            bank_debit = balance

                            Journel.objects.create(
                                date = datetime.date.today(),
                                cash_debit = cash_debit,
                                bank_debit = bank_debit,
                                transaction = transaction,
                                income = balance
                            )
                            
                instance.balance = instance.balance - actual_amount
                instance.paid = instance.paid + actual_amount
                instance.save()                  

            response_data = {
                'status' : 'true',     
                'title' : "Fee Payment Created",       
                'redirect' : 'true', 
                'redirect_url' : reverse('academics:admission',kwargs={'pk':pk} ),
                'message' : "Fee Payment Created Successfully"
            }
                    
        else:           
            message = generate_form_errors(transaction_form,formset=False) 
            print(transaction_form.errors)     
            response_data = {
                "status" : 'false',
                "title" : "Form Validation Error" ,
                "stable" : "true",
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        if month_fees:
            transaction_form = FeeTransactionForm() 
            transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False)
            transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False)
            payment_form = PaymentForm(initial={'total_amount':special_fee})
            context = {
                "transaction_form" : transaction_form,
                "monthly_fees" : month_fees,
                "fee_title" : "Special Fee",
                "title" : "Create Special Fee Payment", 
                "payment_form" : payment_form,           
                "url" : reverse('finance:create_special_fee_payment',kwargs={'pk':pk}),

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
                "is_need_animations": True,
            }
        else:
            context = {
                "monthly_fees" : month_fees,
                "fee_title" : "Special Fee",
                "title" : "Create Special Fee Payment",        
                "url" : reverse('finance:create_monthly_fee_payment',kwargs={'pk':pk}),

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
                "is_need_animations": True,
            }
        return render(request, 'finance/entry_monthly_payment.html', context)


    
@check_mode
@login_required
@role_required(['superadmin'])
def edit_fee_payment(request,pk): 
    instance = get_object_or_404(Transaction.objects.filter(pk=pk,is_deleted=False))

    admission = instance.admission
    edit_amount = instance.amount

    if request.method == "POST":
        form = FeePaymentForm(request.POST,instance=instance)
        
        if form.is_valid(): 
            
            amount = form.cleaned_data['amount']
            #Update fee payment
            data = form.save(commit=False)                                                                                                                             
            balance = instance.balance
            paid = instance.paid
            fee_amount = admission.fee
            if edit_amount != data.amount:    
                if paid == edit_amount:
                    paid = data.amount
                    balance = fee_amount-data.amount
                else:
                    paid = paid-edit_amount + data.amount
                    balance = fee_amount- paid

            diff_amount = edit_amount - amount
            data.balance += diff_amount

            new_transactions = Transaction.objects.filter(admission=admission,is_deleted=False,date_added__gt=instance.date_added)
            for new_transaction in new_transactions:
                new_transaction.balance += diff_amount
                new_transaction.save()
            cash_balance = data.cash_account.balance
            bank_balance = data.bank_account.balance   
            if transaction_mode == "cash":
                if instance2 == None :
                    cash_balance = cash_balance + amount
                    bank_balance = bank_balance - amount
                else :
                    cash_balance = cash_balance - edit_amount +amount
                BankAccount.objects.filter(pk=data.bank_account.pk).update(balance=bank_balance)
                CashAccount.objects.filter(pk=data.cash_account.pk).update(balance=cash_balance)
                
                data.payment_mode = None
                data.payment_to = "cash_account"
                data.bank_account = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
                
            elif transaction_mode == "bank":
                if instance3 == None :
                    cash_balance = cash_balance - amount
                    bank_balance = bank_balance + amount
                else :
                    bank_balance = bank_balance - edit_amount +amount
                BankAccount.objects.filter(pk=data.bank_account.pk).update(balance=bank_balance)
                CashAccount.objects.filter(pk=data.cash_account.pk).update(balance=cash_balance)
                
                payment_mode = form.cleaned_data['payment_mode'] 
                if payment_mode == "cheque_payment":
                    data.card_details = None
                        
                elif payment_mode == "internet_banking":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False
                
                elif payment_mode == "card_payment":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False
            
                if payment_to == "cash_account":
                    data.bank_account = None
                elif payment_to == "bank_account":
                    data.cash_account = None

            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()
            
            response_data = {
                "status" : 'true',
                "title" : "Successfully Updated" ,
                "redirect" : "true",
                "redirect_url" : reverse('finance:transaction', kwargs = {'pk' : data.pk}),
                "message" : "Fee payment Successfully Updated."
            }
        else:
            
            message = ''            
            message += generate_form_errors(form,formset=False) 
                  
            response_data = {
                "status" : 'false',
                "title" : "Form Validation Error" ,
                "stable" : "true",
                "message" : message,
            }
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = FeePaymentForm(instance=instance)
        transaction_form =TransactionForm(instance=instance)
        context = {
            "form" : form,
            "transaction_form" : transaction_form,
            "title" : "Edit Fee Payment",
            'instance' : instance,
            "is_edit" : True,
            "url" : reverse('finance:edit_fee_payment', kwargs={'pk':pk}),

            "payment_page" : True,
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'finance/edit_fee_payment.html', context)
  

@check_mode
@login_required
@role_required(['superadmin'])
def fee_payments(request):
    instances = Transaction.objects.filter(is_deleted=False,transaction_type="income")

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(student__name__icontains=query) | Q(date__icontains=query) | Q(amount__icontains=query))
    
    date = request.GET.get("date")
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
            instances = instances.filter(date__date=date)
        except ValueError:
            date_error = True
   
    title = "Fee Payments"
    context = {
        'title' : title,
        "instances" : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_chosen_select" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'finance/fee_payments.html',context) 


@check_mode
@login_required
@role_required(['superadmin'])
def print_fee_slip(request,pk):
    instance = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False))
    fee_payments = StudentFee.objects.filter(admission=instance,is_deleted=False)
    today = datetime.date.today()
    total_amount = 0
    if fee_payments:
        total_amount = fee_payments.aggregate(paid=Sum('paid')).get('paid',0.00)

    reports = []
    department=""
    sub_division = ""
    department = instance.department
    sub_division = instance.sub_division
            
    context = {
        'title' : "Fee Slip",
        "fee_payments" : fee_payments,
        "instance" : instance,
        "today" : today,
        "total_amount" : total_amount,
        "department" : department,
        "sub_division" : sub_division,
    }
    return render(request, 'finance/receipt.html', context) 


@check_mode
@login_required
@role_required(['superadmin'])
def create_other_fee_payment(request,pk):
    url = reverse('finance:create_other_fee_payment',kwargs={'pk':pk})
    admission = get_object_or_404(Admission.objects.filter(pk=pk,is_deleted=False))
    current_college = get_current_college(request)
    if request.method == "POST":
        
        transaction_form = FeeTransactionForm(request.POST)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False)
        amount = request.POST.getlist('balance')
        student_fee = request.POST.getlist('student_fee')
        total_amount = 0
        pairing_amount_admission = zip(amount,student_fee)
        if transaction_form.is_valid(): 
            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']

            for item in pairing_amount_admission:
                amount = Decimal(item[0])
                fee_category = item[1]
                if StudentFee.objects.filter(pk=fee_category,admission=admission,is_deleted=False).exists():
                    student_fee = StudentFee.objects.get(pk=fee_category,admission=admission,is_deleted=False)

                    student_fee.paid = student_fee.paid + amount
                    student_fee.balance = student_fee.balance - amount
                    student_fee.save()

                    total_amount += amount
                    transaction_category = None

                    if TransactionCategory.objects.filter(name=student_fee.fee_category.name,is_deleted=False,college=current_college).exists():
                        transaction_category = TransactionCategory.objects.filter(name=student_fee.fee_category.name,is_deleted=False,college=current_college)[0]
                    if AccountHead.objects.filter(name=student_fee.fee_category.name,is_deleted=False,college=current_college).exists():
                        account_head = AccountHead.objects.filter(name=student_fee.fee_category.name,is_deleted=False,college=current_college)[0]
                    auto_id = get_auto_id(Transaction)

                    if transaction_mode == "cash":
                        cash_account = transaction_form.cleaned_data['cash_account']
                        cash_balance = cash_account.balance
                        new_balance = cash_balance + total_amount
                        CashAccount.objects.filter(pk=cash_account.pk).update(balance=new_balance)
                        if student_fee.balance > amount:
                            balance = amount
                            student_fee.paid = student_fee.paid + amount
                            student_fee.balance = Decimal(student_fee.amount) - Decimal(student_fee.paid)
                            student_fee.save()
                        else:
                            balance = student_fee.balance
                            student_fee.paid = student_fee.paid + student_fee.balance
                            amount = Decimal(amount) - Decimal(student_fee.balance)
                            student_fee.balance = 0                
                            student_fee.save()

                        transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "income",
                                    transaction_category = transaction_category,
                                    payment_to = "cash_account",
                                    cash_account = cash_account,
                                    payment_mode = None,
                                    bank_account = None,
                                    card_details = None,
                                    cheque_details = None,
                                    is_cheque_withdrawed = False,
                                    admission = admission,
                                    date = datetime.datetime.now(),
                                    amount = amount,
                                    fee_category = student_fee.fee_category,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                )
                        cash_debit = amount
                        bank_debit = 0

                        Journel.objects.create(
                            date = datetime.date.today(),
                            cash_debit = cash_debit,
                            bank_debit = bank_debit,
                            transaction = transaction,
                            income = amount
                        )

                    elif transaction_mode == "bank":
                            bank_account = transaction_form.cleaned_data['bank_account']
                            bank_balance = bank_account.balance
                            new_balance = bank_balance + total_amount
                            BankAccount.objects.filter(pk=bank_account.pk).update(balance=new_balance)
                            payment_mode = transaction_form.cleaned_data['payment_mode'] 
                            if student_fee.balance > amount:                                
                                balance = amount
                                student_fee.paid = student_fee.paid + amount
                                student_fee.balance = Decimal(student_fee.amount) - Decimal(student_fee.paid)
                                student_fee.save()                                    
                            else:
                                balance = student_fee.balance
                                student_fee.paid = student_fee.paid + student_fee.balance
                                amount = Decimal(amount) - Decimal(student_fee.balance)
                                student_fee.balance = 0                
                                student_fee.save()

                            if payment_mode == "cheque_payment":
                                is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed']
                                cheque_details = transaction_form.cleaned_data['cheque_details']
                                transaction = Transaction.objects.create(
                                        auto_id = auto_id,
                                        creator = request.user,
                                        updater = request.user,
                                        transaction_type = "income",
                                        transaction_category = transaction_category,
                                        transaction_mode = "bank",
                                        payment_to = "bank_account",
                                        bank_account = bank_account,
                                        payment_mode = "cheque_payment",
                                        cash_account = None,
                                        card_details = None,
                                        cheque_details = cheque_details,
                                        is_cheque_withdrawed = is_cheque_withdrawed,
                                        admission = admission,
                                        date = datetime.datetime.now(),
                                        amount = amount,
                                        fee_category = student_fee.fee_category,
                                        a_id = get_a_id(BankAccount,request),
                                        college = current_college,
                                        account_head = account_head
                                    )

                                cash_debit = 0
                                bank_debit = amount

                                Journel.objects.create(
                                    date = datetime.date.today(),
                                    cash_debit = cash_debit,
                                    bank_debit = bank_debit,
                                    transaction = transaction,
                                    income = amount
                                )

                            elif payment_mode == "internet_banking":
                                transaction = Transaction.objects.create(
                                        auto_id = auto_id,
                                        creator = request.user,
                                        updater = request.user,
                                        transaction_type = "income",
                                        transaction_category = transaction_category,
                                        payment_to = "bank_account",                                        
                                        transaction_mode = "bank",
                                        bank_account = bank_account,
                                        payment_mode = "internet_banking",
                                        cash_account = None,
                                        card_details = None,
                                        cheque_details = None,
                                        is_cheque_withdrawed = False,
                                        admission = admission,
                                        date = datetime.datetime.now(),
                                        amount = amount,
                                        fee_category = student_fee.fee_category,
                                        a_id = get_a_id(BankAccount,request),
                                        college = current_college,
                                        account_head = account_head
                                    ) 
                                cash_debit = 0
                                bank_debit = amount

                                Journel.objects.create(
                                    date = datetime.date.today(),
                                    cash_debit = cash_debit,
                                    bank_debit = bank_debit,
                                    transaction = transaction,
                                    income = amount
                                )  

                            elif payment_mode == "card_payment":
                                card_details = transaction_form.cleaned_data['card_details']
                                transaction = Transaction.objects.create(
                                        auto_id = auto_id,
                                        creator = request.user,
                                        updater = request.user,
                                        transaction_type = "income",
                                        transaction_category = transaction_category,
                                        payment_to = "bank_account",
                                        transaction_mode = "bank",
                                        bank_account = bank_account,
                                        payment_mode = "card_payment",
                                        cash_account = None,
                                        card_details = card_details,
                                        cheque_details = None,
                                        is_cheque_withdrawed = False,
                                        admission = admission,
                                        date = datetime.datetime.now(),
                                        amount = amount,
                                        fee_category = student_fee.fee_category,
                                        a_id = get_a_id(BankAccount,request),
                                        college = current_college,
                                        account_head = account_head
                                    )   
                                cash_debit = 0
                                bank_debit = amount

                                Journel.objects.create(
                                    date = datetime.date.today(),
                                    cash_debit = cash_debit,
                                    bank_debit = bank_debit,
                                    transaction = transaction,
                                    income = amount
                                )

            admission.balance = admission.balance - total_amount
            admission.paid = admission.paid + total_amount
            admission.save()

            response_data = {
                "status": "true",
                "title": "Successfully created",
                "message": "other fee payment successfully created.",
                "redirect": "true",
                "redirect_url": reverse('academics:admission',kwargs={'pk':admission.pk})
            }

            return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = OtherFeePaymentForm()
        fees = ["monthly_fee","special_fee","transportation_fee"]
        form.fields['fee_category'].queryset = FeeCategory.objects.filter(is_deleted=False).exclude(name__in=fees)
        transaction_form = FeeTransactionForm()
        transaction_form.fields['fee_category'].queryset = FeeCategory.objects.filter(is_deleted=False).exclude(name__in=fees)

        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False)
        student_fees = StudentFee.objects.filter(admission=admission).exclude(fee_category__name__in=fees)
        context = {
            "title" : "Create Other Fee Payment",
            "form" : form,
            "transaction_form" : transaction_form,

            "url" : url,
            "admission" : admission,
            "student_fees" : student_fees,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'finance/entry_other_fee.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_other_fee_payment(request,pk):
    instance = get_object_or_404(Transaction.objects.filter(pk=pk,is_deleted=False))
    url = reverse('finance:edit_other_fee_payment',kwargs={'pk':pk})
    today = datetime.date.today()
    admission = instance.admission
    edit_amount = instance.amount
    edit_fee_category = instance.fee_category
    if request.method == "POST":

        form = OtherFeePaymentForm(request.POST,instance=instance)
        if form.is_valid(): 
            fee_category = form.cleaned_data['fee_category']
            amount = form.cleaned_data['amount']
            #update other fee payment
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()
            if amount != edit_amount:
                if StudentFee.objects.filter(fee_category=fee_category,admission=admission,is_deleted=False).exists():
                    student_fee = StudentFee.objects.get(fee_category=fee_category,admission=admission,is_deleted=False)
                    student_fee.paid = student_fee.paid - edit_amount + amount
                    student_fee.balance = student_fee.balance + edit_amount - amount
                    student_fee.save()


            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Other Fee Payment successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('academics:admission',kwargs={"pk":admission.pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = OtherFeePaymentForm(instance=instance)
        fees = ["monthly_fee","transportation_fee"]
        form.fields['fee_category'].queryset = FeeCategory.objects.filter(is_deleted=False).exclude(name__in=fees)
        transaction_form = FeeTransactionForm()

        context = {
            "transaction_form" : transaction_form,
            "title" : "Edit other fee payment",
            "form" : form,
            "instance" : instance,
            "url" : url,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'finance/entry_other_fee.html', context)


@check_mode
@login_required
def create_account_group(request):   
    current_college = get_current_college(request)

    if request.method == "POST":
        
        form = AccountGroupForm(request.POST)

        if form.is_valid():
            auto_id = get_auto_id(AccountGroup)

            # create Account Group
            data = form.save(commit=False)
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.college = current_college
            data.a_id = get_a_id(AccountGroup,request)
            data.save()        

            response_data = {
                "status": "true",
                "title": "Successfully created",
                "message": "Account Group Successfully Created.",
                "redirect": "true",
                "redirect_url": reverse('finance:account_group',kwargs={'pk': data.pk})
            }

        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = AccountGroupForm()

        context = {
            "form": form,
            "title": "Create Account Group",
            "redirect": True,

            "is_need_select_picker": True,
            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_chosen_select": True,
            "is_need_grid_system": True,
            "is_need_datetime_picker": True,
        }
        return render(request, 'finance/entry_account_groups.html', context)


@check_mode
@login_required
def edit_account_group(request, pk):
    instance = get_object_or_404(AccountGroup.objects.filter(pk=pk))

    if request.method == "POST":
        response_data = {}
        form = AccountGroupForm(request.POST, instance=instance)

        if form.is_valid():
            name = form.cleaned_data['name'].strip()
            category_type = form.cleaned_data['category_type']

            # update Account Group
            data = form.save(commit=False)
            data.name = name.strip()
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data['status'] = 'true'
            response_data['title'] = "Successfully Updated"
            response_data['redirect'] = 'true'
            response_data['redirect_url'] = reverse(
                'finance:account_group', kwargs={'pk': data.pk})
            response_data['message'] = "Account Group Successfully Updated."

        else:
            response_data['status'] = 'false'
            response_data['stable'] = 'true'
            response_data['title'] = "Form validation error"

            message = ''
            message += generate_form_errors(form, formset=False)
            response_data['message'] = message

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = AccountGroupForm(instance=instance)

        context = {
            "form": form,
            "title": "Edit Account Group : " + instance.name,
            "instance": instance,
            "redirect": True,

            "is_need_select_picker": True,
            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_chosen_select": True,
            "is_need_grid_system": True,
            "is_need_datetime_picker": True,
        }
        return render(request, 'finance/entry_account_groups.html', context)


@check_mode
@login_required
def account_groups(request):
    instances = AccountGroup.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(
            Q(name__icontains=query) | Q(category_type__icontains=query))

    title = "Transaction Categories"
    context = {
        'title': title,
        "instances": instances,
        "redirect": True,

        "is_need_select_picker": True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_chosen_select": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
    }
    return render(request, 'finance/account_groups.html', context)


@check_mode
@login_required
def account_group(request,pk):
    instance = get_object_or_404(AccountGroup.objects.filter(pk=pk))
    context = {
        "instance": instance,
        "title": "Account Group : " + instance.name,
        "single_page": True,
        "redirect": True,

        "is_need_select_picker": True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_chosen_select": True,
        "is_need_grid_system": True,
        "is_need_datetime_picker": True,
    }
    return render(request, 'finance/account_group.html', context)


@check_mode
@ajax_required
@login_required
def delete_account_group(request, pk):
    instance = get_object_or_404(
        AccountGroup.objects.filter(pk=pk, is_deleted=False))

    AccountGroup.objects.filter(pk=pk).update(
        is_deleted=True, name=instance.name + "_deleted")

    response_data = {
        "status": "true",
        "title": "Successfully Deleted",
        "message": "Account Group Successfully Deleted.",
        "redirect": "true",
        "redirect_url": reverse('finance:account_groups')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
def delete_selected_account_groups(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(
                AccountGroup.objects.filter(pk=pk, is_deleted=False))
            AccountGroup.objects.filter(pk=pk).update(
                is_deleted=True, name=instance.name + "_deleted_" + str(instance.auto_id))

        response_data = {
            "status": "true",
            "title": "Successfully Deleted",
            "message": "Selected Account Group(s) Successfully Deleted.",
            "redirect": "true",
            "redirect_url": reverse('finance:account_groups')
        }
    else:
        response_data = {
            "status": "false",
            "title": "Nothing selected",
            "message": "Please select some transaction categories first.",
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_income(request):
    current_college = get_current_college(request)
    if request.method == "POST":
        response_data = {}
        form = TransactionBasicForm(request.POST)
        form.fields['account_head'].queryset = AccountHead.objects.filter(
            Q(account_group__category_type="asset", is_deleted=False) | Q(account_group__category_type="income", is_deleted=False,college=current_college))

        if form.is_valid():

            transaction_mode = form.cleaned_data['transaction_mode']
            print(transaction_mode)
            account_head = form.cleaned_data['account_head']
            amount = form.cleaned_data['amount']

            # create income
            data = form.save(commit=False)

            if transaction_mode == "cash":
                data.payment_mode = None
                data.bank_account = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
            elif transaction_mode == "bank":

                payment_mode = form.cleaned_data['payment_mode']
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = form.cleaned_data['is_cheque_withdrawed']
                    data.card_details = None
                    if not is_cheque_withdrawed:
                        data.payment_to = None

                elif payment_mode == "internet_banking":
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False

                elif payment_mode == "card_payment":
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False

            data.creator = request.user
            data.updater = request.user
            data.college = current_college
            data.a_id = get_a_id(Transaction,request)
            data.auto_id = get_auto_id(Transaction)
            data.save()            

            if transaction_mode == "cash":
                cash_debit = amount
                bank_debit = 0
            elif transaction_mode == "bank":
                cash_debit = 0
                bank_debit = amount

            Journel.objects.create(
                date = datetime.date.today(),
                cash_debit = cash_debit,
                bank_debit = bank_debit,
                transaction = data,
                income = amount
            )
            response_data = {
                'status': 'true',
                'title': "Income Created",
                'redirect': 'true',
                'redirect_url': reverse('finance:transaction', kwargs={'pk': data.pk}),
                'message': "Income Created Successfully"
            }
        else:
            message = generate_form_errors(form, formset=False)
            x = message[0]
            response_data = {
                "status": "false",
                "title": "Form Validation Error",
                "message": str(x)
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = TransactionBasicForm()
        form.fields['account_head'].queryset = AccountHead.objects.filter(
            Q(account_group__category_type="asset", is_deleted=False) | Q(account_group__category_type="income", is_deleted=False,college=current_college))
        context = {
            "form": form,
            "title": "Create Income",
            "redirect": True,
            "redirect_url": reverse('finance:transactions'),
            "payment_page": True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'finance/entry_income.html', context)


@check_mode
@login_required
def edit_income(request, pk):
    instance = get_object_or_404(Transaction.objects.filter(pk=pk))

    if request.method == "POST":
        form = TransactionBasicForm(request.POST, instance=instance)
        form.fields['account_head'].queryset = AccountHead.objects.filter(
            Q(account_group__category_type="asset", is_deleted=False) | Q(account_group__category_type="income", is_deleted=False))

        if form.is_valid():
            transaction_mode = form.cleaned_data['transaction_mode']

            account_head = form.cleaned_data['account_head']
            amount = form.cleaned_data['amount']

            # Update income
            data = form.save(commit=False)

            if transaction_mode == "cash":

                data.payment_mode = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False

            elif transaction_mode == "bank":

                payment_mode = form.cleaned_data['payment_mode']
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = form.cleaned_data['is_cheque_withdrawed']
                    data.card_details = None

                elif payment_mode == "internet_banking":
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False

                elif payment_mode == "card_payment":
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False

            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                'status': 'true',
                'title': "Income Updated",
                'redirect': 'true',
                'redirect_url': reverse('finance:transaction', kwargs={'pk': data.pk}),
                'message': "Income Updated Successfully"
            }
        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status": "false",
                "title": " Error !",
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = TransactionBasicForm(instance=instance)
        form.fields['account_head'].queryset = AccountHead.objects.filter(
            Q(account_group__category_type="asset", is_deleted=False) | Q(account_group__category_type="income", is_deleted=False))

        context = {
            "form": form,
            "title": "Edit Income",
            "instance": instance,
            "redirect": True,

            "payment_page": True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'finance/entry_income.html', context)


@check_mode
@login_required
def transactions(request):
    today = datetime.date.today()
    year = request.GET.get('year')
    month = request.GET.get('month')
    period = request.GET.get('period')
    date = request.GET.get('date')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    transaction_type = request.GET.get('transaction_type')
    title = "Transactions"
    instances = Transaction.objects.filter(is_deleted=False)

    account_heads = AccountHead.objects.filter(is_deleted=False)
    account_groups = AccountGroup.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(
            Q(transaction_type__icontains=query) | Q(date__icontains=query))

    account_head = request.GET.get("account_head")
    if account_head:
        instances = instances.filter(Q(account_head__pk=account_head))

    transaction_type = request.GET.get("transaction_type")
    if transaction_type == "income":
        instances = instances.filter(transaction_type=transaction_type)
        title += " (Transaction Type : Income)"
    elif transaction_type == "expense":
        instances = instances.filter(transaction_type=transaction_type)
        title += " (Transaction Type : Expense)"

    filter_date = date
    filter_from_date = from_date
    filter_to_date = to_date

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
        except ValueError:
            date_error = "yes"

    if year:
        instances = instances.filter(date__year=year)

    if month:
        instances = instances.filter(date__month=month)

    # filter by date created range

    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(
                from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(
                to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)
        except ValueError:
            date_error = "yes"

        filter_date_period = True

    if period:
        if period == "year":
            title = "Transactions : This Year"
            instances = instances.filter(date__year=today.year)
        elif period == 'month':
            title = "Transactions : This Month"
            instances = instances.filter(
                date__year=today.year, date__month=today.month)
        elif period == "today":
            title = "Transactions : Today"
            instances = instances.filter(
                date__year=today.year, date__month=today.month, date__day=today.day)

    elif filter_date_period:
        title = "Transactions : From %s to %s " % (
            str(filter_from_date), str(filter_to_date))
        if date_error == "no":
            instances = instances.filter(
                is_deleted=False, date__range=[from_date, to_date])

    elif date:
        title = "Transactions : Date : %s" % (str(date))
        if date_error == "no":
            instances = instances.filter(
                date__month=date.month, date__year=date.year, date__day=date.day)

    context = {
        'title': title,
        "month": month,
        "instances": instances,
        "is_month": True,        
        "year": year,        
        "transaction_type": transaction_type,
        "account_head": account_head,
        "account_heads": account_heads,
        "account_groups" : account_groups,

        "is_need_select_picker": True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_chosen_select": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
    }
    return render(request, 'finance/transactions.html', context)


@check_mode
@login_required
def incomes(request):
    instances = Transaction.objects.filter(
        is_deleted=False, transaction_type="income")

    query = request.GET.get("q")
    if query:
        instances = instances.filter(
            Q(date__icontains=query) | Q(transaction_type__icontains=query) | Q(account_head__name__icontains=query) | Q(title__icontains=query) | Q(transaction_mode__icontains=query) | Q(amount__icontains=query))

    context = {
        "title": "Incomes",
        "instances": instances,

        "is_need_select_picker": True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_chosen_select": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
    }
    return render(request, 'finance/incomes.html', context)


@check_mode
@login_required
def expenses(request):
    instances = Transaction.objects.filter(
        is_deleted=False, transaction_type="expense")

    query = request.GET.get("q")
    if query:
        instances = instances.filter(
            Q(date__icontains=query) | Q(transaction_type__icontains=query) | Q(account_head__name__icontains=query) | Q(title__icontains=query) | Q(transaction_mode__icontains=query) | Q(amount__icontains=query))

    context = {
        "instances": instances,
        "title": "Expenses",

        "is_need_select_picker": True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_chosen_select": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
    }
    return render(request, 'finance/expenses.html', context)


@check_mode
@login_required
def transaction(request, pk):
    instance = get_object_or_404(Transaction.objects.filter(pk=pk))
    context = {
        "instance": instance,
        "title": "Transaction",
        "single_page": True,

        "is_need_select_picker": True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_chosen_select": True,
        "is_need_grid_system": True,
        "is_need_datetime_picker": True,
    }
    return render(request, 'finance/transaction.html', context)


@check_mode
@login_required
def print_transaction(request, pk):
    instance = get_object_or_404(Transaction.objects.filter(pk=pk))
    context = {
        "instance": instance,
        "title": "Receipt Voucher",
        "single_page": True,

        "is_need_select_picker": True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_chosen_select": True,
        "is_need_grid_system": True,
        "is_need_datetime_picker": True,
    }
    return render(request, 'finance/print_receipt.html', context)


@check_mode
@login_required
def print_expense_transaction(request, pk):
    instance = get_object_or_404(Transaction.objects.filter(pk=pk))
    context = {
        "instance": instance,
        "title": "Expense / Payement Voucher ",
        "single_page": True,

        "is_need_select_picker": True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_chosen_select": True,
        "is_need_grid_system": True,
        "is_need_datetime_picker": True,
    }
    return render(request, 'finance/print_expense_receipt.html', context)


@check_mode
@ajax_required
@login_required
def delete_transaction(request, pk):
    instance = get_object_or_404(
        Transaction.objects.filter(pk=pk, is_deleted=False))
    instance.is_deleted = True
    instance.save()

    response_data = {
        'status': 'true',
        'title': "Successfully Deleted",
        'redirect': 'true',
        'redirect_url': reverse('finance:transactions'),
        'message': "Transaction Successfully Deleted."
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
def delete_selected_transactions(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(
                Transaction.objects.filter(pk=pk, is_deleted=False))
            balance = 0
            if instance.admission:
                admission = instance.admission
                paid = admission.paid
                balance = admission.balance
                amount = instance.amount
                admission.paid = paid - amount
                admission.balance = balance + amount
                admission.save()

            elif instance.teacher:
                teacher = instance.teacher
                salary = TeacherSalary.objects.filter(
                    teacher=teacher).latest('auto_id')
                salary.dispersed = salary.dispersed - instance.amount
                salary.balance = salary.balance + instance.amount
                salary.save()

            Transaction.objects.filter(pk=pk).update(is_deleted=True)
        response_data = {
            "status": "true",
            "title": "Successfully Deleted",
            "message": "Selected Transaction(s) Successfully Deleted.",
            "redirect": "true",
            "redirect_url": reverse('finance:transactions')
        }
    else:
        response_data = {
            "status": "false",
            "title": "Nothing selected",
            "message": "Please select some transactions first.",
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_expense(request):
    current_college = get_current_college(request)
    if request.method == "POST":
        response_data = {}
        form = TransactionBasicForm(request.POST)
        form.fields['account_head'].queryset = AccountHead.objects.filter(Q(account_group__category_type="liability", is_deleted=False) | Q(account_group__category_type="expense", is_deleted=False,college=current_college))

        if form.is_valid():

            transaction_mode = form.cleaned_data['transaction_mode']
            account_head = form.cleaned_data['account_head']
            amount = form.cleaned_data['amount']

            # create income
            data = form.save(commit=False)

            if transaction_mode == "cash":
                data.payment_mode = None
                data.bank_account = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
            elif transaction_mode == "bank":

                payment_mode = form.cleaned_data['payment_mode']
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = form.cleaned_data['is_cheque_withdrawed']
                    data.card_details = None
                    if not is_cheque_withdrawed:
                        data.payment_to = None

                elif payment_mode == "internet_banking":
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False

                elif payment_mode == "card_payment":
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False

            data.creator = request.user
            data.updater = request.user
            data.auto_id = get_auto_id(Transaction)
            data.a_id = get_a_id(Transaction,request)
            data.transaction_type = "expense"
            data.save()
            if transaction_mode == "cash":
                cash_credit = amount
                bank_credit = 0
            elif transaction_mode == "bank":
                cash_credit = 0
                bank_credit = amount

            Journel.objects.create(
                date = datetime.date.today(),
                cash_credit = cash_credit,
                bank_credit = bank_credit,
                transaction = data,
                expense = amount
            )

            response_data = {
                'status': 'true',
                'title': "Expense Created",
                'redirect': 'true',
                'redirect_url': reverse('finance:transaction', kwargs={'pk': data.pk}),
                'message': "Expense Created Successfully"
            }
        else:
            print(form.errors)
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status": "false",
                "title": "Form Validation Error",
                "message": str(message),
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = TransactionBasicForm()
        form.fields['account_head'].queryset = AccountHead.objects.filter(
            Q(account_group__category_type="liability", is_deleted=False) | Q(account_group__category_type="expense", is_deleted=False,college=current_college))
        context = {
            "form": form,
            "title": "Create Expense",
            "is_create": True,
            "redirect": True,
            "redirect_url": reverse('finance:transactions'),
            "payment_page": True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'finance/entry_expense.html', context)


@check_mode
@login_required
def edit_expense(request, pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(Transaction.objects.filter(pk=pk,college=current_college))

    if request.method == "POST":
        form = TransactionBasicForm(request.POST, instance=instance)
        form.fields['account_head'].queryset = AccountHead.objects.filter(
            Q(account_group__category_type="liability", is_deleted=False) | Q(account_group__category_type="expense", is_deleted=False,college=current_college))

        if form.is_valid():
            transaction_mode = form.cleaned_data['transaction_mode']

            account_head = form.cleaned_data['account_head']
            amount = form.cleaned_data['amount']

            # Update income
            data = form.save(commit=False)

            if transaction_mode == "cash":

                data.payment_mode = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False

            elif transaction_mode == "bank":

                payment_mode = form.cleaned_data['payment_mode']
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = form.cleaned_data['is_cheque_withdrawed']
                    data.card_details = None

                elif payment_mode == "internet_banking":
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False

                elif payment_mode == "card_payment":
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False

            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                'status': 'true',
                'title': "Expense Updated",
                'redirect': 'true',
                'redirect_url': reverse('finance:transaction', kwargs={'pk': data.pk}),
                'message': "Expense Updated Successfully"
            }
        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status": "false",
                "title": " Error !",
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = TransactionBasicForm(instance=instance)
        form.fields['account_head'].queryset = AccountHead.objects.filter(
            Q(account_group__category_type="liability", is_deleted=False) | Q(account_group__category_type="expense", is_deleted=False,college=current_college))

        context = {
            "form": form,
            "title": "Edit Expense",
            "instance": instance,
            "redirect": True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'finance/entry_expense.html', context)


@check_mode
@login_required
def create_account_head(request):
    current_college = get_current_college(request)
    if request.method == "POST":
        response_data = {}
        form = AccountHeadForm(request.POST)

        if form.is_valid():
            auto_id = get_auto_id(AccountHead)

            # create Transaction Category
            data = form.save(commit=False)
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.college = current_college
            data.a_id = get_a_id(AccountHead,request)
            data.save()

            response_data['status'] = 'true'
            response_data['title'] = "Successfully Created"
            response_data['redirect'] = 'true'
            response_data['redirect_url'] = reverse('finance:account_head', kwargs={'pk': data.pk})
            response_data['message'] = "Account Head Successfully Created."
        else:
            response_data['status'] = 'false'
            response_data['stable'] = 'true'
            response_data['title'] = "Form validation error"

            message = ''
            message += generate_form_errors(form, formset=False)
            response_data['message'] = message

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = AccountHeadForm()

        context = {
            "form": form,
            "title": "Create Account Head",
            "redirect": True,

            "is_need_select_picker": True,
            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_chosen_select": True,
            "is_need_grid_system": True,
            "is_need_datetime_picker": True,
        }
        return render(request, 'finance/entry_account_heads.html', context)


@check_mode
@login_required
def edit_account_head(request, pk):
    instance = get_object_or_404(AccountHead.objects.filter(pk=pk))

    if request.method == "POST":
        response_data = {}
        form = AccountHeadForm(request.POST, instance=instance)

        if form.is_valid():
            name = form.cleaned_data['name'].strip()
            account_group = form.cleaned_data['account_group']

            # update Transaction Category
            data = form.save(commit=False)
            data.name = name.strip()
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data['status'] = 'true'
            response_data['title'] = "Successfully Updated"
            response_data['redirect'] = 'true'
            response_data['redirect_url'] = reverse(
                'finance:account_head', kwargs={'pk': data.pk})
            response_data['message'] = "Account Head Successfully Updated."

        else:
            response_data['status'] = 'false'
            response_data['stable'] = 'true'
            response_data['title'] = "Form validation error"

            message = ''
            message += generate_form_errors(form, formset=False)
            response_data['message'] = message

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = AccountHeadForm(instance=instance)

        context = {
            "form": form,
            "title": "Edit Account Head : " + instance.name,
            "instance": instance,
            "redirect": True,

            "is_need_select_picker": True,
            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_chosen_select": True,
            "is_need_grid_system": True,
            "is_need_datetime_picker": True,
        }
        return render(request, 'finance/entry_account_heads.html', context)


@check_mode
@login_required
def account_heads(request):
    instances = AccountHead.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(
            Q(name__icontains=query) | Q(account_group__name__icontains=query) | Q(account_group__category_type__icontains=query) | Q(opening_balance__icontains=query) | Q(balance_type__icontains=query))

    title = "Account Heads"
    context = {
        'title': title,
        "instances": instances,

        "is_need_select_picker": True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_chosen_select": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
    }
    return render(request, 'finance/account_heads.html', context)


@check_mode
@login_required
def account_head(request, pk):
    instance = get_object_or_404(AccountHead.objects.filter(pk=pk))
    context = {
        "instance": instance,
        "title": "Account Head : " + instance.name,
        "single_page": True,

        "is_need_select_picker": True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_chosen_select": True,
        "is_need_grid_system": True,
        "is_need_datetime_picker": True,
    }
    return render(request, 'finance/account_head.html', context)


@check_mode
@ajax_required
@login_required
def delete_account_head(request, pk):
    instance = get_object_or_404(
        AccountHead.objects.filter(pk=pk, is_deleted=False))

    AccountHead.objects.filter(pk=pk).update(
        is_deleted=True, name=instance.name + "_deleted")

    response_data = {
        "status": "true",
        "title": "Successfully Deleted",
        "message": "Account Head Successfully Deleted.",
        "redirect": "true",
        "redirect_url": reverse('finance:account_heads')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
def get_accountheads(request):
    pk = request.GET.get('pk')
    instances = AccountHead.objects.filter(
        account_group__pk=pk, is_deleted=False)
    json_models = serializers.serialize("json", instances)
    return HttpResponse(json_models, content_type="application/javascript")


@check_mode
@login_required
def print_transactions(request):
    today = datetime.date.today()
    year = request.GET.get('year')
    month = request.GET.get('month')
    period = request.GET.get('period')
    date = request.GET.get('date')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    transaction_type = request.GET.get('transaction_type')
    title = "Transactions"
    instances = Transaction.objects.filter(is_deleted=False)

    account_groups = AccountHead.objects.filter(is_system_generated=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(
            Q(transaction_type__icontains=query) | Q(date__icontains=query))

    account_head = request.GET.get("account_head")
    if account_head:
        instances = instances.filter(Q(account_head=account_head))

    transaction_type = request.GET.get("transaction_type")
    if transaction_type == "income":
        instances = instances.filter(transaction_type=transaction_type)
        title += " (Transaction Type : Income)"
    elif transaction_type == "expense":
        instances = instances.filter(transaction_type=transaction_type)
        title += " (Transaction Type : Expense)"

    filter_date = date
    filter_from_date = from_date
    filter_to_date = to_date

    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
        except ValueError:
            date_error = "yes"

    if year:
        instances = instances.filter(date__year=year)

    if month:
        instances = instances.filter(date__month=month)

    filter_date_period = False

    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(
                from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(
                to_date, '%m/%d/%Y').date() + datetime.timedelta(days=1)
        except ValueError:
            date_error = "yes"

        filter_date_period = True

    if period:
        if period == "year":
            title = "Transactions : This Year"
            instances = instances.filter(date__year=today.year)
        elif period == 'month':
            title = "Transactions : This Month"
            instances = instances.filter(
                date__year=today.year, date__month=today.month)
        elif period == "today":
            title = "Transactions : Today"
            instances = instances.filter(
                date__year=today.year, date__month=today.month, date__day=today.day)

    elif filter_date_period:
        title = "Transactions : From %s to %s " % (
            str(filter_from_date), str(filter_to_date))
        if date_error == "no":
            instances = instances.filter(
                is_deleted=False, date__range=[from_date, to_date])

    elif date:
        title = "Transactions : Date : %s" % (str(date))
        if date_error == "no":
            instances = instances.filter(
                date__month=date.month, date__year=date.year, date__day=date.day)

    context = {
        'title': title,
        "instances": instances,
        "is_month": True,
        "year": year,
        "month": month,
        "account_head": account_head,
        "query": query,
        "transaction_type": transaction_type,
        "account_groups": account_groups,

        "is_need_select_picker": True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_chosen_select": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
    }
    return render(request, 'finance/print_transactions.html', context)


@check_mode
@login_required
def create_special_subscription(request):
    families = Family.objects.filter(is_deleted=False)
    account_head = AccountHead.objects.get(name="special_subscription_fee")
    if request.method == "POST":
        response_data = {}
        form = SpecialSubscriptionForm(request.POST)
        if form.is_valid():

            data = form.save(commit=False)
            auto_id = get_auto_id(SpecialSubscription)
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.save()

            for family in families:
                add_special_subscription(request, data, family)

            response_data = {
                'status': 'true',
                'title': str(_("Successfully Created")),
                'redirect': 'true',
                'redirect_url': reverse('finance:special_subscription', kwargs={'pk': data.pk}),
                'message': str(_("SpecialSubscription Successfully Created"))
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title': str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Create SpecialSubscription"))
        form = SpecialSubscriptionForm()
        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_special_subscription.html', context)


@check_mode
@login_required
def edit_special_subscription(request, pk):
    instance = get_object_or_404(SpecialSubscription.objects.filter(pk=pk))

    payments = SpecialSubscriptionPayment.objects.filter(is_deleted=False,special_subscription=instance)

    if request.method == "POST":
        response_data = {}
        form = SpecialSubscriptionForm(request.POST, instance=instance)
        if form.is_valid():
            amount = form.cleaned_data['amount']
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            for payment in payments:
                payment.amount = amount
                payment.balance = payment.amount - payment.paid
                payment.save()

            response_data = {
                'status': 'true',
                'title':  str(_("Successfully Updated")),
                'redirect': 'true',
                'redirect_url': reverse('finance:special_subscription', kwargs={'pk': data.pk}),
                'message':  str(_("SpecialSubscription Successfully Updated")),
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title':  str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Edit SpecialSubscription"))
        form = SpecialSubscriptionForm(instance=instance)
        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_special_subscription.html', context)


@check_mode
@login_required
def special_subscriptions(request):
    instances = SpecialSubscription.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(
            Q(name__icontains=query)
        )
        title = "SpecialSubscription (Query : %s)" % query

    context = {
        'title': str(_("SpecialSubscriptions")),
        'instances': instances,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/special_subscriptions.html', context)


@check_mode
@login_required
def special_subscription(request, pk):
    instance = get_object_or_404(SpecialSubscription.objects.filter(pk=pk))
    special_subscription_fees = SpecialSubscriptionPayment.objects.filter(
        is_deleted=False, special_subscription=instance)

    context = {
        'title': instance.title,
        'instance': instance,
        "special_subscription_fees": special_subscription_fees,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/special_subscription.html', context)


@check_mode
@login_required
def delete_special_subscription(request, pk):
    instance = get_object_or_404(SpecialSubscription.objects.filter(pk=pk))
    special_subscription = SpecialSubscriptionPayment.objects.filter(
        special_subscription=instance).update(is_deleted=True)
    families = Family.objects.filter(is_deleted=False)

    for family in families:

        family.balance -= instance.amount
        family.save()

    instance.is_deleted = True
    instance.save()
    response_data = {
        "status": "true",
        "title": str(_("Successfully deleted")),
        "message": str(_("SpecialSubscription deleted successfully")),
        "redirect": "true",
        "redirect_url": reverse('finance:special_subscriptions')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_special_subscription_payment(request, pk):
    url = reverse('finance:create_special_subscription_payment',
                  kwargs={"pk": pk})
    family = get_object_or_404(Family.objects.filter(pk=pk))

    if request.method == "POST":
        response_data = {}
        form = SpecialSubscriptionTransactionForm(request.POST)
        if form.is_valid():
            amount = request.POST.getlist('balance')
            special_fee = request.POST.getlist('special_fee')
            total_amount = 0
            pairing_amount_family = zip(amount,special_fee)
            title = ""
            for item in pairing_amount_family:
                amount = Decimal(item[0])
                special_subscription_fee = item[1]

                if SpecialSubscriptionPayment.objects.filter(pk=special_subscription_fee,family=family,is_deleted=False).exists():
                    special_subscription = SpecialSubscriptionPayment.objects.get(pk=special_subscription_fee,family=family,is_deleted=False)

                    special_subscription.paid = special_subscription.paid + amount
                    special_subscription.balance = special_subscription.balance - amount
                    special_subscription.save()

                    title = title + special_subscription.special_subscription.title + ", "

                total_amount += amount

            transaction_mode = form.cleaned_data['transaction_mode']
            date = form.cleaned_data['date']

            actual_amount = amount
            total_amount_deducted = 0

            if AccountHead.objects.filter(name="special_subscription_fee", is_deleted=False, is_system_generated=True).exists():
                account_head = AccountHead.objects.get(
                    name="special_subscription_fee", is_deleted=False, is_system_generated=True)

                advance_amount = family.advance_amount

                total_amount_deducted += amount

            family.balance -= total_amount_deducted
            family.save()

            data = form.save(commit=False)

            if transaction_mode == "cash":
                data.payment_mode = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
            elif transaction_mode == "bank":
                payment_mode = form.cleaned_data['payment_mode']
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = form.cleaned_data['is_cheque_withdrawed']
                    data.card_details = None

                elif payment_mode == "internet_banking":
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False

                elif payment_mode == "card_payment":
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False

            if not account_head == "credit":
                data.auto_id = get_auto_id(Transaction)
                data.creator = request.user
                data.updater = request.user
                data.transaction_type = "income"
                data.family = family
                data.amount = total_amount
                data.title = title
                data.account_head=account_head
                data.save()

            if transaction_mode == "cash":
                cash_debit = total_amount
                bank_debit = 0
            elif transaction_mode == "bank":
                cash_debit = 0
                bank_debit = total_amount

            Journel.objects.create(
                date = datetime.date.today(),
                cash_debit = cash_debit,
                bank_debit = bank_debit,
                transaction = data,
                income = total_amount
            )
               
            response_data = {
                'status': 'true',
                'title': "Fee Payment Created",
                'redirect': 'true',
                'redirect_url': reverse('families:family', kwargs={'pk': family.pk}),
                'message': "Fee Payment Created Successfully"
            }
        else:
            form = SpecialSubscriptionTransactionForm(request.POST)

            response_data = {
                "status": "false",
                "title": " Error !",
                "message": "Form Validation Error",
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        transaction_form = SpecialSubscriptionTransactionForm()
        special_fees = SpecialSubscriptionPayment.objects.filter(family=family,balance__gt=0)
        context = {
            "title": "Create Special Subscription Payment",
            "transaction_form": transaction_form,
            "special_fees" : special_fees,
            "url": url,
            "family": family
        }
        return render(request, 'finance/entry_other_fee.html', context)


@check_mode
@login_required
def create_asset_category(request):
    current_college = get_current_college(request)
    if request.method == "POST":
        response_data = {}
        form = AssetCategoryForm(request.POST)
        if form.is_valid():

            data = form.save(commit=False)
            auto_id = get_auto_id(AssetCategory)
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.a_id = get_a_id(AssetCategory,request)
            data.save()

            response_data = {
                'status': 'true',
                'title': str(_("Successfully Created")),
                'redirect': 'true',
                'redirect_url': reverse('finance:asset_category', kwargs={'pk': data.pk}),
                'message': str(_("Asset Category Successfully Created"))
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title': str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Create Asset Category"))
        form = AssetCategoryForm()
        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_asset_category.html', context)


@check_mode
@login_required
def edit_asset_category(request, pk):
    instance = get_object_or_404(AssetCategory.objects.filter(pk=pk))
    if request.method == "POST":
        response_data = {}
        form = AssetCategoryForm(request.POST, instance=instance)
        if form.is_valid():
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                'status': 'true',
                'title':  str(_("Successfully Updated")),
                'redirect': 'true',
                'redirect_url': reverse('finance:asset_category', kwargs={'pk': data.pk}),
                'message':  str(_("Asset Category Successfully Updated")),
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title':  str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Edit Asset Category"))
        form = AssetCategoryForm(instance=instance)
        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_asset_category.html', context)


@check_mode
@login_required
def asset_categories(request):
    instances = AssetCategory.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(
            Q(name__icontains=query)
        )
        title = "Asset Category (Query : %s)" % query

    context = {
        'title': str(_("Asset Categories")),
        'instances': instances,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/asset_categories.html', context)


@check_mode
@login_required
def asset_category(request, pk):
    instance = get_object_or_404(AssetCategory.objects.filter(pk=pk))

    context = {
        'title': instance.name,
        'instance': instance,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/asset_category.html', context)


@check_mode
@login_required
def delete_asset_category(request, pk):

    instance = get_object_or_404(AssetCategory.objects.filter(pk=pk))
    instance.is_deleted = True
    instance.save()
    response_data = {
        "status": "true",
        "title": str(_("Successfully deleted")),
        "message": str(_("Asset Category deleted successfully")),
        "redirect": "true",
        "redirect_url": reverse('finance:asset_categories')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_asset_subcategory(request):
    current_college = get_current_college(request)
    if request.method == "POST":
        response_data = {}
        form = AssetSubCategoryForm(request.POST)
        if form.is_valid():

            data = form.save(commit=False)
            auto_id = get_auto_id(AssetSubCategory)
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.a_id = get_a_id(AssetSubCategory,request)
            data.save()

            response_data = {
                'status': 'true',
                'title': str(_("Successfully Created")),
                'redirect': 'true',
                'redirect_url': reverse('finance:asset_subcategory', kwargs={'pk': data.pk}),
                'message': str(_("Asset Sub Category Successfully Created"))
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title': str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Create Asset Sub Category"))
        form = AssetSubCategoryForm()
        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_asset_subcategory.html', context)


@check_mode
@login_required
def edit_asset_subcategory(request, pk):
    instance = get_object_or_404(AssetSubCategory.objects.filter(pk=pk))
    if request.method == "POST":
        response_data = {}
        form = AssetSubCategoryForm(request.POST, instance=instance)
        if form.is_valid():
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                'status': 'true',
                'title':  str(_("Successfully Updated")),
                'redirect': 'true',
                'redirect_url': reverse('finance:asset_subcategory', kwargs={'pk': data.pk}),
                'message':  str(_("Asset Sub Category Successfully Updated")),
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title':  str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Edit Asset Sub Category"))
        form = AssetSubCategoryForm(instance=instance)
        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_asset_subcategory.html', context)


@check_mode
@login_required
def asset_subcategories(request):
    instances = AssetSubCategory.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(
            Q(name__icontains=query)
        )
        title = "Asset Sub Category (Query : %s)" % query

    context = {
        'title': str(_("Asset Sub Categories")),
        'instances': instances,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/asset_subcategories.html', context)


@check_mode
@login_required
def asset_subcategory(request, pk):
    instance = get_object_or_404(AssetSubCategory.objects.filter(pk=pk))

    context = {
        'title': instance.name,
        'instance': instance,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/asset_subcategory.html', context)


@check_mode
@login_required
def delete_asset_subcategory(request, pk):

    instance = get_object_or_404(AssetSubCategory.objects.filter(pk=pk))
    instance.is_deleted = True
    instance.save()
    response_data = {
        "status": "true",
        "title": str(_("Successfully deleted")),
        "message": str(_("Asset Sub Category deleted successfully")),
        "redirect": "true",
        "redirect_url": reverse('finance:asset_categories')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_asset(request):
    current_college = get_current_college(request)
    if request.method == "POST":
        response_data = {}
        form = AssetForm(request.POST)
        if form.is_valid():

            data = form.save(commit=False)
            auto_id = get_auto_id(Asset)
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.a_id = get_a_id(Asset,request)
            data.save()

            response_data = {
                'status': 'true',
                'title': str(_("Successfully Created")),
                'redirect': 'true',
                'redirect_url': reverse('finance:asset', kwargs={'pk': data.pk}),
                'message': str(_("Asset Successfully Created"))
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title': str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Create Asset"))
        form = AssetForm()
        # form.fields['account_group'].queryset = AccountGroup.objects.filter(is_deleted=False,category_type="asset",college=current_college)

        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_asset.html', context)


@check_mode
@login_required
def edit_asset(request, pk):
    instance = get_object_or_404(Asset.objects.filter(pk=pk))
    if request.method == "POST":
        response_data = {}
        form = AssetForm(request.POST, instance=instance)
        if form.is_valid():
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                'status': 'true',
                'title':  str(_("Successfully Updated")),
                'redirect': 'true',
                'redirect_url': reverse('finance:asset', kwargs={'pk': data.pk}),
                'message':  str(_("Asset Successfully Updated")),
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title':  str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Edit Asset"))
        form = AssetForm(instance=instance)
        form.fields['account_group'].queryset = AccountGroup.objects.filter(is_deleted=False,category_type="asset")

        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_asset.html', context)


@check_mode
@login_required
def assets(request):
    instances = Asset.objects.filter(is_deleted=False)
    asset_categories = AssetCategory.objects.filter(is_deleted=False)
    

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query) | Q(asset_category__name__icontains=query) | Q(asset_subcategory__name__icontains=query) | Q(count__istartswith=query) | Q(amount__istartswith=query))
        title = "Asset (Query : %s)" % query

    count = request.GET.get("count")
    if count:
        instances = instances.filter(Q(count=count))

    name = request.GET.get("name")
    if name:
        instances = instances.filter(Q(name=name))

    asset_category = request.GET.get("asset_category")
    if asset_category:
        instances = instances.filter(Q(asset_category__pk=asset_category))

    

    context = {
        'title': str(_("Assets")),
        'instances': instances,
        'query': query,
        'count': count,
        "name": name,
        'asset_category': asset_category,
        "asset_categories": asset_categories,       

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/assets.html', context)


@check_mode
@login_required
def print_assets(request):
    instances = Asset.objects.filter(is_deleted=False)
    asset_categories = AssetCategory.objects.filter(is_deleted=False)
    institutes = Institute.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(
            Q(name__icontains=query)
        )
        title = "Asset (Query : %s)" % query

    count = request.GET.get("count")
    if count:
        instances = instances.filter(Q(count=count))

    name = request.GET.get("name")
    if name:
        instances = instances.filter(Q(name=name))

    asset_category = request.GET.get("asset_category")
    if asset_category:
        instances = instances.filter(Q(asset_category__pk=asset_category))

    institute = request.GET.get("institute")
    if institute:
        instances = instances.filter(Q(institute__pk=institute))

    context = {
        'title': str(_("Assets")),
        'instances': instances,
        'query': query,
        'count': count,
        "name": name,
        'asset_category': asset_category,
        "institutes": institutes,
        "asset_categories": asset_categories,
        "institute": institute,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/print_assets.html', context)


@check_mode
@login_required
def asset(request, pk):
    instance = get_object_or_404(Asset.objects.filter(pk=pk))

    context = {
        'title': instance.name,
        'instance': instance,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/asset.html', context)


@check_mode
@login_required
def delete_asset(request, pk):

    instance = get_object_or_404(Asset.objects.filter(pk=pk))
    instance.is_deleted = True
    instance.save()
    response_data = {
        "status": "true",
        "title": str(_("Successfully deleted")),
        "message": str(_("Asset deleted successfully")),
        "redirect": "true",
        "redirect_url": reverse('finance:assets')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@ajax_required
def get_asset_subcategories(request):
    pk = request.GET.get('pk')
    instances = AssetSubCategory.objects.filter(
        asset_category__pk=pk, is_deleted=False)
    json_models = serializers.serialize("json", instances)
    return HttpResponse(json_models, content_type="application/javascript")


@check_mode
@login_required
def create_liability_category(request):
    current_college = get_current_college(request)
    if request.method == "POST":
        response_data = {}
        form = LiabilityCategoryForm(request.POST)
        if form.is_valid():

            data = form.save(commit=False)
            auto_id = get_auto_id(LiabilityCategory)
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.a_id = get_a_id(LiabilityCategory,request)
            data.save()

            response_data = {
                'status': 'true',
                'title': str(_("Successfully Created")),
                'redirect': 'true',
                'redirect_url': reverse('finance:liability_category', kwargs={'pk': data.pk}),
                'message': str(_("Liability Category Successfully Created"))
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title': str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Create Liability Category"))
        form = LiabilityCategoryForm()
        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_liability_category.html', context)


@check_mode
@login_required
def edit_liability_category(request, pk):
    instance = get_object_or_404(LiabilityCategory.objects.filter(pk=pk))
    if request.method == "POST":
        response_data = {}
        form = LiabilityCategoryForm(request.POST, instance=instance)
        if form.is_valid():
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                'status': 'true',
                'title':  str(_("Successfully Updated")),
                'redirect': 'true',
                'redirect_url': reverse('finance:liability_category', kwargs={'pk': data.pk}),
                'message':  str(_("Liability Category Successfully Updated")),
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title':  str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Edit Liability Category"))
        form = LiabilityCategoryForm(instance=instance)
        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_liability_category.html', context)


@check_mode
@login_required
def liability_categories(request):
    instances = LiabilityCategory.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(
            Q(name__icontains=query)
        )
        title = "Liability Category (Query : %s)" % query

    context = {
        'title': str(_("Liability Categories")),
        'instances': instances,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/liability_categories.html', context)


@check_mode
@login_required
def liability_category(request, pk):
    instance = get_object_or_404(LiabilityCategory.objects.filter(pk=pk))

    context = {
        'title': instance.name,
        'instance': instance,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/liability_category.html', context)


@check_mode
@login_required
def delete_liability_category(request, pk):
    instance = get_object_or_404(LiabilityCategory.objects.filter(pk=pk))
    instance.is_deleted = True
    instance.save()
    response_data = {
        "status": "true",
        "title": str(_("Successfully deleted")),
        "message": str(_("Liability Category deleted successfully")),
        "redirect": "true",
        "redirect_url": reverse('finance:liability_categories')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_liability_subcategory(request):
    current_college = get_current_college(request)
    if request.method == "POST":
        response_data = {}
        form = LiabilitySubCategoryForm(request.POST)
        if form.is_valid():

            data = form.save(commit=False)
            auto_id = get_auto_id(LiabilitySubCategory)
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.a_id = get_a_id(LiabilitySubCategory,request)
            data.save()

            response_data = {
                'status': 'true',
                'title': str(_("Successfully Created")),
                'redirect': 'true',
                'redirect_url': reverse('finance:liability_subcategory', kwargs={'pk': data.pk}),
                'message': str(_("Liability Sub Category Successfully Created"))
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title': str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Create Liability Sub Category"))
        form = LiabilitySubCategoryForm()
        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_liability_subcategory.html', context)


@check_mode
@login_required
def edit_liability_subcategory(request, pk):
    instance = get_object_or_404(LiabilitySubCategory.objects.filter(pk=pk))
    if request.method == "POST":
        response_data = {}
        form = LiabilitySubCategoryForm(request.POST, instance=instance)
        if form.is_valid():
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                'status': 'true',
                'title':  str(_("Successfully Updated")),
                'redirect': 'true',
                'redirect_url': reverse('finance:liability_subcategory', kwargs={'pk': data.pk}),
                'message':  str(_("Liability Sub Category Successfully Updated")),
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title':  str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Edit Liability Sub Category"))
        form = LiabilitySubCategoryForm(instance=instance)
        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_liability_subcategory.html', context)


@check_mode
@login_required
def liability_subcategories(request):
    instances = LiabilitySubCategory.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(
            Q(name__icontains=query) |
            Q(liability_category__name__icontains=query)
        )
        title = "Liability Sub Category (Query : %s)" % query

    context = {
        'title': str(_("Liability Sub Categories")),
        'instances': instances,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/liability_subcategories.html', context)


@check_mode
@login_required
def liability_subcategory(request, pk):
    instance = get_object_or_404(LiabilitySubCategory.objects.filter(pk=pk))

    context = {
        'title': instance.name,
        'instance': instance,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/liability_subcategory.html', context)


@check_mode
@login_required
def delete_liability_subcategory(request, pk):

    instance = get_object_or_404(LiabilitySubCategory.objects.filter(pk=pk))
    instance.is_deleted = True
    instance.save()
    response_data = {
        "status": "true",
        "title": str(_("Successfully deleted")),
        "message": str(_("Liability Sub Category deleted successfully")),
        "redirect": "true",
        "redirect_url": reverse('finance:liability_categories')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_liability(request):
    current_college = get_current_college(request)
    if request.method == "POST":
        response_data = {}
        form = LiabilityForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            liability_type = form.cleaned_data['liability_type']
            liability_category = form.cleaned_data['liability_category']
            account_group = form.cleaned_data['account_group']
            liability_subcategory = form.cleaned_data['liability_subcategory']
            amount = form.cleaned_data['amount']
            balance = form.cleaned_data['balance']

            data = form.save(commit=False)
            if liability_type == "borrow":
                auto_id = get_auto_id(Liability)
                data.creator = request.user
                data.updater = request.user
                data.auto_id = auto_id
                data.a_id = get_a_id(Liability,request)
                data.balance = amount
                data.save()
                transaction = Transaction.objects.create(
                                account_group  = account_group,
                                amount = amount,
                                # liability_type = liability_type,
                                # name = name,
                                # liability_category = liability_category,
                                # liability_subcategory = liability_subcategory,
                                balance = balance,
                                # family = family,
                                # member = member,
                                # institute = institute,
                                transaction_type = "liability",
                                auto_id = get_auto_id(Transaction),
                                a_id = get_a_id(Liability,request),
                                creator = request.user,
                                updater = request.user,
                                date = datetime.datetime.now()
                            )
                Journel.objects.create(
                    cash_debit = amount,
                    transaction = transaction,
                    liability = data,
                    date = datetime.date.today()
                )
                response_data = {
                    'status': 'true',
                    'title': str(_("Successfully Created")),
                    'redirect': 'true',
                    'redirect_url': reverse('finance:liability', kwargs={'pk': data.pk}),
                    'message': str(_("Liability Successfully Created"))
                }
            elif liability_type == "liquidate":
                data.liability.balance = data.liability.balance - amount
                data.liability.paid = data.liability.paid + amount
                data.liability.save()
                transaction = Transaction.objects.create(
                                account_group  = account_group,
                                amount = amount,
                                transaction_type = "liability",
                                auto_id = get_auto_id(Transaction),
                                a_id = get_a_id(Liability,request),
                                creator = request.user,
                                updater = request.user,
                                date = datetime.datetime.now()
                            )
               
                journel =  Journel.objects.filter(liability=data.liability,is_deleted=False)[0]
                journel.cash_debit -= amount
                journel.save()

                response_data = {
                    'status': 'true',
                    'title': str(_("Successfully Created")),
                    'redirect': 'true',
                    'redirect_url': reverse('finance:liability', kwargs={'pk': data.liability.pk}),
                    'message': str(_("Liability Successfully Created"))
                }
            elif liability_type == "lend":                
                auto_id = get_auto_id(Liability)
                data.creator = request.user
                data.updater = request.user
                data.auto_id = auto_id
                data.balance = amount
                data.save()
                transaction = Transaction.objects.create(
                                account_group  = account_group,
                                amount = amount,
                                transaction_type = "liability",
                                auto_id = get_auto_id(Transaction),
                                a_id = get_a_id(Liability,request),
                                creator = request.user,
                                updater = request.user,
                                date = datetime.datetime.now()
                            )

                response_data = {
                    'status': 'true',
                    'title': str(_("Successfully Created")),
                    'redirect': 'true',
                    'redirect_url': reverse('finance:liability', kwargs={'pk': data.pk}),
                    'message': str(_("Liability Successfully Created"))
                }
            elif liability_type == "satisfy":
                data.liability.balance = data.liability.balance - amount
                data.liability.paid = data.liability.paid + amount
                data.liability.save()
                transaction = Transaction.objects.create(
                                account_group  = account_group,
                                amount = amount,
                                transaction_type = "liability",
                                auto_id = get_auto_id(Transaction),
                                a_id = get_a_id(Liability,request),
                                creator = request.user,
                                updater = request.user,
                                date = datetime.datetime.now()
                            )
               
                response_data = {
                    'status': 'true',
                    'title': str(_("Successfully Created")),
                    'redirect': 'true',
                    'redirect_url': reverse('finance:liability', kwargs={'pk': data.liability.pk}),
                    'message': str(_("Liability Successfully Created"))
                }
            
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title': str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Create Liability"))
        form = LiabilityForm()        
        # form.fields['account_group'].queryset = AccountGroup.objects.filter(is_deleted=False,category_type="liability").exclude(name="capital_account")

        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_liability.html', context)


@check_mode
@login_required
def edit_liability(request, pk):
    instance = get_object_or_404(Liability.objects.filter(pk=pk))
    if request.method == "POST":
        response_data = {}
        form = LiabilityForm(request.POST, instance=instance)
        if form.is_valid():
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                'status': 'true',
                'title':  str(_("Successfully Updated")),
                'redirect': 'true',
                'redirect_url': reverse('finance:liability', kwargs={'pk': data.pk}),
                'message':  str(_("Liability Successfully Updated")),
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                'status': 'false',
                'stable': 'true',
                'title':  str(_("Form validation error")),
                "message": message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = str(_("Edit Liability"))
        form = LiabilityForm(instance=instance)
        form.fields['account_group'].queryset = AccountGroup.objects.filter(is_deleted=False,category_type="liability").exclude(name="capital_account")

        context = {
            "form": form,
            "title": title,
            "redirect": True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker": True
        }
        return render(request, 'finance/entry_liability.html', context)


@check_mode
@login_required
def liabilities(request):
    instances = Liability.objects.filter(is_deleted=False)
    liability_categories = LiabilityCategory.objects.filter(is_deleted=False)    

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query) | Q(liability_type__icontains=query) | Q(liability_category__name__icontains=query) | Q(liability_subcategory__name__icontains=query) | Q(account_group__name__icontains=query) | Q(amount__istartswith=query) | Q(paid__istartswith=query) | Q(balance__istartswith=query))
        title = "Liability (Query : %s)" % query

    amount = request.GET.get("amount")
    if amount:
        instances = instances.filter(Q(amount=amount))

    name = request.GET.get("name")
    if name:
        instances = instances.filter(Q(name=name))

    liability_category = request.GET.get("liability_category")
    if liability_category:
        instances = instances.filter(
            Q(liability_category__pk=liability_category))


    context = {
        'title': str(_("Liabilities")),
        'instances': instances,
        'query': query,
        'amount': amount,
        "name": name,
        'liability_category': liability_category,
        "liability_categories": liability_categories,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/liabilities.html', context)


@check_mode
@login_required
def print_liabilities(request):
    instances = Liability.objects.filter(is_deleted=False)
    liability_categories = LiabilityCategory.objects.filter(is_deleted=False)
    institutes = Institute.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(
            Q(name__icontains=query)
        )
        title = "Liability (Query : %s)" % query

    amount = request.GET.get("amount")
    if amount:
        instances = instances.filter(Q(amount=amount))

    name = request.GET.get("name")
    if name:
        instances = instances.filter(Q(name=name))

    liability_category = request.GET.get("liability_category")
    if liability_category:
        instances = instances.filter(
            Q(liability_category__pk=liability_category))

    institute = request.GET.get("institute")
    if institute:
        instances = instances.filter(Q(institute__pk=institute))

    context = {
        'title': str(_("Liabilitys")),
        'instances': instances,
        'query': query,
        'amount': amount,
        "name": name,
        'liability_category': liability_category,
        "institutes": institutes,
        "liability_categories": liability_categories,
        "institute": institute,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/print_liabilities.html', context)


@check_mode
@login_required
def liability(request, pk):
    instance = get_object_or_404(Liability.objects.filter(pk=pk))

    context = {
        'title': instance.name,
        'instance': instance,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker": True
    }
    return render(request, 'finance/liability.html', context)


@check_mode
@login_required
def delete_liability(request, pk):
    instance = get_object_or_404(Liability.objects.filter(pk=pk))
    instance.is_deleted = True
    instance.save()
    response_data = {
        "status": "true",
        "title": str(_("Successfully deleted")),
        "message": str(_("Liability deleted successfully")),
        "redirect": "true",
        "redirect_url": reverse('finance:liabilities')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
def get_liability_subcategories(request):
    pk = request.GET.get('pk')
    instances = LiabilitySubCategory.objects.filter(
        liability_category__pk=pk, is_deleted=False)
    json_models = serializers.serialize("json", instances)
    return HttpResponse(json_models, content_type="application/javascript")


@check_mode
@login_required
def get_amount(request):
    pk = request.GET.get('id')
    if SpecialSubscriptionPayment.objects.filter(pk=pk).exists():
        member = SpecialSubscriptionPayment.objects.get(pk=pk)
        response_data = {
            "status": "true",
            "message": "Success",
            "balance": str(member.balance),
        }
    else:
        response_data = {
            "status": "false",
            "message": "SpecialSubscriptionPayment not exist."
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_qabar_registration_payment(request, pk):
    url = reverse('finance:create_qabar_registration_payment',
                  kwargs={"pk": pk})
    instance = get_object_or_404(Qabar.objects.filter(pk=pk))

    if request.method == "POST":
        response_data = {}
        form = QabarTransactionForm(request.POST)

        if form.is_valid():

            transaction_mode = form.cleaned_data['transaction_mode']
            amount = form.cleaned_data['amount']
            date = form.cleaned_data['date']

            if AccountHead.objects.filter(name="qabar_fee", is_deleted=False, is_system_generated=True).exists():
                account_head = AccountHead.objects.get(
                    name="qabar_fee", is_deleted=False, is_system_generated=True)

            data = form.save(commit=False)

            if transaction_mode == "cash":
                data.payment_mode = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
            elif transaction_mode == "bank":
                payment_mode = form.cleaned_data['payment_mode']
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = form.cleaned_data['is_cheque_withdrawed']
                    data.card_details = None

                elif payment_mode == "internet_banking":
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False

                elif payment_mode == "card_payment":
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False

            data.auto_id = get_auto_id(Transaction)
            data.creator = request.user
            data.updater = request.user
            data.transaction_type = "income"
            data.account_head = account_head
            data.qabar = instance
            data.save()

            paid = instance.paid + amount
            balance = instance.amount - paid
            instance.paid = paid
            instance.balance = balance
            instance.save()

            response_data = {
                'status': 'true',
                'title': "Fee Payment Created",
                'redirect': 'true',
                'redirect_url': reverse('registrations:qabarregistration', kwargs={'pk': instance.pk}),
                'message': "Fee Payment Created Successfully"
            }
        else:
            form = QabarTransactionForm(request.POST)

            response_data = {
                "status": "false",
                "title": " Error !",
                "message": "Form Validation Error",
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        transaction_form = QabarTransactionForm()

        context = {
            "title": "Create Payment",
            "transaction_form": transaction_form,
            "url": url,
            "instance": instance
        }
        return render(request, 'finance/entry_qabar_fee.html', context)


@check_mode
@login_required
def create_donation_payment(request, pk):
    url = reverse('finance:create_donation_payment',
                  kwargs={"pk": pk})
    instance = get_object_or_404(Donation.objects.filter(pk=pk))

    if request.method == "POST":
        response_data = {}
        form = DonationTransactionForm(request.POST)

        if form.is_valid():

            transaction_mode = form.cleaned_data['transaction_mode']
            amount = form.cleaned_data['amount']
            date = form.cleaned_data['date']

            if AccountHead.objects.filter(name="donation", is_deleted=False, is_system_generated=True).exists():
                account_head = AccountHead.objects.get(
                    name="donation", is_deleted=False, is_system_generated=True)

            data = form.save(commit=False)


            if transaction_mode == "cash":
                data.payment_mode = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
            elif transaction_mode == "bank":
                payment_mode = form.cleaned_data['payment_mode']
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = form.cleaned_data['is_cheque_withdrawed']
                    data.card_details = None

                elif payment_mode == "internet_banking":
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False

                elif payment_mode == "card_payment":
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False

            data.auto_id = get_auto_id(Transaction)
            data.creator = request.user
            data.updater = request.user
            data.transaction_type = "income"
            data.account_head = account_head
            data.donation = instance
            data.save()

            if transaction_mode == "cash":
                cash_debit = amount
                bank_debit = 0
            elif transaction_mode == "bank":
                cash_debit = 0
                bank_debit = amount

            Journel.objects.create(
                date = datetime.date.today(),
                cash_debit = cash_debit,
                bank_debit = bank_debit,
                transaction = data,
                income = amount
            )

            paid = instance.paid + amount
            balance = instance.amount - paid
            instance.paid = paid
            instance.balance = balance
            instance.save()

            response_data = {
                'status': 'true',
                'title': "Donation Payment Created",
                'redirect': 'true',
                'redirect_url': reverse('donations:donation', kwargs={'pk': instance.pk}),
                'message': "Donation Payment Created Successfully"
            }
        else:
            form = DonationTransactionForm(request.POST)

            response_data = {
                "status": "false",
                "title": " Error !",
                "message": "Form Validation Error",
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        transaction_form = DonationTransactionForm()

        context = {
            "title": "Create Payment",
            "transaction_form": transaction_form,
            "url": url,
            "instance": instance
        }
        return render(request, 'finance/entry_donation_payment.html', context)


@check_mode
@login_required
def print_qabar_fee_slip(request, pk):
    instance = get_object_or_404(Qabar.objects.filter(pk=pk, is_deleted=False))
    qabar_fee_payments = Qabar.objects.filter(pk=pk, is_deleted=False)
    qabar_members = QabarMember.objects.filter(qabar=instance)
    today = datetime.date.today()
    total_amount = 0

    if qabar_fee_payments:
        total_amount = qabar_fee_payments.aggregate(
            paid=Sum('paid')).get('paid', 0.00)

    context = {
        'title': "Fee Slip",
        "qabar_fee_payments": qabar_fee_payments,
        "qabar_members": qabar_members,
        "instance": instance,
        "today": today,
        "total_amount": total_amount,
    }
    return render(request, 'finance/qabar_receipt.html', context)


@check_mode
@login_required
def create_balance_sheet(request):
    today = datetime.date.today()
    if today.month > 3 :
        year = today.year 
    else :
        year = today.year - 1 
    from_date = datetime.date(year,4,1)
    form = LedgerForm(initial={'to_date':today,'from_date':from_date})

    context = {
        'title' : "Create Balance Sheet",
        "form" : form,
        "url" : reverse('finance:balance_sheet'),

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,

    }
    return render(request,'finance/create_balance_sheet.html',context)


@check_mode
@login_required
def balance_sheet(request):
    instances = Journel.objects.filter(is_deleted=False)    
    assets = Asset.objects.filter(is_deleted=False)
    liabilities = Liability.objects.filter(is_deleted=False,liability_type='borrow')
    lend = Liability.objects.filter(is_deleted=False,liability_type='lend')
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    date_error = "no"
    filter_date_period = False
    filter_from_date = from_date
    filter_to_date = to_date
    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date()            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True
    
    if filter_date_period:
        title = "Report : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, date__range=[from_date, to_date])  

    income = instances.filter(transaction__isnull=False).aggregate(amount=Sum('income')).get('amount',0) or 0
    expence = instances.filter(transaction__isnull=False).aggregate(amount=Sum('expense')).get('amount',0) or 0

    loss = expence
    profit = income

    if loss > profit :
        net_profit = 0
        net_loss = loss - profit
        net_total = loss
    else :
        net_profit = profit - loss
        net_loss = 0 
        net_total = profit

    capital_account = 0 
    capital_account = capital_account + net_profit - net_loss
    
    cash_debit = instances.aggregate(amount=Sum('cash_debit')).get('amount',0) or 0
    cash_credit = instances.aggregate(amount=Sum('cash_credit')).get('amount',0) or 0
    bank_debit = instances.aggregate(amount=Sum('bank_debit')).get('amount',0) or 0
    bank_credit = instances.aggregate(amount=Sum('bank_credit')).get('amount',0) or 0

    bank_asset = assets.filter(account_group__name="bank_account",account_group__category_type="asset").aggregate(amount=Sum('amount')).get('amount',0) or 0
    cash_in_hand = assets.filter(account_group__name="cash_in_hand",account_group__category_type="asset").aggregate(amount=Sum('amount')).get('amount',0) or 0
    current_asset = assets.filter(account_group__name="current_asset",account_group__category_type="asset").aggregate(amount=Sum('amount')).get('amount',0) or 0
    account_group = ['bank_account','cash_in_hand','current_asset']
    other_asset = assets.exclude(account_group__name__in=account_group).aggregate(balance=Sum('amount')).get('amount',0) or 0

    capital_account_amount = liabilities.filter(account_group__name="capital_account",account_group__category_type="liability").aggregate(balance=Sum('balance')).get('balance',0) or 0 
    current_liability = liabilities.filter(account_group__name="current_liability",account_group__category_type="liability").aggregate(balance=Sum('balance')).get('balance',0) or 0 
    loans_and_liability = liabilities.filter(account_group__name="loans_and_liability",account_group__category_type="liability").aggregate(balance=Sum('balance')).get('balance',0) or 0 
    liability_group = ['capital_account','current_liability','loans_and_liability']
    other_liability = liabilities.exclude(account_group__name__in=liability_group).aggregate(balance=Sum('balance')).get('balance',0) or 0

    lend_amount = lend.aggregate(balance=Sum('balance')).get('balance',0) or 0 
    satisfy_amount = lend.aggregate(paid=Sum('paid')).get('paid',0) or 0 
    
    cash_in_hand_amount = cash_debit + cash_in_hand - lend_amount 
    bank_asset_amount = bank_asset + bank_debit

    capital_amount = other_asset + cash_in_hand + bank_asset + capital_account + current_asset
    loan = cash_credit + loans_and_liability + other_liability

    debit = cash_in_hand_amount + bank_asset_amount + other_asset + current_asset + lend_amount
    credit = capital_amount + loan + bank_credit + current_liability 

    context = {
        "capital_account" : capital_account,
        "capital_amount" : capital_amount,
        "cash_debit" : cash_debit,
        "cash_credit" : cash_credit,
        "bank_credit" : bank_credit,
        "bank_debit" : bank_debit,
        "credit" : credit,
        "debit" : debit,
        "loans_given" : lend_amount,

        "bank_asset_amount" : bank_asset_amount,
        "cash_in_hand_amount" : cash_in_hand_amount,
        "current_asset" : current_asset,
        "other_asset" : other_asset,
        "current_liability" : current_liability,
        "loans_and_liability" : loan,

        "title" : 'Balance Sheet',

        "from_date" : filter_from_date,
        "to_date" : filter_to_date,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "purchases" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,

        }
    return render(request,'finance/balance_sheet.html', context)


@check_mode
@login_required
def print_donation_slip(request, pk):
    instance = get_object_or_404(Donation.objects.filter(pk=pk, is_deleted=False))
    donation_payments = Donation.objects.filter(pk=pk, is_deleted=False)
    today = datetime.date.today()
    total_amount = 0

    if donation_payments:
        total_amount = donation_payments.aggregate(
            paid=Sum('paid')).get('paid', 0.00)

    context = {
        'title': "Fee Slip",
        "donation_payments": donation_payments,
        "instance": instance,
        "today": today,
        "total_amount": total_amount,
    }
    return render(request, 'finance/donation_receipt.html', context)


@check_mode
@login_required
def create_cash_book(request):
    today = datetime.date.today()    
    form = DateForm(initial={'date':today})

    context = {
        'title' : "Create Cash Book",
        "form" : form,
        "url" : reverse('finance:cash_book'),

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'finance/create_cash_book.html',context)


@check_mode
@login_required
def cash_book(request):
    instances = Journel.objects.filter(is_deleted=False)        
    current_college = get_current_college(request)
    created_date = current_college.date_added
    date = request.GET.get('date')
    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
            from_date = created_date
            year_cr = from_date.year
            year = date.year
            last_year = year - 1
            to_date = datetime.date(last_year,12,31)
        except ValueError:
            date_error = "yes" 
        if date_error == "no":
            title = "Daily Cash Book : Date %s" %(str(date))
            if year_cr < last_year:
                instances = instances.filter(is_deleted=False,date__range=[from_date,to_date])
            else:
                instances = instances.filter(is_deleted=False,date = from_date) 

    debit = instances.filter(transaction__isnull=False).aggregate(amount=Sum('cash_debit')).get('amount',0) or 0
    credit = instances.filter(transaction__isnull=False).aggregate(amount=Sum('cash_credit')).get('amount',0) or 0
   
    cash_accounts = CashAccount.objects.filter(is_deleted=False,college=current_college)
    first_balance = cash_accounts.aggregate(amount=Sum('first_time_balance')).get('amount',0) or 0

    debit_amount = debit + first_balance

    context = {
        "credit" : credit,
        "debit" : debit_amount,
        "title" : title,

        "date" : date,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "purchases" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,

        }
    return render(request,'finance/cashbook.html', context)


@check_mode
@login_required
def create_bank_book(request):
    today = datetime.date.today()    
    current_college = get_current_college(request)
    form = DateForm(initial={'date':today}) 
    context = {
        'title' : "Create Bank Book",
        "form" : form,
        "url" : reverse('finance:bank_book'),

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'finance/create_cash_book.html',context)


@check_mode
@login_required
def bank_book(request):
    instances = Journel.objects.filter(is_deleted=False)        
    current_college = get_current_college(request)
    created_date = current_college.date_added
    date = request.GET.get('date')
    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
            from_date = created_date
            year_cr = from_date.year
            year = date.year
            last_year = year - 1
            to_date = datetime.date(last_year,12,31)
        except ValueError:
            date_error = "yes" 
        if date_error == "no":
            title = "Daily Bank Book : Date %s" %(str(date))
            if year_cr < last_year:
                instances = instances.filter(is_deleted=False,date__range=[from_date,to_date])
            else:
                instances = instances.filter(is_deleted=False,date = from_date) 

    debit = instances.filter(transaction__isnull=False).aggregate(amount=Sum('bank_debit')).get('amount',0) or 0
    credit = instances.filter(transaction__isnull=False).aggregate(amount=Sum('bank_credit')).get('amount',0) or 0
   
    bank_accounts = BankAccount.objects.filter(is_deleted=False,college=current_college)
    first_balance = bank_accounts.aggregate(amount=Sum('first_time_balance')).get('amount',0) or 0

    debit_amount = debit + first_balance

    context = {
        "credit" : credit,
        "debit" : debit_amount,
        "title" : title,
        "bank_accounts" : bank_accounts,

        "date" : date,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "purchases" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,

        }
    return render(request,'finance/bank_book.html', context)


@check_mode
@login_required
def create_monthly_transaction(request):
    today = datetime.date.today()
    if today.month > 3 :
        year = today.year 
    else :
        year = today.year - 1 
    from_date = datetime.date(year,4,1)
    form = LedgerForm(initial={'to_date':today,'from_date':from_date})

    context = {
        'title' : "Daily/ Monthly Transaction",
        "form" : form,
        "url" : reverse('finance:monthly_transaction'),

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,

    }
    return render(request,'finance/create_balance_sheet.html',context)


@check_mode
@login_required
def monthly_transaction(request):        
    current_college = get_current_college(request)
    transaction_categories = AccountHead.objects.filter(is_deleted=False,college=current_college)
    instances = Journel.objects.filter(is_deleted=False,transaction__account_head__in=transaction_categories)
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    date_error = "no"
    filter_date_period = False
    filter_from_date = from_date
    filter_to_date = to_date
    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date()            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True
    
    if filter_date_period:
        title = "Finance Transaction Between %s And %s" %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, date__range=[from_date, to_date])  

    date = str(from_date) + "," +str(to_date)

    income = instances.filter(transaction__isnull=False).aggregate(amount=Sum('income')).get('amount',0) or 0
    expense = instances.filter(transaction__isnull=False).aggregate(amount=Sum('expense')).get('amount',0) or 0

    loss = expense
    profit = income

    if loss > profit :
        net_profit = 0
        net_loss = loss - profit
    else :
        net_profit = profit - loss
        net_loss = 0 

    context = {
        "title" : title,
        "instances" : instances,
        "transaction_categories" : transaction_categories,

        "from_date" : from_date,
        "to_date" : to_date,
        "date" : date,

        "income" : income,
        "expense" : expense,
        "net_loss" : net_loss,
        "net_profit" : net_profit,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "purchases" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,

        }
    return render(request,'finance/monthly_transaction.html', context)


@check_mode
@ajax_required
@login_required
def get_chart(request):
    date = request.GET.get('date')
    date_list =  date.split(',')
    from_date = date_list[0]
    to_date = date_list[1]
    current_college = get_current_college(request)
    account_heads = AccountHead.objects.filter(is_deleted=False,college=current_college)
    debit_amounts = []
    credit_amounts = []
    categories = []
    for category in account_heads:
        instances = Journel.objects.filter(is_deleted=False,transaction__account_head=category,date__range=[from_date,to_date])
        debit_amount = instances.aggregate(amount=Sum('income')).get('amount',0) or 0
        credit_amount = instances.aggregate(amount=Sum('expense')).get('amount',0) or 0
        credit_amount = -1*credit_amount
        category_name = category.name.replace("_", " ")
        category_name = category_name.capitalize()
        debit_amounts.append(float(debit_amount))
        credit_amounts.append(float(credit_amount))
        categories.append(category_name)
    response_data = {
        "status" : "true",
        "debit_amounts" : debit_amounts,
        "credit_amounts" : credit_amounts,
        "categories" : categories,
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_compare_transaction(request):
    today = datetime.date.today()
    if today.month > 3 :
        year = today.year 
    else :
        year = today.year - 1 
    from_date = datetime.date(year,4,1)
    form = CompareForm(initial={'to_date':today,'from_date':from_date})

    context = {
        'title' : "Compare Transaction",
        "form" : form,
        "url" : reverse('finance:compare_transaction'),

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,

    }
    return render(request,'finance/create_compare_transaction.html',context)


@check_mode
@login_required
def compare_transaction(request):          
    current_college = get_current_college(request)
    transaction_categories = AccountHead.objects.filter(is_deleted=False,college=current_college)
    instances = Journel.objects.filter(is_deleted=False,transaction__account_head__in=transaction_categories)  
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')
    date_error = "no"
    filter_date_period = False
    filter_from_date = from_date
    filter_to_date = to_date
    filter_start_date = start_date
    filter_end_date = end_date
    if from_date and to_date and start_date and end_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() 
            start_date = datetime.datetime.strptime(start_date, '%m/%d/%Y').date()
            end_date = datetime.datetime.strptime(end_date, '%m/%d/%Y').date()         
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True
    
    if filter_date_period:
        title = "Comparison of Transaction Between %s - %s And %s - %s" %(str(filter_from_date),str(filter_to_date),str(filter_start_date),str(filter_end_date))
        if date_error == "no":
            first_instances = instances.filter(is_deleted=False, date__range=[from_date, to_date])  
            last_instances = instances.filter(is_deleted=False, date__range=[start_date, end_date])  

    first_date = str(from_date) + "," + str(to_date)
    last_date = str(start_date) + "," + str(end_date)

    first_date1 = str(filter_from_date) + "-" + str(filter_to_date)
    last_date1 = str(filter_start_date) + "-" + str(filter_end_date)

    first_income = first_instances.filter(transaction__isnull=False).aggregate(amount=Sum('income')).get('amount',0) or 0
    first_expense = first_instances.filter(transaction__isnull=False).aggregate(amount=Sum('expense')).get('amount',0) or 0

    first_loss = first_expense
    first_profit = first_income

    if first_loss > first_profit :
        first_net_profit = 0
        first_net_loss = first_loss - first_profit
    else :
        first_net_profit = first_profit - first_loss
        first_net_loss = 0 

    last_income = last_instances.filter(transaction__isnull=False).aggregate(amount=Sum('income')).get('amount',0) or 0
    last_expense = last_instances.filter(transaction__isnull=False).aggregate(amount=Sum('expense')).get('amount',0) or 0

    last_loss = last_expense
    last_profit = last_income

    if last_loss > last_profit :
        last_net_profit = 0
        last_net_loss = last_loss - last_profit
    else :
        last_net_profit = last_profit - last_loss
        last_net_loss = 0 

    context = {
        "title" : title,
        "first_instances" : first_instances,
        "last_instances" : last_instances,
        "transaction_categories" : transaction_categories,

        "from_date" : from_date,
        "to_date" : to_date,
        "start_date" : start_date,
        "end_date" : end_date,

        "filter_from_date" : filter_from_date,
        "filter_to_date" : filter_to_date,
        "filter_start_date" : filter_start_date,
        "filter_end_date" : filter_end_date,

        "first_date" : first_date,
        "last_date" : last_date,
        "first_date1" : first_date1,
        "last_date1" : last_date1,

        "first_income" : first_income,
        "first_expense" : first_expense,
        "first_net_loss" : first_net_loss,
        "first_net_profit" : first_net_profit,

        "last_income" : last_income,
        "last_expense" : last_expense,
        "last_net_loss" : last_net_loss,
        "last_net_profit" : last_net_profit,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "purchases" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,

        }
    return render(request,'finance/compare_transaction.html', context)


@check_mode
@ajax_required
@login_required
def get_compare_chart(request):
    first_date = request.GET.get('first_date')
    date_list =  first_date.split(',')
    from_date = date_list[0]
    to_date = date_list[1]
    last_date = request.GET.get('last_date')
    date_list1 =  last_date.split(',')
    start_date = date_list1[0]
    end_date = date_list1[1]
    current_college = get_current_college(request)
    account_heads = AccountHead.objects.filter(is_deleted=False,college=current_college)
    debit_amounts = []
    credit_amounts = []
    categories = []
    debit_amounts1 = []
    credit_amounts1 = []
    for category in account_heads:
        instances = Journel.objects.filter(is_deleted=False,transaction__account_head=category,date__range=[from_date,to_date])
        debit_amount = instances.aggregate(amount=Sum('income')).get('amount',0) or 0
        credit_amount = instances.aggregate(amount=Sum('expense')).get('amount',0) or 0
        credit_amount = -1 * credit_amount
        category_name = category.name.replace("_", " ")
        category_name = category_name.capitalize()
        debit_amounts.append(float(debit_amount))
        credit_amounts.append(float(credit_amount))
        categories.append(category_name)

        instances1 = Journel.objects.filter(is_deleted=False,transaction__account_head=category,date__range=[start_date,end_date])
        debit_amount1 = instances1.aggregate(amount=Sum('income')).get('amount',0) or 0
        credit_amount1 = instances1.aggregate(amount=Sum('expense')).get('amount',0) or 0
        credit_amount1 = -1 * credit_amount1
        debit_amounts1.append(float(debit_amount1))
        credit_amounts1.append(float(credit_amount1))
    response_data = {
        "status" : "true",
        "debit_amounts" : debit_amounts,
        "credit_amounts" : credit_amounts,
        "categories" : categories,
        "debit_amounts1" : debit_amounts1,
        "credit_amounts1" : credit_amounts1,
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_ledger_transaction(request):
    today = datetime.date.today()
    if today.month > 3 :
        year = today.year 
    else :
        year = today.year - 1 
    from_date = datetime.date(year,4,1)
    form = LedgerTransactionForm(initial={'to_date':today,'from_date':from_date})

    context = {
        'title' : "Ledger Report",
        "form" : form,
        "url" : reverse('finance:ledger_transaction'),

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,

    }
    return render(request,'finance/create_ledger_transaction.html',context)


@check_mode
@login_required
def ledger_transaction(request):
    instances = Journel.objects.filter(is_deleted=False)        
    current_college = get_current_college(request)
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    account_head = request.GET.get('account_head')
    date_error = "no"
    filter_date_period = False
    filter_from_date = from_date
    filter_to_date = to_date
    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date() 
        
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True
    
    if filter_date_period:
        title = "Finance Transaction Between %s And %s" %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, date__range=[from_date, to_date],transaction__account_head__pk=account_head)  

    date = str(from_date) + "," +str(to_date)

    income = instances.filter(transaction__isnull=False).aggregate(amount=Sum('income')).get('amount',0) or 0
    expense = instances.filter(transaction__isnull=False).aggregate(amount=Sum('expense')).get('amount',0) or 0

    loss = expense
    profit = income

    if loss > profit :
        net_profit = 0
        net_loss = loss - profit
    else :
        net_profit = profit - loss
        net_loss = 0 
    opening_balance_debit = Journel.objects.filter(is_deleted=False,date__lt=from_date,transaction__account_head__pk=account_head).aggregate(amount=Sum('income')).get('amount',0) or 0
    opening_balance_credit = Journel.objects.filter(is_deleted=False,date__lt=from_date,transaction__account_head__pk=account_head).aggregate(amount=Sum('expense')).get('expense',0) or 0
    context = {
        "title" : title,
        "instances" : instances,
        "account_head" : account_head,
        "opening_balance_debit" : opening_balance_debit,
        "opening_balance_credit" : opening_balance_credit,

        "from_date" : from_date,
        "to_date" : to_date,
        "date" : date,

        "income" : income,
        "expense" : expense,
        "net_loss" : net_loss,
        "net_profit" : net_profit,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "purchases" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,

        }
    return render(request,'finance/ledger_transaction.html', context)


@check_mode
@login_required
def create_daily_fee_report(request):
    today = datetime.date.today()
    if today.month > 3 :
        year = today.year 
    else :
        year = today.year - 1 
    from_date = datetime.date(year,4,1)
    form = LedgerForm(initial={'to_date':today,'from_date':from_date})

    context = {
        'title' : "Create Daily Report",
        "form" : form,
        "url" : reverse('finance:daily_fee_report'),

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,

    }
    return render(request,'finance/create_balance_sheet.html',context)


@check_mode
@login_required
def daily_fee_report(request):  
    current_college = get_current_college(request)
    fee_categories = FeeCategory.objects.filter(is_deleted=False,college=current_college)
    instances = Journel.objects.filter(is_deleted=False,transaction__fee_category__in=fee_categories)  
    from_date = request.GET.get('from_date')
    to_date = request.GET.get('to_date')
    date_error = "no"
    filter_date_period = False
    filter_from_date = from_date
    filter_to_date = to_date
    if from_date and to_date:
        try:
            from_date = datetime.datetime.strptime(from_date, '%m/%d/%Y').date()
            to_date = datetime.datetime.strptime(to_date, '%m/%d/%Y').date()            
        except ValueError:
            date_error = "yes"  
            
        filter_date_period = True
    
    if filter_date_period:
        title = "Report : From %s to %s " %(str(from_date),str(to_date))
        if date_error == "no":
            instances = instances.filter(is_deleted=False, date__range=[from_date, to_date])  

    income = instances.filter(transaction__isnull=False).aggregate(amount=Sum('income')).get('amount',0) or 0
    bank_debit = instances.filter(transaction__isnull=False).aggregate(amount=Sum('bank_debit')).get('amount',0) or 0
    cash_debit = instances.filter(transaction__isnull=False).aggregate(amount=Sum('cash_debit')).get('amount',0) or 0

    context = {

        "title" : 'Daily Fees Report',
        "instances" : instances,
        "from_date" : filter_from_date,
        "to_date" : filter_to_date,

        "income" : income,
        "bank_debit" : bank_debit,
        "cash_debit" : cash_debit,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "purchases" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,

        }
    return render(request,'finance/daily_fee_report.html', context)


@check_mode
@login_required
def create_income_expenditure_account(request):
    today = datetime.date.today()    
    current_college = get_current_college(request)
    form = DateForm(initial={'date':today}) 
    context = {
        'title' : "Create Income and Expenditure Account",
        "form" : form,
        "url" : reverse('finance:income_expenditure_account'),

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'finance/create_cash_book.html',context)


@check_mode
@login_required
def income_expenditure_account(request):
    instances = Journel.objects.filter(is_deleted=False)     
    account_head = []   
    for instance in instances:
        if instance.transaction.account_head:
            account_head.append(instance.transaction.account_head.pk)
    account_heads = AccountHead.objects.filter(is_deleted=False,pk__in=account_head)
    current_college = get_current_college(request)
    created_date = current_college.date_added
    date = request.GET.get('date')
    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
        except ValueError:
            date_error = "yes" 
        if date_error == "no":
            title = "Income And Expenditure Account : Date %s" %(str(date))
            instances = instances.filter(is_deleted=False,date__lte = date,transaction__account_head__pk__in=account_heads) 

    incomes = instances.filter(transaction__isnull=False,income__gt=0,transaction__account_head__pk__in=account_heads)
    expenses = instances.filter(transaction__isnull=False,expense__gt=0,transaction__account_head__pk__in=account_heads)

    debit = instances.filter(transaction__isnull=False,transaction__account_head__pk__in=account_heads).aggregate(amount=Sum('income')).get('amount',0) or 0
    credit = instances.filter(transaction__isnull=False,transaction__account_head__pk__in=account_heads).aggregate(amount=Sum('expense')).get('amount',0) or 0
   
    profit = debit - credit
    if profit > 0:
        profit = profit
        loss = 0
    else:
        loss = credit - debit
        profit = 0
    context = {
        "credit" : credit,
        "debit" : debit,
        "title" : title,
        "date" : date,

        "incomes" : incomes,
        "expenses" : expenses,
        "account_heads" : account_heads,

        "profit" : profit,
        "loss" : loss,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "purchases" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,

        }
    return render(request,'finance/income_expenditure_account.html', context)


@check_mode
@login_required
def create_trail_balance(request):
    today = datetime.date.today()    
    current_college = get_current_college(request)
    form = DateForm(initial={'date':today}) 
    context = {
        'title' : "Create Trail Balance",
        "form" : form,
        "url" : reverse('finance:trail_balance'),

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'finance/create_cash_book.html',context)


@check_mode
@login_required
def trail_balance(request):
    instances = Journel.objects.filter(is_deleted=False)     
    account_heads = AccountHead.objects.filter(is_deleted=False)
    current_college = get_current_college(request)
    created_date = current_college.date_added
    date = request.GET.get('date')
    date_error = "no"
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
        except ValueError:
            date_error = "yes" 
        if date_error == "no":
            title = "Trail Balance : Date %s" %(str(date))
            instances = instances.filter(is_deleted=False,date__lte = date) 

    incomes = instances.filter(transaction__isnull=False,income__gt=0)
    expenses = instances.filter(transaction__isnull=False,expense__gt=0)

    debit = instances.filter(transaction__isnull=False,transaction__account_head__in=account_heads).aggregate(amount=Sum('income')).get('amount',0) or 0
    credit = instances.filter(transaction__isnull=False,transaction__account_head__in=account_heads).aggregate(amount=Sum('expense')).get('amount',0) or 0
   
    profit = debit - credit
    if profit > 0:
        profit = profit
        loss = 0
    else:
        loss = credit - debit
        profit = 0
    credit_amount = credit + profit
    debit_amount = debit + loss
    count = 1
    for account_head in account_heads:
        count += 1
    context = {
        "credit" : credit,
        "debit" : debit,
        "title" : title,
        "date" : date,
        "count" : count,

        "credit_amount" : credit_amount,
        "debit_amount" : debit_amount,
        "incomes" : incomes,
        "expenses" : expenses,
        "account_heads" : account_heads,

        "profit" : profit,
        "loss" : loss,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "purchases" : True,
        "is_need_grid_system" : True,
        "is_need_animations": True,
        "is_need_datetime_picker" : True,

        }
    return render(request,'finance/trail_balance.html', context)


@check_mode
@login_required
def create_fitr_zakat_payment(request, pk):
    url = reverse('finance:create_fitr_zakat_payment',
                  kwargs={"pk": pk})
    family = get_object_or_404(Family.objects.filter(pk=pk))

    if request.method == "POST":
        response_data = {}
        form = FitrzakatTransactionForm(request.POST)
        if form.is_valid():
            amount = request.POST.getlist('balance')
            special_fee = request.POST.getlist('special_fee')
            total_amount = 0
            pairing_amount_family = zip(amount,special_fee)
            title = ""
            for item in pairing_amount_family:
                amount = Decimal(item[0])
                fitr_zakat_fee = item[1]
                if FitrZakatCollection.objects.filter(pk=fitr_zakat_fee,family=family,is_deleted=False).exists():
                    fitr_zakat = FitrZakatCollection.objects.get(pk=fitr_zakat_fee,family=family,is_deleted=False)

                    fitr_zakat.paid = fitr_zakat.paid + amount
                    fitr_zakat.balance = fitr_zakat.balance - amount
                    fitr_zakat.save()

                    title = title + fitr_zakat.fitr_zakat.title + ", "

                total_amount += amount

            transaction_mode = form.cleaned_data['transaction_mode']
            date = form.cleaned_data['date']

            actual_amount = amount
            total_amount_deducted = 0

            if AccountHead.objects.filter(name="fitre_zakat_collection", is_deleted=False, is_system_generated=True).exists():
                account_head = AccountHead.objects.get(
                    name="fitre_zakat_collection", is_deleted=False, is_system_generated=True)

                advance_amount = family.advance_amount

                total_amount_deducted += amount



            family.balance -= total_amount_deducted
            family.save()

            data = form.save(commit=False)

            if transaction_mode == "cash":
                data.payment_mode = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
            elif transaction_mode == "bank":
                payment_mode = form.cleaned_data['payment_mode']
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = form.cleaned_data['is_cheque_withdrawed']
                    data.card_details = None

                elif payment_mode == "internet_banking":
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False

                elif payment_mode == "card_payment":
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False

            if not account_head == "credit":
                data.auto_id = get_auto_id(Transaction)
                data.creator = request.user
                data.updater = request.user
                data.transaction_type = "income"
                data.family = family
                data.amount = total_amount
                data.title = family.family_name
                data.description = title
                data.account_head=account_head
                data.save()

            if transaction_mode == "cash":
                cash_debit = total_amount
                bank_debit = 0
            elif transaction_mode == "bank":
                cash_debit = 0
                bank_debit = total_amount

            Journel.objects.create(
                date = datetime.date.today(),
                cash_debit = cash_debit,
                bank_debit = bank_debit,
                transaction = data,
                income = total_amount
            )
               
            response_data = {
                'status': 'true',
                'title': "Fee Payment Created",
                'redirect': 'true',
                'redirect_url': reverse('families:family', kwargs={'pk': family.pk}),
                'message': "Fee Payment Created Successfully"
            }
        else:
            form = FitrzakatTransactionForm(request.POST)

            response_data = {
                "status": "false",
                "title": " Error !",
                "message": "Form Validation Error",
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        transaction_form = FitrzakatTransactionForm()
        special_fees = FitrZakatCollection.objects.filter(family=family,balance__gt=0)
        context = {
            "title": "Create Fitr Zakat Payment",
            "transaction_form": transaction_form,
            "special_fees" : special_fees,
            "url": url,
            "family": family
        }
        return render(request, 'finance/entry_fitr_zakat_fee.html', context)


@check_mode
@login_required
def create_zakat_payment(request, pk):
    url = reverse('finance:create_zakat_payment',
                  kwargs={"pk": pk})
    instance = get_object_or_404(Zakat.objects.filter(pk=pk))

    if request.method == "POST":
        response_data = {}
        form = ZakatCollectionForm(request.POST)
        transaction_form = ZakatTransactionForm(request.POST)

        if form.is_valid() and transaction_form.is_valid():

            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            member = form.cleaned_data['member']
            amount = form.cleaned_data['amount']
            date = form.cleaned_data['date']

            data1 = form.save(commit=False)
            transaction_form.amount = amount
            data1.zakat = instance

            if AccountHead.objects.filter(name="zakat_collection", is_deleted=False, is_system_generated=True).exists():
                account_head = AccountHead.objects.get(
                    name="zakat_collection", is_deleted=False, is_system_generated=True)

                data1 = ZakatCollection.objects.create(
                    date=date,
                    member = member,
                    amount=amount,
                    zakat = instance,
                    auto_id=get_auto_id(ZakatCollection),
                    creator=request.user,
                    updater=request.user
                )
                data1.save()
            # create income
            data = transaction_form.save(commit=False)

            if transaction_mode == "cash":
                data.payment_mode = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
            elif transaction_mode == "bank":
                payment_mode = transaction_form.cleaned_data['payment_mode']
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed']
                    data.card_details = None

                elif payment_mode == "internet_banking":
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False

                elif payment_mode == "card_payment":
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False

            if not account_head == "credit":
                data.auto_id = get_auto_id(Transaction)
                data.creator = request.user
                data.updater = request.user
                data.transaction_type = "income"
                data.date = date
                data.title = member.name
                data.amount = amount
                data.zakat_payment = data1
                data.account_head = account_head
                data.save()


            response_data = {
                'status': 'true',
                'title': "Zakat Payment Created",
                'redirect': 'true',
                'redirect_url': reverse('zakat:zakat', kwargs={'pk': instance.pk}),
                'message': "Zakat Payment Created Successfully"
            }
        else:
            transaction_form = ZakatTransactionForm(request.POST)
            form = ZakatCollectionForm(request.POST)

            message = generate_form_errors(form,formset=False)
            message1 = generate_form_errors(transaction_form,formset=False)

            response_data = {
                "status": "false",
                "title": " Error !",
                "message": "Form Validation Error",
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        transaction_form = ZakatTransactionForm()
        form = ZakatCollectionForm()


        context = {
            "title": "Create Zakat Payment",
            "transaction_form": transaction_form,
            "form" : form,
            "url": url,
            "instance": instance
        }
        return render(request, 'finance/entry_zakat_fee.html', context)


@check_mode
@login_required
def edit_zakat_payment(request, pk):
    url = reverse('finance:edit_zakat_payment',
                  kwargs={"pk": pk})
    instance = get_object_or_404(ZakatCollection.objects.filter(pk=pk))
    zakat = instance.zakat

    if request.method == "POST":
        response_data = {}
        form = ZakatCollectionForm(request.POST,instance=instance)
        transaction_form = ZakatTransactionForm(request.POST,instance=instance)

        if form.is_valid() and transaction_form.is_valid():

            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            member = form.cleaned_data['member']
            amount = form.cleaned_data['amount']
            date = form.cleaned_data['date']

            data1 = form.save(commit=False)
            transaction_form.amount = amount


            if AccountHead.objects.filter(name="zakat_collection", is_deleted=False, is_system_generated=True).exists():
                account_head = AccountHead.objects.get(
                    name="zakat_collection", is_deleted=False, is_system_generated=True)

                Transaction.objects.filter(zakat_payment=instance,zakat_payment__zakat=zakat).update(amount=amount)

            # create income
            data = transaction_form.save(commit=False)

            if transaction_mode == "cash":
                data.payment_mode = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
            elif transaction_mode == "bank":
                payment_mode = transaction_form.cleaned_data['payment_mode']
                if payment_mode == "cheque_payment":
                    is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed']
                    data.card_details = None

                elif payment_mode == "internet_banking":
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False

                elif payment_mode == "card_payment":
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False

            if not account_head == "credit":
                data.auto_id = get_auto_id(Transaction)
                data.creator = request.user
                data.updater = request.user
                data.transaction_type = "income"
                data.date = date
                data.title = member.name
                data.amount = amount
                data.account_head = account_head
                data.save()

            response_data = {
                'status': 'true',
                'title': "Fee Payment Updated",
                'redirect': 'true',
                'redirect_url': reverse('zakat:zakat', kwargs={'pk': instance.zakat.pk}),
                'message': "Fee Payment Updated Successfully"
            }
        else:
            transaction_form = ZakatTransactionForm(instance=instance)
            form = ZakatCollectionForm(instance=instance)

            message = generate_form_errors(form,formset=False)
            message1 = generate_form_errors(transaction_form,formset=False)

            response_data = {
                "status": "false",
                "title": " Error !",
                "message": "Form Validation Error",
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        transaction_form = ZakatTransactionForm(instance=instance)
        form = ZakatCollectionForm(instance=instance)


        context = {
            "title": "Create Zakat Payment",
            "transaction_form": transaction_form,
            "form" : form,
            "url": url,
            "instance": instance
        }
        return render(request, 'finance/entry_zakat_fee.html', context)


@check_mode
@login_required
def print_fee_slip(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(Transaction.objects.filter(pk=pk,college=current_college,is_deleted=False))
    today = datetime.date.today()
    reports = []
    
    student = instance.admission
    name = student.name
    batch = student.batch.name
    fee = student.department.fee
    date = instance.date
    month = instance.month
    year = instance.date.year
    paid = instance.admission.paid  
    balance = instance.admission.balance
    amount = instance.amount
    report = {
        "name": name,
        "batch": batch,
        "date" : date,
        "month": month,
        "year": year,
        "amount" : amount,
        "fee": fee,
        "paid":paid,
        "balance":balance
    }
    context = {
        'title' : "Fee Slip",
        'institute_title' : current_college.name,
        'report' : report,
        "instance" : instance,
        "current_college" : current_college,
        "today" : today,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'finance/receipt.html', context) 


@check_mode
@login_required
def print_salary_slip(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(Transaction.objects.filter(pk=pk,college=current_college,is_deleted=False))

    reports = []
    
    staff = instance.staff
    name = staff.name
    amount = instance.amount
    designation = staff.designation
    date = instance.date
    staff_salary = instance.staff_salary 
    month = staff_salary.month
    year = staff_salary.year
    salary = staff.salary
    advance = staff_salary.advance
    paid = staff_salary.paid
    balance = staff_salary.balance
    report = {
        "name": name,
        "designation": designation,
        "date" : date,
        "month": month,
        "year": year,
        "salary":salary,
        "amount" : amount,
        "paid":paid,
        "balance":balance,
        "advance":advance
    }


    context = {
        'title' : "Salary Slip",
        'report' : report,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'finance/print_salary_slip.html', context) 


@check_mode
@login_required
def print_sale_receipt(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(Transaction.objects.filter(pk=pk,college=current_college,is_deleted=False))

    reports = []
    
    student = instance.student
    name = student.name
    batch = student.batch.name
    date = instance.date
    sale = instance.sale 
    paid = sale.paid  
    sale_amount = sale.total
    sale_item_amount = sale.sub_total_amount
    discount = sale.discount
    advance = sale.advance
    balance = sale.balance
    amount = instance.amount

    items = SaleItem.objects.filter(sale=sale,college=current_college,is_deleted=False)

    report = {
        "name": name,
        "batch": batch,
        "date" : date,
        "amount" : amount,
        "sale_amount": sale_amount,
        "sale_item_amount":sale_item_amount,
        "discount":discount,
        "paid":paid,
        "balance":balance,
        "advance":advance,
    }
    context = {
        'title' : "Sale Receipt",
        'report' : report,
        'items':items,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'finance/print_sale_receipt.html', context) 


# @check_mode
# @login_required
# def create_fee_payment(request): 
#     current_college = get_current_college(request) 
#     transaction_category =get_object_or_404(Transaction.objects.filter(is_deleted=False,college=current_college))
#     if request.method == "POST":
#         form = FeePaymentForm(request.POST)
#         form.fields['account_heads'].queryset = AccountHead.objects.filter(is_deleted=False,college=current_college)
#         form.fields['account_groups'].queryset = AccountGroup.objects.filter(is_deleted=False,college=current_college)
        
#         if form.is_valid(): 

#             auto_id = get_auto_id(Transaction)
#             a_id = get_a_id(Transaction,request)
            
#             transaction_mode = form.cleaned_data['transaction_mode']
#             payment_to = form.cleaned_data['payment_to']
#             amount = form.cleaned_data['amount']
#             student = form.cleaned_data['student']

#             amount_remaining = amount
#             student_fees = StudentFee.objects.filter(college=current_college,is_deleted=False,student=student).order_by('date_added')

#             latest_student_fee = None
#             if student_fees:
#                 latest_student_fee = StudentFee.objects.filter(college=current_college,is_deleted=False,student=student).latest('date_added')

#             if latest_student_fee:
#                 advance_amount = latest_student_fee.advance
#                 amount_remaining += advance_amount
#             saved_fees = []
#             for student_fee in student_fees:
#                 balance = student_fee.balance
#                 if balance > 0:
#                     if balance <= amount_remaining:
#                         student_fee.balance = 0
#                         student_fee.paid = student_fee.amount
#                         student_fee.save()
#                         amount_remaining -= balance

#                         dict_obj = {
#                             "student_fee" : student_fee,
#                             "amount" : student_fee.amount,
#                         }
#                         saved_fees.append(dict_obj)

#                     else:
#                         if amount_remaining > 0:
#                             student_fee.balance = student_fee.balance - amount_remaining
#                             student_fee.paid =  student_fee.paid + amount_remaining
#                             student_fee.save()
#                             amount_remaining -= balance

#                             dict_obj = {
#                                 "student_fee" : student_fee,
#                                 "amount" : student_fee.paid + amount_remaining,
#                             }
#                             saved_fees.append(dict_obj)

#                         else:
#                             break

#             if amount_remaining > 0:
#                 if StudentFee.objects.filter(college=current_college,is_deleted=False,student=student):
#                     latest_student_fee = StudentFee.objects.filter(college=current_college,is_deleted=False,student=student).latest('date_added')
#                     latest_student_fee.advance = amount_remaining
#                     latest_student_fee.save()

#             #create fee payment
#             data = form.save(commit=False)
            
#             if transaction_mode == "cash":
#                 data.payment_mode = None
#                 data.payment_to = "cash_account"
#                 data.bank_account = None
#                 data.cheque_details = None
#                 data.card_details = None
#                 data.is_cheque_withdrawed = False
#                 balance = data.cash_account.balance
#                 balance = balance + amount
#                 CashAccount.objects.filter(pk=data.cash_account.pk,college=current_college).update(balance=balance)
#             elif transaction_mode == "bank":
#                 balance = data.bank_account.balance
#                 balance = balance + amount
#                 BankAccount.objects.filter(pk=data.bank_account.pk,college=current_college).update(balance=balance)
#                 payment_mode = form.cleaned_data['payment_mode'] 
#                 if payment_mode == "cheque_payment":
#                     data.card_details = None
                        
#                 elif payment_mode == "internet_banking":
#                     data.payment_to = "bank_account"
#                     data.cash_account = None
#                     data.cheque_details = None
#                     data.card_details = None
#                     data.is_cheque_withdrawed = False
                
#                 elif payment_mode == "card_payment":
#                     data.payment_to = "bank_account"
#                     data.cash_account = None
#                     data.cheque_details = None
#                     data.is_cheque_withdrawed = False
            
#                 if payment_to == "cash_account":
#                     data.bank_account = None
#                 elif payment_to == "bank_account":
#                     data.cash_account = None

                
#             data.creator = request.user
#             data.updater = request.user
#             data.auto_id = auto_id            
#             data.transaction_type = "income"
#             data.college = current_college
#             data.transaction_category = transaction_category
#             data.student_fee = student_fee
#             data.a_id = a_id
#             data.title = "Fee from %s" %(student.name)
#             data.save()
#             for saved_fee in saved_fees:
#                 amount = saved_fee['amount']
#                 student_fee = saved_fee['student_fee']
#             auto_id = get_auto_id(StudentFeePayment)
#             a_id = get_a_id(StudentFeePayment,request)

#             StudentFeePayment.objects.create(
#                 college=current_college,
#                 auto_id=auto_id,
#                 a_id=a_id,
#                 transaction=data,
#                 amount=amount,
#                 student_fee=student_fee,
#                 creator=request.user,
#                 updater=request.user,
#                 )

#             response_data = {
#                 "status" : 'true',
#                 "title" : "Successfully Created" ,
#                 "redirect" : "true",
#                 "redirect_url" : reverse('finance:transaction', kwargs = {'pk' : data.pk}),
#                 "message" : "Fee payment Successfully Created."
#             }
#         else:
                      
#             message = generate_form_errors(form,formset=False) 
                  
#             response_data = {
#                 "status" : 'false',
#                 "title" : "Form Validation Error" ,
#                 "stable" : "true",
#                 "message" : str_(message),
#             }
            
#         return HttpResponse(json.dumps(response_data), content_type='application/javascript')

#     else: 
#         form = FeePaymentForm()
#         form.fields['account_heads'].queryset = AccountHead.objects.filter(is_deleted=False,college=current_college)
#         form.fields['account_groups'].queryset = AccountGroup.objects.filter(is_deleted=False,college=current_college)
#         context = {
#             "form" : form,
#             "title" : "Create Fee Payment " ,
#             "url" : reverse('finance:create_fee_payment'),
#             "redirect" : True,

#             "payment_page" : True,

#             "is_need_select_picker" : True,
#             "is_need_popup_box" : True,
#             "is_need_custom_scroll_bar" : True,
#             "is_need_wave_effect" : True,
#             "is_need_bootstrap_growl" : True,
#             "is_need_grid_system" : True,
#             "is_need_datetime_picker" : True,
#         }
#         return render(request, 'finance/create_fee_payment.html', context)


# @check_mode
# @login_required
# def edit_fee_payment(request,pk): 
#     current_college = get_current_college(request) 
#     instance = get_object_or_404(Transaction.objects.filter(pk=pk,is_deleted=False,college=current_college))

#     instance1 = instance.student_fee
#     edit_amount = instance.amount
#     instance2 = instance.cash_account
#     instance3 = instance.bank_account

#     if request.method == "POST":
#         form = FeePaymentForm(request.POST,instance=instance)
#         form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False,college=current_college)
#         form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False,college=current_college)
        
#         if form.is_valid(): 
#             transaction_mode = form.cleaned_data['transaction_mode']
#             payment_to = form.cleaned_data['payment_to']
#             amount = form.cleaned_data['amount']
            
#             #Update fee payment
#             data = form.save(commit=False)                                                                                                                               

#             balance = instance1.balance
#             paid = instance1.paid
#             advance = instance1.advance
#             fee_amount = instance1.amount
#             if edit_amount != data.amount:    
#                 if paid == edit_amount:
#                     paid = data.amount
#                     balance = fee_amount-data.amount
#                     if balance<0:
#                         advance = data.amount-fee_amount
#                 else:
#                     paid = paid-edit_amount + data.amount
#                     balance = fee_amount- paid
#                     if balance<0:
#                         advance = paid -fee_amount

#             StudentFee.objects.filter(pk=instance1.pk,college=current_college).update(balance=balance,paid=paid,advance=advance)
            
#             cash_balance = data.cash_account.balance
#             bank_balance = data.bank_account.balance   
#             if transaction_mode == "cash":
#                 if instance2 == None :
#                     cash_balance = cash_balance + amount
#                     bank_balance = bank_balance - amount
#                 else :
#                     cash_balance = cash_balance - edit_amount +amount
#                 BankAccount.objects.filter(pk=data.bank_account.pk).update(balance=bank_balance)
#                 CashAccount.objects.filter(pk=data.cash_account.pk).update(balance=cash_balance)
                
#                 data.payment_mode = None
#                 data.payment_to = "cash_account"
#                 data.bank_account = None
#                 data.cheque_details = None
#                 data.card_details = None
#                 data.is_cheque_withdrawed = False
                
#             elif transaction_mode == "bank":
#                 if instance3 == None :
#                     cash_balance = cash_balance - amount
#                     bank_balance = bank_balance + amount
#                 else :
#                     bank_balance = bank_balance - edit_amount +amount
#                 BankAccount.objects.filter(pk=data.bank_account.pk).update(balance=bank_balance)
#                 CashAccount.objects.filter(pk=data.cash_account.pk).update(balance=cash_balance)
                
#                 payment_mode = form.cleaned_data['payment_mode'] 
#                 if payment_mode == "cheque_payment":
#                     data.card_details = None
                        
#                 elif payment_mode == "internet_banking":
#                     data.payment_to = "bank_account"
#                     data.cash_account = None
#                     data.cheque_details = None
#                     data.card_details = None
#                     data.is_cheque_withdrawed = False
                
#                 elif payment_mode == "card_payment":
#                     data.payment_to = "bank_account"
#                     data.cash_account = None
#                     data.cheque_details = None
#                     data.is_cheque_withdrawed = False
            
#                 if payment_to == "cash_account":
#                     data.bank_account = None
#                 elif payment_to == "bank_account":
#                     data.cash_account = None
            
                
#             data.updater = request.user
#             data.date_updated = datetime.datetime.now()
#             data.college=current_college
#             data.save()
            
#             response_data = {
#                 "status" : 'true',
#                 "title" : "Successfully Updated" ,
#                 "redirect" : "true",
#                 "redirect_url" : reverse('finance:transaction', kwargs = {'pk' : data.pk}),
#                 "message" : "Fee payment Successfully Updated."
#             }
#         else:
            
#             message = ''            
#             message += generate_form_errors(form,formset=False) 
                  
#             response_data = {
#                 "status" : 'false',
#                 "title" : "Form Validation Error" ,
#                 "stable" : "true",
#                 "message" : message,
#             }
            
#         return HttpResponse(json.dumps(response_data), content_type='application/javascript')

#     else: 
#         form = FeePaymentForm(instance=instance)
#         form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False,college=current_college)
#         form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False,college=current_college)
#         context = {
#             "form" : form,
#             "title" : "Edit Fee Payment",
#             'instance' : instance,
#             "url" : reverse('finance:edit_fee_payment', kwargs={'pk':pk}),

#             "payment_page" : True,

#             "is_need_select_picker" : True,
#             "is_need_popup_box" : True,
#             "is_need_custom_scroll_bar" : True,
#             "is_need_wave_effect" : True,
#             "is_need_bootstrap_growl" : True,
#             "is_need_grid_system" : True,
#             "is_need_datetime_picker" : True,
#         }
#         return render(request, 'finance/entry_fee_payment.html', context)
  

# @check_mode
# @login_required
# def fee_payments(request):
#     current_college = get_current_college(request) 
 
#     instances = Transaction.objects.filter(is_deleted=False,college=current_college,transaction_category__name='student_fee')

#     query = request.GET.get("q")
#     if query:
#         instances = instances.filter(Q(student__name__icontains=query) | Q(date__icontains=query) | Q(amount__icontains=query))
        
#     title = "Fee Payments"
#     context = {
#         'title' : title,
#         "instances" : instances,

#         "is_need_select_picker" : True,
#         "is_need_popup_box" : True,
#         "is_need_custom_scroll_bar" : True,
#         "is_need_wave_effect" : True,
#         "is_need_bootstrap_growl" : True,
#         "is_need_grid_system" : True,
#         "is_need_chosen_select" : True,
#         "is_need_animations" : True,
#         "is_need_datetime_picker" : True,
#     }
#     return render(request,'finance/fee_payments.html',context) 


@check_mode
@login_required
def create_fee(request):    
    current_college = get_current_college(request)    
    if request.method == 'POST':
        form = FeeForm(request.POST)

        if form.is_valid() :
            data = form.save(commit=False)
            auto_id = get_auto_id(Fee)
            a_id = get_a_id(Fee,request) 

            admission_fee = form.cleaned_data['admission_fee']  
            application_fee = form.cleaned_data['application_fee'] 
            registration_fee = form.cleaned_data['registration_fee'] 
            special_fee = form.cleaned_data['special_fee']          
            caution_deposit = form.cleaned_data['caution_deposit'] 
            tution_fee = form.cleaned_data['tution_fee'] 
            bus_fare = form.cleaned_data['bus_fare'] 
            magazine_fee = form.cleaned_data['magazine_fee'] 
            computer_fee = form.cleaned_data['computer_fee'] 
            examination_fee = form.cleaned_data['examination_fee'] 
            other_income = form.cleaned_data['other_income'] 
            development_fund = form.cleaned_data['development_fund'] 

            subtotal = admission_fee + application_fee + registration_fee + special_fee + caution_deposit + tution_fee + bus_fare + magazine_fee + computer_fee + examination_fee + other_income + development_fund
            
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.college = current_college
            data.a_id = a_id
            data.subtotal = subtotal
            data.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Fee created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('finance:fee',kwargs={'pk':data.pk})
            }   
        
        else:                       
            message = generate_form_errors(form,formset=False)          
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message,
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        form = FeeForm(initial={'college':current_college})
        context = {
            "title" : "Create fee",
            "form" : form,
            "url" : reverse('finance:create_fee'),
            "redirect" : "true",
            "is_create_page" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'finance/create_fee.html',context)


@check_mode
@login_required
def create_fee_from_admission(request,pk):    
    current_college = get_current_college(request)
    if pk :
        student = Student.objects.get(pk=pk)
    if request.method == 'POST':
        form = FeeForm(request.POST)
        transaction_form = TransactionForm(request.POST)
        if form.is_valid() and transaction_form.is_valid() : 

            auto_id = get_auto_id(Fee)
            a_id = get_a_id(Fee,request) 
            admission_fee = form.cleaned_data['admission_fee']  
            application_fee = form.cleaned_data['application_fee'] 
            registration_fee = form.cleaned_data['registration_fee'] 
            special_fee = form.cleaned_data['special_fee']          
            caution_deposit = form.cleaned_data['caution_deposit'] 
            tution_fee = form.cleaned_data['tution_fee'] 
            bus_fare = form.cleaned_data['bus_fare'] 
            magazine_fee = form.cleaned_data['magazine_fee'] 
            computer_fee = form.cleaned_data['computer_fee'] 
            examination_fee = form.cleaned_data['examination_fee'] 
            other_income = form.cleaned_data['other_income'] 
            development_fund = form.cleaned_data['development_fund'] 

            subtotal = admission_fee + application_fee + registration_fee + special_fee + caution_deposit + tution_fee + bus_fare + magazine_fee + computer_fee + examination_fee + other_income + development_fund
            data = form.save(commit=False)
            data.admission = student.admission
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.college = current_college
            data.a_id = a_id
            data.subtotal = subtotal
            data.save()

            add_transaction(request,transaction_form,data,subtotal,"student_fee_from_admission","income",0,0)

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Fee created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('finance:print_fee',kwargs={'pk':data.pk})
            }   
        
        else:            
            message = generate_form_errors(form,formset=False) 
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        if pk :
            form = FeeForm(initial={'college':current_college,'student':student})
        else :
            form = FeeForm(initial={'college':current_college})
        transaction_form = TransactionForm()
        context = {
            "title" : "Create fee",
            "form" : form,
            "url" : reverse('finance:create_fee_from_admission',kwargs={'pk':pk}),
            "transaction_form" : transaction_form,
            "redirect" : "true",
            "is_create_page" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'finance/create_fee.html',context)


@check_mode
@login_required
def fees(request):
    current_college = get_current_college(request)
    instances = Fee.objects.filter(is_deleted=False,college=current_college)
    title = "Fees"

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(a_id__icontains=query) | Q(name__icontains=query) | Q(email__icontains=query) | Q(phone__icontains=query) | Q(address__icontains=query))
        title = "Fees - %s" %query  

    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'finance/fees.html',context) 


@check_mode
@login_required
def fee(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(Fee.objects.filter(pk=pk,is_deleted=False,college=current_college))
    context = {
        "instance" : instance,
        "title" : "Fee : ",
        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'finance/fee.html',context)


@check_mode
@login_required
def edit_fee(request,pk):
    instance = get_object_or_404(Fee.objects.filter(pk=pk,is_deleted=False)) 
    if request.method == 'POST':
            
        response_data = {}
        form = FeeForm(request.POST,request.FILES,instance=instance)

        if form.is_valid(): 
            
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()      
            
            return HttpResponseRedirect(reverse('finance:fee', kwargs={'pk':data.pk}))    
        else:
            message = generate_form_errors(form,formset=False) 
                    
            form = FeeForm(instance=instance)
        
            context = {
                "form" : form,
                "title" : "Edit Fee  : " + instance.student.first_name,
                "instance" : instance,
                "message" : message,
                "is_edit" : True,
                "url" : reverse('finance:edit_fee',kwargs={'pk':instance.pk}),
                "redirect" : True,
                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,

                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
            }
            return render(request, 'finance/create_fee.html', context)
    else: 

        form = FeeForm(instance=instance)
        
        context = {
            "form" : form,
            "title" : "Edit Fee  : " + instance.student.first_name,
            "instance" : instance,
            "is_edit" : True,
            "url" : reverse('finance:edit_fee',kwargs={'pk':instance.pk}),
            "redirect" : True,
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'finance/create_fee.html', context)


@check_mode
@login_required
def delete_fee(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(Fee.objects.filter(pk=pk,is_deleted=False,college=current_college))
    
    Fee.objects.filter(pk=pk,college=current_college).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Fee Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('finance:fees')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def delete_selected_fees(request):
    current_college = get_current_college(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Fee.objects.filter(pk=pk,is_deleted=False,college=current_college)) 
            Fee.objects.filter(pk=pk).update(is_deleted=True)
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Fee(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('finance:fees')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select fees first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def print_fee(request,pk):
    instance = get_object_or_404(Fee.objects.filter(pk=pk,is_deleted=False))

    
    context = {
        "instance" : instance,
        "title" : "Invoice Details",
        "single_page" : True,
        
        "is_need_bootstrap_growl" : True,
        "is_need_wave_effect" : True,
    }

    return render(request, 'finance/index.html', context)


# @check_mode
# @login_required
# def create_salary_payment(request): 
#     current_college = get_current_college(request) 
#     transaction_category =get_object_or_404(TransactionCategory.objects.filter(is_system_generated=True,is_deleted=False,college=current_college,category_type="expense",name="staff_salary"))
#     if request.method == "POST":
#         form = SalaryPaymentForm(request.POST)
#         form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False,college=current_college)
#         form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False,college=current_college)
        
#         if form.is_valid(): 
#             auto_id = get_auto_id(Transaction)
#             a_id = get_a_id(Transaction,request)
            
#             transaction_mode = form.cleaned_data['transaction_mode']
#             payment_to = form.cleaned_data['payment_to']
#             amount = form.cleaned_data['amount']
#             staff = form.cleaned_data['staff']

#             amount_remaining = amount
#             staff_salaries = StaffSalary.objects.filter(college=current_college,is_deleted=False,staff=staff).order_by('date_added')

#             latest_staff_salary = None
#             if staff_salaries:
#                 latest_staff_salary = StaffSalary.objects.filter(college=current_college,is_deleted=False,staff=staff).latest('date_added')

#             if latest_staff_salary:
#                 advance_amount = latest_staff_salary.advance
#                 amount_remaining += advance_amount
#             saved_salaries = []
#             for staff_salary in staff_salaries:
#                 balance = staff_salary.balance
#                 advance = staff_salary.advance
#                 if balance > 0:
#                     if balance <= amount_remaining:
#                         staff_salary.balance = 0
#                         staff_salary.paid = staff_salary.amount
#                         staff_salary.save()
#                         amount_remaining -= balance

#                         dict_obj = {
#                             "staff_salary" : staff_salary,
#                             "amount" : staff_salary.amount,
#                         }
#                         saved_salaries.append(dict_obj)

#                     else:
#                         if amount_remaining > 0:
#                             staff_salary.balance = staff_salary.balance - amount_remaining
#                             staff_salary.paid = staff_salary.paid + amount_remaining
#                             staff_salary.save()
#                             amount_remaining -= balance

#                             dict_obj = {
#                                 "staff_salary" : staff_salary,
#                                 "amount" : staff_salary.paid + amount_remaining,
#                             }
#                             saved_salaries.append(dict_obj)
#                         else:
#                             break

#             if amount_remaining > 0:
#                 if StaffSalary.objects.filter(college=current_college,is_deleted=False,staff=staff):
#                     latest_staff_salary = StaffSalary.objects.filter(college=current_college,is_deleted=False,staff=staff).latest('date_added')
#                     latest_staff_salary.advance = amount_remaining
#                     latest_staff_salary.save()

#             #create salary payment
#             data = form.save(commit=False)
            
#             if transaction_mode == "cash":
#                 data.payment_mode = None
#                 data.payment_to = "cash_account"
#                 data.bank_account = None
#                 data.cheque_details = None
#                 data.card_details = None
#                 data.is_cheque_withdrawed = False
#                 balance = data.cash_account.balance
#                 balance = balance - amount
#                 CashAccount.objects.filter(pk=data.cash_account.pk,college=current_college).update(balance=balance)
#             elif transaction_mode == "bank":
#                 balance = data.bank_account.balance
#                 balance = balance - amount
#                 BankAccount.objects.filter(pk=data.bank_account.pk,college=current_college).update(balance=balance)
#                 payment_mode = form.cleaned_data['payment_mode'] 
#                 if payment_mode == "cheque_payment":
#                     data.card_details = None
                        
#                 elif payment_mode == "internet_banking":
#                     data.payment_to = "bank_account"
#                     data.cash_account = None
#                     data.cheque_details = None
#                     data.card_details = None
#                     data.is_cheque_withdrawed = False
                
#                 elif payment_mode == "card_payment":
#                     data.payment_to = "bank_account"
#                     data.cash_account = None
#                     data.cheque_details = None
#                     data.is_cheque_withdrawed = False
            
#                 if payment_to == "cash_account":
#                     data.bank_account = None
#                 elif payment_to == "bank_account":
#                     data.cash_account = None

                
#             data.creator = request.user
#             data.updater = request.user
#             data.auto_id = auto_id            
#             data.transaction_type = "expense"
#             data.college = current_college
#             data.transaction_category = transaction_category
#             data.staff_salary = staff_salary
#             data.a_id = a_id
#             data.title = "%s:%s" %(staff.designation,staff.name)
#             data.save()
#             for saved_salary in saved_salaries:
#                 amount = saved_salary['amount']
#                 staff_salary = saved_salary['staff_salary']
#             auto_id = get_auto_id(StaffSalaryPayment)
#             a_id = get_a_id(StaffSalaryPayment,request)

#             StaffSalaryPayment.objects.create(
#                 college=current_college,
#                 auto_id=auto_id,
#                 a_id=a_id,
#                 transaction=data,
#                 amount=amount,
#                 staff_salary=staff_salary,
#                 creator=request.user,
#                 updater=request.user,
#                 )

#             response_data = {
#                 "status" : 'true',
#                 "title" : "Successfully Created" ,
#                 "redirect" : "true",
#                 "redirect_url" : reverse('finance:transaction', kwargs = {'pk' : data.pk}),
#                 "message" : "Salary payment Successfully Created."
#             }
#         else:
            
#             message = ''            
#             message += generate_form_errors(form,formset=False) 
                  
#             response_data = {
#                 "status" : 'false',
#                 "title" : "Form Validation Error" ,
#                 "stable" : "true",
#                 "message" : message,
#             }
            
#         return HttpResponse(json.dumps(response_data), content_type='application/javascript')

#     else:    
#         form = SalaryPaymentForm()
#         form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False,college=current_college)
#         form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False,college=current_college)
#         context = {
#             "form" : form,
#             "title" : "Create Salary Payment ",
#             "url" : reverse('finance:create_salary_payment'),

#             "payment_page" : True,

#             "is_need_select_picker" : True,
#             "is_need_popup_box" : True,
#             "is_need_custom_scroll_bar" : True,
#             "is_need_wave_effect" : True,
#             "is_need_bootstrap_growl" : True,
#             "is_need_grid_system" : True,
#             "is_need_datetime_picker" : True,
#         }
#         return render(request, 'finance/entry_salary_payment.html', context) 


# @check_mode
# @login_required
# def edit_salary_payment(request,pk): 
#     current_college = get_current_college(request) 
#     instance = get_object_or_404(Transaction.objects.filter(pk=pk,is_deleted=False,college=current_college))

#     instance1 = instance.staff_salary
#     edit_amount = instance.amount
#     instance2 = instance.cash_account
#     instance3 = instance.bank_account

#     if request.method == "POST":
#         form = SalaryPaymentForm(request.POST,instance=instance)
#         form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False,college=current_college)
#         form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False,college=current_college)
        
#         if form.is_valid(): 
#             transaction_mode = form.cleaned_data['transaction_mode']
#             payment_to = form.cleaned_data['payment_to']
#             amount = form.cleaned_data['amount']
            
#             #Update salary payment
#             data = form.save(commit=False)                                                                                                                                  
            
#             balance = data.cash_account.balance
#             balance1 = data.bank_account.balance

#             if transaction_mode == "cash":
#                 if instance2 == None :
#                     balance = balance - amount
#                     balance1 = balance1 + amount
#                 else :
#                     balance = balance + edit_amount - amount
#                 BankAccount.objects.filter(pk=data.bank_account.pk).update(balance=balance1)
#                 CashAccount.objects.filter(pk=data.cash_account.pk).update(balance=balance)
                
#                 data.payment_mode = None
#                 data.payment_to = "cash_account"
#                 data.bank_account = None
#                 data.cheque_details = None
#                 data.card_details = None
#                 data.is_cheque_withdrawed = False
                
#             elif transaction_mode == "bank":
#                 if instance3 == None :
#                     balance = balance + amount
#                     balance1 = balance1 - amount
#                 else :
#                     balance1 = balance1 + edit_amount -amount
#                 BankAccount.objects.filter(pk=data.bank_account.pk).update(balance=balance1)
#                 CashAccount.objects.filter(pk=data.cash_account.pk).update(balance=balance)
                
#                 payment_mode = form.cleaned_data['payment_mode'] 
#                 if payment_mode == "cheque_payment":
#                     data.card_details = None
                        
#                 elif payment_mode == "internet_banking":
#                     data.payment_to = "bank_account"
#                     data.cash_account = None
#                     data.cheque_details = None
#                     data.card_details = None
#                     data.is_cheque_withdrawed = False
                
#                 elif payment_mode == "card_payment":
#                     data.payment_to = "bank_account"
#                     data.cash_account = None
#                     data.cheque_details = None
#                     data.is_cheque_withdrawed = False
            
#                 if payment_to == "cash_account":
#                     data.bank_account = None
#                 elif payment_to == "bank_account":
#                     data.cash_account = None
#             balance = instance1.balance
#             paid = instance1.paid
#             advance = instance1.advance
#             salary_amount = instance1.amount
#             if edit_amount != data.amount:   
#                 if paid == edit_amount:
#                     paid = data.amount
#                     balance = salary_amount-data.amount
#                     if balance<0:
#                         advance = data.amount-salary_amount
#                 else:
#                     paid = paid-edit_amount + data.amount
#                     balance = salary_amount- paid
#                     if balance<0:
#                         advance = paid -salary_amount
#             StaffSalary.objects.filter(pk=instance1.pk,college=current_college).update(balance=balance,paid=paid,advance=advance)               
#             data.updater = request.user
#             data.date_updated = datetime.datetime.now()
#             data.college=current_college
#             data.save()
            
#             response_data = {
#                 "status" : 'true',
#                 "title" : "Successfully Updated" ,
#                 "redirect" : "true",
#                 "redirect_url" : reverse('finance:transaction', kwargs = {'pk' : data.pk}),
#                 "message" : "Salary payment Successfully Updated."
#             }
#         else:
            
#             message = ''            
#             message += generate_form_errors(form,formset=False) 
                  
#             response_data = {
#                 "status" : 'false',
#                 "title" : "Form Validation Error" ,
#                 "stable" : "true",
#                 "message" : message,
#             }
            
#         return HttpResponse(json.dumps(response_data), content_type='application/javascript')

#     else: 
#         form = SalaryPaymentForm(instance=instance)
#         form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False,college=current_college)
#         form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False,college=current_college)
#         context = {
#             "form" : form,
#             "title" : "Edit Salary Payment",
#             'instance' : instance,
#             "url" : reverse('finance:edit_salary_payment', kwargs={'pk':pk}),
#             "payment_page" : True,

#             "is_need_select_picker" : True,
#             "is_need_popup_box" : True,
#             "is_need_custom_scroll_bar" : True,
#             "is_need_wave_effect" : True,
#             "is_need_bootstrap_growl" : True,
#             "is_need_grid_system" : True,
#             "is_need_datetime_picker" : True,
#         }
#         return render(request, 'finance/entry_salary_payment.html', context)
  

# @check_mode
# @login_required
# def salary_payments(request):
#     current_college = get_current_college(request) 
 
#     instances = Transaction.objects.filter(is_deleted=False,college=current_college,transaction_category__name='staff_salary')

#     query = request.GET.get("q")
#     if query:
#         instances = instances.filter(Q(staff__name__icontains=query) | Q(date__icontains=query) | Q(amount__icontains=query))
        
#     title = "Salary Payments"
#     context = {
#         'title' : title,
#         "instances" : instances,

#         "is_need_select_picker" : True,
#         "is_need_popup_box" : True,
#         "is_need_custom_scroll_bar" : True,
#         "is_need_wave_effect" : True,
#         "is_need_bootstrap_growl" : True,
#         "is_need_grid_system" : True,
#         "is_need_chosen_select" : True,
#         "is_need_animations" : True,
#         "is_need_datetime_picker" : True,
#     }
#     return render(request,'finance/salary_payments.html',context)


@check_mode
@login_required
def create_sale_payment(request):
    current_college = get_current_college(request) 
    transaction_category =get_object_or_404(TransactionCategory.objects.filter(is_system_generated=True,is_deleted=False,college=current_college,category_type="income",name="sale_payment"))
    if request.method == "POST":
        form = SalePaymentForm(request.POST)
        form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False,college=current_college)
        form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False,college=current_college)
        
        if form.is_valid(): 
            auto_id = get_auto_id(Transaction)
            a_id = get_a_id(Transaction,request)
            
            transaction_mode = form.cleaned_data['transaction_mode']
            payment_to = form.cleaned_data['payment_to']
            amount = form.cleaned_data['amount']
            student = form.cleaned_data['student']

            amount_remaining = amount
            sales = Sale.objects.filter(college=current_college,is_deleted=False,student=student).order_by('date_added')

            latest_sale = None
            if sales:
                latest_sale = Sale.objects.filter(college=current_college,is_deleted=False,student=student).latest('date_added')

            if latest_sale:
                advance_amount = latest_sale.advance
                amount_remaining += advance_amount
            
            saved_sales = []
            for sale in sales:
                sale_amount=sale.total
                balance=sale.balance
                paid=sale.paid
                advance = sale.advance
                if balance>0:
                    if balance <= amount_remaining:
                        sale.balance = 0
                        sale.paid = sale.total
                        sale.save()
                        amount_remaining -= balance

                        dict_obj = {
                            "sale" : sale,
                            "amount" : sale.total,
                        }
                        saved_sales.append(dict_obj)
                    else:
                        if amount_remaining > 0:
                            sale.balance = sale.balance - amount_remaining
                            sale.paid = sale.paid + amount_remaining
                            sale.save()
                            amount_remaining -= balance

                            dict_obj = {
                                "sale" : sale,
                                "amount" : sale.paid + amount_remaining,
                            }
                            saved_sales.append(dict_obj)
                        else:
                            break
            if amount_remaining > 0:
                if Sale.objects.filter(college=current_college,is_deleted=False,student=student):
                    latest_sale = Sale.objects.filter(college=current_college,is_deleted=False,student=student).latest('date_added')
                    latest_sale.advance = amount_remaining
                    latest_sale.save()

            #create sale payment
            data = form.save(commit=False)
            
            if transaction_mode == "cash":
                data.payment_mode = None
                data.payment_to = "cash_account"
                data.bank_account = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
                balance = data.cash_account.balance
                balance = balance + amount
                CashAccount.objects.filter(pk=data.cash_account.pk,college=current_college).update(balance=balance)
            elif transaction_mode == "bank":
                balance = data.bank_account.balance
                balance = balance + amount
                BankAccount.objects.filter(pk=data.bank_account.pk,college=current_college).update(balance=balance)
                payment_mode = form.cleaned_data['payment_mode'] 
                if payment_mode == "cheque_payment":
                    data.card_details = None
                        
                elif payment_mode == "internet_banking":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False
                
                elif payment_mode == "card_payment":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False
            
                if payment_to == "cash_account":
                    data.bank_account = None
                elif payment_to == "bank_account":
                    data.cash_account = None

                
            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id            
            data.transaction_type = "income"
            data.college = current_college
            data.transaction_category = transaction_category
            data.sale = sale
            data.a_id = a_id
            data.title = "%s:%s" %(sale.student,sale.sale_id)
            data.save()

            for saved_sale in saved_sales:
                amount = saved_sale['amount']
                sale = saved_sale['sale']
            auto_id = get_auto_id(SalePayment)
            a_id = get_a_id(SalePayment,request)

            SalePayment.objects.create(
                college=current_college,
                auto_id=auto_id,
                a_id=a_id,
                transaction=data,
                amount=amount,
                sale=sale,
                creator=request.user,
                updater=request.user,
                )
                
            response_data = {
                "status" : 'true',
                "title" : "Successfully Created" ,
                "redirect" : "true",
                "redirect_url" : reverse('finance:transaction', kwargs = {'pk' : data.pk}),
                "message" : "Sale payment Successfully Created."
            }
        else:
            
            message = ''            
            message += generate_form_errors(form,formset=False) 
                  
            response_data = {
                "status" : 'false',
                "title" : "Form Validation Error" ,
                "stable" : "true",
                "message" : message,
            }
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:  
        form = SalePaymentForm()
        form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False,college=current_college)
        form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False,college=current_college)
        context = {
            "form" : form,
            "title" : "Create Sale Payment " ,
            "url" : reverse('finance:create_sale_payment'),

            "payment_page" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'finance/entry_sale_payment.html', context)


@check_mode
@login_required
def edit_sale_payment(request,pk): 
    current_college = get_current_college(request) 
    instance = get_object_or_404(Transaction.objects.filter(pk=pk,is_deleted=False,college=current_college))

    instance1 = instance.sale
    edit_amount = instance.amount
    instance2 = instance.cash_account
    instance3 = instance.bank_account

    if request.method == "POST":
        form = SalePaymentForm(request.POST,instance=instance)
        form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False,college=current_college)
        form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False,college=current_college)
        
        if form.is_valid(): 
            transaction_mode = form.cleaned_data['transaction_mode']
            payment_to = form.cleaned_data['payment_to']
            amount = form.cleaned_data['amount']
            
            #Update salary payment
            data = form.save(commit=False)                                                                                                                                  
            
            balance = data.cash_account.balance
            balance1 = data.bank_account.balance

            if transaction_mode == "cash":
                if instance2 == None :
                    balance = balance + amount
                    balance1 = balance1 - amount
                else :
                    balance = balance - edit_amount + amount
                BankAccount.objects.filter(pk=data.bank_account.pk).update(balance=balance1)
                CashAccount.objects.filter(pk=data.cash_account.pk).update(balance=balance)
                
                data.payment_mode = None
                data.payment_to = "cash_account"
                data.bank_account = None
                data.cheque_details = None
                data.card_details = None
                data.is_cheque_withdrawed = False
                
            elif transaction_mode == "bank":
                if instance3 == None :
                    balance = balance - amount
                    balance1 = balance1 + amount
                else :
                    balance1 = balance1 - edit_amount + amount
                BankAccount.objects.filter(pk=data.bank_account.pk).update(balance=balance1)
                CashAccount.objects.filter(pk=data.cash_account.pk).update(balance=balance)
                
                payment_mode = form.cleaned_data['payment_mode'] 
                if payment_mode == "cheque_payment":
                    data.card_details = None
                        
                elif payment_mode == "internet_banking":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.card_details = None
                    data.is_cheque_withdrawed = False
                
                elif payment_mode == "card_payment":
                    data.payment_to = "bank_account"
                    data.cash_account = None
                    data.cheque_details = None
                    data.is_cheque_withdrawed = False
            
                if payment_to == "cash_account":
                    data.bank_account = None
                elif payment_to == "bank_account":
                    data.cash_account = None
            balance = instance1.balance
            paid = instance1.paid
            advance = instance1.advance
            sale_amount = instance1.total
            if edit_amount != data.amount:   
                if paid == edit_amount:
                    paid = data.amount
                    balance = sale_amount-data.amount
                    if balance<0:
                        advance = data.amount-sale_amount
                else:
                    paid = paid-edit_amount + data.amount
                    balance = sale_amount- paid
                    if balance<0:
                        advance = paid -sale_amount
            Sale.objects.filter(pk=instance1.pk,college=current_college).update(balance=balance,paid=paid,advance=advance)               
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.college=current_college
            data.save()
            
            response_data = {
                "status" : 'true',
                "title" : "Successfully Updated" ,
                "redirect" : "true",
                "redirect_url" : reverse('finance:transaction', kwargs = {'pk' : data.pk}),
                "message" : "Sale payment Successfully Updated."
            }
        else:
            
            message = ''            
            message += generate_form_errors(form,formset=False) 
                  
            response_data = {
                "status" : 'false',
                "title" : "Form Validation Error" ,
                "stable" : "true",
                "message" : message,
            }
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = SalePaymentForm(instance=instance)
        form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False,college=current_college)
        form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False,college=current_college)
        context = {
            "form" : form,
            "title" : "Edit Sale Payment",
            'instance' : instance,
            "url" : reverse('finance:edit_sale_payment', kwargs={'pk':pk}),
            "payment_page" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'finance/entry_sale_payment.html', context)



@check_mode
@login_required
def sale_payments(request):
    current_college = get_current_college(request) 
 
    instances = Transaction.objects.filter(is_deleted=False,college=current_college,transaction_category__name='sale_payment')

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(student__name__icontains=query) | Q(date__icontains=query) | Q(amount__icontains=query))
        
    title = "Sale Payments"
    context = {
        'title' : title,
        "instances" : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'finance/sale_payments.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def day_report(request):
    current_college = get_current_college(request) 
    recent_admissions = Admission.objects.filter(is_deleted=False,is_roll_out=False,college=current_college) 
    today = datetime.date.today()
    month = today.month
    if month == 1:
        month = "January"
    if month == 2:
        month = "February"
    if month == 3:
        month = "March"
    if month == 4:
        month = "April"
    if month == 5:
        month = "May"
    if month == 6:
        month = "June"
    if month == 7:
        month = "July"
    if month == 8:
        month = "August"
    if month == 9:
        month = "September"
    if month == 10:
        month = "October"
    if month == 11:
        month = "November"
    if month == 12:
        month = "December"

    admissions = []
    admission_admissions_taken = ""
    admission_total_fee_amount = 0
    admission_balance_amount = 0
    admission_total_paid_amount = 0
    admission_total_balance_amount = 0

    fee_payment_total_amount = 0
    fee_payment_total_balance_amount = 0

    salary_payment_total_amount = 0
    salary_payment_total_balance_amount = 0
    salary_payment_total_paid_amount = 0

    teacher_salary_total_amount = 0
    teacher_salary_total_dispersed_amount = 0
    teacher_salary_total_advance_amount = 0
    teacher_salary_total_balance_amount = 0

    fee_payments = []
    salary_payments = []

    date = request.GET.get("date")
    date_string = date
    date_error = False
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
            title = " Date - %s" %date_string                
        except ValueError:
            date_error = True
    else :
        date = today
        date_string = today
   
    if date and not date_error:  
        
        admissions = Admission.objects.filter(is_deleted=False,date_added__date=date,college=current_college)

        admission_amount_dict = admissions.aggregate(Sum('paid'),Sum('balance'),Sum('fee_amount'))
        admission_total_fee_amount = admission_amount_dict['fee_amount__sum']
        admission_total_balance_amount = admission_amount_dict['balance__sum']
        admission_total_paid_amount = admission_amount_dict['paid__sum']
        
        admission_admissions_taken = admissions.count()

        fee_payments = Transaction.objects.filter(is_deleted=False,date__date=date,transaction_type="income",transaction_category__is_system_generated=True)
        fee_payment_amount_dict = fee_payments.aggregate(Sum('amount'))
        fee_payment_total_amount = fee_payment_amount_dict['amount__sum']

        salary_payments = Transaction.objects.filter(is_deleted=False,date__date=date,transaction_type="expense",transaction_category__name="teacher_salary")
        salary_payment_amount_dict = salary_payments.aggregate(Sum('amount'),Sum('balance'),Sum('paid'))
        salary_payment_total_amount = salary_payment_amount_dict['amount__sum']

        other_income = Transaction.objects.filter(is_deleted=False,date__date=date,transaction_type="income",transaction_category__is_system_generated=False)
        other_income_amount_dict = other_income.aggregate(Sum('amount'))
        other_income_amount = other_income_amount_dict['amount__sum']

        other_expense = Transaction.objects.filter(is_deleted=False,date__date=date,transaction_type="expense").exclude(transaction_category__name="teacher_salary",transaction_category__is_system_generated=True)
        other_expense_amount_dict = other_expense.aggregate(Sum('amount'))
        other_expense_amount = other_expense_amount_dict['amount__sum']
        student_batch = get_current_batch()
        from_date = student_batch.start_date
        to_date = student_batch.end_date
        # teacher_salaries = TeacherSalary.objects.filter(is_deleted=False,date__gte=from_date,date__lte=to_date)
        # teacher_salary_amount_dict = teacher_salaries.aggregate(Sum('amount'),Sum('balance'),Sum('dispersed'),Sum('advance'))
        # teacher_salary_total_amount = teacher_salary_amount_dict['amount__sum']
        # teacher_salary_total_dispersed_amount = teacher_salary_amount_dict['dispersed__sum']        
        # teacher_salary_total_advance_amount = teacher_salary_amount_dict['advance__sum']
        # if teacher_salaries:
        #     teacher_salary_total_balance_amount = teacher_salary_total_amount - teacher_salary_total_dispersed_amount + teacher_salary_total_advance_amount

    context = {
        "title" : "Day Report : " + str(date),

        "admissions" : admissions,
        "today" : today,
        "recent_admissions" : recent_admissions,
       
        "admission_admissions_taken" : admission_admissions_taken,
        "admission_total_paid_amount" : admission_total_paid_amount,
        "admission_total_balance_amount" : admission_total_balance_amount,
        "admission_total_fee_amount" : admission_total_fee_amount,

        "fee_payments" : fee_payments,
        "fee_payment_total_amount" : fee_payment_total_amount,  

        "other_income_amount" : other_income_amount,
        "other_income" : other_income,
        "other_expense" : other_expense,
        "other_expense_amount" : other_expense_amount,

        "salary_payments" : salary_payments,
        "salary_payment_total_amount" : salary_payment_total_amount,
        "teacher_salary_total_amount" : teacher_salary_total_amount,
        "teacher_salary_total_advance_amount" : teacher_salary_total_advance_amount,
        "teacher_salary_total_balance_amount" : teacher_salary_total_balance_amount,
        "teacher_salary_total_dispersed_amount" : teacher_salary_total_dispersed_amount,

        # "teacher_salaries" : teacher_salaries,

        "date_string" : date_string,
        "date" : date,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request,'reports/day_report.html',context)


@check_mode
@login_required
def monthly_report(request):
    current_college = get_current_college(request) 
    today = datetime.date.today()
    year = request.GET.get('year')
    month = request.GET.get('month')
    recent_admissions = Admission.objects.filter(is_deleted=False,is_roll_out=False)  
    prev_month  = today.month 
    prev_year = today.year
    if today.month == 1:
        prev_month = 12
        prev_year = today.year - 1

    if month :
        prev_month = month

    if year:
        prev_year = year

    month_text = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    
    title = "Report : %s, %s" %(month_text[int(prev_month)-1], prev_year)
    instances = Transaction.objects.filter(is_deleted=False,date__month=prev_month,date__year=prev_year)

    income_amount = 0
    expense_amount = 0
    student_fee_amount = 0
    teacher_salary_amount = 0
    student_fee_created = 0
    teacher_salary_created = 0

    admissions = []
    admission_admissions_taken = ""
    admission_total_fee_amount = 0
    admission_balance_amount = 0
    admission_total_paid_amount = 0
    admission_total_balance_amount = 0

    fee_payment_total_amount = 0
    fee_payment_total_balance_amount = 0

    salary_payment_total_amount = 0
    salary_payment_total_balance_amount = 0
    salary_payment_total_paid_amount = 0

    teacher_salary_total_amount = 0
    teacher_salary_total_dispersed_amount = 0
    teacher_salary_total_advance_amount = 0
    teacher_salary_total_balance_amount = 0

    fee_payments = []
    salary_payments = []

    admissions = Admission.objects.filter(is_deleted=False,date_added__date__month=prev_month,date_added__date__year=prev_year)

    admission_amount_dict = admissions.aggregate(Sum('paid'),Sum('balance'),Sum('fee_amount'))
    admission_total_fee_amount = admission_amount_dict['fee_amount__sum']
    admission_total_balance_amount = admission_amount_dict['balance__sum']
    admission_total_paid_amount = admission_amount_dict['paid__sum']
    
    admission_admissions_taken = admissions.count()

    fee_payments = Transaction.objects.filter(is_deleted=False,date__month=prev_month,date__year=prev_year,transaction_type="income")
    fee_payment_amount_dict = fee_payments.aggregate(Sum('amount'),Sum('balance'))
    fee_payment_total_amount = fee_payment_amount_dict['amount__sum']
    fee_payment_total_balance_amount = fee_payment_amount_dict['balance__sum']

    salary_payments = Transaction.objects.filter(is_deleted=False,date__month=prev_month,date__year=prev_year,transaction_type="expense",transaction_category__name="teacher_salary")
    salary_payment_amount_dict = salary_payments.aggregate(Sum('amount'),Sum('balance'),Sum('paid'))
    salary_payment_total_amount = salary_payment_amount_dict['amount__sum']

    other_income = Transaction.objects.filter(is_deleted=False,date__month=prev_month,date__year=prev_year,transaction_type="income",transaction_category__is_system_generated=False)
    other_income_amount_dict = other_income.aggregate(Sum('amount'))
    other_incomeAdmissions_amount = other_income_amount_dict['amount__sum']

    other_expense = Transaction.objects.filter(is_deleted=False,date__month=prev_month,date__year=prev_year,transaction_type="expense").exclude(transaction_category__name="teacher_salary",transaction_category__is_system_generated=True)
    other_expense_amount_dict = other_expense.aggregate(Sum('amount'))
    other_expense_amount = other_expense_amount_dict['amount__sum']
    # teacher_salaries = TeacherSalary.objects.filter(is_deleted=False,date__month=prev_month,date__year=prev_year)
    # teacher_salary_amount_dict = teacher_salaries.aggregate(Sum('amount'),Sum('balance'),Sum('dispersed'),Sum('advance'))
    # teacher_salary_total_amount = teacher_salary_amount_dict['amount__sum']
    # teacher_salary_total_dispersed_amount = teacher_salary_amount_dict['dispersed__sum']        
    # teacher_salary_total_advance_amount = teacher_salary_amount_dict['advance__sum']
    # if teacher_salaries:
    #     teacher_salary_total_balance_amount = teacher_salary_total_amount - teacher_salary_total_dispersed_amount + teacher_salary_total_advance_amount

    
    context = {
        'title' : title,
        "year" : year,
        "month" : month,
        "instances" : instances,
        "recent_admissions" : recent_admissions,

        "is_month" : True,
        "prev_month" : prev_month,
        "prev_year" : prev_year,

        "admissions" : admissions,
        "admission_total_fee_amount" : admission_total_fee_amount,
        "admission_total_balance_amount" : admission_total_balance_amount,
        "admission_total_paid_amount" : admission_total_paid_amount,

        "fee_payments" : fee_payments,
        "fee_payment_total_amount" : fee_payment_total_amount, 
        "fee_payment_total_balance_amount" : fee_payment_total_balance_amount, 

        # "other_income_amount" : other_income_amount,
        "other_income" : other_income,
        "other_expense" : other_expense,
        "other_expense_amount" : other_expense_amount,

        "salary_payments" : salary_payments,
        "salary_payment_total_amount" : salary_payment_total_amount,
        "teacher_salary_total_amount" : teacher_salary_total_amount,
        "teacher_salary_total_advance_amount" : teacher_salary_total_advance_amount,
        "teacher_salary_total_balance_amount" : teacher_salary_total_balance_amount,
        "teacher_salary_total_dispersed_amount" : teacher_salary_total_dispersed_amount,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
         
    return render(request, 'reports/monthly_report.html', context) 


@check_mode
@login_required
@role_required(['superadmin'])
def bank_accounts(request):
    instances = BankAccount.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(name__icontains=query) | Q(ifsc__istartswith=query) | Q(branch__icontains=query) | Q(account_type__icontains=query) | Q(account_no__istartswith=query))
        title = "Bank Accounts - %s" %query 

    context = {
        "instances" : instances,
        "title" : "Bank Accounts",
        "redirect": "true",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'finance/bank_accounts.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def bank_account(request,pk):
    instance = get_object_or_404(BankAccount.objects.filter(pk=pk,is_deleted=False))
    context = {
        "title" : "Bank Account",
        'instance' : instance,
        "redirect": "true",            

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'finance/bank_account.html', context)



@check_mode
@login_required
@role_required(['superadmin'])
def create_bank_account(request):
    url = reverse('finance:create_bank_account')
    current_college = get_current_college(request)
    if request.method == "POST":

        form = BankAccountForm(request.POST)

        if form.is_valid(): 
            first_balance = form.cleaned_data['first_time_balance']
            name = form.cleaned_data['name']

            #create bank account
            data = form.save(commit=False)
            auto_id = get_auto_id(BankAccount)
            data.auto_id = auto_id
            data.creator = request.user
            data.updater = request.user
            data.college = current_college
            data.balance = first_balance
            data.a_id = get_a_id(BankAccount,request)  
            data.save()

            bank_account_group =  AccountGroup.objects.filter(name='bank_account',college=current_college,is_system_generated=True,is_deleted=False)[0]
            AccountHead.objects.create(
                is_system_generated=True,
                account_group = bank_account_group,
                is_deleted=False,
                name=name,
                auto_id=get_auto_id(AccountHead),
                a_id=get_a_id(AccountHead,request),
                college=current_college,
                creator=request.user,
                updater=request.user
            )

            response_data = {
                "status": "true",
                "title": "Successfully created",
                "message": "Bank Account successfully created.",
                "redirect": "true",
                "redirect_url": reverse('finance:bank_accounts')
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = BankAccountForm()
        context = {
            "title" : "Create Bank Account",
            "form" : form,
            "url" : url,
            "redirect": "true",          

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }

        return render(request, 'finance/entry_bank_account.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_bank_account(request,pk):
    instance = get_object_or_404(BankAccount.objects.filter(pk=pk,is_deleted=False))
    url = reverse('finance:edit_bank_account',kwargs={'pk':pk})
    if request.method == "POST":

        form = BankAccountForm(request.POST,instance=instance)
        if form.is_valid(): 

            #update bank account
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Bank Account successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('finance:bank_account',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = BankAccountForm(instance=instance)

        context = {
            "title" : "Edit bank account",
            "form" : form,
            "instance" : instance,
            "url" : url,
            "redirect": "true",            

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'finance/entry_bank_account.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_bank_account(request,pk):
    BankAccount.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Bank Account successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('finance:bank_accounts')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
def delete_selected_bank_accounts(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(BankAccount.objects.filter(pk=pk,is_deleted=False)) 
            BankAccount.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Bank Account(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('finance:bank_accounts')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some bank accounts first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def cash_accounts(request):
    instances = CashAccount.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query) | Q(balance__icontains=query))
        title = "Cash Accounts - %s" %query 

    context = {
        "instances" : instances,
        "title" : "Cash Accounts",
        "redirect": "true",    

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'finance/cash_accounts.html', context)
   

@check_mode
@login_required
@role_required(['superadmin'])
def cash_account(request,pk):
    instance = get_object_or_404(CashAccount.objects.filter(pk=pk,is_deleted=False))
    context = {
        "title" : "Cash Account",
        'instance' : instance,
        "redirect": "true",   


        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'finance/cash_account.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def create_cash_account(request):
    url = reverse('finance:create_cash_account')
    current_college = get_current_college(request)
    if request.method == "POST":

        form = CashAccountForm(request.POST)

        if form.is_valid(): 
            first_balance = form.cleaned_data['first_time_balance']
            name = form.cleaned_data['name']
            #create cash account
            data = form.save(commit=False)
            auto_id = get_auto_id(CashAccount)
            data.auto_id = auto_id
            data.creator = request.user
            data.updater = request.user
            data.college = current_college
            data.balance = first_balance
            data.user = request.user
            data.a_id = get_a_id(CashAccount,request)  
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully created",
                "message": "Cash Account successfully created.",
                "redirect": "true",
                "redirect_url": reverse('finance:cash_accounts')
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = CashAccountForm()
        context = {
            "title" : "Create Cash Account",
            "form" : form,
            "url" : url,
            "redirect": "true",          

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }

        return render(request, 'finance/entry_cash_account.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_cash_account(request,pk):
    instance = get_object_or_404(CashAccount.objects.filter(pk=pk,is_deleted=False))
    url = reverse('finance:edit_cash_account',kwargs={'pk':pk})
    if request.method == "POST":

        form = CashAccountForm(request.POST,instance=instance)
        if form.is_valid(): 

            #update cash account
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Cash Account successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('finance:cash_account',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = CashAccountForm(instance=instance)

        context = {
            "title" : "Edit cash account",
            "form" : form,
            "instance" : instance,
            "url" : url,
            "redirect": "true",            

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'finance/entry_cash_account.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_cash_account(request,pk):
    CashAccount.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Cash Account successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('finance:cash_accounts')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
def delete_selected_cash_accounts(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(CashAccount.objects.filter(pk=pk,is_deleted=False)) 
            CashAccount.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Cash Account(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('finance:cash_accounts')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some cash accounts first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def yearly_report(request):
    current_college = get_current_college(request) 
    today = datetime.date.today()
    year = request.GET.get('year')
    recent_admissions = Admission.objects.filter(is_deleted=False,is_roll_out=False)
    prev_year = today.year
    if year:
        prev_year = year

    title = "Report : %s" %(prev_year)
    instances = Transaction.objects.filter(is_deleted=False,date__year=prev_year)

    income_amount = 0
    expense_amount = 0
    student_fee_amount = 0
    teacher_salary_amount = 0
    student_fee_created = 0
    teacher_salary_created = 0

    admissions = []
    admission_admissions_taken = ""
    admission_total_fee_amount = 0
    admission_balance_amount = 0
    admission_total_paid_amount = 0
    admission_total_balance_amount = 0

    fee_payment_total_amount = 0
    fee_payment_total_balance_amount = 0

    salary_payment_total_amount = 0
    salary_payment_total_balance_amount = 0
    salary_payment_total_paid_amount = 0

    teacher_salary_total_amount = 0
    teacher_salary_total_dispersed_amount = 0
    teacher_salary_total_advance_amount = 0
    teacher_salary_total_balance_amount = 0

    fee_payments = []
    salary_payments = []

    admissions = Admission.objects.filter(is_deleted=False,date_added__date__year=prev_year)

    admission_amount_dict = admissions.aggregate(Sum('paid'),Sum('balance'),Sum('fee_amount'))
    admission_total_fee_amount = admission_amount_dict['fee_amount__sum']
    admission_total_balance_amount = admission_amount_dict['balance__sum']
    admission_total_paid_amount = admission_amount_dict['paid__sum']
    
    admission_admissions_taken = admissions.count()

    fee_payments = Transaction.objects.filter(is_deleted=False,date__year=prev_year,transaction_type="income")
    fee_payment_amount_dict = fee_payments.aggregate(Sum('amount'),Sum('balance'))
    fee_payment_total_amount = fee_payment_amount_dict['amount__sum']
    fee_payment_total_balance_amount = fee_payment_amount_dict['balance__sum']

    salary_payments = Transaction.objects.filter(is_deleted=False,date__year=prev_year,transaction_type="expense",transaction_category__name="teacher_salary")
    salary_payment_amount_dict = salary_payments.aggregate(Sum('amount'),Sum('balance'),Sum('paid'))
    salary_payment_total_amount = salary_payment_amount_dict['amount__sum']

    other_income = Transaction.objects.filter(is_deleted=False,date__year=prev_year,transaction_type="income",transaction_category__is_system_generated=False)
    other_income_amount_dict = other_income.aggregate(Sum('amount'))
    other_income_amount = other_income_amount_dict['amount__sum']

    other_expense = Transaction.objects.filter(is_deleted=False,date__year=prev_year,transaction_type="expense").exclude(transaction_category__name="teacher_salary",transaction_category__is_system_generated=True)
    other_expense_amount_dict = other_expense.aggregate(Sum('amount'))
    other_expense_amount = other_expense_amount_dict['amount__sum']
    # teacher_salaries = TeacherSalary.objects.filter(is_deleted=False,date__year=prev_year)
    # teacher_salary_amount_dict = teacher_salaries.aggregate(Sum('amount'),Sum('balance'),Sum('dispersed'),Sum('advance'))
    # teacher_salary_total_amount = teacher_salary_amount_dict['amount__sum']
    # teacher_salary_total_dispersed_amount = teacher_salary_amount_dict['dispersed__sum']        
    # teacher_salary_total_advance_amount = teacher_salary_amount_dict['advance__sum']
    # if teacher_salaries:
    #     teacher_salary_total_balance_amount = teacher_salary_total_amount - teacher_salary_total_dispersed_amount + teacher_salary_total_advance_amount

    
    context = {
        'title' : title,
        "year" : year,
        "prev_year" : prev_year,
        "recent_admissions" : recent_admissions,

        "admissions" : admissions,
        "admission_total_fee_amount" : admission_total_fee_amount,
        "admission_total_balance_amount" : admission_total_balance_amount,
        "admission_total_paid_amount" : admission_total_paid_amount,

        "fee_payments" : fee_payments,
        "fee_payment_total_amount" : fee_payment_total_amount, 
        "fee_payment_total_balance_amount" : fee_payment_total_balance_amount, 

        "other_income_amount" : other_income_amount,
        "other_income" : other_income,
        "other_expense" : other_expense,
        "other_expense_amount" : other_expense_amount,

        "salary_payments" : salary_payments,
        "salary_payment_total_amount" : salary_payment_total_amount,
        "teacher_salary_total_amount" : teacher_salary_total_amount,
        "teacher_salary_total_advance_amount" : teacher_salary_total_advance_amount,
        "teacher_salary_total_balance_amount" : teacher_salary_total_balance_amount,
        "teacher_salary_total_dispersed_amount" : teacher_salary_total_dispersed_amount,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'reports/yearly_report.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def create_salary_payment(request,pk):  
    instance = get_object_or_404(Staff.objects.filter(pk=pk,is_deleted=False))
    salaries = StaffSalary.objects.filter(staff=instance,is_deleted=False,balance__gt=0)
    current_college = get_current_college(request)
    today = datetime.date.today()
    if request.method == "POST":
        transaction_form = SalaryPaymentForm(request.POST)
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False)
        if transaction_form.is_valid(): 
            
            transaction_mode = transaction_form.cleaned_data['transaction_mode']
            payment_to = transaction_form.cleaned_data['payment_to']

            amount = transaction_form.cleaned_data['amount']
            actual_amount = amount

            if TransactionCategory.objects.filter(name="staff_salary",is_deleted=False,is_system_generated=True,college=current_college).exists():
                transaction_category = TransactionCategory.objects.get(name="staff_salary",is_deleted=False,is_system_generated=True,college=current_college)
            if AccountHead.objects.filter(name="salary",is_deleted=False,is_system_generated=True,college=current_college).exists():
                account_head = AccountHead.objects.get(name="salary",is_deleted=False,is_system_generated=True,college=current_college)
               
                #create fee_payment
                
                for student_fee in salaries:
                    auto_id = get_auto_id(Transaction)
                    if transaction_mode == "cash":
                        cash_account = transaction_form.cleaned_data['cash_account']
                        cash_balance = cash_account.balance
                        new_balance = cash_balance + Decimal(actual_amount)
                        CashAccount.objects.filter(pk=cash_account.pk).update(balance=new_balance)
                        if student_fee.balance > amount:
                            balance = amount
                            student_fee.paid = student_fee.paid + amount
                            student_fee.balance = Decimal(student_fee.amount) - Decimal(student_fee.paid)
                            student_fee.save()
                        else:
                            balance = student_fee.balance
                            student_fee.paid = student_fee.paid + student_fee.balance
                            amount = Decimal(amount) - Decimal(student_fee.balance)
                            student_fee.balance = 0                
                            student_fee.save()

                        transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "expense",
                                    transaction_category = transaction_category,
                                    payment_to = "cash_account",
                                    cash_account = cash_account,
                                    payment_mode = None,
                                    bank_account = None,
                                    card_details = None,
                                    cheque_details = None,
                                    is_cheque_withdrawed = False,
                                    staff = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                )
                        cash_credit = balance
                        bank_credit = 0

                        Journel.objects.create(
                            date = datetime.date.today(),
                            cash_credit = cash_credit,
                            bank_credit = bank_credit,
                            transaction = transaction,
                            expense = balance
                        )
                    elif transaction_mode == "bank":
                        bank_account = transaction_form.cleaned_data['bank_account']
                        bank_balance = bank_account.balance
                        new_balance = bank_balance + actual_amount
                        BankAccount.objects.filter(pk=bank_account.pk).update(balance=new_balance)
                        payment_mode = transaction_form.cleaned_data['payment_mode'] 
                        if student_fee.balance > amount:                                
                            balance = amount
                            student_fee.paid = student_fee.paid + amount
                            student_fee.balance = Decimal(student_fee.amount) - Decimal(student_fee.paid)
                            student_fee.save()                                    
                        else:
                            balance = student_fee.balance
                            student_fee.paid = student_fee.paid + student_fee.balance
                            amount = Decimal(amount) - Decimal(student_fee.balance)
                            student_fee.balance = 0                
                            student_fee.save()

                        if payment_mode == "cheque_payment":
                            is_cheque_withdrawed = transaction_form.cleaned_data['is_cheque_withdrawed']
                            cheque_details = transaction_form.cleaned_data['cheque_details']
                            transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "expense",
                                    transaction_category = transaction_category,
                                    transaction_mode = "bank",
                                    payment_to = "bank_account",
                                    bank_account = bank_account,
                                    payment_mode = "cheque_payment",
                                    cash_account = None,
                                    card_details = None,
                                    cheque_details = cheque_details,
                                    is_cheque_withdrawed = is_cheque_withdrawed,
                                    staff = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                )
                            cash_credit = 0
                            bank_credit = balance

                            Journel.objects.create(
                                date = datetime.date.today(),
                                bank_credit = bank_credit,
                                cash_credit = cash_credit,
                                transaction = transaction,
                                expense = balance
                            )
                        elif payment_mode == "internet_banking":
                            transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "expense",
                                    transaction_category = transaction_category,
                                    payment_to = "bank_account",                                        
                                    transaction_mode = "bank",
                                    bank_account = bank_account,
                                    payment_mode = "internet_banking",
                                    cash_account = None,
                                    card_details = None,
                                    cheque_details = None,
                                    is_cheque_withdrawed = False,
                                    staff = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,
                                    account_head = account_head
                                ) 
                            cash_credit = 0
                            bank_credit = balance

                            Journel.objects.create(
                                date = datetime.date.today(),
                                cash_credit = cash_credit,
                                bank_credit = bank_credit,
                                transaction = transaction,
                                expense = balance
                            )  
                        elif payment_mode == "card_payment":
                            card_details = transaction_form.cleaned_data['card_details']
                            transaction = Transaction.objects.create(
                                    auto_id = auto_id,
                                    creator = request.user,
                                    updater = request.user,
                                    transaction_type = "expense",
                                    transaction_category = transaction_category,
                                    payment_to = "bank_account",
                                    transaction_mode = "bank",
                                    bank_account = bank_account,
                                    payment_mode = "card_payment",
                                    cash_account = None,
                                    card_details = card_details,
                                    cheque_details = None,
                                    is_cheque_withdrawed = False,
                                    staff = instance,
                                    date = datetime.datetime.now(),
                                    amount = balance,
                                    month = student_fee.month,
                                    a_id = get_a_id(BankAccount,request),
                                    college = current_college,                                    
                                    account_head = account_head
                                )   

                            cash_credit = 0
                            bank_credit = balance

                            Journel.objects.create(
                                date = datetime.date.today(),
                                cash_debit = cash_debit,
                                bank_credit = bank_credit,
                                transaction = transaction,
                                expense = balance
                            )
                                             

            response_data = {
                'status' : 'true',     
                'title' : "Salary Payment Created",       
                'redirect' : 'true', 
                'redirect_url' : reverse('staffs:staff',kwargs={'pk':pk} ),
                'message' : "Salary Payment Created Successfully"
            }
                    
        else:           
            message = generate_form_errors(transaction_form,formset=False) 
            print(transaction_form.errors)     
            response_data = {
                "status" : 'false',
                "title" : "Form Validation Error" ,
                "stable" : "true",
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:        
        total_amount = salaries.aggregate(amount=Sum('balance'))
        transaction_form = SalaryPaymentForm(initial={'amount':total_amount['amount']}) 
        transaction_form.fields['bank_account'].queryset = BankAccount.objects.filter(is_deleted=False)
        transaction_form.fields['cash_account'].queryset = CashAccount.objects.filter(is_deleted=False)
        context = {
            "transaction_form" : transaction_form,
            "monthly_fees" : salaries,
            "fee_title" : "Monthly Salary",
            "title" : "Create Salary Payment",  
            "url" : reverse('finance:create_salary_payment',kwargs={'pk':pk}),

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'finance/entry_salary_payment.html', context)