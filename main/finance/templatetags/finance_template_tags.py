from django.shortcuts import get_object_or_404
from django import template
from django.template.defaultfilters import stringfilter
import datetime
from django.db.models import Sum
register = template.Library()
from finance.models import Journel,TransactionCategory


@register.filter
def get_debit(bank_account,date):
	college = bank_account.college
	created_date = college.date_added
	instances = Journel.objects.filter(is_deleted=False,transaction__bank_account=bank_account)
	if date:
		from_date = created_date
		year_cr = from_date.year
		year = date.year
		last_year = year - 1
		to_date = datetime.date(last_year,12,31)
		title = "Daily Bank Book : Date %s" %(str(date))
		if year_cr < last_year:
			instances = instances.filter(is_deleted=False,date__range=[from_date,to_date])
		else:
			instances = instances.filter(is_deleted=False,date = from_date) 
	debit = instances.aggregate(amount=Sum('bank_debit')).get('amount',0) or 0
	debit_amount = debit + bank_account.first_time_balance
	return debit_amount


@register.filter
def get_credit(bank_account,date):
	college = bank_account.college
	instances = Journel.objects.filter(is_deleted=False,transaction__bank_account=bank_account)
	created_date = college.date_added
	if date:
		from_date = created_date
		year_cr = from_date.year
		year = date.year
		last_year = year - 1
		to_date = datetime.date(last_year,12,31)
		title = "Daily Bank Book : Date %s" %(str(date))
		if year_cr < last_year:
			instances = instances.filter(is_deleted=False,date__range=[from_date,to_date])
		else:
			instances = instances.filter(is_deleted=False,date = from_date) 
	credit = instances.aggregate(amount=Sum('bank_credit')).get('amount',0) or 0
	return credit


@register.filter
def get_transaction(from_date,to_date):
	instances = Journel.objects.filter(is_deleted=False,date__range=[from_date,to_date])
	transaction_categories = AccountHead.objects.filter(is_deleted=False)
	categories = []	
	for category in transaction_categories:
		for instance in instances:
			if instance.transaction.account_head == category :
				categories.append(instance)
	return categories


@register.filter
def get_debit_amount(date,category):
	date_list =  date.split(',')
	from_date = date_list[0]
	to_date = date_list[1]
	instances = Journel.objects.filter(is_deleted=False,transaction__account_head__pk=category,date__range=[from_date,to_date])
	debit = instances.aggregate(amount=Sum('income')).get('amount',0) or 0
	return debit


@register.filter
def get_credit_amount(date,category):
	date_list =  date.split(',')
	from_date = date_list[0]
	to_date = date_list[1]
	instances = Journel.objects.filter(is_deleted=False,transaction__account_head__pk=category,date__range=[from_date,to_date])
	credit = instances.aggregate(amount=Sum('expense')).get('amount',0) or 0
	return credit


@register.filter
def get_income(date,category):
	instances = Journel.objects.filter(is_deleted=False,transaction__account_head__pk=category,date__lte=date)
	debit = instances.aggregate(amount=Sum('income')).get('amount',0) or 0
	return debit


@register.filter
def get_expense(date,category):
	instances = Journel.objects.filter(is_deleted=False,transaction__account_head__pk=category,date__lte=date)
	credit = instances.aggregate(amount=Sum('expense')).get('amount',0) or 0
	return credit