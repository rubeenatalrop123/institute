from django import forms
from django.forms.widgets import TextInput, Textarea, Select
from django.utils.translation import ugettext_lazy as _
from main.models import College, Batch


class CollegeForm(forms.ModelForm):

    class Meta:
        model = College
        exclude = ['creator','updater','auto_id','is_deleted','auto_id','package_type']
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'College Name'}),
            'address': TextInput(attrs={'class': 'required form-control','placeholder' : 'Address'}),
            'email': TextInput(attrs={'class': 'required form-control','placeholder' : 'Email'}),
            'phone': TextInput(attrs={'class': 'required form-control','placeholder' : 'Phone/Mobile'}),
            'office_number': TextInput(attrs={'class': 'required form-control','placeholder' : 'Office Number'}),
            'website': TextInput(attrs={'class': 'form-control','placeholder' : 'Website'}),
            'institution_type': TextInput(attrs={'class': 'required form-control','placeholder' : 'Institution Type'}),
            'affiliation': TextInput(attrs={'class': 'required form-control','placeholder' : 'Affiliation'}),
            'institution_code': TextInput(attrs={'class': 'required form-control','placeholder' : 'Institution Code'}),
            'founder_name': TextInput(attrs={'class': 'form-control','placeholder' : 'Founder Name'}),
            'theme': Select(attrs={'class': 'required selectpicker'}),
            'print_type': Select(attrs={'class': 'required selectpicker'}),
        }

        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
            'email' : {
                'required' : _("Email field is required."),
            },
            'phone' : {
                'required' : _("Phone field is required."),
            },
            'address' : {
                'required' : _("Address field is required."),
            }
        }

        labels = {
            "name" : "College Name",
            'phone' : "Phone/Mobile",
            'office_number' : "Office Number",
            'founder_name' : "Founder Name",
            'institution_code' : "Institution Code",
            'institution_type' : "Institution Type"
        }


class AcademicYearForm(forms.ModelForm):

    class Meta:
        model = Batch
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control','placeholder' : 'Name'}),
            'start_date': TextInput(attrs={'class': ' form-control date-picker', 'placeholder': 'Start Date'}),
            'end_date': TextInput(attrs={'class': ' form-control date-picker', 'placeholder': 'End Date'}),
        }

        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
        }

        labels = {
            "name" : "Name",
            "start_date" : "Start Date",
            "end_date" : "End Date",
            "is_active" : "Is Active"
        }
