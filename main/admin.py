from django.contrib import admin
from main.models import College


class CollegeAdmin(admin.ModelAdmin):
    list_display = ('auto_id','name','address','phone','email','website','logo','date_added')
admin.site.register(College,CollegeAdmin)
