from __future__ import unicode_literals
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from enquiries.models import Enquiry
from academics.models import Admission
from enquiries.forms import EnquiryForm
from django.utils.translation import ugettext_lazy as _
from main.functions import get_auto_id, generate_form_errors, get_current_role, generate_unique_id, get_admn_no, get_a_id
from users.functions import get_current_college, college_access,get_current_batch
from django.forms.formsets import formset_factory
import json
from django.forms.models import inlineformset_factory
from django.forms.widgets import TextInput, Select
from main.decorators import check_mode, ajax_required, role_required
import datetime
import xlrd
import random
from dal import autocomplete
from django.core import serializers
from decimal import Decimal
from django.db.models import Q
from django.contrib.auth.models import User, Group


class AdmissionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = Admission.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(auto_id__istartswith=self.q) |
                                 Q(name__istartswith=self.q)
                                )

        return items


@check_mode
@login_required
@role_required(['superadmin'])
def create_enquiry(request):
    url = reverse('enquiries:create_enquiry')
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)

    if request.method == "POST":
        form = EnquiryForm(request.POST)
        if form.is_valid():             
            data = form.save(commit=False)
            auto_id = get_auto_id(Enquiry)
            data.auto_id = auto_id  
            data.creator = request.user
            data.updater = request.user
            data.college = current_college
            data.batch = current_batch
            data.a_id = get_a_id(Enquiry,request)
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully created",
                "message": "Enquiry successfully created.",
                "redirect": "true",
                "redirect_url": reverse('enquiries:enquiry', kwargs={'pk':data.pk})
                }

        else:
            message = generate_form_errors(form, formset=False)            

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = EnquiryForm()

        context = {
            "title" : "Create Enquiry",
            "form" : form,            
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'enquiries/entry_enquiry.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_enquiry(request,pk):
    instance = get_object_or_404(Enquiry.objects.filter(pk=pk,is_deleted=False))
    url = reverse('enquiries:edit_enquiry',kwargs={'pk':pk})

    if request.method == "POST":

        form = EnquiryForm(request.POST,instance=instance)

        if form.is_valid(): 

            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Enquiry successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('enquiries:enquiry',kwargs={"pk":pk})
                }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = EnquiryForm(instance=instance)

        context = {
            "title" : "Edit Enquiry",
            "form" : form,
            "instance" : instance,
            "redirect": "true",            

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'enquiries/entry_enquiry.html', context)



@check_mode
@login_required
@role_required(['superadmin'])
def enquiries(request):
    instances = Enquiry.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(name__icontains=query))
        title = "Classes - %s" %query 

    context = {
        "instances" : instances,
        "title" : "Enquiries",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'enquiries/enquiries.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def enquiry(request,pk):
    instance = get_object_or_404(Enquiry.objects.filter(pk=pk))

    context = {
        'title' : instance.name,
        'instance' : instance,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'enquiries/enquiry.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_enquiry(request,pk):
    instance = get_object_or_404(Enquiry.objects.filter(pk=pk,is_deleted=False))
    Enquiry.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Enquiry successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('enquiries:enquiries')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')