from __future__ import unicode_literals, division
from django.shortcuts import render
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from academics.models import Department
from exams.models import Exam
from main.models import Batch
from exams.forms import ExamForm
from main.functions import get_auto_id, generate_form_errors, get_current_batch, get_current_college,get_a_id
from users.forms import UserForm
from users.models import Notification
from django.forms.formsets import formset_factory
import json
import xlrd
from main.decorators import check_mode, permissions_required, role_required, ajax_required
import datetime
from django.db.models import Q
from exams.forms import EvaluationForm, EvaluationEditForm, ExamResultForm, ExamResultEditForm
from exams.models import Evaluation, ExamResult
from academics.models import SubDivision, Department, Subject, Admission
from dal import autocomplete


class SubjectAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = Subject.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(auto_id__istartswith=self.q) |
                                 Q(name__istartswith=self.q)
                                )

        return items


@check_mode
@login_required
def exams(request):
    instances = Exam.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(title__icontains=query) | Q(from_date__icontains=query) | Q(to_date__icontains=query) | Q(department__name__icontains=query))
        title = "Exams - %s" %query 

    context = {
        "instances" : instances,
        "title" : "Exams",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request, 'exams/exams.html', context)


@check_mode
@login_required
def view_exam(request,pk):
    instance = get_object_or_404(Exam.objects.filter(pk=pk,is_deleted=False))
    context = {
        "title" : "Exam",
        'instance' : instance,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request, 'exams/exam.html', context)


@check_mode
@login_required
def create_exam(request):
    current_batch = get_current_batch()
    current_college = get_current_college(request)
    url = reverse('exams:create_exam')
    if request.method == "POST":

        form = ExamForm(request.POST)

        if form.is_valid(): 

            #create student exam
            data = form.save(commit=False)

            today = datetime.date.today()
            auto_id = get_auto_id(Exam)
            a_id = get_a_id(Exam,request)
            data.auto_id = auto_id
            data.a_id = a_id
            data.creator = request.user
            data.updater = request.user
            data.student_batch = current_batch
            data.college = current_college
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully created",
                "message": "exam successfully created.",
                "redirect": "true",
                "redirect_url": reverse('exams:view_exam',kwargs={"pk":data.pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = ExamForm()

        context = {
            "title" : "Create exam",
            "form" : form,
            "url" : url,
            "redirect" : "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_animations" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'exams/entry_exam.html', context)


@check_mode
@login_required
def edit_exam(request,pk):
    instance = get_object_or_404(Exam.objects.filter(pk=pk,is_deleted=False))
    url = reverse('exams:edit_exam',kwargs={'pk':pk})
    if request.method == "POST":

        form = ExamForm(request.POST,instance=instance)
        if form.is_valid(): 

            #update student medium
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "exam successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('exams:view_exam',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = ExamForm(instance=instance)

        context = {
            "title" : "Edit student exam",
            "form" : form,
            "instance" : instance,
            "url" : url,
            "redirect" : "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_animations" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'exams/entry_exam.html', context)


@check_mode
@login_required
def delete_exam(request,pk):
    Exam.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "exam successfully deleted.",
        "redirect" : "true",
        "redirect_url": reverse('exams:exams')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    

@check_mode
@login_required
def delete_selected_exams(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Exam.objects.filter(pk=pk,is_deleted=False)) 
            Exam.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
        response_data = {
            "status" : "true",
            "title" : "Successfully deleted",
            "message" : "Selected exam successfully deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('exams:exams')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def list_class_and_division_for_exams(request):    
    today = datetime.date.today()
    batch = get_current_batch()
    current_college = get_current_college(request)

    batch_pk = request.GET.get('batch')
    if batch_pk:
        if Batch.objects.filter(pk=batch_pk).exists():
            batch = Batch.objects.get(pk=batch_pk)

    sub_divisions = SubDivision.objects.filter(is_deleted=False,college=current_college)
    departments = Department.objects.filter(is_deleted=False,college=current_college)
    if batch:
        student_batches = Batch.objects.filter(is_deleted=False,college=current_college).exclude(pk=batch.pk)
    else:
        student_batches = Batch.objects.filter(is_deleted=False,college=current_college)
    context = {
        "title" : "Class and Divisions in %s" %batch,
        "current_batch" : batch,
        "student_batches" : student_batches,
        "sub_divisions" : sub_divisions,
        "departments" : departments,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request, 'exams/list_class_and division_for_exams.html', context)


@check_mode
@login_required
def exam(request,pk):
    department_pk = pk
    current_college = get_current_college(request)
    student_batch = get_current_batch()
    instances = Exam.objects.filter(is_deleted=False,department__pk=pk,college=current_college,student_batch=student_batch)


    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(title__icontains=query) | Q(from_date__icontains=query) | Q(to_date__icontains=query) | Q(department__name__icontains=query))
        title = "Exams - %s" %query 

    context = {
        "instances" : instances,
        "title" : "Exams",
        "department_pk":department_pk,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request, 'exams/list_exams.html', context)


@check_mode
@login_required
def create_evaluation(request,pk):
    current_college = get_current_college(request)
    student_batch = get_current_batch()
    exam = get_object_or_404(Exam.objects.filter(pk=pk,is_deleted=False))
    department = exam.department
    url = reverse('exams:create_evaluation',kwargs={'pk':pk})
    EvaluationFormset = formset_factory(EvaluationForm,extra=1)
    if request.method == "POST":        
        evaluation_formset = EvaluationFormset(request.POST,prefix='evaluation_formset')
        if evaluation_formset.is_valid():
            for f in evaluation_formset:
                try:
                    subject = f.cleaned_data['subject']
                    exam_date = f.cleaned_data['exam_date']
                    from_time = f.cleaned_data['from_time']
                    to_time = f.cleaned_data['to_time']
                    max_score = f.cleaned_data['max_score']
                    ce_max_score = f.cleaned_data['ce_max_score']
                    if Evaluation.objects.filter(subject=subject,exam=exam,exam_date=exam_date,).exists():
                        Evaluation.objects.update(
                                is_deleted = False,
                            )
                    elif not Evaluation.objects.filter(subject=subject,exam=exam,exam_date=exam_date,).exists():
                        Evaluation(
                            subject = subject,
                            exam = exam,
                            exam_date = exam_date,
                            from_time = from_time,
                            to_time = to_time, 
                            max_score = max_score,
                            ce_max_score = ce_max_score,
                            department = department,
                            college = current_college,
                            student_batch = student_batch, 
                            auto_id = get_auto_id(Evaluation),
                            a_id = get_a_id(Evaluation,request),
                            creator = request.user,
                            is_deleted = False,
                            updater = request.user
                        ).save()
                except:
                    pass
                else:
                    evaluation = Evaluation.objects.get(subject=subject,exam=exam,exam_date=exam_date)
                    evaluation.from_time = from_time
                    evaluation.to_time = to_time
                    evaluation.save()

            response_data = {

                "status": "true",
                "title": "Successfully created",
                "message": "Evaluation successfully created.",
                "redirect": "true",
                "redirect_url": reverse('exams:evaluations',kwargs={'pk':pk})
            }

        else:
            message = generate_form_errors(evaluation_formset, formset=True)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        evaluation_formset = EvaluationFormset(prefix='evaluation_formset')
        context = {
            "title" : "Create evaluation",
            "evaluation_formset" : evaluation_formset,
            "exam" : exam,
            "url" : url,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_animations" : True,
            "is_need_datetime_picker" : True,
            "is_need_formset" : True
        }
        return render(request, 'exams/entry_evaluation.html', context)


@check_mode
@login_required
def evaluations(request,pk):
    exam_pk = pk
    current_college = get_current_college(request)
    student_batch = get_current_batch()
    exam = get_object_or_404(Exam.objects.filter(pk=pk,is_deleted=False,college=current_college,student_batch=student_batch))
    instances = Evaluation.objects.filter(is_deleted=False,exam=exam,college=current_college,student_batch=student_batch)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(subject__name__icontains=query) | Q(exam__title__icontains=query) | Q(exam_date__istartswith=query) | Q(from_time__istartswith=query) | Q(to_time__icontains=query) | Q(department__name__icontains=query) | Q(sub_division__name__icontains=query) | Q(max_score__istartswith=query) | Q(ce_max_score__istartswith=query))
        title = "Evaluations - %s" %query 

    context = {
        "instances" : instances,
        "title" : "Evaluations",
        "exam" : exam,
        "exam_pk" : exam_pk,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request, 'exams/evaluations.html', context)


@check_mode
@login_required
def view_evaluation(request,pk):
    instance = get_object_or_404(Evaluation.objects.filter(pk=pk,is_deleted=False))
    exam = instance.exam
    context = {
        "title" : "Evaluation",
        "exam" : exam,
        'instance' : instance,


        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request, 'exams/evaluation.html', context)


@check_mode
@login_required
def edit_evaluation(request,pk):
    instance = get_object_or_404(Evaluation.objects.filter(pk=pk,is_deleted=False))
    url = reverse('exams:edit_evaluation',kwargs={'pk':pk})
    if request.method == "POST":

        form = EvaluationEditForm(request.POST,instance=instance)
        if form.is_valid(): 

            #update student medium
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Evaluation successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('exams:view_evaluation',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message,
                    
                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_grid_system" : True,
                "is_need_animations" : True,
                "is_need_datetime_picker" : True,
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = EvaluationEditForm(instance=instance)

        context = {
            "title" : "Edit Evaluation",
            "form" : form,
            "instance" : instance,
            "url" : url,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_animations" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'exams/edit_evaluation.html', context)


@check_mode
@login_required
def delete_evaluation(request,pk):
    Evaluation.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Evaluation successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('exams:evaluations', kwargs={'pk':pk})
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def list_divisions_for_evaluation_results(request,pk):
    instance = get_object_or_404(Evaluation.objects.filter(pk=pk,is_deleted=False))
    department = instance.department
    sub_divisions = SubDivision.objects.filter(department=department)
    context = {
        "sub_divisions" : sub_divisions,
        "title" : "Divisions of Class - %s" %(department),
        "instance" : instance,
        "department" : department,


        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,

    }
    return render(request, 'exams/list_divisions_for_evaluation_results.html', context)


@check_mode
@login_required
def create_exam_result(request,pk,division_pk):
    current_college = get_current_college(request)
    evaluation = get_object_or_404(Evaluation.objects.filter(is_deleted=False,pk=pk))
    sub_division = get_object_or_404(SubDivision.objects.filter(is_deleted=False,pk=division_pk))

    url = reverse('exams:create_exam_result', kwargs={'pk':pk,'division_pk':sub_division.pk})
    ExamResultFormset = formset_factory(ExamResultForm,extra=0)
    max_score = evaluation.max_score
    ce_max_score = evaluation.ce_max_score
    if request.method == "POST":
        result_formset = ExamResultFormset(request.POST,prefix='result_formset')

        if result_formset.is_valid(): 
            for f in result_formset:
                admission = f.cleaned_data['admission']
                score = f.cleaned_data['score']
                ce_score = f.cleaned_data['ce_score']
                principal_remark = f.cleaned_data['principal_remark']
                teacher_remark = f.cleaned_data['class_teacher_remark']
                guardian_remark = f.cleaned_data['guardian_remark']
                percentage = (score + ce_score)/(max_score + ce_max_score)*100

                if percentage >= 90 :
                    grade = "A+"
                elif percentage >= 80 :
                    grade = "A"
                elif percentage >= 70 :
                    grade = "B+"
                elif percentage >= 60 :
                    grade = "B"
                elif percentage >= 50 :
                    grade = "C+"
                elif percentage >= 40 : 
                    grade = "C"
                elif percentage >= 30 : 
                    grade = "D+"
                elif percentage >= 20 :
                    grade = "D"
                else :
                    grade = "E"
               

                if not ExamResult.objects.filter(admission=admission,evaluation=evaluation,evaluation__exam_date=evaluation.exam_date,is_deleted=False).exists():
                    ExamResult(
                        auto_id = get_auto_id(ExamResult),
                        a_id = get_auto_id(ExamResult),
                        creator = request.user,
                        updater = request.user,
                        score = score,
                        ce_score = ce_score,
                        evaluation = evaluation, 
                        grade= grade,
                        principal_remark = principal_remark,
                        guardian_remark = guardian_remark,
                        class_teacher_remark = teacher_remark,
                        admission = admission,
                        college = current_college
                    ).save()  
                else:
                    exam_result = ExamResult.objects.get(admission=admission,evaluation=evaluation,evaluation__exam_date=evaluation.exam_date,is_deleted=False)
                    exam_result.score = score
                    exam_result.grade = grade
                    exam_result.ce_score = ce_score
                    exam_result.principal_remark = principal_remark,
                    exam_result.guardian_remark = guardian_remark,
                    exam_result.class_teacher_remark = teacher_remark
                    exam_result.save()


                # Notification(
                #     user = request.user,
                #     student = admission,
                #     description = "Exam Result of %s has been Published." %(evaluation)
                # ).save()

            response_data = {
                "status": "true",
                "title": "Successfully created",
                "message": "Exam result successfully created.",
                "redirect": "true",
                "redirect_url": reverse('exams:exam_results',kwargs={'pk':pk,'division_pk':sub_division.pk}),

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_grid_system" : True,
                "is_need_animations" : True,
                "is_need_datetime_picker" : True,
            }

        else:
            message = generate_form_errors(result_formset, formset=True)
           

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message,

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_grid_system" : True,
                "is_need_animations" : True,
                "is_need_datetime_picker" : True,
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        division = sub_division
        today = datetime.date.today()
        batch = get_current_batch()
        message=""
        admissions = Admission.objects.filter(sub_division=division,batch=batch,is_deleted=False)

        initial = []
        for admission in admissions:
            admission_dict = {
                "admission" : admission,
                "admission_name" : admission.name,
                "max_score" : max_score,
                "ce_max_score" : ce_max_score,
            }
            initial.append(admission_dict)

        result_formset = ExamResultFormset(prefix='result_formset',initial=initial)

        

        context = {
            "title" : "Create Exam Result",
            "result_formset" : result_formset,
            "evaluation" : evaluation,
            "message" : message,
            "division" : division,
            "url" : url,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_animations" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'exams/entry_exam_result.html', context)

@check_mode
@login_required
def edit_exam_result(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(ExamResult.objects.filter(pk=pk,is_deleted=False))
    url = reverse('exams:edit_exam_result',kwargs={'pk':pk})
    if request.method == "POST":

        form = ExamResultEditForm(request.POST,instance=instance)
        if form.is_valid(): 

            #update student medium
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.college = current_college
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Exam result successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('exams:view_exam_result',kwargs={"pk":pk}),

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
                "is_need_animations": True,
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message,

                "is_need_select_picker" : True,
                "is_need_popup_box" : True,
                "is_need_custom_scroll_bar" : True,
                "is_need_wave_effect" : True,
                "is_need_bootstrap_growl" : True,
                "is_need_chosen_select" : True,
                "is_need_grid_system" : True,
                "is_need_datetime_picker" : True,
                "is_need_animations": True,
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = ExamResultEditForm(instance=instance)

        context = {
            "title" : "Edit Exam Result",
            "form" : form,
            "instance" : instance,
            "url" : url,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'exams/edit_exam_result.html', context)


@check_mode
@login_required
def delete_exam_result(request,pk):
    instance = get_object_or_404(ExamResult.objects.filter(pk=pk,is_deleted=False))
    sub_division = instance.admission.sub_division
    ExamResult.objects.filter(pk=pk).update(is_deleted=True)

    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Exam Result successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('exams:exam_results',kwargs={'pk':pk,'division_pk':sub_division.pk}),
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')



@check_mode
@login_required
def view_exam_result(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(ExamResult.objects.filter(pk=pk,is_deleted=False))
    evaluation = instance.evaluation
    context = {
        "title" : "Exam Result",
        "evaluation" : evaluation,
        'instance' : instance,
        "current_college" : current_college,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'exams/exam_result.html', context)

@check_mode
@login_required
def list_divisions_for_view_results(request,pk):
    instance = get_object_or_404(Evaluation.objects.filter(pk=pk,is_deleted=False))
    current_college = get_current_college(request)
    department = instance.department
    sub_divisions = SubDivision.objects.filter(department=department)
    context = {
        "sub_divisions" : sub_divisions,
        "title" : "Divisions of Class - %s" %(department),
        "instance" : instance,
        "department" : department,
        "current_college" : current_college,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,

    }
    return render(request, 'exams/list_divisions_for_view_results.html', context)


@check_mode
@login_required
def exam_results(request,pk,division_pk):
    current_college = get_current_college(request)
    evaluation = get_object_or_404(Evaluation.objects.filter(pk=pk,is_deleted=False))
    sub_division = get_object_or_404(SubDivision.objects.filter(is_deleted=False,pk=division_pk))
    department = evaluation.department
    
    instances = ExamResult.objects.filter(is_deleted=False,evaluation=evaluation,admission__sub_division=sub_division,admission__department=department)
    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(name__icontains=query))
        title = "Exam Results - %s" %query 

    context = {
        "instances" : instances,
        "title" : "Exam Results",
        "department" : department,
        "sub_division" : sub_division,
        "evaluation" : evaluation,
        "current_college" : current_college,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'exams/exam_results.html', context)
