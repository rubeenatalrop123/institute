from django import forms
from django.forms.widgets import TextInput, Textarea, HiddenInput, CheckboxInput, EmailInput, Select
from exams.models import Exam, Evaluation, ExamResult
from django.utils.translation import ugettext_lazy as _
from dal import autocomplete

class ExamForm(forms.ModelForm):
    
    class Meta:
        model = Exam
        fields = ["title","from_date","to_date",'department']
        widgets = {
            "title": TextInput(
                attrs = {
                    "class": "required form-control",
                    "placeholder" : "Exam Name",
                }
            ),
            "from_date": TextInput(
                attrs = {
                    "class": "required form-control date-picker",
                    "placeholder" : "From Date",
                }
            ),
            "to_date": TextInput(
                attrs = {
                    "class": "required form-control date-picker",
                    "placeholder" : "To Date ",
                }
            ),
            "department" :  autocomplete.ModelSelect2(url='academics:class_autocomplete',attrs={'data-placeholder': 'Classes','data-minimum-input-length': 1},),
           
        }
        error_messages = {
            "from_date" : {
                "required" : _("From Date field is required.")
            },
            "to_date" : {
                "required" : _("To date field is required.")
            },
            "name" : {
                "required" : _("Name field is required.")
            },
            "department" : {
                "required" : _("Student Class field is required.")
            }
        }



class EvaluationForm(forms.ModelForm):
    
    class Meta:
        model = Evaluation
        fields = ["subject","from_time","to_time",'exam_date','max_score','ce_max_score']
        widgets = {
            "subject": Select(
                attrs = {
                    "class": "required form-control",
                    "placeholder" : "Subject",
                }
            ),
            "exam_date": TextInput(
                attrs = {
                    "class": "required form-control",
                    "placeholder" : "mm/dd/yyyy",
                }
            ),
            "from_time": TextInput(
                attrs = {
                    "class": "required form-control",
                    "placeholder" : "HH:MM",
                }
            ),
            "to_time": TextInput(
                attrs = {
                    "class": "required form-control",
                    "placeholder" : "HH:MM",
                }
            ),
             
            "max_score": TextInput(
                attrs = {
                    "class": "required form-control number",
                }
            ),
            "ce_max_score": TextInput(
                attrs = {
                    "class": "required form-control number",
                }
            )
        }
        error_messages = {
            "from_time" : {
                "required" : _("From time field is required.")
            },
            "to_time" : {
                "required" : _("To time field is required.")
            },
            "subject" : {
                "required" : _("Subject field is required.")
            },
            "exam_date" : {
                "required" : _("date field is required.")
            }
        }


class EvaluationEditForm(forms.ModelForm):
    
    class Meta:
        model = Evaluation
        fields = ["subject","from_time","to_time",'exam_date','max_score']
        widgets = {
            "subject": Select(
                attrs = {
                    "class": "required form-control selectordie",
                    "placeholder" : "Subject Name",
                }
            ),
            "exam_date": TextInput(
                attrs = {
                    "class": "required form-control datepicker",
                    "placeholder" : "Exam Date",
                }
            ),
            "from_time": TextInput(
                attrs = {
                    "class": "required form-control",
                    "placeholder" : "HH:MM",
                }
            ),
            "to_time": TextInput(
                attrs = {
                    "class": "required form-control",
                    "placeholder" : "HH:MM",
                }
            ),
             
            "max_score": TextInput(
                attrs = {
                    "class": "required form-control number",
                    "placeholder" : "Maximum Score",
                }
            )
        }
        error_messages = {
            "from_time" : {
                "required" : _("From time field is required.")
            },
            "to_time" : {
                "required" : _("To time field is required.")
            },
            "subject" : {
                "required" : _("Subject field is required.")
            },
            "sub_division" : {
                "required" : _("Student Division field is required.")
            },
            "exam_date" : {
                "required" : _("Exam Date field is required.")
            },
            "max_score" : {
                "required" : _("Max Score field is required.")
            }
        }


class ExamResultForm(forms.ModelForm):
    admission_name =  forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))   
    max_score =  forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'})) 
    ce_max_score =  forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'})) 
    score_show =  forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))   
    grade_show =  forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))     
    class Meta:
        model = ExamResult
        fields = ["admission","score","principal_remark",'guardian_remark','class_teacher_remark','ce_score']
        widgets = {
            "admission": HiddenInput(),
            "score": TextInput(
                attrs = {
                    "class": "required form-control number",
                    "placeholder" : "Score",
                }
            ),
            "ce_score": TextInput(
                attrs = {
                    "class": "required form-control number",
                    "placeholder" : "CE Score",
                }
            ),
            "principal_remark": Textarea(
                attrs = {
                    "class": "form-control",
                    "placeholder" : "Principal remark ",
                    "rows" : 3,
                }
            ),
            "guardian_remark": Textarea(
                attrs = {
                    "class": "form-control",
                    "placeholder" : "Guardian remark ",
                    "rows" : 3,
                }
            ),
            "class_teacher_remark": Textarea(
                attrs = {
                    "class": "form-control",
                    "placeholder" : "Class Teacher remark ",
                    "rows" : 3,
                }
            )

        }
        error_messages = {
            "admission" : {
                "required" : _("Admission field is required.")
            },
            "score" : {
                "required" : _("Score field is required.")
            },
            "grade" : {
                "required" : _("Grade field is required.")
            },
            "guardian_remark" : {
                "required" : _("Guardian Remark field is required.")
            }
        }


class ExamResultEditForm(forms.ModelForm):
    class Meta:
        model = ExamResult
        fields = ["admission","score","principal_remark"]
        widgets = {
            "admission":  Select(
                attrs = {
                    "class": "required form-control selectordie",
                    "placeholder" : "Admission Name",
                }
            ),
            "score": TextInput(
                attrs = {
                    "class": "required form-control number",
                    "placeholder" : "Score",
                }
            ),
            "principal_remark": Textarea(
                attrs = {
                    "class": "form-control",
                    "placeholder" : "Principal remark",
                }
            ),
            # "guardian_remark": Textarea(
            #     attrs = {
            #         "class": "form-control",
            #         "placeholder" : "Guardian remark",
            #     }
            # ),
            # "class_teacher_remark": Textarea(
            #     attrs = {
            #         "class": "form-control",
            #         "placeholder" : "Class Teacher remark",
            #     }
            # )

        }
        error_messages = {
            "admission" : {
                "required" : _("Admission field is required.")
            },
            "score" : {
                "required" : _("Score field is required.")
            },
            "grade" : {
                "required" : _("Grade field is required.")
            },
            "guardian_remark" : {
                "required" : _("Guardian Remark field is required.")
            }
        }