from django.shortcuts import get_object_or_404
from django import template
from django.template.defaultfilters import stringfilter
import datetime
from django.db.models import Sum
register = template.Library()


@register.filter
def get_total(score,ce_score):
	total = score + ce_score
	return total
