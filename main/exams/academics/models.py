# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from main.models import BaseModel
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator
import uuid
from decimal import Decimal


BATCH_CHOICES = (
    ('science','Science'),
    ('humanities','Humanities'),  
    ('commerse','Commerse')  
)

CATEGORY_CHOICES = (
    ('general','GENERAl'),
    ('obc','OBC'),
    ('oec','OEC'),  
    ('sc/st','SC/ST')  
)

TRANSPORT = (
    ('college bus','College Bus'),
    ('private bus','Private Bus'),  
    ('others','Others')  
)

HOSTELER = (
    ('yes','Yes'),
    ('no','No')  
)

GENDER = (
    ('male','Male'),
    ('female','Female'),  
    ('other','Other')  
)

FEE_TYPE = (
    ('one_time','One Time'),
    ('monthly','Monthly')
)

WEEKDAY_CHOICES = (    

    ('forenoon','FORENOON'),
    ('afternoon',' AFTERNOON')
    
)

MONTH = (
    ('january','January'),
    ('february','February'),
    ('march','March'),
    ('april','April'),
    ('may','May'),
    ('june','June'),
    ('july','July'),
    ('august','August'),
    ('september','September'),
    ('october','October'),
    ('november','November'),
    ('december','December'),
)

WEEKDAY_CHOICES = (
    ('monday','MONDAY'),
    ('tuesday','TUESDAY'),
    ('wednesday','WEDNESDAY'),
    ('thursday','THURSDAY'),
    ('friday','FRIDAY'),
    ('saturday','SATURDAY')
)


class Department(BaseModel):
    name = models.CharField(max_length=128)
    fee = models.DecimalField(max_digits=20,decimal_places=2,default=0)
    diary_fee = models.DecimalField(max_digits=20,decimal_places=2,default=0)
    admission_fee = models.DecimalField(max_digits=20,decimal_places=2,default=0)
    library_fee = models.DecimalField(max_digits=20,decimal_places=2,default=0)
    computer_fee = models.DecimalField(max_digits=20,decimal_places=2,default=0) 
    special_fee = models.DecimalField(max_digits=20,decimal_places=2,default=0)       

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'academics_department'
        verbose_name = _('Student class')
        verbose_name_plural = _('Student Classes')
        ordering = ('auto_id',)

    def __str__(self):
        return self.name

    def get_divisions(self):
        divisions = SubDivision.objects.filter(department=self,is_deleted=False)
        return divisions


class Course(BaseModel):
    name = models.CharField(max_length=128,blank=True,null=True)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'academics_course'
        verbose_name = _('Student medium')
        verbose_name_plural = _('Student mediums')

    def __str__(self):
        return self.name


class Subject(BaseModel):
    name = models.CharField(max_length=128)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'subject'
        verbose_name = _('Subject')
        verbose_name_plural = _('Subjects')
        ordering = ('name',)

    def __str__(self):
        return self.name


class SubDivision(BaseModel):
    name = models.CharField(max_length=128,blank=True,null=True) 
    department = models.ForeignKey("academics.Department",limit_choices_to={'is_deleted': False},related_name="department%(class)s_objects",on_delete=models.CASCADE)         
    batch_type = models.CharField(max_length=128,choices=BATCH_CHOICES,blank=True,null=True)    
    course = models.ForeignKey("academics.Course",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
  
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'academics_sub_division'
        verbose_name = _('Student division')
        verbose_name_plural = _('Student Divisions')
        ordering = ('name',)

    def __str__(self):
        return "%s - %s " %(self.department,self.name)


class Admission(BaseModel): 
    is_former_student= models.BooleanField(default=False)
    former_student = models.ForeignKey("academics.Admission",related_name="former_student_%(class)s_objects",blank=True,null=True,on_delete=models.CASCADE)
    department= models.ForeignKey("academics.Department",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    sub_division = models.ForeignKey("academics.SubDivision",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    course = models.ForeignKey("academics.Course",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    batch = models.ForeignKey("main.Batch",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    name = models.CharField(max_length=128,blank=True,null=True)
    disabilities = models.ManyToManyField('academics.Disability')
    facilities = models.ManyToManyField('academics.Facility')
    address = models.TextField(blank=True,null=True)
    std_phone = models.CharField(max_length=15,blank=True,null=True)
    aadhar_no = models.CharField(max_length=15,blank=True,null=True)
    blood_group = models.CharField(max_length=15,blank=True,null=True)
    whatsapp_number = models.CharField(max_length=15,blank=True,null=True)
    actual_fee = models.DecimalField(max_digits=20,decimal_places=2,default=0,validators=[MinValueValidator(Decimal('0.00'))])
    discount = models.DecimalField(max_digits=20,decimal_places=2,default=0,validators=[MinValueValidator(Decimal('0.00'))])
    fee_amount = models.DecimalField(max_digits=20,decimal_places=2,validators=[MinValueValidator(Decimal('0.00'))],default=0)
    paid = models.DecimalField(max_digits=20,decimal_places=2,default=0,validators=[MinValueValidator(Decimal('0.00'))])
    balance = models.DecimalField(max_digits=20,decimal_places=2,default=0,validators=[MinValueValidator(Decimal('0.00'))])
    dob = models.DateField(blank=True,null=True)
    age = models.CharField(max_length=128,null=True,blank=True)
    gender = models.CharField(max_length=128,choices=GENDER,null=True,blank=True)    
    photo = models.ImageField(upload_to="students/",blank=True,null=True)
    upload = models.FileField(upload_to="students/",blank=True,null=True)
    upload_tc = models.FileField(upload_to="students/",blank=True,null=True)
    upload_aadhar_card = models.FileField(upload_to="students/",blank=True,null=True)
    upload_curricular_activity = models.FileField(upload_to="students/",blank=True,null=True)
    
    guardian_name = models.CharField(max_length=128,blank=True,null=True)
    guardian_email = models.EmailField(max_length=128,blank=True,null=True)
    guardian_phone = models.CharField(max_length=15,blank=True,null=True)
    guardian_occupation = models.CharField(max_length=128,blank=True,null=True)
    father_name = models.CharField(max_length=128,blank=True,null=True)
    father_email = models.EmailField(max_length=128,blank=True,null=True)
    phone = models.CharField(max_length=15,blank=True,null=True)
    father_occupation = models.CharField(max_length=128,blank=True,null=True)
    father_qualification = models.CharField(max_length=128,blank=True,null=True)
    father_annual_income = models.CharField(max_length=128,blank=True,null=True)
    father_aadhar_no = models.CharField(max_length=15,blank=True,null=True)
    
    mother_name = models.CharField(max_length=128,blank=True,null=True)
    mother_email = models.EmailField(max_length=128,blank=True,null=True)
    mother_phone = models.CharField(max_length=128,blank=True,null=True)
    mother_occupation = models.CharField(max_length=128,blank=True,null=True)
    mother_qualification = models.CharField(max_length=128,blank=True,null=True)
    mother_annual_income = models.CharField(max_length=128,blank=True,null=True)
    mother_aadhar_no = models.CharField(max_length=15,blank=True,null=True)    
    
    date_joined = models.DateField(blank=True,null=True)
    register_no = models.PositiveIntegerField(db_index=True,unique=True,blank=True,null=True)
    admn_no = models.PositiveIntegerField(db_index=True,unique=True,blank=True,null=True)
    co_curricular = models.CharField(max_length=128,blank=True,null=True)

    caste = models.CharField(max_length=128,blank=True,null=True)
    religion = models.CharField(max_length=128,blank=True,null=True)
    category = models.CharField(max_length=128,choices=CATEGORY_CHOICES,blank=True,null=True)  

    mode_of_transport = models.CharField(max_length=128,choices=TRANSPORT,blank=True,null=True) 

    are_you_hosteler = models.CharField(max_length=128,choices=HOSTELER,blank=True,null=True,default='no') 
    hostel_name = models.CharField(max_length=128,blank=True,null=True)
    place = models.CharField(max_length=128,blank=True,null=True)    

    is_deleted = models.BooleanField(default=False)
    is_roll_out = models.BooleanField(default=False)    

    admission_fee_paid = models.BooleanField(default=False)
    diary_fee_paid = models.BooleanField(default=False)
    admission_fee = models.DecimalField(max_digits=20,decimal_places=2,validators=[MinValueValidator(Decimal('0.00'))],default=0)
    diary_fee = models.DecimalField(max_digits=20,decimal_places=2,validators=[MinValueValidator(Decimal('0.00'))],default=0)

    vacation = models.BooleanField(default=False)
    first_term = models.BooleanField(default=False)    
    second_term = models.BooleanField(default=False)
    final = models.BooleanField(default=False)

    class Meta:
        db_table = 'academics_admission'
        verbose_name = _('admission')
        verbose_name_plural = _('admissions')
        ordering = ('department','sub_division','name')

    def __str__(self):
        return "%s - %s " %(self.name,self.admn_no)


class Disability(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128)
    is_deleted = models.BooleanField(default=False)

    
    class Meta:
        db_table = 'academics_disability'
        verbose_name = _('disability')
        verbose_name_plural = _('disabilities')
        ordering = ('name',)

    def __str__(self):
        return self.name


class Facility(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128)
    is_deleted = models.BooleanField(default=False)

    
    class Meta:
        db_table = 'academics_facility'
        verbose_name = _('facility')
        verbose_name_plural = _('facilities')
        ordering = ('name',)

    def __str__(self):
        return self.name


class FeeCategory(BaseModel):
    name = models.CharField(max_length=128)
    department= models.ForeignKey("academics.Department", blank=True,null=True,on_delete=models.CASCADE)
    sub_division = models.ForeignKey("academics.SubDivision", blank=True,null=True,on_delete=models.CASCADE)
    student_batch = models.ForeignKey("main.Batch", blank=True,null=True,on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=20,decimal_places=2,default=0,validators=[MinValueValidator(Decimal('0.00'))])
    fee_type = models.CharField(max_length=128,choices=FEE_TYPE,default="one time")

    is_general = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'fee_category'
        verbose_name = _('Fee category')
        verbose_name_plural = _('Fee categories')

    def __str__(self):
        return self.name


class Attendance(BaseModel):
    admission= models.ForeignKey("academics.Admission",on_delete=models.CASCADE)
    date = models.DateField()
    weekday = models.CharField(max_length=128,choices=WEEKDAY_CHOICES)
    department= models.ForeignKey("academics.Department",on_delete=models.CASCADE)
    sub_division = models.ForeignKey("academics.SubDivision",on_delete=models.CASCADE)
    is_present = models.BooleanField(default=True)
    is_genuene = models.BooleanField(default=False)
    reason = models.TextField(blank=True,null=True)
    behaviour = models.TextField(blank=True,null=True)
    absent_days_count = models.PositiveIntegerField(default=0)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'student_attendance'
        verbose_name = _('attendance')
        verbose_name_plural = _('attendances')
        ordering = ('date',)
        
    def __str__(self):
        return self.date


class StudentFee(BaseModel):
    admission= models.ForeignKey("academics.Admission",on_delete=models.CASCADE)
    date = models.DateField()
    amount = models.DecimalField(max_digits=20,decimal_places=2,default=0,validators=[MinValueValidator(Decimal('0.00'))])
    paid = models.DecimalField(max_digits=20,decimal_places=2,default=0,validators=[MinValueValidator(Decimal('0.00'))])
    balance = models.DecimalField(max_digits=20,decimal_places=2,default=0,validators=[MinValueValidator(Decimal('0.00'))])
    fee_category = models.ForeignKey("academics.FeeCategory",on_delete=models.CASCADE)
    month = models.CharField(max_length=128,choices=MONTH,blank=True,null=True)
    is_general = models.BooleanField(default=False)

    def percentage(self):
        return self.paid/self.amount * 100

    class Meta:
        db_table = 'student_fee'
        verbose_name = _('Student fee')
        verbose_name_plural = _('Student fee')
        ordering = ('date_added',)

    def __unicode__(self):
        return self.amount


class Period(BaseModel):   
    weekday = models.CharField(max_length=128,choices=WEEKDAY_CHOICES)
    from_time = models.TimeField()
    to_time = models.TimeField()

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'academics_period'
        verbose_name = _('Period')
        verbose_name_plural = _('Periods')

    def __str__(self):
        return str(self.from_time) + "-" + str(self.to_time)


class TimeTable(BaseModel):
    period = models.ForeignKey("academics.Period",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    class_subject = models.ForeignKey("academics.PeriodSubject",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    department = models.ForeignKey("academics.Department",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    sub_division = models.ForeignKey("academics.SubDivision",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    weekday = models.CharField(max_length=128,choices=WEEKDAY_CHOICES)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'academics_time_table'
        verbose_name = _('Time table')
        verbose_name_plural = _('Time tables')        
        ordering = ('department','sub_division','period')

    def __str__(self):
        return str(self.teacher)


class PeriodSubject(BaseModel):
    subject = models.ForeignKey("academics.Subject",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE) 
    sub_division = models.ForeignKey("academics.SubDivision",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    no_of_period = models.PositiveIntegerField()    
    
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'academics_class_subject'
        verbose_name = _('Class Subject')
        verbose_name_plural = _('Class Subjects')
        ordering = ('no_of_period',)

    def __str__(self):
        return "%s" %(self.subject)


class StudentClub(BaseModel):
    name = models.CharField(max_length=128,blank=True,null=True) 
    issue_date = models.DateField()
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'academics_student_club'
        verbose_name = _('Student club')
        verbose_name_plural = _('Student Clubs')
        ordering = ('name',)

    def __str__(self):
        return "%s " %(self.name)


class StudentClubMember(BaseModel):
    club = models.ForeignKey("academics.StudentClub",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    student = models.ForeignKey("academics.Admission",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'academics_student_club_member'
        verbose_name = _('Student club member')
        verbose_name_plural = _('Student Club members')
        ordering = ('student',)

    def __str__(self):
        return "%s " %(self.student)