from enquiries import views
from django.conf.urls import url
from enquiries.views import AdmissionAutocomplete
from dal import autocomplete


urlpatterns = [
    url(r'^admission-autocomplete/$',AdmissionAutocomplete.as_view(),name='admission_autocomplete'),

    url(r'^create/enquiry/$', views.create_enquiry, name='create_enquiry'),
    url(r'^view/enquiry/(?P<pk>.*)/$', views.enquiry, name='enquiry'),
    url(r'^edit/enquiry/(?P<pk>.*)/$', views.edit_enquiry, name='edit_enquiry'),
    url(r'^delete/enquiry/(?P<pk>.*)/$', views.delete_enquiry, name='delete_enquiry'),
    url(r'^enquiries/$', views.enquiries, name='enquiries'),
    url(r'^enquiry/(?P<pk>.*)/$', views.enquiry, name='enquiry'),
]