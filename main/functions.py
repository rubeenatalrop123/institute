import string
import random
from django.http import HttpResponse
from decimal import Decimal
from users.functions import get_current_college
import datetime
from main.models import Batch, College, CollegeAccess
from store.product.models import Product
import json
from uuid import UUID


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def generate_unique_id(size=8, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def generate_form_errors(args,formset=False):
    message = ''
    if not formset:
        for field in args:
            if field.errors:
                message += field.errors[0]  + "|"
        for err in args.non_field_errors():
            message += str(err) + "|"

    elif formset:
        for form in args:
            for field in form:
                if field.errors:
                    message +=field.errors[0] + "|"
            for err in form.non_field_errors():
                message += str(err) + "|"
    return message[:-1]


def get_auto_id(model):
    auto_id = 1
    latest_auto_id =  model.objects.all().order_by("-date_added")[:1]
    if latest_auto_id:
        for auto in latest_auto_id:
            auto_id = auto.auto_id + 1
    return auto_id


def get_a_id(model,request):
    a_id = 1
    current_college = get_current_college(request)
    latest_a_id =  model.objects.filter(college=current_college).order_by("-date_added")[:1]
    if latest_a_id:
        for auto in latest_a_id:
            a_id = auto.a_id + 1
    return a_id


def get_timezone(request):
    if "set_user_timezone" in request.session:
        user_time_zone = request.session['set_user_timezone']
    else:
        user_time_zone = "Asia/Kolkata"
    return user_time_zone


def get_current_role(request):
    current_role = "user"
    today = datetime.date.today()
    if request.user.is_authenticated:
        if request.user.is_superuser:
            current_role = "superadmin"
        elif role == "staff" and is_college_staff:
            current_role = "staff"
    return current_role


def get_current_batch():

    current_batch = None
    if Batch.objects.filter(is_active=True,is_deleted=False).exists():
        current_batch = Batch.objects.get(is_active=True,is_deleted=False)

    return current_batch


def get_current_role(request):
    current_role = "user"
    today = datetime.date.today()
    if request.user.is_authenticated:
        if request.user.is_superuser:
            current_role = "superadmin"
        elif role == "staff" and is_college_staff:
            current_role = "staff"
    return current_role

def exp_duration(join,leaving):
    join = join
    leaving = leaving
    year = leaving.year - join.year
    if leaving.month > join.month:
        month = leaving.month - join.month
    elif join.month > leaving.month:
        month = join.month - leaving.month
    else:
        month = "0"

    print(month)
    return {"year": year,"month":month}


# def get_current_role(request):
#     is_college_superadmin = False
#     is_college_administrator = False
#     is_college_staff = False

#     current_college = get_current_college(request)
#     if request.user.is_authenticated and current_college:

#         if College.objects.filter(pk=current_college.pk,creator=request.user).exists():
#             is_college_superadmin = True

#         if CollegeAccess.objects.filter(user=request.user,college=current_college,group__name="administrator").exists():
#             is_college_administrator = True

#         if CollegeAccess.objects.filter(user=request.user,college=current_college,group__name="staff").exists():
#             is_college_staff = True


#     current_role = "user"

#     if "current_role" in request.session:
#         role =  request.session['current_role']
#         if role == "administrator" and is_college_superadmin:
#             current_role = "superadmin"
#         elif role == "administrator" and is_college_administrator:
#             current_role = role
#         elif role == "staff" and is_college_staff:
#             current_role = role
#     else:
#         if is_college_superadmin:
#             current_role = "superadmin"
#         elif is_college_administrator:
#             current_role = "administrator"
#         elif is_college_staff:
#             current_role = "staff"

#     return current_role


def get_colleges(request):
    colleges = []
    if request.user.is_authenticated:
        college_access = CollegeAccess.objects.filter(user=request.user,is_accepted=True)
        for access in college_access:
            if not access.college in colleges:
                colleges.append(access.college)

    return colleges

def get_admn_no(Model):
    admn_no = 10000
    latest_admn_no =  Model.objects.filter(former_student=None).order_by("-date_added")[:1]
    if latest_admn_no:
        for admn in latest_admn_no:
            admn_no = admn.admn_no + 1
    return admn_no


def update_stock(request,pk,qty,status):
    product = Product.objects.get(pk=pk)
    stock = product.stock
    if status == "increase":
        balance_stock = stock + qty
    elif status == "decrease":
        balance_stock = stock - qty

    product.stock = balance_stock
    product.save()


def sendSMS(mobiles,message):
    url = "http://sms.zentapps.com/sendsms?uname=aprm&pwd=aprm@567&senderid=APRMCS&to=%s&msg=%s&route=T" %(mobiles,message)
    r = requests.get(url=url)
    data = r.content
    print (data)
    return data


class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            # if the obj is uuid, we simply return the value of uuid
            return obj.hex
        return json.JSONEncoder.default(self, obj)