from academics.models import StudentFee, FeeCategory
from main.functions import get_auto_id,get_a_id
from users.functions import get_current_college
import datetime 


def update_student_fee(request,admission,fee_category,amount,month=None):
	today = datetime.date.today()
	current_college = get_current_college(request)
	admission_fee = FeeCategory.objects.filter(name="admission_fee",is_deleted=False)[0]

	StudentFee(
	    auto_id = get_auto_id(StudentFee),
	    admission = admission,
	    date = today,
	    amount = amount,
	    balance = amount,
	    fee_category = fee_category,
	    month = month,
	    creator = request.user,
	    updater = request.user,
        a_id = get_a_id(StudentFee,request),
        college=current_college
	).save()