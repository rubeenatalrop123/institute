from users.functions import college_access, get_current_college, get_current_batch
from main.functions import get_current_role
from users.models import Notification
from main.models import CollegeAccess, Batch
import datetime


def main_context(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    today = datetime.date.today().strftime('%m/%d/%Y')

    previous_date = datetime.date.today() - datetime.timedelta(days=31)
    is_superuser = False
    user_instance =None
    staff_instance = None
    
    if "set_user_timezone" in request.session:
        user_session_ok = True
        user_time_zone = request.session['set_user_timezone']
    else:
        user_session_ok = False
        user_time_zone = "Asia/Kolkata"
       
    current_theme = 'cyan-600'
    if current_college:
        current_theme = current_college.theme

    if request.user.is_authenticated:
        if CollegeAccess.objects.filter(user=request.user).exists():
            if request.user.is_superuser:
                is_superuser=True
        current_role = get_current_role(request)    
        if current_role == "staff":
            user_instance = Staff.objects.get(user=request.user,college=current_college)
            staff_instance = user_instance
        recent_notifications = Notification.objects.filter(user=request.user,is_deleted=False).order_by('-time')[:6]
    else:
        recent_notifications = []   

    all_batches = Batch.objects.filter(is_deleted=False,college=current_college) 

    active_parent = request.GET.get('active_parent')
    active = request.GET.get('active')
        
    return {
        'app_title' : "Institute",
        "user_session_ok" : user_session_ok,        
        "user_time_zone" : user_time_zone,
        "confirm_delete_message" : "Are you sure want to delete this item. All associated data may be removed.",
        "revoke_access_message" : "Are you sure to revoke this user's login access",
        "confirm_college_delete_message" : "Your college will deleted permanantly. All data will lost.",
        "confirm_delete_selected_message" : "Are you sure to delete all selected items.",
        "confirm_read_message" : "Are you sure want to mark as read this item.",
        "confirm_read_selected_message" : "Are you sure to mark as read all selected items.",
        'domain' : request.META['HTTP_HOST'],
        "college_access" : college_access(request),
        'current_college' : current_college,
        "current_theme" : current_theme,
        "current_batch" : current_batch,
        "previous_date" :previous_date,
        
        "active_parent" : active_parent,
        "active_menu" : active,
        "user_instance" : user_instance,
        "staff_instance" : staff_instance,
        "is_superuser" : is_superuser,
        "recent_notifications" : recent_notifications,
        "all_batches": all_batches,
    }
    
    





