import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator
from decimal import Decimal


BOOL_CHOICES = ((1, 'Yes'), (0, 'No'))

THEME_CHOICES = (
    ('teal', 'Teal'),
    ('blue', 'Blue'),
    ('bluegrey', 'Blue Grey'),
    ('cyan-600', 'Cyan'),
    ('green', 'Green'),
    ('lightgreen', 'Light Green'),
    ('purple-400', 'Purple'),
    ('red-400', 'Red'),
    ('pink-400', 'Pink'),
    ('brown', 'Brown'),
    ('grey-600', 'Grey'),
    ('orange', 'Orange')
)

PACKAGE_CHOICES=(
    ('free', 'FREE'),
    ('premium',"PREMIUM")
)

PRINT_CHOICES = (
    ('a4', 'A4'),
    ('compact', 'Compact(77mm)'),
)

PACKAGE_TYPE = (
    ('premium' , 'Premium'),
    ('free' , 'Free')
)

class BaseModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    auto_id = models.PositiveIntegerField(db_index=True,unique=True)
    creator = models.ForeignKey("auth.User",blank=True,related_name="creator_%(class)s_objects",on_delete=models.CASCADE)
    updater = models.ForeignKey("auth.User",blank=True,related_name="updater_%(class)s_objects",on_delete=models.CASCADE)
    date_added = models.DateTimeField(db_index=True,auto_now_add=True)
    date_updated = models.DateTimeField(auto_now_add=True)
    is_deleted = models.BooleanField(default=False)
    a_id = models.PositiveIntegerField()
    college = models.ForeignKey("main.College",on_delete=models.CASCADE,blank=True,null=True)

    class Meta:
        abstract = True


class Mode(models.Model):
    readonly = models.BooleanField(default=False)
    maintenance = models.BooleanField(default=False)
    down = models.BooleanField(default=False)

    class Meta:
        db_table = 'mode'
        verbose_name = _('mode')
        verbose_name_plural = _('mode')
        ordering = ('id',)

    class Admin:
        list_display = ('id', 'readonly', 'maintenance', 'down')

    def __str__(self):
        return self.id


class CollegeAccess(models.Model):
    user = models.ForeignKey('auth.User',null=True,on_delete=models.CASCADE)
    college = models.ForeignKey('main.College',blank=True,on_delete=models.CASCADE)
    group = models.ForeignKey('auth.Group',on_delete=models.CASCADE)
    is_accepted = models.BooleanField(default=False)
    is_default = models.BooleanField(default=False)

    class Meta:
        db_table = 'college_access'
        verbose_name = _('college_access')
        verbose_name_plural = _('college_access')
        ordering = ('college',)

    class Admin:
        list_dispay = ('college','group','is_accepted',)

    def __str__(self):
        return self.college.name + ' ' + self.group.name


class College(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    auto_id = models.PositiveIntegerField(db_index=True,unique=True)
    creator = models.ForeignKey("auth.User",blank=True,related_name="creator_%(class)s_objects",on_delete=models.CASCADE)
    updater = models.ForeignKey("auth.User",blank=True,related_name="updater_%(class)s_objects",on_delete=models.CASCADE)
    date_added = models.DateTimeField(db_index=True,auto_now_add=True)
    date_updated = models.DateTimeField(auto_now_add=True)
    is_deleted = models.BooleanField(default=False)
    theme = models.CharField(max_length=40,choices=THEME_CHOICES,default="teal")
    print_type = models.CharField(max_length=128,choices=PRINT_CHOICES,default="a4")

    name = models.CharField(max_length=128)
    address = models.TextField()
    phone = models.CharField(max_length=128)
    office_number = models.CharField(max_length=128)
    email = models.EmailField()
    website = models.CharField(null=True,blank=True,max_length=128)
    logo = models.ImageField(upload_to="college/",blank=True,null=True)

    institution_type = models.CharField(max_length=128)
    institution_code = models.CharField(max_length=128)
    affiliation = models.CharField(max_length=128)
    founder_name = models.CharField(max_length=128, blank=True, null=True)
    package_type = models.CharField(max_length=128,choices=PACKAGE_TYPE,default="free")

    class Meta:
        db_table = 'college'
        verbose_name = _('college')
        verbose_name_plural = _('colleges')
        ordering = ('name',)

    def __str__(self):
        return self.name


class Batch(BaseModel):
    name = models.CharField(max_length=128)
    start_date = models.DateField(blank=True,null=True)
    end_date = models.DateField(blank=True,null=True)
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'students_batch'
        verbose_name = _('batch')
        verbose_name_plural = _('batchs')
        ordering = ('name',)

    def __str__(self):
        return self.name


class Place(BaseModel):
    name = models.CharField(max_length=128)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'places_place'
        verbose_name = _('Place')
        verbose_name_plural = _('Places')

    def __str__(self):
        return self.name
