from django.conf.urls import  url,include
from main import views

app_name = "main"
urlpatterns = [
    url(r'^$', views.dashboard, name='dashboard'),
    
    url(r'^create-batch/$', views.create_batch, name='create_batch'),
    url(r'^batches/$', views.batches, name='batches'),
    url(r'^batch/(?P<pk>.*)/$', views.batch, name='batch'),
    url(r'^delete-batch/(?P<pk>.*)/$', views.delete_batch, name='delete_batch'),
    url(r'^edit-batch/(?P<pk>.*)/$', views.edit_batch, name='edit_batch'),

    url(r'^search/$', views.search, name='search'), 
]