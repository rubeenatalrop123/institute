from django import forms
from store.product.models import Product, Category, Sale ,SaleItem
from django.forms.widgets import TextInput, Select
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from dal import autocomplete


class ProductForm(forms.ModelForm):
    
    class Meta:
        model = Product
        exclude = ['creator','updater','auto_id','is_deleted','a_id','college']
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Enter product name','class':'form-control required'}),
            'code': TextInput(attrs={'placeholder': 'Enter product code','class': 'form-control required'}),
            'category': autocomplete.ModelSelect2(url='store_product:product_category_autocomplete', attrs={'data-placeholder': 'Category', 'data-minimum-input-length': 1},),
            'cost': TextInput(attrs={'placeholder': 'Enter product cost','class':'form-control required number'}),
            'price': TextInput(attrs={'placeholder': 'Enter product price','class': 'form-control required number'}),
            'stock': TextInput(attrs={'placeholder': 'Enter Stock','class': 'form-control required number'}),
        }
        error_messages = {
            'category' : {
                'required' : _("Category field is required."),
            },
        }


class ProductCategoryForm(forms.ModelForm):
    
    class Meta:
        model = Category
        exclude = ['creator','updater','auto_id','is_deleted','a_id','college']
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Enter Category','class':'form-control required'}),
        }


class SaleForm(forms.ModelForm):
    
    class Meta:
        model = Sale
        exclude = ['creator','updater','auto_id','is_deleted','a_id','college','balance','advance','total','sub_total_amount','paid','batch','cost']
        widgets = {
            'student': autocomplete.ModelSelect2(url='academics:admission_autocomplete', attrs={'data-placeholder': 'Student', 'data-minimum-input-length': 1},),
            'sale_id' : TextInput(attrs={'placeholder': 'Sale ID','class':'form-control required'}),
            "date" : TextInput(attrs={"class":"date-picker required form-control","placeholder":"Enter date"}),
            "discount" : TextInput(attrs={'placeholder': 'Total Discount','class':'discount form-control number'}),
            "total_amount" : TextInput(attrs={'placeholder': 'Total Discount','class':'discount form-control number'}),
        }
        
        
        
class SaleItemForm(forms.ModelForm):
    
    class Meta:
        model = SaleItem
        exclude = ['is_deleted','college','sale','sub_total','cost']
        widgets = {
            'product': autocomplete.ModelSelect2(url='store_product:products_autocomplete', attrs={'data-placeholder': 'Product', 'data-minimum-input-length': 1},),
            # 'product': Select(attrs={'data-placeholder': 'Product', 'data-minimum-input-length': 1,'class' : 'selectpicker',}),
            'qty': TextInput(attrs={'placeholder': 'Quantity','class':'required form-control'}),
            'price': TextInput(attrs={'placeholder': 'Price','class':'required form-control number',}),
            'discount_amount': TextInput(attrs={'placeholder': 'Discount','class':'required form-control number'}),
        }
        error_messages = {
            'unit' : {
                'required' : _("Unit field is required."),
            }
        }

# class SaleItemForm(forms.ModelForm):
    
#     class Meta:
#         model = Transaction
#         exclude = ['is_deleted','college','sale','sub_total','cost']
#         widgets = {
#             'product': autocomplete.ModelSelect2(url='store_product:products_autocomplete', attrs={'data-placeholder': 'Product', 'data-minimum-input-length': 1},),
#             # 'product': Select(attrs={'data-placeholder': 'Product', 'data-minimum-input-length': 1,'class' : 'selectpicker',}),
#             'qty': TextInput(attrs={'placeholder': 'Quantity','class':'required form-control'}),
#             'cost': TextInput(attrs={'placeholder': 'Cost','class':'required form-control'}),
#             'price': TextInput(attrs={'placeholder': 'Price','class':'required form-control number',}),
#             'discount': TextInput(attrs={'placeholder': 'Discount','class':'required form-control number'}),
#         }
#         error_messages = {
#             'unit' : {
#                 'required' : _("Unit field is required."),
#             }
#         }