from django.db import models
from django.utils.translation import ugettext_lazy as _
from main.models import BaseModel
from django.core.validators import MinValueValidator
from decimal import Decimal


class Category(BaseModel):
    name = models.CharField(max_length=128)
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'product_category'
        verbose_name = _('product category')
        verbose_name_plural = _('product categories')
        ordering = ('name',)
        
    class Admin:
        list_display = ('name',)
        
    def __str__(self):
        return self.name


class Product(BaseModel): 
    code = models.CharField(max_length=128)
    name = models.CharField(max_length=128)
    category = models.ForeignKey("product.Category",null=True,blank=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE) 
    cost = models.DecimalField(decimal_places=2, max_digits=15, validators=[MinValueValidator(Decimal('0.00'))])
    price = models.DecimalField(decimal_places=2, max_digits=15, validators=[MinValueValidator(Decimal('0.00'))])
    stock = models.FloatField(validators=[MinValueValidator(0.0)])

    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'product_product'
        verbose_name = _('product')
        verbose_name_plural = _('products')
        ordering = ('name',)
        
    class Admin:
        list_display = ('name',)
        
    def __str__(self):
        return self.name 


class Sale(BaseModel):
    batch = models.ForeignKey("main.Batch",on_delete=models.CASCADE,limit_choices_to={'is_deleted': False})
    student = models.ForeignKey("academics.Admission",on_delete=models.CASCADE)
    sale_id = models.CharField(max_length=128)
    date = models.DateField(blank=True)  
    sub_total_amount = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])    
    discount = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])  
    total = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])      
    paid = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])    
    balance = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])  
    advance = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))]) 

    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'sale'
        verbose_name = _('sale')
        verbose_name_plural = _('sales')
        ordering = ('-date',)  
    
    class Admin:
        list_display = ('-date',)
        
    def __str__(self):
        return self.sale_id
    
    
class SaleItem(models.Model):
    sale = models.ForeignKey("product.Sale",on_delete=models.CASCADE)
    product = models.ForeignKey("product.Product",on_delete=models.CASCADE)
    qty = models.FloatField()
    cost =  models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    price =  models.DecimalField(decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    discount_amount = models.DecimalField(default=0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])
    sub_total = models.DecimalField(default=0.0,decimal_places=2, max_digits=15,validators=[MinValueValidator(Decimal('0.00'))])    


    is_deleted = models.BooleanField(default=False)
    
    def subtotal(self):
        return Decimal(self.qty) * self.price
    
    class Meta:
        db_table = 'sale_item'
        verbose_name = _('sale item')
        verbose_name_plural = _('sale items')
        ordering = ('sale',)  
    
    class Admin:
        list_display = ('sale',)
        
    def __str__(self):
        return self.product.name