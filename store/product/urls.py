from django.conf.urls import url
from store.product import views
from store.product.views import ProductAutocomplete, ProductCategoryAutocomplete

urlpatterns = [
    url(r'^product-autocomplete/$',ProductAutocomplete.as_view(),name='products_autocomplete'),
    url(r'^product-category-autocomplete/$',ProductCategoryAutocomplete.as_view(create_field='name'),name='product_category_autocomplete'),             

    url(r'^create-product/$',views.create_product,name='create_product'),    
    url(r'^view/$',views.products,name='products'), 
    url(r'^product/view/(?P<pk>.*)/$', views.product, name='product'),
    url(r'^product/edit/(?P<pk>.*)/$', views.edit_product, name='edit_product'),
    url(r'^product/delete/(?P<pk>.*)/$', views.delete_product, name='delete_product'),
    url(r'^delete-selected-products/$', views.delete_selected_products, name='delete_selected_products'),
    
    url(r'^create-product-category/$',views.create_product_category,name='create_product_category'),   
    url(r'^product-categories/$',views.product_categories,name='product_categories'),   
    url(r'^product-category/(?P<pk>.*)/$', views.product_category, name='product_category'),
    url(r'^edit-product-category/(?P<pk>.*)/$', views.edit_product_category, name='edit_product_category'),
    url(r'^delete-product-category/(?P<pk>.*)/$', views.delete_product_category, name='delete_product_category'),
    url(r'^delete-selected-product_categories/$', views.delete_selected_product_categories, name='delete_selected_product_categories'),

    url(r'^create-sale/$',views.create_sale,name='create_sale'),    
    url(r'^sale-view/$',views.sales,name='sales'), 
    url(r'^sale/view/(?P<pk>.*)/$', views.sale, name='sale'),
    url(r'^sale/edit-sale/(?P<pk>.*)/$', views.edit_sale, name='edit_sale'),
    url(r'^sale/delete/(?P<pk>.*)/$', views.delete_sale, name='delete_sale'),
    url(r'^delete-selected-sales/$', views.delete_selected_sales, name='delete_selected_sales'),
    
    url(r'^get-price/$',views.get_price,name='get_price'),
    url(r'^get-stock/$',views.get_stock,name='get_stock'),
    url(r'^get-sale/$',views.get_sale,name='get_sale'),


 ] 