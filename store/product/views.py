from django.shortcuts import render,get_object_or_404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, college_required, ajax_required,permissions_required
from main.functions import get_auto_id, generate_form_errors, get_a_id, update_stock
from main.models import Batch
import json
from django.db.models import Q, Sum
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory
from django.forms.widgets import TextInput, Select
from django.views.decorators.http import require_GET
from users.functions import get_current_college, college_access, get_current_batch
from store.product.models import Product, Category, SaleItem, Sale
from finance.models import CashAccount, SalePayment
from main.models import CollegeAccess
from finance.forms import SalePaymentForm
from finance.models import Transaction, AccountGroup, AccountHead
from store.product.forms import ProductForm,ProductCategoryForm , SaleForm ,SaleItemForm
from django.contrib.auth.models import User, Group
from dal import autocomplete
import datetime
from users.models import Permission


class ProductAutocomplete(autocomplete.Select2QuerySetView):
	def get_queryset(self):
		current_college = get_current_college(self.request)
		products = Product.objects.filter(college=current_college,is_deleted=False)

		if self.q:
			name = products.filter(Q(name__istartswith=self.q))

		return name


class ProductCategoryAutocomplete(autocomplete.Select2QuerySetView):
	def get_queryset(self):
		current_college = get_current_college(self.request)
		categories = Category.objects.filter(college=current_college,is_deleted=False)

		if self.q:
			categories = categories.filter(Q(name__istartswith=self.q))

		return categories

	def create_object(self, text):
		current_college = get_current_college(self.request)
		auto_id = get_auto_id(Category)
		a_id = get_a_id(Category,self.request)
		return Category.objects.create(
			auto_id=auto_id,
			a_id=a_id,
			name=text,
			college=current_college,
			creator=self.request.user,
			updater=self.request.user
		)


@check_mode
@login_required 
@college_required
@permissions_required(['can_view_product_category'])
def product_categories(request):
	current_college = get_current_college(request)
	instances = Category.objects.filter(is_deleted=False,college=current_college)
	title = "Product Categories"

	query = request.GET.get("q")
	if query:
		instances = instances.filter( Q(name__icontains=query))
		title = "Product Categories - %s" %query  

	context = {
		"instances" : instances,
		'title' : title,

		"is_need_select_picker" : True,
		"is_need_popup_box" : True,
		"is_need_custom_scroll_bar" : True,
		"is_need_wave_effect" : True,
		"is_need_bootstrap_growl" : True,
		"is_need_grid_system" : True,
		"is_need_animations" : True,
		"is_need_datetime_picker" : True,
	}
	return render(request,'store/product_categories.html',context) 


@check_mode
@login_required
@college_required
@permissions_required(['can_view_product_category'])
def product_category(request,pk):
	current_college = get_current_college(request)
	instance = get_object_or_404(Category.objects.filter(pk=pk,is_deleted=False,college=current_college))
	context = {
		"instance" : instance,
		"title" : "Product Category : " + instance.name,
		"single_page" : True,
		"is_need_select_picker" : True,
		"is_need_popup_box" : True,
		"is_need_custom_scroll_bar" : True,
		"is_need_wave_effect" : True,
		"is_need_bootstrap_growl" : True,
		"is_need_grid_system" : True,
		"is_need_animations" : True,
		"is_need_datetime_picker" : True,
	}
	return render(request,'store/product_category.html',context)


@check_mode
@login_required
@college_required
@permissions_required(['can_create_product'])
def create_product_category(request):    
	current_college = get_current_college(request)
	if request.method == 'POST':
		form = ProductCategoryForm(request.POST)
		if form.is_valid(): 
			data = form.save(commit=False)
			auto_id = get_auto_id(Category)
			a_id = get_a_id(Category,request)            
			
			data.creator = request.user
			data.updater = request.user
			data.auto_id = auto_id
			data.college = current_college
			data.a_id = a_id
			data.save()
		 
			response_data = {
				"status" : "true",
				"title" : "Successfully Created",
				"message" : "Product Category created successfully.",
				"redirect" : "true",
				"redirect_url" : reverse('store_product:product_category',kwargs={'pk':data.pk})
			}   
		
		else:            
			message = generate_form_errors(form,formset=False)     
					
			response_data = {
				"status" : "false",
				"stable" : "true",
				"title" : "Form validation error",
				"message" : message
			}   
		
		return HttpResponse(json.dumps(response_data), content_type='application/javascript')
	
	else:
		form = ProductCategoryForm(initial={'college':current_college})
		context = {
			"title" : "Create product category",
			"form" : form,
			"redirect" : True,

			"is_need_select_picker" : True,
			"is_need_popup_box" : True,
			"is_need_custom_scroll_bar" : True,
			"is_need_wave_effect" : True,
			"is_need_bootstrap_growl" : True,

			"is_need_grid_system" : True,
			"is_need_datetime_picker" : True,
		}
		return render(request,'store/entry_product_category.html',context)

		
@check_mode
@login_required
@college_required
@permissions_required(['can_modify_product_category'])
def edit_product_category(request,pk):
	current_college = get_current_college(request)
	instance = get_object_or_404(Category.objects.filter(pk=pk,is_deleted=False,college=current_college)) 
	if request.method == 'POST':
			
		response_data = {}
		form = ProductCategoryForm(request.POST,instance=instance)

		if form.is_valid():  
			
			data = form.save(commit=False)
			data.updater = request.user
			data.date_updated = datetime.datetime.now()
			data.save()      
			
			response_data = {
				"status" : "true",
				"title" : "Successfully Updated",
				"message" : "Product Category Successfully Updated.",
				"redirect" : "true",
				"redirect_url" : reverse('store_product:product_category',kwargs={'pk':data.pk})
			}   
		else:
			message = generate_form_errors(form,formset=False)     
					
			response_data = {
				"status" : "false",
				"stable" : "true",
				"title" : "Form validation error",
				"message" : message
			}  
			
		return HttpResponse(json.dumps(response_data), content_type='application/javascript')

	else: 

		form = ProductCategoryForm(instance=instance,initial={'college':current_college})
		
		context = {
			"form" : form,
			"title" : "Edit Product Category : " + instance.name,
			"instance" : instance,
			"is_edit" : True,			
			"redirect" : True,
			"is_need_select_picker" : True,
			"is_need_popup_box" : True,
			"is_need_custom_scroll_bar" : True,
			"is_need_wave_effect" : True,
			"is_need_bootstrap_growl" : True,

			"is_need_grid_system" : True,
			"is_need_datetime_picker" : True,
		}
		return render(request, 'store/entry_product_category.html', context)


@check_mode
@login_required
@permissions_required(['can_delete_product_category'])
def delete_product_category(request,pk):
	current_college = get_current_college(request)
	instance = get_object_or_404(Category.objects.filter(pk=pk,is_deleted=False,college=current_college))
	
	Category.objects.filter(pk=pk,college=current_college).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
	
	response_data = {
		"status" : "true",
		"title" : "Successfully Deleted",
		"message" : "Product Category Successfully Deleted.",
		"redirect" : "true",
		"redirect_url" : reverse('store_product:product_categories')
	}
	return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@college_required
@permissions_required(['can_delete_product_category'])
def delete_selected_product_categories(request):
	current_college = get_current_college(request)
	pks = request.GET.get('pk')
	if pks:
		pks = pks[:-1]
		
		pks = pks.split(',')
		for pk in pks:      
			instance = get_object_or_404(Category.objects.filter(pk=pk,is_deleted=False,college=current_college)) 
			Category.objects.filter(pk=pk).update(is_deleted=True)
	
		response_data = {
			"status" : "true",
			"title" : "Successfully Deleted",
			"message" : "Selected Product Category(s) Successfully Deleted.",
			"redirect" : "true",
			"redirect_url" : reverse('store_product:product_categories')
		}
	else:
		response_data = {
			"status" : "false",
			"title" : "Nothing selected",
			"message" : "Please select product category first.",
		}
		
	return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@college_required
@permissions_required(['can_create_product'])
def create_product(request):    
	current_college = get_current_college(request)
	if request.method == 'POST':
		form = ProductForm(request.POST)
		if form.is_valid(): 
			data = form.save(commit=False)
			auto_id = get_auto_id(Product)
			a_id = get_a_id(Product,request)            
			
			data.creator = request.user
			data.updater = request.user
			data.auto_id = auto_id
			data.college = current_college
			data.a_id = a_id
			data.save()
		 
			response_data = {
				"status" : "true",
				"title" : "Successfully Created",
				"message" : "Product created successfully.",
				"redirect" : "true",
				"redirect_url" : reverse('store_product:product',kwargs={'pk':data.pk})
			}   
		
		else:            
			message = generate_form_errors(form,formset=False)     
					
			response_data = {
				"status" : "false",
				"stable" : "true",
				"title" : "Form validation error",
				"message" : message
			}   
		
		return HttpResponse(json.dumps(response_data), content_type='application/javascript')
	
	else:
		form = ProductForm(initial={'college':current_college})
		context = {
			"title" : "Create product",
			"form" : form,
			"redirect" : True,

			"is_need_select_picker" : True,
			"is_need_popup_box" : True,
			"is_need_custom_scroll_bar" : True,
			"is_need_wave_effect" : True,
			"is_need_bootstrap_growl" : True,

			"is_need_grid_system" : True,
			"is_need_datetime_picker" : True,
		}
		return render(request,'store/entry_product.html',context)


@check_mode
@login_required 
@college_required
@permissions_required(['can_view_product'])
def products(request):
	current_college = get_current_college(request)
	instances = Product.objects.filter(is_deleted=False,college=current_college)
	title = "Products"

	query = request.GET.get("q")
	if query:
		instances = instances.filter( Q(name__icontains=query))
		title = "Products - %s" %query  

	context = {
		"instances" : instances,
		'title' : title,

		"is_need_select_picker" : True,
		"is_need_popup_box" : True,
		"is_need_custom_scroll_bar" : True,
		"is_need_wave_effect" : True,
		"is_need_bootstrap_growl" : True,
		"is_need_grid_system" : True,
		"is_need_animations" : True,
		"is_need_datetime_picker" : True,
	}
	return render(request,'store/products.html',context) 


@check_mode
@login_required
@college_required
@permissions_required(['can_view_product'])
def product(request,pk):
	current_college = get_current_college(request)
	instance = get_object_or_404(Product.objects.filter(pk=pk,is_deleted=False,college=current_college))
	context = {
		"instance" : instance,
		"title" : "Product : " + instance.name,
		"single_page" : True,
		"is_need_select_picker" : True,
		"is_need_popup_box" : True,
		"is_need_custom_scroll_bar" : True,
		"is_need_wave_effect" : True,
		"is_need_bootstrap_growl" : True,
		"is_need_grid_system" : True,
		"is_need_animations" : True,
		"is_need_datetime_picker" : True,
	}
	return render(request,'store/product.html',context)


@check_mode
@login_required
@college_required
@permissions_required(['can_modify_product'])
def edit_product(request,pk):
	current_college = get_current_college(request)
	instance = get_object_or_404(Product.objects.filter(pk=pk,is_deleted=False,college=current_college)) 
	if request.method == 'POST':
			
		response_data = {}
		form = ProductForm(request.POST,instance=instance)

		if form.is_valid():  
			
			data = form.save(commit=False)
			data.updater = request.user
			data.date_updated = datetime.datetime.now()
			data.save()      
			
			response_data = {
				"status" : "true",
				"title" : "Successfully Updated",
				"message" : "Product  Successfully Updated.",
				"redirect" : "true",
				"redirect_url" : reverse('store_product:product',kwargs={'pk':data.pk})
			}   
		else:
			message = generate_form_errors(form,formset=False)     
					
			response_data = {
				"status" : "false",
				"stable" : "true",
				"title" : "Form validation error",
				"message" : message
			}  
			
		return HttpResponse(json.dumps(response_data), content_type='application/javascript')

	else: 

		form = ProductForm(instance=instance,initial={'college':current_college})
		
		context = {
			"form" : form,
			"title" : "Edit Product  : " + instance.name,
			"instance" : instance,
			"is_edit" : True,
			"url" : reverse('store_product:edit_product',kwargs={'pk':instance.pk}),
			"redirect" : True,
			"is_need_select_picker" : True,
			"is_need_popup_box" : True,
			"is_need_custom_scroll_bar" : True,
			"is_need_wave_effect" : True,
			"is_need_bootstrap_growl" : True,

			"is_need_grid_system" : True,
			"is_need_datetime_picker" : True,
		}
		return render(request, 'store/entry_product.html', context)


@check_mode
@ajax_required
@login_required
@college_required
@permissions_required(['can_delete_product'])
def delete_product(request,pk):
	current_college = get_current_college(request)
	instance = get_object_or_404(Product.objects.filter(pk=pk,is_deleted=False,college=current_college))
	
	Product.objects.filter(pk=pk,college=current_college).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
	
	response_data = {
		"status" : "true",
		"title" : "Successfully Deleted",
		"message" : "Product Successfully Deleted.",
		"redirect" : "true",
		"redirect_url" : reverse('store_product:products')
	}
	return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@login_required
@college_required
@permissions_required(['can_delete_product'])
def delete_selected_products(request):
	current_college = get_current_college(request)
	pks = request.GET.get('pk')
	if pks:
		pks = pks[:-1]
		
		pks = pks.split(',')
		for pk in pks:      
			instance = get_object_or_404(Product.objects.filter(pk=pk,is_deleted=False,college=current_college)) 
			Product.objects.filter(pk=pk).update(is_deleted=True)
	
		response_data = {
			"status" : "true",
			"title" : "Successfully Deleted",
			"message" : "Selected Product(s) Successfully Deleted.",
			"redirect" : "true",
			"redirect_url" : reverse('store_product:products')
		}
	else:
		response_data = {
			"status" : "false",
			"title" : "Nothing selected",
			"message" : "Please select product first.",
		}
		
	return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required 
def get_price(request):
	pk = request.GET.get('id')
	current_college = get_current_college(request)

	product_exists = False
	item = None
	if Product.objects.filter(college=current_college,pk=pk).exists():
		item = Product.objects.get(college=current_college,pk=pk)
		product_exists = True
	
	if product_exists:
		cost = item.cost
		price = item.price
		stock = item.stock
		response_data = {
			"status" : "true",
			'price' : str(price),
			'cost' : str(cost),
			'stock' : str(stock),
		}
	else:
		response_data = {
			"status" : "false",
			"message" : "Product not found"
		}

	return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def get_stock(request):
	pk = request.GET.get('id')
	if Product.objects.filter(pk=pk).exists(): 
		product = Product.objects.get(pk=pk)

		response_data = {
		'status' : 'true',
		'code' : str(product.code),
		'stock' : str(product.stock),
		'cost' : str(product.cost),
		'price' : str(product.price),
		'pk' : str(product.pk)
		}
	else:
		response_data = {
		'status' : 'false',
		'message' : "Not exist",
		}

	return HttpResponse(json.dumps(response_data), content_type='application/javascript')




@check_mode
@login_required
@college_required
@permissions_required(['can_create_sale'])
def create_sale(request):
	response_data = {}  
	current_college = get_current_college(request)
	transaction_category = get_object_or_404(Transaction.objects.filter(is_deleted=False,college=current_college))

	SaleItemFormset = formset_factory(SaleItemForm)
	
	if request.method == "POST":
		sale_form = SaleForm(request.POST)
		batch = get_current_batch(request)
		# sale_form.fields['batch'].queryset = Batch.objects.filter(college=current_college,is_deleted=False)

		payment_form = SalePaymentForm(request.POST)
		payment_form.fields['account_head'].queryset = AccountHead.objects.filter(is_deleted=False,college=current_college)
		payment_form.fields['account_group'].queryset = AccountGroup.objects.filter(is_deleted=False,college=current_college)
		
		sale_item_formset = SaleItemFormset(request.POST,prefix='sale_item_formset')
		for form in sale_item_formset:
			form.fields['product'].queryset = Product.objects.filter(college=current_college,is_deleted=False)
			
			
		if sale_form.is_valid() and sale_item_formset.is_valid() and payment_form.is_valid(): 
			discount = sale_form.cleaned_data['discount']
			transaction_mode = payment_form.cleaned_data['transaction_mode']
			payment_to = payment_form.cleaned_data['payment_to']
			amount = payment_form.cleaned_data['amount']

			amount_remaining = amount
			error_messages = ''
			products_dict = {}
			
			for f in sale_item_formset:
				product = f.cleaned_data['product']
				qty = f.cleaned_data['qty']
				cost = product.cost
				price = f.cleaned_data['price']     
				discount_amount = f.cleaned_data['discount_amount']
				sub_total = (float(qty) * float(price)) - float(discount_amount)
				
				if product.code in products_dict:    
					q = products_dict[product.code]
					products_dict[product.code] = qty + q
				else:
					products_dict[product.code] = qty
					
			for p in products_dict:
				product = Product.objects.get(college=current_college,code=p)
				qty = products_dict[p]
				stock = 0
				stock = product.stock
		
				if stock < qty:
					error_messages += "%s is out of stock. Only %s unit(s) exists </br >" % (product,stock)
			if error_messages:
				response_data = {
					"status" :'false',
					"stable" : 'true',
					"title" : 'Out of stock',
					"message" : error_messages
				}        
			else:
				auto_id = get_auto_id(Sale)
				a_id = get_a_id(Sale,request)

				data = sale_form.save(commit=False)

				data.creator = request.user
				data.updater = request.user
				data.auto_id = auto_id
				data.college = current_college
				data.a_id = a_id
				data.batch = batch
				data.save()
				
				amount=0
				sub_total=0
				total = 0
				#save SaleItem
				for f in sale_item_formset:
					product = f.cleaned_data['product']
					qty = f.cleaned_data['qty']
					cost = product.cost
					price = f.cleaned_data['price']    
					discount_amount = f.cleaned_data['discount_amount']
					sub_total = (float(qty) * float(price)) - float(discount_amount)
					amount += (float(qty) * float(price)) - float(discount_amount)
					update_stock(request,product.pk,qty,"decrease")
					
					if SaleItem.objects.filter(sale=data,product=product,price=price).exists():
						current_qty =  SaleItem.objects.get(sale=data,product=product,price=price).qty
						SaleItem.objects.filter(sale=data,product=product,price=price).update(qty=qty + current_qty)
					else:  
						SaleItem(      
							sale = data,
							product = product,
							qty = qty,
							cost = cost,
							price = price,
							discount_amount = discount_amount,
							sub_total=sub_total,
						).save()
				total=amount-float(discount)
				data.sub_total_amount = amount
				data.total=total
				data.balance = data.total
				data.save()

				if amount_remaining > 0:
					saved_sales = []
					sale_amount=data.total
					balance=data.balance
					paid=data.paid
					advance = data.advance
					if balance>0:
						if balance <= amount_remaining:
							data.balance = 0
							data.paid = data.total
							data.save()
							amount_remaining = float(amount_remaining) - float(balance)

							dict_obj = {
								"sale" : data,
								"amount" : data.total,
							}
							saved_sales.append(dict_obj)
						else:
							if amount_remaining > 0:
								data.balance = float(data.balance) - float(amount_remaining)
								data.paid = float(data.paid) + float(amount_remaining)
								data.save()
								amount_remaining = float(amount_remaining) - (balance)

								dict_obj = {
									"sale" : data,
									"amount" : data.paid + amount_remaining,
								}
								saved_sales.append(dict_obj)
					if amount_remaining > 0:
						if Sale.objects.filter(college=current_college,is_deleted=False):
							latest_sale = Sale.objects.filter(college=current_college,is_deleted=False).latest('date_added')
							latest_sale.advance = amount_remaining
							latest_sale.save()

					payment = payment_form.save(commit=False)
				
					if transaction_mode == "cash":
						payment.payment_mode = None
						payment.payment_to = "cash_account"
						payment.bank_account = None
						payment.cheque_details = None
						payment.card_details = None
						payment.is_cheque_withdrawed = False
						balance = payment.cash_account.balance
						balance = float(balance) + float(amount)
						CashAccount.objects.filter(pk=payment.cash_account.pk,college=current_college).update(balance=balance)
					elif transaction_mode == "bank":
						balance = payment.bank_account.balance
						balance = float(balance) + float(amount)
						BankAccount.objects.filter(pk=payment.bank_account.pk,college=current_college).update(balance=balance)
						payment_mode = form.cleaned_payment['payment_mode'] 
						if payment_mode == "cheque_payment":
							payment.card_details = None
								
						elif payment_mode == "internet_banking":
							payment.payment_to = "bank_account"
							payment.cash_account = None
							payment.cheque_details = None
							payment.card_details = None
							payment.is_cheque_withdrawed = False
						
						elif payment_mode == "card_payment":
							payment.payment_to = "bank_account"
							payment.cash_account = None
							payment.cheque_details = None
							payment.is_cheque_withdrawed = False
					
						if payment_to == "cash_account":
							payment.bank_account = None
						elif payment_to == "bank_account":
							payment.cash_account = None

						
					payment.creator = request.user
					payment.updater = request.user
					payment.auto_id = auto_id            
					payment.transaction_type = "income"
					payment.college = current_college
					payment.transaction_category = transaction_category.transaction_category
					payment.sale = data
					payment.student = data.student
					payment.a_id = a_id
					payment.title = "%s:%s" %(data.student,data.sale_id)
					payment.save()

					for saved_sale in saved_sales:
						amount = saved_sale['amount']
						sale = saved_sale['sale']
					auto_id = get_auto_id(SalePayment)
					a_id = get_a_id(SalePayment,request)

					SalePayment.objects.create(
						college=current_college,
						auto_id=auto_id,
						a_id=a_id,
						transaction=payment,
						amount=amount,
						college_sale=sale,
						creator=request.user,
						updater=request.user,
						)
				response_data = {
					"status" : 'true',
					"title" : "Successfully Created" ,
					"redirect" : "true",
					"redirect_url" : reverse('store_product:sales'),
					"message" : "Sale Successfully created."
				}
		   
		else:
			print("sale form")
			print(sale_form.errors)
			print("sale item form") 
			print(sale_item_formset.errors)
			print("payment form")   
			print(payment_form.errors)      
			message1 = generate_form_errors(sale_form,formset=False)
			message2 = generate_form_errors(payment_form,formset=False)
			message3 = generate_form_errors(sale_item_formset,formset=True)
			message = str(message1) + str(message2)
			response_data = {
				"status" : 'false',
				"stable" : 'true',
				"title" : "Form Validation error" ,
				"message" : message
			}
		
		return HttpResponse(json.dumps(response_data), content_type='application/javascript')

	else: 
		new_sale_id = get_a_id(Sale,request)
		sale_form = SaleForm(initial={"sale_id":new_sale_id})

		payment_form = SalePaymentForm()
		payment_form.fields['account_head'].queryset = AccountHead.objects.filter(is_deleted=False,college=current_college)
		payment_form.fields['account_group'].queryset = AccountGroup.objects.filter(is_deleted=False,college=current_college)

		sale_item_formset = SaleItemFormset(prefix='sale_item_formset')
		for form in sale_item_formset:
			form.fields['product'].queryset = Product.objects.filter(college=current_college,is_deleted=False)
			
		context = {
			"form" : sale_form,
			"sale_item_formset" : sale_item_formset,
			"title" : "Create Sale",
			"payment_form":payment_form,

			"payment_page" : True,

			"is_need_select_picker" : True,
			"is_need_popup_box" : True,
			"is_need_custom_scroll_bar" : True,
			"is_need_wave_effect" : True,
			"is_need_bootstrap_growl" : True,
			"is_need_grid_system" : True,
			"is_need_datetime_picker" : True, 
			"is_need_formset" : True,
		}
		return render(request, 'store/create_sale.html', context)
	
	
@check_mode
@login_required
@college_required
@permissions_required(['can_view_sale'])
def sales(request):
	current_college = get_current_college(request)
	instances = Sale.objects.filter(college=current_college,is_deleted=False)
	
	title = "Sales"

	query = request.GET.get("q")
	if query:
			instances = instances.filter(Q(student__name__icontains=query) | Q(sale_id__icontains=query))
		
	context = {
		'title' : title,
		"instances" : instances,

		"is_need_select_picker" : True,
		"is_need_popup_box" : True,
		"is_need_custom_scroll_bar" : True,
		"is_need_wave_effect" : True,
		"is_need_bootstrap_growl" : True,
		"is_need_grid_system" : True,
		"is_need_animations" : True,
		"is_need_datetime_picker" : True,
	}
	return render(request,'store/sales.html',context) 


def combine(L):
	results = {}
	for item in L:
		key = (item["code"],item["price"])
		if key in results:  # combine them
			results[key] = {"code": item["code"], "cost" : item["cost"], "price" : item["price"], "qty": item["qty"] + results[key]["qty"]}
		else:  # don't need to combine them
			results[key] = item
	return results.values()


@check_mode
@login_required
@college_required
@permissions_required(['can_modify_sale'])
def edit_sale(request,pk):
	current_college = get_current_college(request)
	sale_instance = get_object_or_404(Sale.objects.filter(pk=pk,college=current_college,is_deleted=False))

	if SaleItem.objects.filter(sale=sale_instance).exists():
		extra = 0
	else:
		extra= 1  
		
	SaleItemFormset = inlineformset_factory(
											  	Sale, 
											  	SaleItem, 
											  	can_delete=True,
											  	extra=extra,
											  	exclude=('college','sale','sub_total'),
											  	widgets = {
													'product': autocomplete.ModelSelect2(url='store_product:products_autocomplete', attrs={'data-placeholder': 'Product', 'data-minimum-input-length': 1},),
													'qty': TextInput(attrs={'placeholder': 'Quantity','class':'form-control required'}),
													'price': TextInput(attrs={'placeholder': 'Price','class':'form-control required number'}),
													'discount_amount': TextInput(attrs={'placeholder': 'Price','class':'form-control required number'}),
												}
											)
	
	if request.method == "POST":
		response_data = {}
	
		sale_form = SaleForm(request.POST,instance=sale_instance)
		# sale_form.fields['student'].queryset = Student.objects.filter(college=current_college,is_deleted=False)
		
		sale_item_formset = SaleItemFormset(request.POST,prefix='sale_item_formset',instance=sale_instance)
		
		if sale_form.is_valid() and sale_item_formset.is_valid(): 
			discount = sale_form.cleaned_data['discount']

			sale_items = SaleItem.objects.filter(sale=sale_instance)
			for item in sale_items:
				qty = item.qty
				product = item.product
				update_stock(request,product.pk,qty,"increase")            
			error_messages = ''
			products_dict = {}
			
			for f in sale_item_formset:
				product = f.cleaned_data['product']
				qty = f.cleaned_data['qty']
				
				if product.code in products_dict: 
					q = products_dict[product.code]
					products_dict[product.code] =  q
				else:
					products_dict[product.code] = qty
					
			for p in products_dict:
				product = Product.objects.get(college=current_college,code=p)
				qty = products_dict[p]
				stock = 0
				stock = product.stock
		
				if stock < qty:
					error_messages += "%s is out of stock. Only %s unit(s) exists </br >" % (product,stock)
					
			if not error_messages:
				amount=0
				sub_total=0
				total=0

				sale_items = SaleItem.objects.filter(is_deleted=False,college=current_college,sale=sale_instance)
				for item in sale_items:
					product = item.product
					
				#Update SaleItem
				sale_items = sale_item_formset.save(commit=False)  
				for instance in sale_items:
					instance.sale = sale_instance
					instance.college = current_college
					instance.save()
			  
				sale_item_list = []
				
				sale_items = SaleItem.objects.filter(sale=sale_instance)
				for item in sale_items:
					product = item.product
					qty = item.qty
					price = item.price
					cost = item.cost
					sub_total=float(qty) * float(price)
					amount += float(qty) * float(price)
					dict1 = {
						"code" : product.code,
						"price" : price,
						"cost" : cost,
						"qty" : qty,
						"sub_total " : sub_total,
					}
					
					sale_item_list.append(dict1)   
				
				new_list = combine(sale_item_list)
				sale_items.delete()
				
				for n in new_list:
					product = Product.objects.get(college=current_college,code=n['code'])
					
					SaleItem(
						college=current_college,
						sale=sale_instance,
						product = product,
						qty = n["qty"],
						cost = n["cost"],
						price = n["price"],
						sub_total = sub_total
					).save()
					
					update_stock(request,product,qty,"decrement")

				data = sale_form.save(commit=False)
				total=amount-float(discount)
				data.sub_total_amount = amount
				data.total=total
				data.balance = data.total
				data.updater = request.user
				data.date_updated = datetime.datetime.now()
				data.save()
				
				response_data['status'] = 'true'     
				response_data['title'] = "Successfully Updated"       
				response_data['redirect'] = 'true' 
				response_data['redirect_url'] = reverse('store_product:sale', kwargs = {'pk' : data.pk})
				response_data['message'] = "Sale Successfully Updated."
			else:
				sale_items = SaleItem.objects.filter(sale=sale_instance)
				for item in sale_items:
					qty = item.qty
					product = item.product
					update_stock(request,product,qty,"decrement")
					
				response_data['status'] = 'false'     
				response_data['title'] = "Out of Stock"
				response_data['stable'] = "true"       
				response_data['message'] = error_messages
		else:
			print("sale form")
			print(sale_form.errors)
			print("saleitem form")
			print(sale_item_formset.errors)
			response_data['status'] = 'false'
			response_data['stable'] = 'true'
			response_data['title'] = "Form validation error"        
			message = ''         
			message1 = generate_form_errors(sale_form,formset=False)
			message2 = generate_form_errors(sale_item_formset,formset=True)  
			message = str(message1) + str(message2)            
			response_data['message'] = sale_item_formset.errors
			
		return HttpResponse(json.dumps(response_data), content_type='application/javascript')

	else: 
		sale_form = SaleForm(instance=sale_instance)
		
		
		sale_item_formset = SaleItemFormset(prefix='sale_item_formset',instance=sale_instance)
		
		for form in sale_item_formset:
			form.fields['product'].queryset = Product.objects.filter(college=current_college)
		  
		context = {
			"form" : sale_form,
			'sale_item_formset' : sale_item_formset,
			"title" : "Edit Sale : " + sale_instance.sale_id,
			"instance" : sale_instance,
			"is_edit" : True,

			"is_need_select_picker" : True,
			"is_need_popup_box" : True,
			"is_need_custom_scroll_bar" : True,
			"is_need_wave_effect" : True,
			"is_need_bootstrap_growl" : True,
			"is_need_grid_system" : True,
			"is_need_datetime_picker" : True, 
			"is_need_formset" : True,
		}
		return render(request, 'store/create_sale.html', context)
	
		
@check_mode
@login_required
@college_required
@permissions_required(['can_view_sale'])
def sale(request,pk):
	current_college = get_current_college(request)
	instance = get_object_or_404(Sale.objects.filter(pk=pk,college=current_college))
	sale_items = SaleItem.objects.filter(sale=instance,is_deleted=False)
	
	context = {
		"instance" : instance,
		"sale_items" : sale_items,
		"title" : "Sale : " + instance.sale_id,
		"single_page" : True,

		"is_need_select_picker" : True,
		"is_need_popup_box" : True,
		"is_need_custom_scroll_bar" : True,
		"is_need_wave_effect" : True,
		"is_need_bootstrap_growl" : True,
		"is_need_grid_system" : True,
		"is_need_datetime_picker" : True,
	}
	return render(request,'store/sale.html',context)


@check_mode
@ajax_required
@login_required
@college_required
@permissions_required(['can_delete_sale'])
def delete_sale(request,pk):
	current_college = get_current_college(request)
	instance = get_object_or_404(Sale.objects.filter(pk=pk,college=current_college))

	transactions = Transaction.objects.filter(is_deleted=False,college=current_college,sale=instance)
	for transaction in transactions:
		if transaction.transaction_mode == "cash":
			cash_account = transaction.cash_account
			if cash_account:
				balance = cash_account.balance
				balance = balance -transaction.amount
				CashAccount.objects.filter(pk=transaction.cash_account.pk,college=current_college).update(balance=balance)
		elif transaction.transaction_mode == "bank":
			bank_account = transaction.bank_account
			if bank_account:
				balance = bank_account.balance
				balance = balance -transaction.amount
				BankAccount.objects.filter(pk=transaction.bank_account.pk,college=current_college).update(balance=balance)
		advance = instance.advance
		if advance>0:
			latest_sale = Sale.objects.filter(college=current_college,is_deleted=False,student=instance.student).latest('date_added')
			latest_paid = latest_sale.paid
			latest_balance = latest_sale.balance
			sale_amount = latest_paid + latest_balance
			latest_amount = latest_sale.total
			if (latest_balance < latest_amount) & (sale_amount == latest_amount):
				latest_sale.balance = latest_balance + latest_paid
				latest_sale.paid =latest_paid - advance
				latest_sale.save()
		Transaction.objects.filter(sale=transaction.sale,college=current_college).update(is_deleted=True)


	Sale.objects.filter(pk=pk,college=current_college).update(is_deleted=True, sale_id =instance.sale_id  + "_deleted")
	SaleItem.objects.filter(sale=instance).update(is_deleted=True)
	
	response_data = {
		"status" : "true",
		"title" : "Successfully Deleted",
		"message" : "Sale Successfully Deleted.",
		"redirect" : "true",
		"redirect_url" : reverse('store_product:sales')
	}
	return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@college_required
@permissions_required(['can_delete_sale'])
def delete_selected_sales(request):
	current_college = get_current_college(request)
	pks = request.GET.get('pk')
	if pks:
		pks = pks[:-1]   
		pks = pks.split(',')
		for pk in pks:     
			instance = get_object_or_404(Sale.objects.filter(pk=pk,is_deleted=False,college=current_college)) 
			transactions = Transaction.objects.filter(is_deleted=False,college=current_college,sale=instance)
			for transaction in transactions:
				if transaction.transaction_mode == "cash":
					cash_account = transaction.cash_account
					if cash_account:
						balance = cash_account.balance
						balance = balance -transaction.amount
						CashAccount.objects.filter(pk=transaction.cash_account.pk,college=current_college).update(balance=balance)
				elif transaction.transaction_mode == "bank":
					bank_account = transaction.bank_account
					if bank_account:
						balance = bank_account.balance
						balance = balance -transaction.amount
						BankAccount.objects.filter(pk=transaction.bank_account.pk,college=current_college).update(balance=balance)
				advance = instance.advance
				if advance>0:
					latest_sale = Sale.objects.filter(college=current_college,is_deleted=False,student=instance.student).latest('date_added')
					latest_paid = latest_sale.paid
					latest_balance = latest_sale.balance
					sale_amount = latest_paid + latest_balance
					latest_amount = latest_sale.amount
					if (latest_balance < latest_amount) & (sale_amount == latest_amount):
						latest_sale.balance = latest_balance + latest_paid
						latest_sale.paid =latest_paid - advance
						latest_sale.save()
				Transaction.objects.filter(sale=transaction.sale,college=current_college).update(is_deleted=True)

			Sale.objects.filter(pk=pk,college=current_college).update(is_deleted=True)
			SaleItem.objects.filter(sale=instance,college=current_college).update(is_deleted=True)

		response_data = {
			"status" : "true",
			"title" : "Successfully Deleted",
			"message" : "Selected Sale(s) Successfully Deleted.",
			"redirect" : "true",
			"redirect_url" : reverse('store_product:sales')
		}
	else:
		response_data = {
			"status" : "false",
			"title" : "Nothing selected",
			"message" : "Please select sales first.",
		}
		
	return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required 
def get_sale(request):
	pk = request.GET.get('id')
	current_college = get_current_college(request)
	instance = Student.objects.get(pk=pk,college=current_college,is_deleted=False)
	sale_payments = Transaction.objects.filter(student=instance,college=current_college,is_deleted=False,transaction_category__name="sale_payment")

	payments = 0
	if sale_payments:
		payments = sale_payments.aggregate(amount=Sum('amount')).get("amount",0)

	sales = Sale.objects.filter(student=instance,college=current_college,is_deleted=False)

	sale_amounts = 0
	if sales:
		sale_amounts = sales.aggregate(total=Sum('total')).get("total",0)

	balance = sale_amounts - payments
	if instance:
		response_data = {
			"status" : "true", 
			'balance' : str(balance), 
		}
	else:
		response_data = {
			"status" : "false",
			"message" : "Balance Error"
		}
	return HttpResponse(json.dumps(response_data), content_type='application/javascript')

