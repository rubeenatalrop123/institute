from django.conf.urls import  url,include
from users import views
from main import views as general_views


urlpatterns = [
    url(r'^$', views.dashboard, name='dashboard'),
    url(r'^view/(?P<pk>.*)/$', views.view, name='view'),
    
    url(r'^check-notification/$', views.check_notification, name='check_notification'),
    url(r'^notifications/$', views.notifications, name='notifications'),
    url(r'^notification/delete/(?P<pk>.*)/$', views.delete_notification, name='delete_notification'),
    url(r'^delete-selected-notifications/$', views.delete_selected_notifications, name='delete_selected_notifications'),
    url(r'^notification/read/(?P<pk>.*)/$', views.read_notification, name='read_notification'),
    url(r'^read-selected-notifications/$', views.read_selected_notifications, name='read_selected_notifications'),
    
    url(r'^set-user-timezone/$', views.set_user_timezone, name='set_user_timezone'),
    
    url(r'^settings/$', views.settings, name='settings'),
    url(r'^settings/update-college/$', views.update_college, name='update_college'),
    url(r'^settings/update-profile/$', views.update_profile, name='update_profile'),

    url(r'^users/$', views.users, name='users'), 
]