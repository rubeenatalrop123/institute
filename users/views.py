from django.http.response import HttpResponse
import json
from django.urls import reverse
from django.http.response import HttpResponseRedirect, HttpResponse
import json
from users.forms import UserForm
from users.models import Notification, Permission
from django.views.decorators.http import require_GET, require_POST
from django.contrib.auth.decorators import login_required
from main.decorators import ajax_required,check_mode,college_required,permissions_required
from django.shortcuts import resolve_url, render, get_object_or_404
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.tokens import default_token_generator
from django.template.response import TemplateResponse
from django.forms.models import modelformset_factory
from django.contrib.auth.models import User, Group
from django.core.mail import send_mail
from users.models import Notification
from main.models import College
from users.functions import get_current_college,get_current_batch
from main.forms import CollegeForm
from users.models import Profile
from academics.models import FeeCategory
from finance.models import TransactionCategory,AccountGroup,AccountHead
from users.forms import ProfileForm,UserForm
from main.functions import get_auto_id,get_a_id,generate_form_errors
from django.contrib.auth.forms import PasswordChangeForm
from django.db.models import Q
import uuid


@check_mode
@login_required
def dashboard(request):
    return HttpResponseRedirect(reverse('app'))


@check_mode
@login_required
def view(request,pk):
    instance = get_object_or_404(User.objects.filter(username=pk))    
    context = {
        "title" : "User : " + instance.username,
        "instance" : instance,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_js_moment" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request, 'users/user.html', context)


@check_mode
@login_required
@college_required
@permissions_required(['can_view_user'])
def users(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    instances = User.objects.filter(is_active=True,is_superuser=False)
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(username__icontains=query)) 
    context = {
        "title" : "Users",
        "instances" : instances, 

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True,
    }
    return render(request, 'users/users.html', context)  


@check_mode
@login_required
@ajax_required
@require_GET
def check_notification(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    user = request.user
    count = Notification.objects.filter(user=user,is_read=False,is_active=True,college=current_college,batch=current_batch).count()
    return HttpResponse(json.dumps(count), content_type='application/javascript')


@check_mode
@login_required
def notifications(request):
    title = "Notifications"
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    instances = Notification.objects.filter(user=request.user,is_deleted=False,is_active=True,college=current_college,batch=current_batch)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(subject__name__icontains=query) | Q(product__name__icontains=query))
        title = "Notifications - %s" %query

    context = {
        'title' : title,
        "instances" : instances,
        "is_need_select_picker": True,
        "is_need_popup_box" : True,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request,"users/notifications.html",context)


@check_mode
@login_required
def delete_notification(request,pk):
    Notification.objects.filter(pk=pk,user=request.user).update(is_deleted=True,is_read=True)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Notification Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('users:notifications')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def delete_selected_notifications(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            Notification.objects.filter(pk=pk,user=request.user).update(is_deleted=True,is_read=True)

        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Notification(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('users:notifications')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def read_selected_notifications(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            Notification.objects.filter(pk=pk,user=request.user).update(is_deleted=True,is_read=True)

        response_data = {
            "status" : "true",
            "title" : "Successfully marked as read",
            "message" : "Selected notification(s) successfully marked as read.",
            "redirect" : "true",
            "redirect_url" : reverse('users:notifications')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def read_notification(request,pk):
    Notification.objects.filter(pk=pk,user=request.user).update(is_read=True)

    response_data = {
        "status" : "true",
        "title" : "Successfully marked as read",
        "message" : "Notification successfully marked as read.",
        "redirect" : "true",
        "redirect_url" : reverse('users:notifications')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@login_required
@ajax_required
@require_GET
def set_user_timezone(request):
    timezone = request.GET.get('timezone')
    request.session["set_user_timezone"] = timezone
    response_data = {}
    response_data['status'] = 'true'
    response_data['title'] = "Success"
    response_data['message'] = 'user timezone set successfully.'
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def change_password(request,pk):
    instance = get_object_or_404(User.objects.filter(pk=pk,is_active=True))
    if request.method == "POST":
        response_data = {}
        form = PasswordChangeForm(user=instance, data=request.POST)
        if form.is_valid():
            form.save()

            response_data = {
                'status' : 'true',
                'title' : "Successfully Changed",
                'redirect' : 'false',
                'message' : "Password Successfully Changed."
            }
        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : "Form validation error",
                "message" : message,
            }
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        title = "Change Password"
        change_password_form = PasswordChangeForm(user=instance)
        context = {
            "change_password_form" : change_password_form,
            "title" : title,
            "instance" : instance,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request, 'users/change_password.html', context)


@ajax_required
@login_required
def revoke_access(request,pk):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    instance = get_object_or_404(Staff.objects.filter(pk=pk,is_deleted=False,college=current_college,batch=current_batch))

    User.objects.filter(pk=instance.user.pk).update(is_active=False,
                                                    username=instance.user.username + "_deleted"+ str(instance.user.id) ,
                                                    email=instance.user.email + "_deleted")
    #unset user
    Staff.objects.filter(pk=pk).update(user=None)

    instance.permissions.clear()

    response_data = {
        "status" : "true",
        "title" : "Access Revoked.",
        "message" : "Access Revoked Successfully.",
        "redirect" : "true",
        "redirect_url" : reverse('staffs:staff', kwargs = {'pk' : pk})
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')



@check_mode
@login_required
def settings(request):
    current_college = get_current_college(request)
    college_form = CollegeForm(instance=current_college)
    profile,create = Profile.objects.get_or_create(user=request.user)
    profile_form = ProfileForm(instance=profile)
    
    active_menu = "college"
    q = request.GET.get('active')
    if q:
        active_menu = q
        
    context = {
        "title" : "Settings",
        "college_form" : college_form,
        "active_menu" : active_menu,
        "profile_form" : profile_form,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_js_moment" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,"users/settings.html",context)


@require_POST
@check_mode
@login_required
def update_college(request):
    current_college = get_current_college(request)
    form = CollegeForm(request.POST,request.FILES,instance=current_college)
    
    if form.is_valid(): 
        name = form.cleaned_data['name']
        name = name.upper()

        #update college
        data = form.save(commit=False)
        data.name = name
        auto_id = get_auto_id(College)
        data.creator = request.user
        data.updater = request.user
        data.auto_id = auto_id
        data.save()
        
        if not FeeCategory.objects.filter(name='admission_fee',college=data).exists():
            fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,name="admission_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

        if not FeeCategory.objects.filter(name='diary_fee',college=data).exists():
            fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,name="diary_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

        if not FeeCategory.objects.filter(name='monthly_fee',college=data).exists():
            fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,fee_type="monthly",name="monthly_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

        if not FeeCategory.objects.filter(name='computer_fee',college=data).exists():
            fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,name="computer_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

        if not FeeCategory.objects.filter(name='library_fee',college=data).exists():
            fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,name="library_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

        if not FeeCategory.objects.filter(name='transportation_fee',college=data).exists():
            fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,name="transportation_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

        if not FeeCategory.objects.filter(name='special_fee',college=data).exists():
            fee_category = FeeCategory.objects.create(is_general=True,is_deleted=False,college=data,name="special_fee",auto_id=get_auto_id(FeeCategory),a_id=get_a_id(FeeCategory,request),creator=request.user,updater=request.user)

        if not TransactionCategory.objects.filter(name='admission_fee',college=data).exists():
            transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="admission_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

        if not TransactionCategory.objects.filter(name='diary_fee',college=data).exists():
            transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="diary_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

        if not TransactionCategory.objects.filter(name='monthly_fee',college=data).exists():
            transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="monthly_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

        if not TransactionCategory.objects.filter(name='computer_fee',college=data).exists():
            transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="computer_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

        if not TransactionCategory.objects.filter(name='library_fee',college=data).exists():
            transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="library_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

        if not TransactionCategory.objects.filter(name='transportation_fee',college=data).exists():
            transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="transportation_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

        if not TransactionCategory.objects.filter(name='special_fee',college=data).exists():
            transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="special_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

        if not TransactionCategory.objects.filter(name='special_fee',college=data).exists():
                transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="income",college=data,name="special_fee",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

        if not TransactionCategory.objects.filter(name='staff_salary',college=data).exists():
            transaction_category = TransactionCategory.objects.create(is_system_generated=True,is_deleted=False,category_type="expense",college=data,name="staff_salary",auto_id=get_auto_id(TransactionCategory),a_id=get_a_id(TransactionCategory,request),creator=request.user,updater=request.user)

        if not AccountGroup.objects.filter(name='bank_account',college=data).exists():            
            AccountGroup.objects.create(
                is_system_generated=True,
                is_deleted=False,
                category_type="asset",
                name="bank_account",
                auto_id=get_auto_id(AccountGroup),
                a_id=get_a_id(AccountGroup,request),
                college=data,
                creator=request.user,
                updater=request.user
            )

        if not AccountGroup.objects.filter(name='capital_account',college=data).exists():
            AccountGroup.objects.create(
                is_system_generated=True,
                is_deleted=False,
                category_type="liability",
                name="capital_account",
                auto_id=get_auto_id(AccountGroup),
                a_id=get_a_id(AccountGroup,request),
                college=data,
                creator=request.user,
                updater=request.user
            )

        if not AccountGroup.objects.filter(name='cash_in_hand',college=data).exists():
            AccountGroup.objects.create(
                is_system_generated=True,
                is_deleted=False,
                category_type="asset",
                name="cash_in_hand",
                auto_id=get_auto_id(AccountGroup),
                a_id=get_a_id(AccountGroup,request),
                college=data,
                creator=request.user,
                updater=request.user
            )

        if not AccountGroup.objects.filter(name='current_asset',college=data).exists():
            AccountGroup.objects.create(
                is_system_generated=True,
                is_deleted=False,
                category_type="asset",
                name="current_asset",
                auto_id=get_auto_id(AccountGroup),
                a_id=get_a_id(AccountGroup,request),
                college=data,
                creator=request.user,
                updater=request.user
            )

        if not AccountGroup.objects.filter(name='current_liability',college=data).exists():
            AccountGroup.objects.create(
                is_system_generated=True,
                is_deleted=False,
                category_type="liability",
                name="current_liability",
                auto_id=get_auto_id(AccountGroup),
                a_id=get_a_id(AccountGroup,request),
                college=data,
                creator=request.user,
                updater=request.user
            )

        if not AccountGroup.objects.filter(name='direct_expense',college=data,is_system_generated=True,is_deleted=False).exists():
            AccountGroup.objects.create(
                is_system_generated=True,
                is_deleted=False,
                category_type="expense",
                name="direct_expense",
                auto_id=get_auto_id(AccountGroup),
                a_id=get_a_id(AccountGroup,request),
                college=data,
                creator=request.user,
                updater=request.user
            )
       
        if not AccountGroup.objects.filter(name='direct_income',college=data).exists():
            AccountGroup.objects.create(
                is_system_generated=True,
                is_deleted=False,
                category_type="income",
                name="direct_income",
                auto_id=get_auto_id(AccountGroup),
                a_id=get_a_id(AccountGroup,request),
                college=data,
                creator=request.user,
                updater=request.user
            )

        if not AccountGroup.objects.filter(name='fees',college=data,is_system_generated=True,is_deleted=False).exists():
            fee_group = AccountGroup.objects.create(
                            is_system_generated=True,
                            is_deleted=False,
                            category_type="income",
                            name="fees",
                            auto_id=get_auto_id(AccountGroup),
                            a_id=get_a_id(AccountGroup,request),
                            college=data,
                            creator=request.user,
                            updater=request.user
                        )
        else:
            fee_group = AccountGroup.objects.filter(name='fees',college=data,is_system_generated=True,is_deleted=False)[0]

        if not AccountGroup.objects.filter(name='indirect_expense',college=data).exists():
            AccountGroup.objects.create(
                is_system_generated=True,
                is_deleted=False,
                category_type="expense",
                name="indirect_expense",
                auto_id=get_auto_id(AccountGroup),
                a_id=get_a_id(AccountGroup,request),
                college=data,
                creator=request.user,
                updater=request.user
            )

        if not AccountGroup.objects.filter(name='indirect_income',college=data).exists():
            AccountGroup.objects.create(
                is_system_generated=True,
                is_deleted=False,
                category_type="income",
                name="indirect_income",
                auto_id=get_auto_id(AccountGroup),
                a_id=get_a_id(AccountGroup,request),
                college=data,
                creator=request.user,
                updater=request.user
            )

        if not AccountGroup.objects.filter(name='loans_and_liability',college=data).exists():
            AccountGroup.objects.create(
                is_system_generated=True,
                is_deleted=False,
                category_type="liability",
                name="loans_and_liability",
                auto_id=get_auto_id(AccountGroup),
                a_id=get_a_id(AccountGroup,request),
                college=data,
                creator=request.user,
                updater=request.user
            )

        if not AccountGroup.objects.filter(name='fixed_asset',college=data).exists():
            AccountGroup.objects.create(
                is_system_generated=True,
                is_deleted=False,
                category_type="asset",
                name="fixed_asset",
                auto_id=get_auto_id(AccountGroup),
                a_id=get_a_id(AccountGroup,request),
                college=data,
                creator=request.user,
                updater=request.user
            )

        if not AccountGroup.objects.filter(name='salary',college=data,is_system_generated=True,is_deleted=False).exists():
            salary_group = AccountGroup.objects.create(
                            is_system_generated=True,
                            is_deleted=False,
                            category_type="expense",
                            name="salary",
                            auto_id=get_auto_id(AccountGroup),
                            a_id=get_a_id(AccountGroup,request),
                            college=data,
                            creator=request.user,
                            updater=request.user
                        )
        else:
            salary_group = AccountGroup.objects.filter(name='salary',college=data,is_system_generated=True,is_deleted=False)[0]

        if not AccountHead.objects.filter(name='admission_fee',college=data).exists():
            AccountHead.objects.create(
                is_system_generated=True,
                account_group = fee_group,
                is_deleted=False,
                name="admission_fee",
                auto_id=get_auto_id(AccountHead),
                a_id=get_a_id(AccountHead,request),
                college=data,
                creator=request.user,
                updater=request.user
            )
            
        if not AccountHead.objects.filter(name='transportation_fee',college=data).exists():
            AccountHead.objects.create(
                is_system_generated=True,
                account_group = fee_group,
                is_deleted=False,
                name="transportation_fee",
                auto_id=get_auto_id(AccountHead),
                a_id=get_a_id(AccountHead,request),
                college=data,
                creator=request.user,
                updater=request.user
            )

        if not AccountHead.objects.filter(name='computer_fee',college=data).exists():
            AccountHead.objects.create(
                is_system_generated=True,
                account_group = fee_group,
                is_deleted=False,
                name="computer_fee",
                auto_id=get_auto_id(AccountHead),
                a_id=get_a_id(AccountHead,request),
                college=data,
                creator=request.user,
                updater=request.user
            )
        if not AccountHead.objects.filter(name='special_fee',college=data).exists():
            AccountHead.objects.create(
                is_system_generated=True,
                account_group = fee_group,
                is_deleted=False,
                name="special_fee",
                auto_id=get_auto_id(AccountHead),
                a_id=get_a_id(AccountHead,request),
                college=data,
                creator=request.user,
                updater=request.user
            )
        if not AccountHead.objects.filter(name='exam_fee',college=data).exists():
            AccountHead.objects.create(
                is_system_generated=True,
                account_group = fee_group,
                is_deleted=False,
                name="exam_fee",
                auto_id=get_auto_id(AccountHead),
                a_id=get_a_id(AccountHead,request),
                college=data,
                creator=request.user,
                updater=request.user
            )
        if not AccountHead.objects.filter(name='monthly_fee',college=data).exists():
            AccountHead.objects.create(
                is_system_generated=True,
                account_group = fee_group,
                is_deleted=False,
                name="monthly_fee",
                auto_id=get_auto_id(AccountHead),
                a_id=get_a_id(AccountHead,request),
                college=data,
                creator=request.user,
                updater=request.user
            )
        if not AccountHead.objects.filter(name='library_fee',college=data).exists():
            AccountHead.objects.create(
                is_system_generated=True,
                account_group = fee_group,
                is_deleted=False,
                name="library_fee",
                auto_id=get_auto_id(AccountHead),
                a_id=get_a_id(AccountHead,request),
                college=data,
                creator=request.user,
                updater=request.user
            )
        if not AccountHead.objects.filter(name='pta_fee',college=data).exists():
            AccountHead.objects.create(
                is_system_generated=True,
                account_group = fee_group,
                is_deleted=False,
                name="pta_fee",
                auto_id=get_auto_id(AccountHead),
                a_id=get_a_id(AccountHead,request),
                college=data,
                creator=request.user,
                updater=request.user
            )
        if not AccountHead.objects.filter(name='salary',college=data).exists():
            AccountHead.objects.create(
                is_system_generated=True,
                account_group = salary_group,
                is_deleted=False,
                name="salary",
                auto_id=get_auto_id(AccountHead),
                a_id=get_a_id(AccountHead,request),
                college=data,
                creator=request.user,
                updater=request.user
            )


        return HttpResponseRedirect(reverse("users:settings"))
    
    else:    
        college_form = CollegeForm(instance=current_college)
        profile_form = ProfileForm(instance=request.user.profile)
        
        active_menu = "college"
        q = request.GET.get('active')
        if q:
            active_menu = q
                   
        context = {
            "title" : "Settings",
            "college_form" : college_form,
            "active_menu" : active_menu,
            "profile_form" : profile_form,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_js_moment" : True,
            "is_need_grid_system" : True,
            "is_need_animations" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,"users/settings.html",context)


@ajax_required 
@require_POST
@check_mode
@login_required
def update_profile(request):
    user = request.user

    instance = None
    if Profile.objects.filter(user=user).exists():
        instance = Profile.objects.get(user=user)
    if instance:
        form = ProfileForm(request.POST,instance=instance)
        
        if form.is_valid(): 
            
            #update profile
            
            form.save()
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Profile successfully updated.",
            }   
        
        else:            
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
    else:
         response_data = {
            "status" : "false",
            "stable" : "true",
            "title" : "Profile Not Found",
            "message" : "Profile Not Found"
        } 
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')
