from registration.forms import RegistrationForm
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from users.models import Profile
from django.forms.widgets import TextInput



class ProfileForm(forms.ModelForm):

    class Meta:
        model = Profile
        exclude = ['user', 'invite_code']
        widgets = {
            'first_name': TextInput(attrs={'class': 'required form-control','placeholder' : 'First Name'}),
            'last_name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Last Name'}),
            'address': TextInput(attrs={'class': 'required form-control','placeholder' : 'Address'}),
            'city': TextInput(attrs={'class': 'required form-control','placeholder' : 'City'}),
            'state': TextInput(attrs={'class': 'required form-control','placeholder' : 'State'}),
            'country': TextInput(attrs={'class': 'required form-control','placeholder' : 'Country'}),
            'pin': TextInput(attrs={'class': 'required form-control','placeholder' : 'PIN/ZIP'}),
            'phone': TextInput(attrs={'class': 'required form-control','placeholder' : 'Phone'}),
        }
        error_messages = {
            'first_name' : {
                'required' : _("First name field is required."),
            },
            'last_name' : {
                'required' : _("Last name field is required."),
            },
            'address' : {
                'required' : _("Address field is required."),
            },
            'city' : {
                'required' : _("City field is required."),
            },
            'state' : {
                'required' : _("State field is required."),
            },
            'country' : {
                'required' : _("Country field is required."),
            },
            'pin' : {
                'required' : _("Pin field is required."),
            },
            'phone' : {
                'required' : _("Phone field is required."),
            }
        }
        labels = {
            "pin" : "Pin/Zip"
        }



class UserForm(RegistrationForm):
    
    username = forms.CharField(label=_("Username"), 
                               max_length=254,
                               widget=forms.TextInput(
                                    attrs={'placeholder': 'Enter username','class':'required form-control'})
                               )
    email = forms.EmailField(label=_("Email"), 
                             max_length=254,
                             widget=forms.TextInput(
                                attrs={'placeholder': 'Enter email','class':'required form-control'})
                             )
    password1 = forms.CharField(label=_("Password"), 
                               widget=forms.PasswordInput(
                                    attrs={'placeholder': 'Enter password','class':'required form-control'})
                               )
    password2 = forms.CharField(label=_("Repeat Password"), 
                               widget=forms.PasswordInput(
                                    attrs={'placeholder': 'Enter password again','class':'required form-control'})
                               )
    
    bad_domains = ['guerrillamail.com']
    
    def clean_email(self):
        email_domain = self.cleaned_data['email'].split('@')[1]
        if User.objects.filter(email__iexact=self.cleaned_data['email'],is_active=True):
            raise forms.ValidationError(_("This email address is already in use."))        
        elif email_domain in self.bad_domains:
            raise forms.ValidationError(_("Registration using %s email addresses is not allowed. Please supply a different email address." %email_domain))
        return self.cleaned_data['email']
    
    min_password_length = 6
    
    def clean_password1(self):
        password1 = self.cleaned_data.get('password1', '')
        if len(password1) < self.min_password_length:
            raise forms.ValidationError("Password must have at least %i characters" % self.min_password_length)
        else:
            return password1
    
    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2
        
    min_username_length = 6

    def clean_username(self):
        username = self.cleaned_data['username']
        existing = User.objects.filter(username__iexact=self.cleaned_data['username'])
        if existing.exists():
            raise forms.ValidationError(_("A user with that username already exists."))
        elif len(username) < self.min_username_length:
            raise forms.ValidationError("Username must have at least %i characters" % self.min_password_length)
        else:
            return self.cleaned_data['username']  


class RegForm(RegistrationForm):

    bad_domains = ['guerrillamail.com']

    def clean_email(self):
        email_domain = self.cleaned_data['email'].split('@')[1]
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_("This email address is already in use."))
        elif email_domain in self.bad_domains:
            raise forms.ValidationError(_("Registration using %s email addresses is not allowed. Please supply a different email address." %email_domain))
        return self.cleaned_data['email']

    min_password_length = 6

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1', '')
        if len(password1) < self.min_password_length:
            raise forms.ValidationError("Password must have at least %i characters" % self.min_password_length)
        else:
            return password1

    min_username_length = 5

    def clean_username(self):
        username = self.cleaned_data['username']
        existing = User.objects.filter(username__iexact=self.cleaned_data['username'])
        if existing.exists():
            raise forms.ValidationError(_("A user with that username already exists."))
        elif len(username) < self.min_username_length:
            raise forms.ValidationError("Username must have at least %i characters" % self.min_password_length)
        else:
            return self.cleaned_data['username']


