from mailqueue.models import MailerMessage  
from django.conf import settings
from django.contrib.auth.models import User
from users.models import NotificationSubject, Notification
from main.models import College,CollegeAccess,Batch


def send_email(to_address,subject,content,html_content,bcc_address=settings.DEFAULT_BCC_EMAIL,attachment=None,attachment2=None,attachment3=None):
    new_message = MailerMessage()
    new_message.subject = subject
    new_message.to_address = to_address
    if bcc_address:
        new_message.bcc_address = bcc_address
    new_message.from_address = settings.DEFAULT_FROM_EMAIL
    new_message.content = content
    new_message.html_content = html_content
    if attachment:
        new_message.add_attachment(attachment)
    if attachment2:
        new_message.add_attachment(attachment2)
    if attachment3:
        new_message.add_attachment(attachment3)
    new_message.app = "default"
    new_message.save()
    
    
def get_name(user_id):
    name = User.objects.get(id=user_id).username
            
    return name


def get_email(user_id):
    email = User.objects.get(id=user_id).email            
    return email


def get_current_college(request):
    college = None
    if request.user.is_authenticated:
        if "current_college" in request.session:
            pk =  request.session['current_college']
            if College.objects.filter(pk=pk).exists():
                college = College.objects.get(pk=pk)
        elif CollegeAccess.objects.filter(user=request.user,is_default=True).exists():
            college = CollegeAccess.objects.get(user=request.user,is_default=True).college        
                    
    return college


def college_access(request):
    colleges = []    
    if request.user.is_authenticated:
        college_access = College.objects.filter(creator=request.user,is_deleted=False)
        for college in college_access:
            colleges.append(college) 
    return colleges
            

# def get_current_batch(request):
#     batch = None
#     if request.user.is_authenticated:
#         if "current_batch" in request.session:
#             pk = request.session['current_batch']
#             if Batch.objects.filter(pk=pk,is_deleted=False).exists():
#                 batch = Batch.objects.get(pk=pk,is_deleted=False)
#         elif Batch.objects.filter(is_active=True,is_deleted=False).exists():
#             batch = Batch.objects.get(is_active=True,is_deleted=False)      

#     return batch

def get_current_batch(request):

    current_batch = None
    if Batch.objects.filter(is_active=True,is_deleted=False).exists():
        current_batch = Batch.objects.get(is_active=True,is_deleted=False)

    return current_batch
