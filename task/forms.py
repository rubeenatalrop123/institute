from django import forms
from django.forms.widgets import TextInput, Select, Textarea, HiddenInput
from task.models import Task
from django.utils.translation import ugettext_lazy as _
from dal import autocomplete
from decimal import Decimal


class TaskForm(forms.ModelForm):

    class Meta:
        model = Task
        fields = ["task_name","assigned_date","due_date","department","sub_division","is_general","task_description","is_active"]
        widgets = {
            "task_name": TextInput(
                attrs = {
                    "class": "required form-control extra",
                    "placeholder" : "Task Name",
                }
            ),
            "task_description": Textarea(
                attrs = {
                    "class": "form-control extra size",
                    "placeholder" : "Task Description",
                }
            ),
            'assigned_date': TextInput(
                attrs = {
                'class': 'required form-control date-picker extra',
                'placeholder' : 'Assigned Date'
                }
            ),
            'due_date': TextInput(
                attrs = {
                'class': 'required form-control date-picker extra',
                'placeholder' : 'Due Date'
                }
            ),
            "department": Select(
                attrs = {
                    "class": "form-control extra selectordie selectpicker show-tick",
                    "placeholder" : "Class",
                }
            ),
            "sub_division": Select(
                attrs = {
                    "class": "form-control extra selectordie selectpicker show-tick",
                    "placeholder" : "Division",
                }
            )
        }
        error_messages = {
            "department" : {
                "required" : _("Student class field is required.")
            }
        }
        help_texts = {
        }

    def clean(self):
        cleaned_data = super(TaskForm, self).clean()
        return cleaned_data
