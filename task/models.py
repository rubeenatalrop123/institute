from django.db import models
from django.core.validators import MinValueValidator
from decimal import Decimal
from main.models import BaseModel
from django.utils.translation import ugettext_lazy as _
import uuid


class Task(BaseModel):
    task_name = models.CharField(max_length=128,blank=True,null=True)
    task_description = models.TextField(blank=True,null=True)
    sub_division = models.ForeignKey("academics.SubDivision",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    department = models.ForeignKey("academics.Department",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    batch = models.ForeignKey("main.Batch",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    assigned_date = models.DateTimeField(blank=True,null=True)
    due_date = models.DateTimeField(blank=True,null=True)
    is_active = models.BooleanField(default=True)
    is_completed = models.BooleanField(default=False)
    is_general = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'task'
        verbose_name = _('Task')
        verbose_name_plural = _('Tasks')

    def __str__(self):
        return str(self.task_name)
