from django.shortcuts import render
from main.decorators import check_mode, role_required
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from task.models import Task
from academics.models import Admission, SubDivision, Department
from task.forms import TaskForm
from users.models import Notification
from django.urls import reverse
import json
import datetime
from django.http.response import HttpResponse, HttpResponseRedirect
from main.functions import get_auto_id, generate_form_errors, get_current_role, generate_unique_id, get_current_batch, get_a_id
from users.functions import get_current_college
from django.db.models import Q


def get_divisions(request):
    pk = request.GET.get('id')
    if SubDivision.objects.filter(department=pk).exists():
        divisions = SubDivision.objects.filter(department=pk, is_deleted=False)

        div = []
        for division in divisions:
            data = {
                'id': str(division.pk),
                'name': division.name
            }
            div.append(data)

        response_data = {
            "status" : "true",
            "division" : div,
        }
    else:
        response_data = {
            "status" : "false",
        }
    return HttpResponse(json.dumps(response_data),content_type='application/javascript')


@check_mode
@login_required
def create_task(request):
    url = reverse('task:create_task')
    current_college = get_current_college(request)
    batch = get_current_batch()
    if request.method == "POST":

        form = TaskForm(request.POST)

        if form.is_valid():
            #create task
            department = form.cleaned_data['department']
            sub_division = form.cleaned_data['sub_division']
            is_general = form.cleaned_data['is_general']

            data = form.save(commit=False)
            auto_id = get_auto_id(Task)
            a_id = get_a_id(Task,request)
            data.auto_id = auto_id
            data.a_id = a_id
            data.batch = batch
            data.creator = request.user
            data.updater = request.user
            data.college = current_college
            data.save()

            # if sub_division:
            #     students = Admission.objects.filter(is_deleted=False,batch=batch,sub_division=sub_division)

            #     for student in students:
            #         TaskAssigned.objects.create(
            #                 task = data,
            #                 student = student,
            #             )

            #         Notification(
            #             user = request.user,
            #             student = student,
            #             description = data,

            #         ).save()

            # elif department:
            #     students = Admission.objects.filter(is_deleted=False,batch=batch,department=department)

            #     for student in students:
            #         TaskAssigned.objects.create(
            #                 task = data,
            #                 student = student,
            #             )

            #         Notification(
            #             user = request.user,
            #             student = student,
            #             description = data,
            #         ).save()

            # elif is_general:
            #     students = Admission.objects.filter(is_deleted=False,batch=batch)

            #     for student in students:
            #         TaskAssigned.objects.create(
            #             task = data,
            #             student = student,
            #         )

            #         Notification(
            #             user = request.user,
            #             student = student,
            #             description = data,
            #         ).save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Task successfully updated.",
                "redirect": "true",
                "redirect_url" : reverse('task:task',kwargs={'pk':data.pk})
            }
        else:
            message = generate_form_errors(form, formset=False)
            print(form.errors)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = TaskForm(request.POST)

        context = {
            "title" : "Create Task",
            "batch" : batch,
            "form" : form,
            "url" : url,
            "is_create" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'task/entry_task.html', context)


@check_mode
@login_required
def task(request,pk):
    instance = get_object_or_404(Task.objects.filter(pk=pk,is_deleted=False))

    context = {
        'instance' : instance,
        'title' : "Tasks",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,

    }
    return render(request, 'task/task.html', context)


@check_mode
@login_required
def tasks(request):
    instances = Task.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( (Q(task_name__icontains=query)) | (Q(assigned_date__icontains=query)) )
        title = "Tasks - %s" %query


    context = {
        'instances' : instances,
        'title' : "Tasks",
        'query' : "query",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,

    }
    return render(request, 'task/tasks.html', context)



@check_mode
@login_required
def edit_task(request,pk):
    instance = get_object_or_404(Task.objects.filter(pk=pk,is_deleted=False))
    batch = get_current_batch()
    url = reverse('task:edit_task',kwargs={'pk':pk})
    if request.method == "POST":

        form = TaskForm(request.POST,instance=instance)
        if form.is_valid():
            department = form.cleaned_data['department']
            sub_division = form.cleaned_data['sub_division']
            is_general = form.cleaned_data['is_general']

            data = form.save(commit=False)
            auto_id = get_auto_id(Task)
            data.auto_id = auto_id
            data.batch = batch
            data.creator = request.user
            data.updator = request.user
            data.save()

            # if sub_division:
            #     students = Admission.objects.filter(is_deleted=False,student_batch=batch,sub_division=sub_division)

            #     for student in students:
            #         TaskAssigned.objects.create(
            #                 task = data,
            #                 student = student,
            #             )

            #         Notification(
            #             user = request.user,
            #             student = student,
            #             description = data.task_name,

            #         ).save()

            # elif department:
            #     students = Admission.objects.filter(is_deleted=False,student_batch=batch,department=department)

            #     for student in students:
            #         TaskAssigned.objects.create(
            #                 task = data,
            #                 student = student,
            #             )

            #         Notification(
            #             user = request.user,
            #             student = student,
            #             description = data.task_name,
            #         ).save()

            # elif is_general:
            #     students = Admission.objects.filter(is_deleted=False,student_batch=batch)

            #     for student in students:
            #         TaskAssigned.objects.create(
            #                 task = data,
            #                 student = student,
            #                     )

            #         Notification(
            #             user = request.user,
            #             student = student,
            #             description = data.task_name,
            #         ).save()

            response_data = {
                "status": "true",
                "title": "Successfully Edited",
                "message": "Task successfully Edited.",
                "redirect": "true",
                "redirect_url": reverse('task:task', kwargs={"pk" : data.pk})
            }
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = TaskForm(instance=instance)

        context = {
            "title" : "Create Task",
            "batch" : batch,
            "form" : form,
            "url" : url,
            "is_create" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'task/entry_task.html', context)


@check_mode
@login_required
def delete_task(request,pk):
    Task.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Task successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('task:tasks')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')
