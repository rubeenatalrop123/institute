from django.conf.urls import url
from task import views


app_name="task"

urlpatterns = [
    url(r'^tasks/$', views.tasks, name='tasks'),
    url(r'^create/task/$', views.create_task, name='create_task'),
    url(r'^view/task/(?P<pk>.*)/$', views.task, name='task'),
    # url(r'^task/(?P<pk>.*)/$', views.task, name='task'),
    url(r'^edit/task/(?P<pk>.*)/$', views.edit_task, name='edit_task'),
    url(r'^delete/task/(?P<pk>.*)/$', views.delete_task, name='delete_task'),

    url(r'^get-divisions/$',views.get_divisions,name='get_divisions'),

]
