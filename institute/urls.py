from django.conf.urls import url, include
from django.contrib import admin
from main import views as general_views
from django.views.static import serve
from django.conf import settings
from registration.backends.default.views import RegistrationView
from django.views.generic import TemplateView


urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^$', general_views.app,name='app'),

    url(r'^app/$',general_views.app,name='app'),
    url(r'^app/dashboard/$',general_views.dashboard,name='dashboard'),
    url(r'^app/create-college/$',general_views.create_college,name='create_college'),
    url(r'^app/switch-college/$', general_views.switch_college, name='switch_college'),
    url(r'^app/switch-theme/$', general_views.switch_theme, name='switch_theme'),
    url(r'^app/switch-batch/$', general_views.switch_batch, name='switch_batch'),

    url(r'^app/accounts/', include('registration.backends.default.urls')),
    url(r'^users/', include(('users.urls', 'users'), namespace='users')),
    url(r'^academics/', include(('academics.urls', 'academics'), namespace='academics')),
    url(r'^transports/', include(('transports.urls', 'transports'), namespace='transports')),
    url(r'^main/', include(('main.urls', 'main'), namespace='main')),
    url(r'^staffs/', include(('staffs.urls', 'staffs'), namespace='staffs')),
    url(r'^finance/', include(('finance.urls', 'finance'), namespace='finance')),
    url(r'^scholarships/', include(('scholarships.urls', 'scholarships'), namespace='scholarships')),
    url(r'^task/', include(('task.urls', 'task'), namespace='task')),

    url(r'^exams/', include(('exams.urls', 'exams'), namespace='exams')),
    url(r'^enquiries/', include(('enquiries.urls', 'enquiries'), namespace='enquiries')),
    url(r'^store/product/', include(('store.product.urls', 'store_product'), namespace='store_product')),
    url(r'^pta/', include(('pta.urls', 'pta'), namespace='pta')),
    url(r'^notifications/', include(('notifications.urls', 'notifications'), namespace='notifications')),

    url(r'^media/(?P<path>.*)$', serve, { 'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve, { 'document_root': settings.STATIC_FILE_ROOT}),
]
