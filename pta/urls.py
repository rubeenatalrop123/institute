from django.conf.urls import url
from pta import views
from academics.views import DivisionAutocomplete, ClassAutocomplete
from dal import autocomplete


urlpatterns = [
    url(r'^class-autocomplete/$', ClassAutocomplete.as_view(),name='class_autocomplete'),
	url(r'^division-autocomplete/$',DivisionAutocomplete.as_view(),name='division_autocomplete'),

    url(r'^get-divisions/$', views.get_divisions, name='get_divisions'),
    url(r'^get-semesters/$', views.get_semesters, name='get_semesters'),

    url(r'^ptas/$', views.ptas, name='ptas'),
    url(r'^create/pta/$', views.create_pta, name='create_pta'),
    url(r'^pta/(?P<pk>.*)/$', views.pta, name='pta'),
    url(r'^edit/pta/(?P<pk>.*)/$', views.edit_pta, name='edit_pta'),
    url(r'^delete/pta/(?P<pk>.*)/$', views.delete_pta, name='delete_pta'),

    url(r'^create/pta-attendance/(?P<pk>.*)/$', views.create_pta_attendance, name='create_pta_attendance'),
    url(r'^edit/pta-attendance/(?P<pk>.*)/$', views.edit_pta_attendance, name='edit_pta_attendance'),
    url(r'^pta-attendances/(?P<pk>.*)/$', views.pta_attendances, name='pta_attendances'),
    url(r'^view/pta-attendance/(?P<pk>.*)/$', views.pta_attendance, name='pta_attendance'),
    url(r'^delete/delete-pta-attendance/(?P<pk>.*)/$', views.delete_pta_attendance, name='delete_pta_attendance'),
    url(r'^show-calendar-pta-attendances/(?P<pk>.*)/$', views.show_calendar_pta_attendances, name='show_calendar_pta_attendances'),
]
