from __future__ import unicode_literals
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from pta.models import Pta, PtaAttendance
from pta.forms import PTAForm, PTAForm, PtaAttendanceMaleForm, PtaAttendanceFemaleForm, PtaAttendanceDateForm, PtaAttendanceEditForm
from academics.models import Department, SubDivision, Admission, Semester
from django.utils.translation import ugettext_lazy as _
from main.functions import get_auto_id, generate_form_errors, get_current_role, generate_unique_id, get_admn_no, get_a_id
from django.views.decorators.http import require_GET, require_POST
from users.functions import get_current_college, college_access,get_current_batch
from academics.functions import update_student_fee
from django.forms.formsets import formset_factory
import json
from django.forms.models import inlineformset_factory
from django.forms.widgets import TextInput, Select
from main.decorators import check_mode, ajax_required, role_required
from users.models import Notification, NotificationSubject
from finance.models import Transaction
import datetime
import xlrd
import random
from main.models import Place, Batch
from dal import autocomplete
from django.core import serializers
from decimal import Decimal
from django.db.models import Q
from django.contrib.auth.models import User, Group




@login_required
@ajax_required
@require_GET
def get_divisions(request):
    pk = request.GET.get('pk')
    if Department.objects.filter(pk=pk,is_deleted=False).exists():
        department = get_object_or_404(Department.objects.filter(pk=pk,is_deleted=False))
        divisions = SubDivision.objects.filter(department=department,is_deleted=False)

        response_data    = []
        for division in divisions:
            sub_division = str(division.department) + '-' + str(division.name)
            division_dict = {
                'division_id' : str(division.pk),
                'division_name' : sub_division,
            }
            response_data.append(division_dict)

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@login_required
@ajax_required
@require_GET
def get_semesters(request):
    pk = request.GET.get('pk')
    if SubDivision.objects.filter(pk=pk,is_deleted=False).exists():
        sub_div = get_object_or_404(SubDivision.objects.filter(pk=pk,is_deleted=False))
        semesters = Semester.objects.filter(division=sub_div,is_deleted=False)

        response_data = []

        for semester in semesters:
            from_year = str(semester.from_date.year)
            from_y = from_year[2] + from_year[3]
            to_year = str(semester.to_date.year)
            to_y = to_year[2] + to_year[3]
            sem = f"SEM {semester.semester} : {str(semester.from_date.month)}/{from_y} - {str(semester.to_date.month)}/{to_y}"
            semester_dict = {
                'semester_id' : str(semester.pk),
                'semester_name' : sem,
            }
            response_data.append(semester_dict)

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['superadmin'])
def create_pta(request):
    url = reverse('pta:create_pta')
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)

    if request.method == "POST":
        form = PTAForm(request.POST)
        if form.is_valid():
            is_full_division = request.POST.get('is_full_division')
            date = form.cleaned_data['date']
            department = form.cleaned_data['department']
            sub_division = form.cleaned_data['sub_division']
            semester = form.cleaned_data['semester']

            if is_full_division:
                sub_divisions = SubDivision.objects.filter(department=department,is_deleted=False)
                for division in sub_divisions:
                    semesters = Semester.objects.filter(division=division,is_deleted=False)
                    for semester_item in semesters:
                        Pta.objects.create(
                            student_batch = current_batch,
                            date = date,
                            department = department,
                            sub_division = division,
                            semester = semester_item,
                            creator = request.user,
                            updater = request.user,
                            auto_id = get_auto_id(Pta),
                            a_id = get_a_id(Pta, request),
                            college = current_college
                        )
                response_data = {
                    "status": "true",
                    "title": "Successfully created",
                    "message": "PTA successfully created.",
                    "redirect": "true",
                    "redirect_url": reverse('pta:ptas')
                }
            elif not is_full_division and sub_division and not semester:
                semesters = Semester.objects.filter(division=sub_division,is_deleted=False)
                for semester_item in semesters:
                    Pta.objects.create(
                        student_batch = current_batch,
                        date = date,
                        department = department,
                        sub_division = sub_division,
                        semester = semester_item,
                        creator = request.user,
                        updater = request.user,
                        auto_id = get_auto_id(Pta),
                        a_id = get_a_id(Pta, request),
                        college = current_college
                    )
                response_data = {
                    "status": "true",
                    "title": "Successfully created",
                    "message": "PTA successfully created.",
                    "redirect": "true",
                    "redirect_url": reverse('pta:ptas')
                }
            elif not is_full_division and sub_division and semester:
                Pta.objects.create(
                    student_batch = current_batch,
                    date = date,
                    department = department,
                    sub_division = sub_division,
                    semester = semester,
                    creator = request.user,
                    updater = request.user,
                    auto_id = get_auto_id(Pta),
                    a_id = get_a_id(Pta, request),
                    college = current_college
                )

                response_data = {
                    "status": "true",
                    "title": "Successfully created",
                    "message": "PTA successfully created.",
                    "redirect": "true",
                    "redirect_url": reverse('pta:ptas')
                }
            else:
                message = 'Select is all divisions or Select a Department and/or Division'

                response_data = {
                    "status": "false",
                    "stable": "true",
                    "title": "Form validation error",
                    "message": str(message)
                }
            return HttpResponse(json.dumps(response_data), content_type='application/javascript')
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:

        form = PTAForm()
        context = {
            "title" : "Create PTA",
            "form" : form,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'pta/entry_pta.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_pta(request,pk):
    instance = get_object_or_404(Pta.objects.filter(pk=pk,is_deleted=False))
    url = reverse('pta:edit_pta',kwargs={'pk':pk})

    if  request.method == 'POST':
        form = PTAForm(request.POST)
        if form.is_valid():
            date = form.cleaned_data['date']
            department = form.cleaned_data['department']
            sub_division = form.cleaned_data['sub_division']
            semester = form.cleaned_data['semester']

            if department and sub_division and semester:
                instance.date = date
                instance.department = department
                instance.sub_division = sub_division
                instance.semester = semester
                instance.updater = request.user
                instance.save()
                # instance.update(date=date, department=department, sub_division=sub_division, updater=request.user)

                response_data = {
                    "status": "true",
                    "title": "Successfully Edited",
                    "message": "PTA successfully Edited.",
                    "redirect": "true",
                    "redirect_url": reverse('pta:ptas')
                }
            else:
                response_data = {
                    "status": "false",
                    "stable": "true",
                    "title": "Form validation error",
                    "message": 'Select the value of department, division and semester'
                }
            return HttpResponse(json.dumps(response_data), content_type='application/javascript')
        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        initial_data={
            'date' : instance.date,
            'department' : instance.department,
            'sub_division' : instance.sub_division,
            'semester': instance.semester
        }
        form = PTAForm(initial=initial_data)

        context = {
            "title" : "Edit PTA",
            "form" : form,
            "instance" : instance,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'pta/entry_pta.html', context)



@check_mode
@login_required
@role_required(['superadmin'])
def ptas(request):
    instances = Pta.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(date__icontains=query) | Q(department__name__icontains=query) | Q(sub_division__name__icontains=query) )
        title = "Classes - %s" %query

    context = {
        "instances" : instances,
        "title" : "PTA",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }
    return render(request, 'pta/ptas.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def pta(request,pk):
    instance = get_object_or_404(Pta.objects.filter(pk=pk))

    context = {
        'title' : instance,
        'instance' : instance,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'pta/pta.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_pta(request,pk):
    instance = get_object_or_404(Pta.objects.filter(pk=pk,is_deleted=False))
    Pta.objects.filter(pk=pk).update(is_deleted=True)

    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Enquiry successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('pta:ptas')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_pta_attendance(request,pk):
    college = get_current_college(request)
    pta = get_object_or_404(Pta.objects.filter(pk=pk,is_deleted=False))
    sub_division = get_object_or_404(SubDivision.objects.filter(pk=pta.sub_division.pk,is_deleted=False))
    today = datetime.date.today()
    last_day = today - datetime.timedelta(days=1)
    second_last_day = today - datetime.timedelta(days=2)

    student_batch = get_current_batch(request)

    admissions = Admission.objects.filter(is_deleted=False,batch=student_batch,sub_division=sub_division,department=sub_division.department)

    AttendanceMaleFormset = formset_factory(PtaAttendanceMaleForm,extra=0)
    AttendanceFemaleFormset = formset_factory(PtaAttendanceFemaleForm,extra=0)

    if request.method == 'POST':

        attendance_male_formset = AttendanceMaleFormset(request.POST,prefix='attendance_male_formset')
        attendance_female_formset = AttendanceFemaleFormset(request.POST,prefix='attendance_female_formset')
        attendance_date_form = PtaAttendanceDateForm(request.POST)

        if attendance_male_formset.is_valid() and attendance_date_form.is_valid() and attendance_female_formset.is_valid():
            weekday = attendance_date_form.cleaned_data['weekday']
            date = attendance_date_form.cleaned_data['date']

            for attendance in attendance_male_formset:
                reason = ""
                is_genuene = False
                is_present = attendance.cleaned_data['is_present']

                remarks = attendance.cleaned_data['remarks']
                if not remarks:
                    remarks = None

                admission = attendance.cleaned_data['admission']
                auto_id = get_auto_id(PtaAttendance)
                a_id = get_a_id(PtaAttendance,request)

                if not PtaAttendance.objects.filter(weekday=weekday,date=date,sub_division=sub_division,admission=admission,is_deleted=False).exists():
                    PtaAttendance(
                        pta = pta,
                        weekday = weekday,
                        date = date,
                        admission = admission,
                        is_present = is_present,
                        sub_division = sub_division,
                        department = sub_division.department,
                        remarks = remarks,
                        auto_id = auto_id,
                        a_id = a_id,
                        creator = request.user,
                        updater = request.user,
                        college = college
                    ).save()

                elif not PtaAttendance.objects.filter(weekday=weekday,date=date,sub_division=sub_division,admission=admission,is_present=is_present).exists():
                    attendance = PtaAttendance.objects.get(weekday=weekday,date=date,admission=admission,sub_division=sub_division)
                    attendance.is_present=is_present
                    attendance.save()


            for attendance in attendance_female_formset:
                is_present = attendance.cleaned_data['is_present']
                admission = attendance.cleaned_data['admission']

                remarks = attendance.cleaned_data['remarks']
                if not remarks:
                    remarks = None

                is_genuene = False
                auto_id = get_auto_id(PtaAttendance)
                a_id = get_a_id(PtaAttendance,request)

                if not PtaAttendance.objects.filter(weekday=weekday,date=date,sub_division=sub_division,admission=admission,is_deleted=False).exists():
                    PtaAttendance(
                        pta = pta,
                        weekday = weekday,
                        date = date,
                        admission = admission,
                        is_present = is_present,
                        sub_division = sub_division,
                        department = sub_division.department,
                        remarks = remarks,
                        auto_id = auto_id,
                        a_id = a_id,
                        creator = request.user,
                        updater = request.user,
                        college = college
                    ).save()

                elif not PtaAttendance.objects.filter(weekday=weekday,date=date,sub_division=sub_division,admission=admission,is_present=is_present).exists():
                    attendance = PtaAttendance.objects.get(weekday=weekday,date=date,admission=admission,sub_division=sub_division)
                    attendance.is_present=is_present
                    attendance.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Attendance created successfully.",
                "redirect" : "true",
                "redirect_url": reverse('pta:pta_attendances' , kwargs={'pk':pk})
            }

        else:
            message = generate_form_errors(attendance_female_formset,formset=True)
            message = generate_form_errors(attendance_male_formset,formset=True)
            message = generate_form_errors(attendance_date_form,formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        male_initial = []
        female_initial = []
        for admission in admissions:
            today_attendance = None
            is_present = ""
            remarks = None
            if PtaAttendance.objects.filter(date=today,admission=admission).exists():
                today_attendance = PtaAttendance.objects.filter(date=today,admission=admission)[0]
                is_present = today_attendance.is_present
                remarks = today_attendance.remarks
            if admission.gender == "male":
                attendance_male_dict = {
                    'admission' : admission,
                    'admission_name' : admission.name,
                    'is_present' : is_present,
                    "remarks" : remarks,
                }
                male_initial.append(attendance_male_dict)
            elif admission.gender == "female":
                attendance_female_dict = {
                    'admission' : admission,
                    'admission_name' : admission.name,
                    'is_present' : is_present,
                    "remarks" : remarks,
                }
                female_initial.append(attendance_female_dict)
        weekday_no = today.weekday()
        if weekday_no == 0:
            weekday = "monday"
        elif weekday_no == 1:
            weekday = "tuesday"
        elif weekday_no == 2:
            weekday = "wednesday"
        elif weekday_no == 3:
            weekday = "thursday"
        elif weekday_no == 4:
            weekday = "friday"
        elif weekday_no == 5:
            weekday = "saturday_forenoon"
        elif weekday_no == 6:
            weekday = "sunday_forenoon"

        attendance_male_formset = AttendanceMaleFormset(prefix='attendance_male_formset',initial=male_initial)
        attendance_female_formset = AttendanceFemaleFormset(prefix='attendance_female_formset',initial=female_initial)
        attendance_date_form = PtaAttendanceDateForm(initial={"date":today,"weekday":weekday})
        context = {
            "title" : "Create Attendance",
            "attendance_male_formset" : attendance_male_formset,
            "attendance_female_formset" : attendance_female_formset,
            "attendance_date_form" : attendance_date_form,
            "url" : reverse('pta:create_pta_attendance',kwargs={'pk':pk}),
            "today" : today,
            "admissions" : admissions,
            "sub_division" : sub_division,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request,'pta/entry_pta_attendance.html',context)


@check_mode
@login_required
def pta_attendances(request,pk):
    batch = get_current_batch(request)
    current_role = get_current_role(request)
    pta = get_object_or_404(Pta.objects.filter(pk=pk,is_deleted=False))
    sub_division = get_object_or_404(SubDivision.objects.filter(pk=pta.sub_division.pk,is_deleted=False))
    instance = get_object_or_404(SubDivision.objects.filter(pk=pta.sub_division.pk,is_deleted=False))
    department = sub_division.department
    today = datetime.date.today()
    date_string = today
    instances_today = PtaAttendance.objects.filter(is_deleted=False,sub_division=sub_division,date=today)

    title = "PTA Attendances %s - %s " %(sub_division,today)
    month = today.strftime('%m')
    year = today.strftime('%Y')
    instances = PtaAttendance.objects.none()
    morethan_three_absent_students = []
    remarks_students = []
    morethan_three_absent_students_count = 0
    remarks_students_count = 0
    date_error = False
    date = request.GET.get("date")
    # tasks = []

    # if Task.objects.filter(is_completed=False,batch=batch,sub_division=sub_division).exists():
    #     task_list = Task.objects.filter(is_completed=False,batch=batch,sub_division=sub_division)
    #     for task in task_list:
    #         tasks.append(task)
    # if Task.objects.filter(is_completed=False,batch=batch,sub_division=None,department=department).exists():
    #     task_list = Task.objects.filter(is_completed=False,batch=batch,sub_division=None,department=department)
    #     for task in task_list:
    #         tasks.append(task)
    # if Task.objects.filter(is_completed=False,batch=batch,sub_division=None,department=None,is_general=True).exists():
    #     task_list = Task.objects.filter(is_completed=False,batch=batch,sub_division=None,department=None,is_general=True)
    #     for task in task_list:
    #         tasks.append(task)
    # print tasks
    if date:
        try:
            date = datetime.datetime.strptime(date, '%m/%d/%Y').date()
            title = "PTA Attendances %s - %s" %(sub_division,date)
            date_string = date
        except ValueError:
            date_error = True
    if not date_error and date:
        instances_today = []
        instances = PtaAttendance.objects.filter(is_deleted=False,sub_division=sub_division,date=date_string)

    total_students = PtaAttendance.objects.filter(is_deleted=False,sub_division=sub_division,date=date_string).count()
    present_students = PtaAttendance.objects.filter(is_deleted=False,sub_division=sub_division,is_present=True,date=date_string).count()
    absent_students = PtaAttendance.objects.filter(is_deleted=False,sub_division=sub_division,is_present=False,date=date_string).count()

    administrator = None
    present = request.GET.get("present")
    if present:
        instances = instances.filter(is_present=present,date=date_string)

    attendance_dates = instances
    query = request.GET.get("q")
    if  instances_today:
        if query:
            instances = instances_today.filter( Q(admission__name__icontains=query))
            title = "PTA Attendances- %s" %query
    else:
        if query:
            instances = instances.filter( Q(admission__name__icontains=query) | Q(date__icontains=query) | Q(is_present__icontains=query))
            title = "PTA Attendances- %s" %query

    period = request.GET.get("period")
    if period:
        instances = instances.filter(period=period)

    remarks = request.GET.get("remarks")
    if remarks == "yes":
        instances = instances.filter(is_deleted=False,sub_division=sub_division,is_present=True,date=date_string).exclude(remarks=None)

    context = {
        "instances" : instances,
        "instances_today" : instances_today,
        "title" : title,
        "date_string" : date_string,
        "pta" : pta,
        "present_students" :  present_students,
        "absent_students" : absent_students,
        "total_students" : total_students,
        "remarks_students" : remarks_students,
        "sub_division" : sub_division,
        "remarks" : remarks,
        "instance" : instance,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request,'pta/pta_attendances.html',context)


@check_mode
@login_required
def pta_attendance(request,pk):
    instance = get_object_or_404(Attendance.objects.filter(pk=pk,is_deleted=False))

    context = {
        "instance" : instance,
        "title" : "Attendance ",

        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'pta/pta_attendance.html',context)


@check_mode
@login_required
def edit_pta_attendance(request,pk):
    instance = get_object_or_404(PtaAttendance.objects.filter(pk=pk,is_deleted=False))
    url = reverse('pta:edit_pta_attendance',kwargs={'pk':pk})
    if request.method == "POST":
        previous_attendance_status = instance.is_present

        form = PtaAttendanceEditForm(request.POST,instance=instance)

        if form.is_valid():
            is_present = form.cleaned_data['is_present']
            remarks = form.cleaned_data['remarks']
            if not remarks:
                remarks = None

            #update attendance
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.remarks = remarks
            data.save()


            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Attendance successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('pta:pta_attendances' , kwargs={'pk':data.pta.pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = PtaAttendanceEditForm(instance=instance)
        context = {
            "title" : "Edit Attendance",
            "form" : form,
            "instance" : instance,
            "url" : url,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_custom_scroll_bar": True,
            "is_need_wave_effect": True,
            "is_need_bootstrap_growl": True,
            "is_need_animations": True,
            "is_need_grid_system": True,
            "is_need_select_picker": True,
            "is_need_datetime_picker" : True
        }
        return render(request, 'pta/edit_pta_attendance.html', context)


@check_mode
@login_required
def delete_pta_attendance(request,pk):
    attandence = get_object_or_404(PtaAttendance.objects.filter(pk=pk,is_deleted=False))
    sub_division = attandence.sub_division
    PtaAttendance.objects.filter(pk=pk).update(is_deleted=True,is_present=False)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "Attendance successfully deleted.",
        "reload": "true",
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')



@check_mode
@login_required
def show_calendar_pta_attendances(request,pk):
    pta = get_object_or_404(Pta.objects.filter(pk=pk,is_deleted=False))
    instance = get_object_or_404(SubDivision.objects.filter(pk=pta.sub_division.pk,is_deleted=False))


    today = datetime.datetime.today()
    weekday_no = today.weekday()
    if weekday_no == 0:
        weekday = "monday"
    elif weekday_no == 1:
        weekday = "tuesday"
    elif weekday_no == 2:
        weekday = "wednesday"
    elif weekday_no == 3:
        weekday = "thursday"
    elif weekday_no == 4:
        weekday = "friday"
    elif weekday_no == 5:
        weekday = "saturday"
    elif weekday_no == 6:
        weekday = "sunday"
    # periods = Period.objects.filter(is_deleted=False,weekday=weekday)
    sub_divisions = SubDivision.objects.filter(is_deleted=False)
    # instances = FacultyAttendance.objects.filter(is_deleted=False,date=today.date())
    # title = "Faculty Attendances - %s " %today.date()

    context = {
        # "title" : title,
        "instance" : instance,
        # "periods" : periods,
        "date" : today.date(),
        "pta" : pta,
        "is_need_calender" : True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request, 'pta/pta_attendance_calender.html', context)


@check_mode
@login_required
def show_class_pta_attendence(request):
    today = datetime.date.today()
    batch = get_current_batch(request)

    batch_pk = request.GET.get('batch')
    if batch_pk:
        if Batch.objects.filter(pk=batch_pk).exists():
            batch = Batch.objects.get(pk=batch_pk)

    sub_divisions = SubDivision.objects.filter(is_deleted=False)
    departments = Department.objects.filter(is_deleted=False)
    if batch:
        student_batches = Batch.objects.filter(is_deleted=False).exclude(pk=batch.pk)
    else:
        student_batches = Batch.objects.filter(is_deleted=False)

    context = {
        "title" : "Class and Divisions in %s" %batch,
        "current_batch" : batch,
        "student_batches" : student_batches,
        "sub_divisions" : sub_divisions,
        "departments" : departments,

        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request, 'pta/show_class_pta_attendence.html', context)


@check_mode
@login_required
def show_division_pta_attendence(request,pk):
    department = get_object_or_404(Department.objects.filter(pk=pk,is_deleted=False))
    instances = SubDivision.objects.filter(department=department,is_deleted=False)
    title = "Divisions inside class %s" %department

    context = {
        "instances" : instances,
        "title" : title,
        "is_need_calender" : True,
        "is_need_popup_box": True,
        "is_need_custom_scroll_bar": True,
        "is_need_wave_effect": True,
        "is_need_bootstrap_growl": True,
        "is_need_animations": True,
        "is_need_grid_system": True,
        "is_need_select_picker": True,
        "is_need_datetime_picker" : True
    }
    return render(request, 'pta/show_division_pta_attendence.html', context)
