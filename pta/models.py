import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator
from decimal import Decimal
import datetime
from main.models import BaseModel


WEEKDAY_CHOICES = (
    ('monday','MONDAY'),
    ('tuesday','TUESDAY'),
    ('wednesday','WEDNESDAY'),
    ('thursday','THURSDAY'),
    ('friday','FRIDAY'),
    ('saturday','SATURDAY')
)


class Pta(BaseModel):
    student_batch = models.ForeignKey("main.Batch",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    department = models.ForeignKey("academics.Department",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    sub_division = models.ForeignKey("academics.SubDivision",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    semester = models.ForeignKey("academics.Semester",blank=True,null=True,limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    date = models.DateField()

    class Meta:
        db_table = 'pta'
        verbose_name = 'pta'
        verbose_name_plural = 'pta'
        ordering = ('date',)

    def __str__(self):
        return self.department.name


class PtaAttendance(BaseModel):
    pta = models.ForeignKey("pta.Pta",on_delete=models.CASCADE)
    admission= models.ForeignKey("academics.Admission",on_delete=models.CASCADE)
    date = models.DateField()
    weekday = models.CharField(max_length=128,choices=WEEKDAY_CHOICES)
    department= models.ForeignKey("academics.Department",on_delete=models.CASCADE)
    sub_division = models.ForeignKey("academics.SubDivision",on_delete=models.CASCADE)
    is_present = models.BooleanField(default=True)
    remarks = models.TextField(blank=True,null=True)

    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'pta_attendance'
        verbose_name = _('pta attendance')
        verbose_name_plural = _('pta attendances')
        ordering = ('date',)

    def __str__(self):
        return self.date
