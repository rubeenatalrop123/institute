from django import forms
from django.forms.widgets import TextInput, Textarea, HiddenInput, CheckboxInput, EmailInput, Select
from exams.models import Exam, Evaluation, ExamResult
from academics.models import Department, SubDivision
from pta.models import PtaAttendance, Pta
from django.utils.translation import ugettext_lazy as _
from dal import autocomplete


class PTAForm(forms.ModelForm):
    
    class Meta:
        model = Pta
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id','student_batch']
        widgets = {
            'department': autocomplete.ModelSelect2(url='academics:department_autocomplete',attrs={'data-placeholder': 'Department','data-minimum-input-length': 0},),
            'sub_division': Select(attrs={"class":"form-control","placeholder":"Division Wise"}),
            'semester': Select(attrs={"class":"form-control","placeholder":"Semester wise"}),
            'date': TextInput(attrs={"class":"datepicker form-control","placeholder":"Date"})
        }


class PtaAttendanceMaleForm(forms.ModelForm):

    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = PtaAttendance
        exclude = ['creator','updater','auto_id','department','sub_division','is_deleted','date','weekday','absent_days_count','college','a_id','pta']
        widgets = {
            'admission' : HiddenInput(),
            "reason": TextInput(
                attrs = {
                    "class": "form-control",
                }
            ),
        }
        error_messages = {
            "admission" : {
                    "required" : _("Admission field is required.")
                },
        }


class PtaAttendanceFemaleForm(forms.ModelForm):

    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = PtaAttendance
        exclude = ['creator','updater','auto_id','department','sub_division','is_deleted','date','weekday','absent_days_count','college','a_id','pta']
        widgets = {
            'admission' : HiddenInput(),
            "reason": TextInput(
                attrs = {
                    "class": "form-control",
                }
            ),
        }
        error_messages = {
            "admission" : {
                    "required" : _("Admission field is required.")
                },
        }


class PtaAttendanceDateForm(forms.ModelForm):

    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = PtaAttendance
        exclude = ['creator','updater','auto_id','department','sub_division','is_deleted','admission','is_present','absent_days_count','college','a_id','pta']

        widgets = {
                "date": TextInput(
                    attrs = {
                        "class": "required form-control date-picker",
                        "placeholder" : " Date ",
                    }
                ),
                "weekday": Select(
                    attrs = {
                        "class": "required form-control selectpicker",
                    }
                )
            }
        error_messages = {
                "date" : {
                    "required" : _("Date field is required.")
                },
                "weekday" : {
                    "required" : _("Period field is required.")
                },
            }

class PtaAttendanceForm(forms.ModelForm):

    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = PtaAttendance
        exclude = ['creator','updater','auto_id','department','sub_division','is_deleted','date','weekday','absent_days_count','college','a_id','pta']
        widgets = {
            'admission' : HiddenInput(),
            "reason": TextInput(attrs = {"class": "form-control"}),
            "remarks": TextInput(attrs = {"class": "form-control"}),
        }
        error_messages = {
            "admission" : {
                    "required" : _("Admission field is required.")
                },
        }


class PtaAttendanceEditForm(forms.ModelForm):

    admission_name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','disabled':'disabled'}))
    class Meta:
        model = PtaAttendance
        exclude = ['creator','admission','updater','auto_id','department','sub_division','is_deleted','date','weekday','absent_days_count','college','a_id','pta']
        widgets = {
            "reason": TextInput(
                attrs = {
                    "class": "form-control",
                }
            ),
            "remarks": TextInput(
                attrs = {
                    "class": "form-control",
                }
            ),
        }
        error_messages = {

        }
