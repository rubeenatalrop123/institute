from django import forms
from django.forms.widgets import TextInput,Textarea,Select
from django.utils.translation import ugettext_lazy as _
from transports.models import BusRoute, Vehicle, BoardingPoint, Transport, BusPass
from dal import autocomplete


class BusRouteForm(forms.ModelForm):
    
    class Meta:
        model=BusRoute
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']            
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Route'}),
        }
        error_messages = {
            'name' : {
                'required' : _("Bus Route field is required."),
            },
            
        }


class VehicleForm(forms.ModelForm):
    
    class Meta:
        model=Vehicle
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']            
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Vehicle Number'}),
            'available_seat': TextInput(attrs={'class': 'required form-control','placeholder' : 'Available Seats'}),
            'route': autocomplete.ModelSelect2(url='transports:routes_autocomplete', attrs={'data-placeholder': 'Route', 'data-minimum-input-length': 1,'class':'form-control required'},),    
            'staff': autocomplete.ModelSelect2(url='staffs:staffs_autocomplete', attrs={'data-placeholder': 'Driver', 'data-minimum-input-length': 1},),    
        }
        error_messages = {
            'name' : {
                'required' : _("Vehicle Number field is required."),
            },
            'available_seat' : {
                'required' : _("Available Seats field is required."),
            },
            'route' : {
                'required' : _("Available Route field is required."),
            },
        }

class BoardingPointForm(forms.ModelForm):
    
    class Meta:
        model = BoardingPoint
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']    
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : "Enter BoardingPoint",}),
            'route': autocomplete.ModelSelect2(url='transports:routes_autocomplete', attrs={'data-placeholder': 'Route', 'data-minimum-input-length': 1},),    
            "amount": TextInput(attrs={"class": "required form-control","placeholder" : "Enter the amount",})
        }
        error_messages = {
            "name" : {
                "required" : _("Name field is required.")
            },
            
        }
        help_texts = {
        }
        
    def clean(self):
        cleaned_data = super(BoardingPointForm, self).clean()
        return cleaned_data
        


class TransportForm(forms.ModelForm):
    
    class Meta:
        model=Transport
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']            
        widgets = {
            'student': autocomplete.ModelSelect2(url='academics:admission_autocomplete', attrs={'data-placeholder': 'Student', 'data-minimum-input-length': 1},),
            'staff': autocomplete.ModelSelect2(url='staffs:staffs_autocomplete', attrs={'data-placeholder': 'Staff', 'data-minimum-input-length': 1},),
            'choice': Select(attrs={'class': 'required form-control selectpicker', 'placeholder': 'Choice'}),            
            'boarding_point' : autocomplete.ModelSelect2(url='transports:boarding_point_autocomplete',attrs={'data-placeholder': 'Boarding Point','data-minimum-input-length': 1},),
            'morning_bus' : autocomplete.ModelSelect2(url='transports:vehicle_autocomplete',attrs={'data-placeholder': 'Morning Bus','data-minimum-input-length': 1},),
            'evening_bus' : autocomplete.ModelSelect2(url='transports:vehicle_autocomplete',attrs={'data-placeholder': 'Evening Bus','data-minimum-input-length': 1},),
        }
        error_messages = {
            'student' : {
                'required' : _("Student field is required."),
            },            
            'staff' : {
                'required' : _("Staff field is required."),
            },            
        }


class BusPassForm(forms.ModelForm):
    
    class Meta:
        model=BusPass
        exclude = ['creator','updater','auto_id','is_deleted','a_id','college']            
        widgets = {
            'student': autocomplete.ModelSelect2(url='academics:admission_autocomplete', attrs={'data-placeholder': 'Student', 'data-minimum-input-length': 1},),
            'boarding_point' : autocomplete.ModelSelect2(url='transports:boarding_point_autocomplete',attrs={'data-placeholder': 'Boarding Point','data-minimum-input-length': 1},),
            'date_of_issue': TextInput(attrs={'class': 'required form-control date-picker','placeholder' : 'Date of Issue'}),
            'valid_up_to': TextInput(attrs={'class': 'required form-control date-picker','placeholder' : 'Valid Up to'}),
        }
        error_messages = {
            'student' : {
                'required' : _("Student field is required."),
            },
            'route' : {
                'required' : _("Route field is required."),
            },
            'date_of_issue' : {
                'required' : _("Date of Issue field is required."),
            },
            'valid_up_to' : {
                'required' : _("Valid up to field is required."),
            },
        }
