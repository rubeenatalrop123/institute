from django.conf.urls import url
from transports import views
from transports.views import RouteAutocomplete, VehicleAutocomplete, BoardingPointAutocomplete


urlpatterns = [
    url(r'^route-autocomplete/$',RouteAutocomplete.as_view(),name='routes_autocomplete'), 
    url(r'^vehicle-autocomplete/$',VehicleAutocomplete.as_view(),name='vehicle_autocomplete'),
    url(r'^boarding-point-autocomplete/$',BoardingPointAutocomplete.as_view(create_field='name'),name='boarding_point_autocomplete'),

    url(r'^bus-route/create-bus-route/$',views.create_bus_route,name='create_bus_route'),    
    url(r'^bus-routes/view/$',views.bus_routes,name='bus_routes'), 
    url(r'^bus-route/bus-route/view/(?P<pk>.*)/$', views.bus_route, name='bus_route'),
    url(r'^bus-route/bus-route/edit-bus_route/(?P<pk>.*)/$', views.edit_bus_route, name='edit_bus_route'),
    url(r'^bus-route/bus-route/delete/(?P<pk>.*)/$', views.delete_bus_route, name='delete_bus_route'),

    url(r'^vehicle/create-vehicle/$',views.create_vehicle,name='create_vehicle'),    
    url(r'^vehicle/view/$',views.vehicles,name='vehicles'), 
    url(r'^vehicle/vehicle/view/(?P<pk>.*)/$', views.vehicle, name='vehicle'),
    url(r'^vehicle/vehicle/edit-vehicle/(?P<pk>.*)/$', views.edit_vehicle, name='edit_vehicle'),
    url(r'^vehicle/vehicle/delete/(?P<pk>.*)/$', views.delete_vehicle, name='delete_vehicle'),
    url(r'^vehicle/delete-selected-vehicles/$', views.delete_selected_vehicles, name='delete_selected_vehicles'),

    url(r'^boarding-points/$', views.boarding_points, name='boarding_points'),
    url(r'^create/boarding-point/$', views.create_boarding_point, name='create_boarding_point'),
    url(r'^view/boarding-point/(?P<pk>.*)/$', views.view_boarding_point, name='view_boarding_point'),
    url(r'^edit/boarding-point/(?P<pk>.*)/$', views.edit_boarding_point, name='edit_boarding_point'),
    url(r'^delete/boarding-point/(?P<pk>.*)/$', views.delete_boarding_point, name='delete_boarding_point'),

    url(r'^transport/create-transport/$',views.create_transport,name='create_transport'),    
    url(r'^transport/view/$',views.transports,name='transports'), 
    url(r'^transport/transport/view/(?P<pk>.*)/$', views.transport, name='transport'),
    url(r'^transport/transport/edit-transport/(?P<pk>.*)/$', views.edit_transport, name='edit_transport'),
    url(r'^transport/transport/delete/(?P<pk>.*)/$', views.delete_transport, name='delete_transport'),
    url(r'^transport/delete-selected-transports/$', views.delete_selected_transports, name='delete_selected_transports'),

    url(r'^bus-pass/create-bus-pass/$',views.create_bus_pass,name='create_bus_pass'),    
    url(r'^bus-pass/view/$',views.bus_passes,name='bus_passes'), 
    url(r'^bus-pass/bus-pass/view/(?P<pk>.*)/$', views.bus_pass, name='bus_pass'),
    url(r'^bus-pass/bus-pass/edit-bus-pass/(?P<pk>.*)/$', views.edit_bus_pass, name='edit_bus_pass'),
    url(r'^bus-pass/bus-pass/delete/(?P<pk>.*)/$', views.delete_bus_pass, name='delete_bus_pass'),
    url(r'^bus-pass/delete-selected-bus-passes/$', views.delete_selected_bus_passes, name='delete_selected_bus_passes'),
 ]