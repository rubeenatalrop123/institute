import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator
from decimal import Decimal
from main.models import BaseModel


CHOICE = (
    ('student','Student'),
    ('staff','Staff')
)

class BusRoute(BaseModel):
    name = models.CharField(max_length=128)
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'logistics_bus'
        verbose_name = _('bus')
        verbose_name_plural = _('buses')
        ordering = ('name',)
        
        
    def __str__(self):
        return self.name

class Vehicle(BaseModel):
    staff = models.ForeignKey("staffs.Staff",null=True,blank=True,on_delete=models.CASCADE)
    name = models.CharField(max_length=128,blank=True,null=True)
    route = models.ForeignKey("transports.BusRoute",null=True,blank=True,on_delete=models.CASCADE)
    available_seat = models.DecimalField(null=True,blank=True,decimal_places=2, max_digits=15, validators=[MinValueValidator(Decimal('0.00'))])
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'transports_vehicle'
        verbose_name = _('vehicle')
        verbose_name_plural = _('vehicles')
        ordering = ('name',)
        
    def __str__(self):
        return self.name

class BoardingPoint(BaseModel):
    name = models.CharField(max_length=128)
    route = models.ForeignKey("transports.BusRoute",null=True,blank=True,on_delete=models.CASCADE)
    amount = models.DecimalField(default=0,null=True,blank=True,decimal_places=2, max_digits=15, validators=[MinValueValidator(Decimal('0.00'))])
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'transports_boarding_point'
        verbose_name = _('boarding point')
        verbose_name_plural = _('boarding points')
        ordering = ('name',)
        
    def __str__(self):
        return self.name


class Transport(BaseModel):
    student = models.ForeignKey("academics.Admission",blank=True,null=True,on_delete=models.CASCADE)
    staff = models.ForeignKey("staffs.Staff",blank=True,null=True,on_delete=models.CASCADE)
    choice = models.CharField(max_length=9,choices=CHOICE,blank=True,null=True,default="student")
    boarding_point = models.ForeignKey("transports.BoardingPoint",blank=True,null=True,on_delete=models.CASCADE)
    morning_bus = models.ForeignKey("transports.Vehicle",related_name='morning_%(class)s_objects',blank=True,null=True,on_delete=models.CASCADE)
    evening_bus = models.ForeignKey("transports.Vehicle",related_name='evening_%(class)s_objects',blank=True,null=True,on_delete=models.CASCADE)
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'transports_transport'
        verbose_name = _('transport')
        verbose_name_plural = _('transports')
        ordering = ('boarding_point',)
        
    def __str__(self):
        return self.boarding_point


class BusPass(BaseModel):
    student = models.ForeignKey("academics.Admission",on_delete=models.CASCADE)
    boarding_point = models.ForeignKey("transports.BoardingPoint",on_delete=models.CASCADE)
    valid_up_to= models.DateField(blank=True,null=True)
    date_of_issue= models.DateField(blank=True,null=True)
    is_deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'transports_bus_pass'
        verbose_name = _('bus_pass')
        verbose_name_plural = _('bus_passes')
        ordering = ('boarding_point',)
        
    def __str__(self):
        return self.student
