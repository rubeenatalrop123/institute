from django.shortcuts import render,get_object_or_404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, permissions_required, role_required, ajax_required
from main.functions import get_auto_id, generate_form_errors, get_current_role, generate_unique_id, get_a_id
from users.functions import get_current_college, college_access, get_current_batch
import json
from django.db.models import Q, Sum
from django.forms.formsets import formset_factory
from django.views.decorators.http import require_GET
from transports.models import BusRoute, Vehicle, BoardingPoint, Transport, BusPass
from transports.forms import BusRouteForm, VehicleForm, BoardingPointForm,TransportForm, BusPassForm
from academics.models import FeeCategory
from academics.functions import update_student_fee
import datetime
from dal import autocomplete


class RouteAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        routes = BusRoute.objects.filter(is_deleted=False)

        if self.q:
            name = routes.filter(Q(name__istartswith=self.q))

        return name


class VehicleAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        vehicles = Vehicle.objects.filter(is_deleted=False)

        if self.q:
            name = vehicles.filter(Q(name__istartswith=self.q))

        return name
        

class BoardingPointAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        boarding_points = BoardingPoint.objects.filter(is_deleted=False)

        if self.q:
            boarding_points = boarding_points.filter(Q(name__istartswith=self.q))

        return boarding_points

    def create_object(self, text):
        auto_id = get_auto_id(BoardingPoint)
        a_id = get_a_id(BoardingPoint,self.request)
        return BoardingPoint.objects.create(
            auto_id=auto_id,
            a_id=a_id,
            name=text,
            creator=self.request.user,
            updator=self.request.user
        )


@check_mode
@login_required
def create_bus_route(request):
    url = reverse('transports:create_bus_route')
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)

    if request.method == 'POST':
        form = BusRouteForm(request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            auto_id = get_auto_id(BusRoute)

            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.college = current_college
            data.batch = current_batch
            data.a_id = get_a_id(BusRoute,request)
            data.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Bus Route created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('transports:bus_route', kwargs={'pk':data.pk})
            }

        else:
            message = generate_form_errors(form,formset=False)
            print(form.errors)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = BusRouteForm()
        context = {
            "title" : "Create Bus Route",
            "form" : form,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'transports/create_bus_route.html',context)


@check_mode
@login_required
def bus_routes(request):
    instances = BusRoute.objects.filter(is_deleted=False)
    title = "Routes"

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query))
        title = "Routes - %s" %query

    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'transports/bus_routes.html',context)


@check_mode
@login_required
def bus_route(request,pk):
    instance = get_object_or_404(BusRoute.objects.filter(pk=pk,is_deleted=False))
    context = {
        "instance" : instance,
        "title" : instance.name,
        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'transports/bus_route.html',context)


@check_mode
@login_required
def edit_bus_route(request,pk):
    instance = get_object_or_404(BusRoute.objects.filter(pk=pk,is_deleted=False))
    url = reverse('transports:edit_bus_route',kwargs={'pk':pk})

    if request.method == "POST":

        form = BusRouteForm(request.POST,instance=instance)
        if form.is_valid():

            #update student medium
            data = form.save(commit=False)
            data.updater = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Bus Route successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('transports:bus_route',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = BusRouteForm(instance=instance)

        context = {
            "title" : "Edit Bus Route  : " + instance.name,
            "form" : form,
            "instance" : instance,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,
        }
        return render(request, 'transports/create_bus_route.html', context)


@check_mode
@login_required
def delete_bus_route(request,pk):
    instance = get_object_or_404(BusRoute.objects.filter(pk=pk,is_deleted=False))

    BusRoute.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Bus Route Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('transports:bus_routes')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')



@check_mode
@login_required
def create_vehicle(request):
    url = reverse('transports:create_vehicle')
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)

    if request.method == 'POST':
        form = VehicleForm(request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            auto_id = get_auto_id(Vehicle)

            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.college = current_college
            data.batch = current_batch
            data.a_id = get_a_id(Vehicle,request)
            data.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Vehicle created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('transports:vehicle', kwargs={'pk':data.pk})
            }

        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = VehicleForm()
        context = {
            "title" : "Create Vehicle",
            "form" : form,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'transports/create_vehicle.html',context)


@check_mode
@login_required
def vehicles(request):
    instances = Vehicle.objects.filter(is_deleted=False)
    title = "Vehicles"

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query) | Q(route__name__icontains=query) | Q(available_seat__icontains=query))
        title = "Vehicles - %s" %query

    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'transports/vehicles.html',context)


@check_mode
@login_required
def vehicle(request,pk):
    instance = get_object_or_404(Vehicle.objects.filter(pk=pk,is_deleted=False))
    context = {
        "instance" : instance,
        "title" : instance.name,
        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'transports/vehicle.html',context)


@check_mode
@login_required
def edit_vehicle(request,pk):
    instance = get_object_or_404(Vehicle.objects.filter(pk=pk,is_deleted=False))
    url = reverse('transports:edit_vehicle',kwargs={'pk':pk})
    if request.method == "POST":

        form = VehicleForm(request.POST,instance=instance)
        if form.is_valid():

            #update vehicle
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Vehicle successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('transports:vehicle',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = VehicleForm(instance=instance)

        context = {
            "title" : "Edit Vehicle  : " + instance.name,
            "form" : form,
            "redirect": "true",
            "instance" : instance,
            "url" : url,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'transports/create_vehicle.html', context)


# @check_mode
# @login_required
# def delete_vehicle(request,pk):
#     current_college = get_current_college(request)
#     current_batch = get_current_batch(request)
#     instance = get_object_or_404(Vehicle.objects.filter(pk=pk,is_deleted=False,college=current_college,batch=current_batch))

#     Vehicle.objects.filter(pk=pk,college=current_college,batch=current_batch).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))

#     response_data = {
#         "status" : "true",
#         "title" : "Successfully Deleted",
#         "message" : "Vehicle Successfully Deleted.",
#         "redirect" : "true",
#         "redirect_url" : reverse('transports:vehicles')
#     }
#     return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def delete_vehicle(request,pk):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)

    Vehicle.objects.filter(pk=pk).update(is_deleted=True)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Vehicle Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('transports:vehicles')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def delete_selected_vehicles(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(Vehicle.objects.filter(pk=pk,is_deleted=False,college=current_college,batch=current_batch))
            Vehicle.objects.filter(pk=pk).update(is_deleted=True)

        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Vehicle(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('transports:vehicles')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select vehicles first.",
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def boarding_points(request):
    instances = BoardingPoint.objects.filter(is_deleted=False)

    query = request.GET.get("q")
    if query:
        instances = instances.filter( Q(name__icontains=query) | Q(route__name__icontains=query) | Q(amount__istartswith=query))
        title = "Boarding Points - %s" %query

    context = {
        "instances" : instances,
        "title" : "BoardingPoint",

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,

        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request, 'transports/boarding_points.html', context)


@check_mode
@login_required
def view_boarding_point(request,pk):
    instance = get_object_or_404(BoardingPoint.objects.filter(pk=pk,is_deleted=False))
    context = {
        "title" : "BoardingPoint",
        'instance' : instance,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,

        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request, 'transports/boarding_point.html', context)


@check_mode
@login_required
def create_boarding_point(request):
    url = reverse('transports:create_boarding_point')
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)

    if request.method == 'POST':
        form = BoardingPointForm(request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            auto_id = get_auto_id(BoardingPoint)

            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.college = current_college
            data.batch = current_batch
            data.a_id = get_a_id(BoardingPoint,request)
            data.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "BoardingPoint created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('transports:view_boarding_point', kwargs={'pk':data.pk})
            }

        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = BoardingPointForm()
        context = {
            "title" : "Create BoardingPoint",
            "form" : form,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'transports/entry_boarding_point.html',context)




@check_mode
@login_required
def edit_boarding_point(request,pk):
    instance = get_object_or_404(BoardingPoint.objects.filter(pk=pk,is_deleted=False))
    url = reverse('transports:edit_boarding_point',kwargs={'pk':pk})
    if request.method == "POST":

        form = BoardingPointForm(request.POST,instance=instance)
        if form.is_valid():

            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "BoardingPoint successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('transports:view_boarding_point',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = BoardingPointForm(instance=instance)

        context = {
            "title" : "Edit BoardingPoint",
            "form" : form,
            "redirect": "true",
            "instance" : instance,
            "url" : url,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'transports/entry_boarding_point.html', context)


@check_mode
@login_required
def delete_boarding_point(request,pk):
    BoardingPoint.objects.filter(pk=pk).update(is_deleted=True)
    response_data = {
        "status": "true",
        "title": "Successfully deleted",
        "message": "BoardingPoint successfully deleted.",
        "redirect": "true",
        "redirect_url": reverse('transports:boarding_points')
    }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def create_transport(request):
    url = reverse('transports:create_transport')
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)

    if request.method == 'POST':
        form = TransportForm(request.POST)
        if form.is_valid():
            student = form.cleaned_data['student']
            boarding_point = form.cleaned_data['boarding_point']
            morning_bus = form.cleaned_data['morning_bus']
            evening_bus = form.cleaned_data['evening_bus']
            fee_amount = boarding_point.amount
            if morning_bus and evening_bus :
                transportation_fee_amount = fee_amount * 2
            elif morning_bus or evening_bus:
                transportation_fee_amount = fee_amount
            data = form.save(commit=False)
            auto_id = get_auto_id(Transport)

            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.college = current_college
            data.batch = current_batch
            data.a_id = get_a_id(Transport,request)
            data.save()

            transportation_fee_category = FeeCategory.objects.get(name="transportation_fee",is_deleted=False,is_general=True)

            if student:
                transport_fee =  transportation_fee_amount * 12
                student.balance = student.balance + transport_fee
                student.save()

                update_student_fee(request,student,transportation_fee_category,transportation_fee_amount,month="april")
                update_student_fee(request,student,transportation_fee_category,transportation_fee_amount,month="may")
                update_student_fee(request,student,transportation_fee_category,transportation_fee_amount,month="june")
                update_student_fee(request,student,transportation_fee_category,transportation_fee_amount,month="july")
                update_student_fee(request,student,transportation_fee_category,transportation_fee_amount,month="august")
                update_student_fee(request,student,transportation_fee_category,transportation_fee_amount,month="september")
                update_student_fee(request,student,transportation_fee_category,transportation_fee_amount,month="october")
                update_student_fee(request,student,transportation_fee_category,transportation_fee_amount,month="november")
                update_student_fee(request,student,transportation_fee_category,transportation_fee_amount,month="december")
                update_student_fee(request,student,transportation_fee_category,transportation_fee_amount,month="january")
                update_student_fee(request,student,transportation_fee_category,transportation_fee_amount,month="february")
                update_student_fee(request,student,transportation_fee_category,transportation_fee_amount,month="march")

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Transport created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('transports:transport', kwargs={'pk':data.pk})
            }

        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = TransportForm()
        context = {
            "title" : "Create Transport",
            "form" : form,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'transports/create_transport.html',context)



@check_mode
@login_required
def transports(request):
    instances = Transport.objects.filter(is_deleted=False)
    title = "Transports"

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(student__name__icontains=query) | Q(staff__name__icontains=query) | Q(choice__icontains=query) | Q(boarding_point__name__icontains=query) | Q(morning_bus__name__icontains=query) | Q(evening_bus__name__icontains=query))
        title = "Transportes - %s" %query

    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'transports/transports.html',context)



@check_mode
@login_required
def transport(request,pk):
    instance = get_object_or_404(Transport.objects.filter(pk=pk,is_deleted=False))
    context = {
        "instance" : instance,
        "title_student" : "Transportation Details of %s" %instance.student,
        "title_staff" : "Transportation Details of %s" %instance.staff,
        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'transports/transport.html',context)


@check_mode
@login_required
def edit_transport(request,pk):
    instance = get_object_or_404(Transport.objects.filter(pk=pk,is_deleted=False))
    url = reverse('transports:edit_transport',kwargs={'pk':pk})
    if request.method == "POST":

        form = TransportForm(request.POST,instance=instance)
        if form.is_valid():

            #update student medium
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Transport successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('transports:transport',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = TransportForm(instance=instance)

        context = {
            "title" : "Edit Transport ",
            "form" : form,
            "redirect": "true",
            "instance" : instance,
            "url" : url,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'transports/create_transport.html', context)


@check_mode
@login_required
def delete_transport(request,pk):
    instance = get_object_or_404(Transport.objects.filter(pk=pk,is_deleted=False))
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    Transport.objects.filter(pk=pk,college=current_college).update(is_deleted=True)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Transport Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('transports:transports')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def delete_selected_transports(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(Transport.objects.filter(pk=pk,is_deleted=False))
            Transport.objects.filter(pk=pk).update(is_deleted=True)

        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Transport(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('transports:transports')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select Transport first.",
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')



@check_mode
@login_required
def create_bus_pass(request):
    url = reverse('transports:create_bus_pass')
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)

    if request.method == 'POST':
        form = BusPassForm(request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            auto_id = get_auto_id(BusPass)

            data.creator = request.user
            data.updater = request.user
            data.auto_id = auto_id
            data.college = current_college
            data.batch = current_batch
            data.a_id = get_a_id(BusPass,request)
            data.save()

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "BusPass created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('transports:bus_pass', kwargs={'pk':data.pk})
            }

        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message": str(message)
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = BusPassForm()
        context = {
            "title" : "Create BusPass",
            "form" : form,
            "redirect": "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'transports/create_bus_pass.html',context)


@check_mode
@login_required
def bus_passes(request):
    instances = BusPass.objects.filter(is_deleted=False)
    title = "BusPasses"

    # date_error = False
    # date = request.GET.get('date')
    # if date:
    #     try:
    #         date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
    #     except ValueError:
    #         date_error = True

    # if not date_error:
    #     instances = instances.filter(valid_up_to_year=date.year, valid_up_tomonth=date.month, valid_up_to_day=date.day)

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(Q(student__name__icontains=query) | Q(boarding_point__name__icontains=query) | Q(valid_up_to__icontains=query) | Q(date_of_issue__icontains=query)))
        title = "BusPasses - %s" %query

    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'transports/bus_passes.html',context)


@check_mode
@login_required
def bus_pass(request,pk):
    instance = get_object_or_404(BusPass.objects.filter(pk=pk,is_deleted=False))
    context = {
        "instance" : instance,
        "title" : "BusPass",
        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'transports/bus_pass.html',context)


@check_mode
@login_required
def edit_bus_pass(request,pk):
    instance = get_object_or_404(BusPass.objects.filter(pk=pk,is_deleted=False))
    url = reverse('transports:edit_bus_pass',kwargs={'pk':pk})
    if request.method == "POST":

        form = BusPassForm(request.POST,instance=instance)
        if form.is_valid():

            #update student medium
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()

            response_data = {
                "status": "true",
                "title": "Successfully updated",
                "message": "Bus Pass successfully updated.",
                "redirect": "true",
                "redirect_url": reverse('transports:bus_pass',kwargs={"pk":pk})
            }

        else:
            message = generate_form_errors(form, formset=False)

            response_data = {
                "status": "false",
                "stable": "true",
                "title": "Form validation error",
                "message": message
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = BusPassForm(instance=instance)

        context = {
            "title" : "Edit Bus Pass  : " + instance.student.name,
            "form" : form,
            "redirect": "true",
            "instance" : instance,
            "url" : url,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'transports/create_bus_pass.html', context)


@check_mode
@login_required
def delete_bus_pass(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(BusPass.objects.filter(pk=pk,is_deleted=False,college=current_college))

    BusPass.objects.filter(pk=pk,college=current_college).update(is_deleted=True)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "BusPass Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('transports:bus_passes')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
def delete_selected_bus_passes(request):
    current_college = get_current_college(request)
    current_batch = get_current_batch(request)
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]

        pks = pks.split(',')
        for pk in pks:
            instance = get_object_or_404(BusPass.objects.filter(pk=pk,is_deleted=False,college=current_college,batch=current_batch))
            BusPass.objects.filter(pk=pk).update(is_deleted=True)

        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected BusPass(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('transports:bus_passes')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select Bus PAsses first.",
        }

    return HttpResponse(json.dumps(response_data), content_type='application/javascript')
