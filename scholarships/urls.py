from django.conf.urls import url
from scholarships import views
from scholarships.views import ScholarshipCategoryAutocomplete, ScholarshipAutocomplete


urlpatterns = [ 
    url(r'^scholarship-autocomplete/$', ScholarshipAutocomplete.as_view(),name='scholarship_autocomplete'),
    url(r'^scholarship-category-autocomplete/$', ScholarshipCategoryAutocomplete.as_view(),name='scholarship_category_autocomplete'),

    url(r'^scholarship-categories/$', views.scholarship_categories, name='scholarship_categories'),
    url(r'^create/scholarship-category/$', views.create_scholarship_category, name='create_scholarship_category'),
    url(r'^scholarship-category/(?P<pk>.*)/$', views.scholarship_category, name='scholarship_category'),
    url(r'^edit/scholarship-category/(?P<pk>.*)/$', views.edit_scholarship_category, name='edit_scholarship_category'),
    url(r'^delete/scholarship-category/(?P<pk>.*)/$', views.delete_scholarship_category, name='delete_scholarship_category'),

    url(r'^scholarships/$', views.scholarships, name='scholarships'),
    url(r'^create/scholarship/$', views.create_scholarship, name='create_scholarship'),
    url(r'^scholarship/(?P<pk>.*)/$', views.scholarship, name='scholarship'),
    url(r'^edit/scholarship/(?P<pk>.*)/$', views.edit_scholarship, name='edit_scholarship'),
    url(r'^delete/scholarship/(?P<pk>.*)/$', views.delete_scholarship, name='delete_scholarship'),

    url(r'^scholarship-applications/$', views.scholarship_applications, name='scholarship_applications'),
    url(r'^create/scholarship-application/$', views.create_scholarship_application, name='create_scholarship_application'),
    url(r'^scholarship-application/(?P<pk>.*)/$', views.scholarship_application, name='scholarship_application'),
    url(r'^edit/scholarship-application/(?P<pk>.*)/$', views.edit_scholarship_application, name='edit_scholarship_application'),
    url(r'^delete/scholarship-application/(?P<pk>.*)/$', views.delete_scholarship_application, name='delete_scholarship_application'),
    ]

