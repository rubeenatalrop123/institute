# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from main.models import BaseModel
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator
from decimal import Decimal


STATUS = (
    ('approved','Approved'),
    ('cancelled','Cancelled')
)

class ScholarshipCategory(BaseModel):
    name = models.CharField(max_length=128)

    class Meta:
        db_table = 'scholarship_categories'
        verbose_name = _('ScholarshipCategory')
        verbose_name_plural = _('ScholarshipCategories')
        ordering = ('name',)

    def __str__(self):
        return self.name


class Scholarship(BaseModel):
    title = models.CharField(max_length=128)  
    amount = models.DecimalField(max_digits=15, decimal_places=2, blank=True, null=True, validators=[MinValueValidator(Decimal('0.00'))])
    from_date = models.DateField()
    to_date = models.DateField()
    category = models.ForeignKey("scholarships.ScholarshipCategory",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)

    class Meta:
        db_table = 'scholarship_scholarship'
        verbose_name = _('Scholarship')
        verbose_name_plural = _('Scholarships')
        ordering = ('title',)

    def __str__(self):
        return self.title


class ScholarshipApplication(BaseModel):
    student = models.CharField(max_length=128)
    date = models.DateField() 
    scholarship_name = models.ForeignKey("scholarships.Scholarship",limit_choices_to={'is_deleted': False},on_delete=models.CASCADE)
    status = models.CharField(max_length=128,choices=STATUS)

    class Meta:
        db_table = 'scholarship_application'
        verbose_name = _('Scholarship Application')
        verbose_name_plural = _('Scholarship Applications')
        ordering = ('scholarship_name',)

    def __str__(self):
        return self.scholarship_name
