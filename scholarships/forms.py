from django import forms
from django.forms.widgets import TextInput, Select, Textarea, HiddenInput
from scholarships.models import ScholarshipCategory, Scholarship, ScholarshipApplication
from django.utils.translation import ugettext_lazy as _
from dal import autocomplete
from decimal import Decimal


class ScholarshipCategoryForm(forms.ModelForm):
    
    class Meta:
        model = ScholarshipCategory
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "name": TextInput(attrs = { "class": "required form-control","placeholder" : "Scholarship Category",}),
            
        }
        error_messages = {
            "name" : {
                "required" : _("Name field is required.")
            }
        }
        
        
    def clean(self):
        cleaned_data = super(ScholarshipCategoryForm, self).clean()
        return cleaned_data


class ScholarshipForm(forms.ModelForm):
    
    class Meta:
        model = Scholarship
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "title": TextInput(attrs = { "class": "required form-control","placeholder" : "title",}),
            "amount": TextInput(attrs = {"class": "required form-control","placeholder" : "amount",}), 
            "from_date": TextInput(attrs = {"class": "required form-control date-picker", "placeholder"  : "from_date", }),
            "to_date": TextInput(attrs = {"class": "form-control date-picker", "placeholder" : "to_date",}),
            "category": autocomplete.ModelSelect2(url='scholarships:scholarship_category_autocomplete',attrs={'data-placeholder': 'Category','data-minimum-input-length': 1},),
        }
        error_messages = {
            "title" : {
                "required" : _("title field is required.")
            },
            "amount" : {
                "required" : _("amount field is required.")
            },
            "from_date" : {
                "required" : _("from_date field is required.")
            },
            "to_date" : {
                "required" : _("to_date field is required.")
            },
            "category" : {
                "required" : _("category field is required.")
            }
        }
        
        
    def clean(self):
        cleaned_data = super(ScholarshipForm, self).clean()
        return cleaned_data


class ScholarshipApplicationForm(forms.ModelForm):
    
    class Meta:
        model = ScholarshipApplication
        exclude = ['creator','updater','auto_id','is_deleted','college','a_id']
        widgets = {
            "student": TextInput(attrs = { "class": "required form-control","placeholder" : "Student",}),
            "date": TextInput(attrs = {"class": "date-picker form-control","placeholder" : "Date",}), 
            "scholarship_name": autocomplete.ModelSelect2(url='scholarships:scholarship_autocomplete',attrs={'data-placeholder': 'Scholorship','data-minimum-input-length': 1},),
            "status": Select(attrs = {"class": "required form-control show-tick","placeholder" : "category",}),
        }
        error_messages = {
            "student" : {
                "required" : _("student field is required.")
            },
            "date" : {
                "required" : _("date field is required.")
            },
            "scholarship_name" : {
                "required" : _("scholarship_name field is required.")
            },
            "status" : {
                "required" : _("status field is required.")
            }
        }
        
        
    def clean(self):
        cleaned_data = super(ScholarshipApplicationForm, self).clean()
        return cleaned_data


