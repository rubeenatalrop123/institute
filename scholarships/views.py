from django.shortcuts import render, get_object_or_404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, college_required, ajax_required,\
    check_account_balance,permissions_required,role_required
from main.functions import get_auto_id, generate_form_errors, get_a_id
import json
from main.models import College,CollegeAccess
from users.functions import get_current_college, college_access
from users.models import Profile
from scholarships.models import ScholarshipCategory, Scholarship, ScholarshipApplication
from scholarships.forms import ScholarshipCategoryForm, ScholarshipForm, ScholarshipApplicationForm
from django.views.decorators.http import require_GET
from django.core import serializers
from django.db.models import Q
from django.contrib.auth.models import Group
from django.db.models import Sum
import datetime 
from dal import autocomplete


class ScholarshipAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = Scholarship.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(title__istartswith=self.q) | Q(auto_id__istartswith=self.q))

        return items


class ScholarshipCategoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = ScholarshipCategory.objects.filter(is_deleted=False)

        if self.q:
            items = items.filter(Q(name__istartswith=self.q) | Q(auto_id__istartswith=self.q))

        return items


@check_mode
@login_required
@role_required(['superadmin'])
def create_scholarship_category(request):  
    current_college = get_current_college(request)  
    
    if request.method == 'POST':
        form = ScholarshipCategoryForm(request.POST)        
        
        if form.is_valid():            
            data = form.save(commit=False)
            auto_id = get_auto_id(ScholarshipCategory)
            data.creator = request.user
            data.updater = request.user            
            data.college = current_college
            data.auto_id = auto_id     
            data.a_id = get_a_id(ScholarshipCategory,request)      
            data.save()    
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Scholarship Category created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('scholarships:scholarship_category',kwargs={'pk':data.pk})
            }
        
        else:            
                    
            message = generate_form_errors(form,formset=False)     
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : str(message)
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        form = ScholarshipCategoryForm()
        context = {
            "title" : "Create Scholarship Category ",
            "form" : form,
            "url" : reverse('scholarships:create_scholarship_category'),
            "redirect" : "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'scholarships/entry_scholarship_category.html',context)


@check_mode
@login_required 
@role_required(['superadmin'])
def scholarship_categories(request):
    current_college = get_current_college(request)
    instances = ScholarshipCategory.objects.filter(is_deleted=False,college=current_college)
    title = "Scholarship Category"
    
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query))
        title = "Scholarship Categoryes - %s" %query  
     
    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'scholarships/scholarship_categories.html',context) 


@check_mode
@login_required
@role_required(['superadmin'])
def scholarship_category(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(ScholarshipCategory.objects.filter(pk=pk,is_deleted=False,college=current_college))
    context = {
        "instance" : instance,
        "title" : "Scholarship Category : " + instance.name,
        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'scholarships/scholarship_category.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_scholarship_category(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(ScholarshipCategory.objects.filter(pk=pk,is_deleted=False,college=current_college)) 
    if request.method == 'POST':
            
        response_data = {}
        form = ScholarshipCategoryForm(request.POST,instance=instance)
        
        if form.is_valid():  
            
            data = form.save(commit=False)
            auto_id = get_auto_id(ScholarshipCategory)
            data.creator = request.user
            data.updater = request.user            
            data.college = current_college
            data.auto_id = auto_id           
            data.date_updated = datetime.datetime.now()
            data.save()         
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "ScholarshipCategory Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('scholarships:scholarship_category',kwargs={'pk':data.pk})
            }   
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 

        form = ScholarshipCategoryForm(instance=instance,initial={'college':current_college})
        
        context = {
            "form" : form,
            "title" : "Edit Scholarship Category : " + instance.name,
            "instance" : instance,
            "is_edit" : True,
            "url" : reverse('scholarships:edit_scholarship_category',kwargs={'pk':instance.pk}),
            "redirect" : True,
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'scholarships/entry_scholarship_category.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_scholarship_category(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(ScholarshipCategory.objects.filter(pk=pk,is_deleted=False,college=current_college))
    
    ScholarshipCategory.objects.filter(pk=pk,college=current_college).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Scholarship Category Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('scholarships:scholarship_categories')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def create_scholarship(request):  
    current_college = get_current_college(request)  
    
    if request.method == 'POST':
        form = ScholarshipForm(request.POST)
        if form.is_valid():            
            data = form.save(commit=False)
            auto_id = get_auto_id(Scholarship)
            data.creator = request.user
            data.updater = request.user            
            data.college = current_college
            data.auto_id = auto_id    
            data.a_id = get_a_id(Scholarship,request)       
            data.save()    
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Scholarship created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('scholarships:scholarship',kwargs={'pk':data.pk})
            }
        
        else:              
            message = generate_form_errors(form,formset=False)     
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : str(message)
            }   
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        form = ScholarshipForm()
        context = {
            "title" : "Create Scholarship ",
            "form" : form,
            "url" : reverse('scholarships:create_scholarship'),
            "redirect" : "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'scholarships/entry_scholarship.html',context)


@check_mode
@login_required 
@role_required(['superadmin'])
def scholarships(request):
    current_college = get_current_college(request)
    instances = Scholarship.objects.filter(is_deleted=False,college=current_college)
    title = "Scholarship"
    
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(title__icontains=query) | Q(from_date__icontains=query) | Q(to_date__icontains=query) | Q(amount__icontains=query) | Q(category__name__icontains=query))
        title = "Scholarships - %s" %query  
     
    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'scholarships/scholarships.html',context) 


@check_mode
@login_required
@role_required(['superadmin'])
def scholarship(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(Scholarship.objects.filter(pk=pk,is_deleted=False,college=current_college))
    context = {
        "instance" : instance,
        "title" : "Scholarship : " + instance.title,
        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'scholarships/scholarship.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_scholarship(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(Scholarship.objects.filter(pk=pk,is_deleted=False,college=current_college)) 
    if request.method == 'POST':
            
        response_data = {}
        form = ScholarshipForm(request.POST,instance=instance)
        
        if form.is_valid():  
            
            data = form.save(commit=False)
            auto_id = get_auto_id(Scholarship)
            data.creator = request.user
            data.updater = request.user            
            data.college = current_college
            data.auto_id = auto_id           
            data.date_updated = datetime.datetime.now()
            data.save()         
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Scholarship Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('scholarships:scholarship',kwargs={'pk':data.pk})
            }   
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 

        form = ScholarshipForm(instance=instance,initial={'college':current_college})
        
        context = {
            "form" : form,
            "title" : "Edit Scholarship : " + instance.title,
            "instance" : instance,
            "is_edit" : True,
            "url" : reverse('scholarships:edit_scholarship',kwargs={'pk':instance.pk}),
            "redirect" : True,
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'scholarships/entry_scholarship.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_scholarship(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(Scholarship.objects.filter(pk=pk,is_deleted=False,college=current_college))
    
    Scholarship.objects.filter(pk=pk,college=current_college).update(is_deleted=True,title=instance.title + "_deleted_" + str(instance.auto_id))
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Scholarship Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('scholarships:scholarships')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')



def create_scholarship_application(request):  
    current_college = get_current_college(request)  
    
    if request.method == 'POST':
        form = ScholarshipApplicationForm(request.POST)        
        if form.is_valid():            
            data = form.save(commit=False)
            auto_id = get_auto_id(ScholarshipApplication)
            data.creator = request.user
            data.updater = request.user            
            data.college = current_college
            data.auto_id = auto_id 
            data.a_id = get_a_id(ScholarshipApplication,request)          
            data.save()    
            print(form.errors)

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "ScholarshipApplication Category created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('scholarships:scholarship_application',kwargs={'pk':data.pk})
            }
        else:            
                    
            message = generate_form_errors(form,formset=False)  
            print(form.errors)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : str(message)
            } 
            print(form.errors)  
        
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    
    else:
        form = ScholarshipApplicationForm()
        context = {
            "title" : "Create Scholarship Application ",
            "form" : form,
            "url" : reverse('scholarships:create_scholarship_application'),
            "redirect" : "true",

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request,'scholarships/entry_scholarship_application.html',context)


@check_mode
@login_required 
@role_required(['superadmin'])
def scholarship_applications(request):
    current_college = get_current_college(request)
    instances = ScholarshipApplication.objects.filter(is_deleted=False,college=current_college)
    title = "Scholarship Application Category"
    
    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(student__icontains=query) | Q(date__icontains=query) | Q(scholarship_name__title__icontains=query) | Q(status__icontains=query))
        title = "ScholarshipApplication Categoryes - %s" %query  
     
    context = {
        "instances" : instances,
        'title' : title,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'scholarships/scholarship_applications.html',context) 


@check_mode
@login_required
@role_required(['superadmin'])
def scholarship_application(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(ScholarshipApplication.objects.filter(pk=pk,is_deleted=False,college=current_college))
    context = {
        "instance" : instance,
        "title" : "Scholarship Application : " + instance.student,
        "single_page" : True,
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_grid_system" : True,
        "is_need_animations" : True,
        "is_need_datetime_picker" : True,
    }
    return render(request,'scholarships/scholarship_application.html',context)


@check_mode
@login_required
@role_required(['superadmin'])
def edit_scholarship_application(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(ScholarshipApplication.objects.filter(pk=pk,is_deleted=False,college=current_college)) 
    if request.method == 'POST':
            
        response_data = {}
        form = ScholarshipApplicationForm(request.POST,instance=instance)
        
        if form.is_valid():  
            
            data = form.save(commit=False)
            auto_id = get_auto_id(ScholarshipApplication)
            data.creator = request.user
            data.updater = request.user            
            data.college = current_college
            data.auto_id = auto_id         
            data.a_id = get_a_id(ScholarshipApplication,request)   
            data.date_updated = datetime.datetime.now()
            data.save()         
            
            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Scholarship Application Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('scholarships:scholarship_application',kwargs={'pk':data.pk})
            }   
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 

        form = ScholarshipApplicationForm(instance=instance,initial={'college':current_college})
        
        context = {
            "form" : form,
            "title" : "Edit Scholarship Application : " + instance.student,
            "instance" : instance,
            "is_edit" : True,
            "url" : reverse('scholarships:edit_scholarship_application',kwargs={'pk':instance.pk}),
            "redirect" : True,
            
            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,

            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }
        return render(request, 'scholarships/entry_scholarship_application.html', context)


@check_mode
@login_required
@role_required(['superadmin'])
def delete_scholarship_application(request,pk):
    current_college = get_current_college(request)
    instance = get_object_or_404(ScholarshipApplication.objects.filter(pk=pk,is_deleted=False,college=current_college))
    
    ScholarshipApplication.objects.filter(pk=pk,college=current_college).update(is_deleted=True,student=instance.student + "_deleted_" + str(instance.auto_id))
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Scholarship Application Category Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('scholarships:scholarship_categories')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')
